<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserProfileController extends MX_Controller
{

    function __construct()
    {
        if ($this->session->userdata('user_id') == null) {
            redirect('/');
        }

        $this->load->model('Userprofile_model');
        $this->load->model('employer_module/Employer_model');
        $this->load->model('log_module/Log_model');
        $this->load->model('user/Ion_auth_model');                                      //for password hashing purposes

        $this->load->library('form_validation');
        $this->load->library('custom_log_library');

        // application/libraries
        $this->load->library('custom_image_library');
        $this->load->library('custom_datetime_library');

        //----------for paystack payments-----------------
        $this->load->library('payment_module/custom_payment_library');
        $this->load->library('GoogleAuthenticator');
        $this->load->model('thrift_module/Thrift_model');
    }

    public function countRunningProjects($user_id)
    {
        $count = 0;

        if ($this->ion_auth->in_group('client', $user_id)) {
            $count = $this->Userprofile_model->getRunningProjects_ofClient($user_id);
        }

        if ($this->ion_auth->in_group('staff', $user_id)) {
            $count = $this->Userprofile_model->getRunningProjects_ofStaff($user_id);
        }

        return $count;
    }

    public function countRunningTasks($user_id)
    {
        $count = 0;

        if ($this->ion_auth->in_group('client', $user_id)) {
            $count = $this->Userprofile_model->getRunningTasks_ofClient($user_id);
        }

        if ($this->ion_auth->in_group('staff', $user_id)) {
            $count = $this->Userprofile_model->getRunningTasks_ofStaff($user_id);
        }

        return $count;
    }

    public function getUserProfileMenuSection($user_id)
    {
        $this->lang->load('userprofile_menu_section');

        if ($this->checkIfOwnProfile($user_id) == true) {
            $data['is_own_profile'] = 'own_profile';
        } else {
            $data['is_own_profile'] = 'others_profile';
        }

        if ($this->ion_auth->is_admin()) {
            $data['is_admin'] = 'admin';
        } else {
            $data['is_admin'] = 'not_admin';
        }

        if ($this->ion_auth->in_group('staff')) {
            $data['is_staff'] = 'staff';
        } else {
            $data['is_staff'] = 'not_staff';
        }

        if ($this->ion_auth->in_group('client')) {
            $data['is_client'] = 'client';
        } else {
            $data['is_client'] = 'not_client';
        }

        $data['groups_of_a_user'] = $this->getGroupsByUser($user_id);

        $data['user_info'] = $this->Userprofile_model->getUserInfo($user_id);
        $data['image_directory'] = $this->custom_image_library->getMainImageDirectory();

        $data['running_projects_count'] = $this->countRunningProjects($user_id);
        $data['running_tasks_count'] = $this->countRunningTasks($user_id);

        $userprofile_menu_section = $this->load->view("user_profile_module/userprofile_menu_section", $data, true);
        return $userprofile_menu_section;
    }

    public function checkIfOwnProfile($user_id)
    {
        if ($this->session->userdata('user_id') == $user_id) {
            return true;
        } else if ($this->session->userdata('org_contact_user_id') == $user_id) {
            return true;
        } else {
            return false;
        }
    }


    /*------------------INFO OVERVIEW STARTS--------------------------------------------------------------------------*/

    private function checkEmployersEmployee($employer_id, $employee_id)
    {
        $ret = false;
        $employee = $this->Userprofile_model->getUserInfo($employee_id);

        if ($employer_id == $employee->user_employer_id) {
            $ret = true;
        }

        return $ret;
    }

    private function checkPermission($user_id)
    {
        $ret = true;

        if ($this->ion_auth->in_group('employee')) {
            if (
            !($this->session->userdata('user_id') == $user_id
                ||
                $this->checkEmployersEmployee($user_id, $this->session->userdata('user_id'))
            )
            ) {
                $ret = false;
            }
        }

        if ($this->ion_auth->in_group('employer')) {
            if (
            !($this->checkEmployersEmployee($this->session->userdata('user_id'), $user_id)
                || $this->session->userdata('user_id') == $user_id)
            ) {
                $ret = false;
            }
        }

        return $ret;
    }

    public function showUserProfile()
    {
        $this->lang->load('user_info_overview');

        $user_id = $this->uri->segment(3);

        if (!$this->checkPermission($user_id)) {
            redirect('users/auth/need_permission');
        }

        $data['user_id'] = $user_id;

        if ($this->checkIfOwnProfile($user_id) == true) {
            $data['is_own_profile'] = 'own_profile';
        } else {
            $data['is_own_profile'] = 'others_profile';
        }


        if ($this->ion_auth->is_admin()) {
            $data['is_admin'] = 'admin';
        } else {
            $data['is_admin'] = 'not_admin';
        }

        $data['user_info'] = $this->Userprofile_model->getUserInfo($user_id);


        $data['is_viewer_user_employer'] = 'not_user_employer';

        if ($this->session->userdata('user_id') == $data['user_info']->user_employer_id) {
            $data['is_viewer_user_employer'] = 'user_employer';
        }

        //echo $data['user_info']->user_dob;die();
        //echo $this->custom_datetime_library->convert_and_return_DateAndTime_To_Timestamp_byGivenDateTimeFormat($data['user_info']->user_dob, 'Y-m-d');die();

        if ($data['user_info']->user_dob == 0 || $data['user_info']->user_dob == null) {
            $data['user_info']->user_dob_datestring = $this->lang->line('unavailable_text');
        } else {
            $data['user_info']->user_dob_datestring = date('Y-m-d', $data['user_info']->user_dob);
        }


        if ($this->ion_auth->in_group('employee', $user_id) || $this->ion_auth->in_group('organization_contact', $user_id)) {

            if ($data['user_info']->user_employer_id != 0) {
                $users_employer = $this->Userprofile_model->getUserInfo($data['user_info']->user_employer_id);

                if ($users_employer) {
                    if ($users_employer->company != '') {
                        $data['user_info']->company = $users_employer->company;
                    }
                }
            }

        }

        $site_name = $this->custom_settings_library->getASettingsValue('general_settings', 'site_name');

        if (!$site_name) {
            $site_name = 'Prosperis';
        }

        if ($this->ion_auth->is_admin($user_id)) {
            $data['user_info']->company = $site_name;
        } else if ($data['user_info']->company == '') {
            $data['user_info']->company = $this->lang->line('unavailable_text');
            if ($this->ion_auth->in_group('employee')) {
                $data['user_info']->company = "<span style='font-style: italic; !important'>Non-organizational thrifter</span>";
            }
        }

        if (trim($data['user_info']->user_street_1) == '' || $data['user_info']->user_street_1 == null) {
            $data['user_info']->user_street_1 = $this->lang->line('unavailable_text');
        }

        if (trim($data['user_info']->user_street_2) == '' || $data['user_info']->user_street_2 == null) {
            $data['user_info']->user_street_2 = $this->lang->line('unavailable_text');
        }

        $data['user_info']->user_country_name = $this->lang->line('unavailable_text');
        $data['user_info']->user_state_name = $this->lang->line('unavailable_text');
        $data['user_info']->user_city_name = $this->lang->line('unavailable_text');

        if ($data['user_info']->user_country > 0) {
            $country = $this->Userprofile_model->getCountry($data['user_info']->user_country);
            if ($country) {
                $data['user_info']->user_country_name = $country->country_name;
            }
        }


        if ($data['user_info']->user_state > 0) {
            $state = $this->Userprofile_model->getState($data['user_info']->user_state);
            if ($state) {
                $data['user_info']->user_state_name = $state->state_name;
            }
        }


        if ($data['user_info']->user_city > 0) {
            $city = $this->Userprofile_model->getCity($data['user_info']->user_city);
            if ($city) {
                $data['user_info']->user_city_name = $city->city_name;
            }
        }

        $data['user_info']->bank_name = $this->lang->line('unavailable_text');

        $banks = $this->Userprofile_model->getBanks($only_undeleted_banks = false);

        if ($banks) {
            foreach ($banks as $bank) {
                if ($data['user_info']->user_bank == $bank->bank_id) {
                    $data['user_info']->bank_name = $bank->bank_name;

                    if ($bank->bank_deletion_status == 1) {
                        $data['user_info']->bank_name .= ' ' . '(' . $this->lang->line('deleted_text') . ')';
                    }
                }
            }
        }

        $data['user_info']->thrift_list = null;

        if ($this->ion_auth->in_group('employee',$user_id)) {
           $data['user_info']->thrift_list = $this->Userprofile_model->getThriftsByUser($user_id, 500);
        }

        $this->load->view("common_module/header");
        // $this->load->view("common_module/common_left");
        $this->load->view("user_profile_module/user_info_overview_page", $data);
        $this->load->view("common_module/footer");

    }

    public function getCountriesBySelect2()
    {
        $total_count = 0;
        $more_pages = false;
        $last_query = 'No query attempted';


        if (isset($_REQUEST['keyword'])) {
            $keyword = $_REQUEST['keyword'];
        } else {
            $keyword = '';
        }

        if (isset($_REQUEST['page'])) {
            $page = $_REQUEST['page'];
        } else {
            $page = 1;
        }


        $limit = 10;

        $total_count = $this->Userprofile_model->countTotalCountries($keyword);
        $offset = ($page - 1) * $limit;

        $end_count = $offset + $limit;
        $more_pages = $total_count > $end_count;

        $countries = $this->Userprofile_model->getTotalCountries($keyword, $limit, $offset);

        $last_query = $this->db->last_query();

        $json_data = array();
        $items = array();


        if ($countries) {

            foreach ($countries as $country) {
                $p = array();
                $p['id'] = $country->country_id;
                $p['text'] = $country->country_name;

                $items = $p;
                $json_data['items'][] = $items;
            }
        } else {
            $p = array();
            $p['id'] = '';
            $p['text'] = 'No Country Found';

            $items = $p;
            $json_data['items'][] = $items;
        }


        //echo '<pre>';print_r($json_data);die();


        $json_data['total_count'] = $total_count;
        $json_data['more_pages'] = $more_pages;
        $json_data['last_query'] = $last_query;

        echo json_encode($json_data);

    }

    public function getStatesBySelect2()
    {
        $total_count = 0;
        $more_pages = false;
        $last_query = 'No query attempted';

        $country_id = false;

        if (isset($_REQUEST['user_country'])) {
            $country_id = $_REQUEST['user_country'];
        }

        if (isset($_REQUEST['keyword'])) {
            $keyword = $_REQUEST['keyword'];
        } else {
            $keyword = '';
        }

        if (isset($_REQUEST['page'])) {
            $page = $_REQUEST['page'];
        } else {
            $page = 1;
        }


        $limit = 10;

        $total_count = $this->Userprofile_model->countTotalStates($keyword, $country_id);
        $offset = ($page - 1) * $limit;

        $end_count = $offset + $limit;
        $more_pages = $total_count > $end_count;

        $states = $this->Userprofile_model->getTotalStates($country_id, $keyword, $limit, $offset);

        $last_query = $this->db->last_query();

        $json_data = array();
        $items = array();


        if ($states) {

            foreach ($states as $state) {
                $p = array();
                $p['id'] = $state->state_id;
                $p['text'] = $state->state_name;

                $items = $p;
                $json_data['items'][] = $items;
            }
        } else {
            $p = array();
            $p['id'] = '';
            $p['text'] = 'No State Found';

            $items = $p;
            $json_data['items'][] = $items;
        }


        //echo '<pre>';print_r($json_data);die();


        $json_data['total_count'] = $total_count;
        $json_data['more_pages'] = $more_pages;
        $json_data['last_query'] = $last_query;

        echo json_encode($json_data);

    }

    public function getCitiesBySelect2()
    {
        $total_count = 0;
        $more_pages = false;
        $last_query = 'No query attempted';

        $state_id = false;

        if (isset($_REQUEST['user_state'])) {
            $state_id = $_REQUEST['user_state'];
        }

        if (isset($_REQUEST['keyword'])) {
            $keyword = $_REQUEST['keyword'];
        } else {
            $keyword = '';
        }

        if (isset($_REQUEST['page'])) {
            $page = $_REQUEST['page'];
        } else {
            $page = 1;
        }


        $limit = 10;

        $total_count = $this->Userprofile_model->countTotalCities($keyword, $state_id);
        $offset = ($page - 1) * $limit;

        $end_count = $offset + $limit;
        $more_pages = $total_count > $end_count;

        $cities = $this->Userprofile_model->getTotalCities($state_id, $keyword, $limit, $offset);

        $last_query = $this->db->last_query();

        $json_data = array();
        $items = array();


        if ($cities) {

            foreach ($cities as $city) {
                $p = array();
                $p['id'] = $city->city_id;
                $p['text'] = $city->city_name;

                $items = $p;
                $json_data['items'][] = $items;
            }
        } else {
            $p = array();
            $p['id'] = '';
            $p['text'] = 'No City Found';

            $items = $p;
            $json_data['items'][] = $items;
        }


        //echo '<pre>';print_r($json_data);die();


        $json_data['total_count'] = $total_count;
        $json_data['more_pages'] = $more_pages;
        $json_data['last_query'] = $last_query;

        echo json_encode($json_data);

    }

    public function getGroupsByUser($user_id)
    {
        $groups_of_a_user = $this->Userprofile_model->getGroupsByUser($user_id);
        return $groups_of_a_user;
    }


    /*------------------INFO OVERVIEW ENDS----------------------------------------------------------------------------*/

    /*------------------google tf auth starts-------------------------------------------------------------------------*/

    public function googleTfAuth()
    {
        $user_id = $this->uri->segment(3);

        if ($this->session->userdata('org_contact_user_id')) {

            if ($user_id != $this->session->userdata('org_contact_user_id')) {
                redirect('users/auth/need_permission');
            }
        } else {
            if (($user_id != $this->session->userdata('user_id'))) {
                redirect('users/auth/need_permission');
            }
        }


        if ($this->checkIfOwnProfile($user_id) == true) {
            $data['is_own_profile'] = 'own_profile';
        } else {
            $data['is_own_profile'] = 'others_profile';
        }


        if ($this->ion_auth->is_admin()) {
            $data['is_admin'] = 'admin';
        } else {
            $data['is_admin'] = 'not_admin';
        }

        $data['user_id'] = $user_id;
        $data['user_info'] = $this->Userprofile_model->getUserInfo($user_id);


        $ga = new GoogleAuthenticator();
        $secret = $ga->createSecret();
        $qr_code = $ga->getQRCodeGoogleUrl($data['user_info']->email, $secret);
        $data['qr_code'] = $qr_code;
        $data['secret'] = $secret;

        if ($this->input->post()) {
            $this->postedGoogleTfAuth();
        } else {


            $data['validation_errors'] = null;

            $this->loadGoogleTfForm($data);
        }
    }


    public function loadGoogleTfForm($data)
    {
        $this->lang->load('user_google_tf_form');

        $data['image_directory'] = $this->custom_image_library->getMainImageDirectory();

        $this->load->view("common_module/header");
        $this->load->view("user_profile_module/user_google_tf_form_page", $data);
        $this->load->view("common_module/footer");
    }

    public function postedGoogleTfAuth()
    {

        if ($this->checkIfOwnProfile($this->session->userdata('user_id')) == true) {
            $data['is_own_profile'] = 'own_profile';
        } else {
            $data['is_own_profile'] = 'others_profile';
        }

        $data['user_info'] = new stdClass();

        $data['user_id'] = $this->input->post('user_id');

        // to get images which can not be loaded from post
        $data['user_info'] = $this->Userprofile_model->getUserInfo($this->input->post('user_id'));

        $data['user_info']->google_tf_auth_status = $this->input->post('google_tf_auth_status');

        $ga = new GoogleAuthenticator();
        $secret = $ga->createSecret();
        $qr_code = $ga->getQRCodeGoogleUrl($data['user_info']->email, $secret);
        $data['qr_code'] = $qr_code;
        $data['secret'] = $secret;

        if ($this->validateGtfForm() == 'validated') {

            $this->updateGoogleTf();
        } else {
            $data['validation_errors'] = $this->validateGtfForm();
            $this->loadGoogleTfForm($data);
        }
    }

    public function google_tf_qr_validate()
    {
        $user_id = $this->input->post('user_id');
        $code = trim($this->input->post('code'));
        $secret = $this->input->post('secret');

        $ga = new GoogleAuthenticator();
        $res = "err";
        $message = "";
        if ($code == "") {
            $message = 'Please Scan above QR code to configure your application and enter genereated authentication code to validated!';
            $res = "err";
        } else {
            if ($ga->verifyCode($secret, $code, 2)) {
                $res = "suc";
                $message = "Validation successful! Click the Continue Setup below";

                $user_data['google_tf_auth_status'] = 1; //registerd but deactivated
                $user_data['google_tf_secret_code'] = $secret;

                $user_info = $this->Userprofile_model->getUserInfo($user_id);

                if ($user_info) {
                    $force_tf_cond = $user_info->google_tf_auth_forced == 1;
                    if ($force_tf_cond && !empty($secret)) {
                        $user_data['google_tf_auth_status'] = 2;//registerd and activated
                    }
                }


                $is_userdata_updated = $this->Userprofile_model->updateUserInfo($user_data, $user_id);
            } else {
                $res = "err";
                $message = 'Invalid Authentication Code!';
            }
        }

        $data = array();
        $data['res'] = $res;
        $data['message'] = $message;

        echo json_encode($data);
        die();

    }

    public function validateGtfForm()
    {
        $this->lang->load('user_google_tf_form');

        $this->form_validation->set_rules('google_tf_auth_status', 'google_tf_auth_status', 'required', array(
                'required' => $this->lang->line('status_required_text')
            )
        );


        if ($this->form_validation->run() == false) {

            $this->session->set_flashdata('validation_errors', validation_errors());

            return validation_errors();
        } else {
            return 'validated';
        }


    }

    public function updateGoogleTf()
    {
        $this->lang->load('user_info_form');
        $user_id = $this->input->post('user_id');

        $user_data = array();

        $user_data['google_tf_auth_status'] = $this->input->post('google_tf_auth_status');


        $is_userdata_updated = false;

        if (!empty($user_data)) {

            $is_userdata_updated = $this->Userprofile_model->updateUserInfo($user_data, $user_id);
        }


        if ($is_userdata_updated) {
            $this->session->set_flashdata('update_success', $this->lang->line('update_success_text'));
        }

        redirect('user_profile_module/google_tf_auth/' . $user_id);

    }

    /*------------------google tf auth ends-------------------------------------------------------------------------*/

    /*------------------INFO EDIT STARTS------------------------------------------------------------------------------*/

    /*form load view func */
    public function editUserInfo()
    {
        $user_id = $this->uri->segment(3);

        if ($this->session->userdata('org_contact_user_id')) {

            if ($user_id != $this->session->userdata('org_contact_user_id')) {
                redirect('users/auth/need_permission');
            }

        } else {

            if (($user_id != $this->session->userdata('user_id'))) {
                redirect('users/auth/need_permission');
            }

        }


        if ($this->checkIfOwnProfile($user_id) == true) {
            $data['is_own_profile'] = 'own_profile';
        } else {
            $data['is_own_profile'] = 'others_profile';
        }


        if ($this->ion_auth->is_admin()) {
            $data['is_admin'] = 'admin';
        } else {
            $data['is_admin'] = 'not_admin';
        }

        if ($this->input->post()) {
            $this->postedEditUserInfo();
        } else {
            $data['user_id'] = $user_id;
            $data['user_info'] = $this->Userprofile_model->getUserInfo($user_id);

            $data['user_info']->user_dob_datestring = date('Y-m-d', $data['user_info']->user_dob);

            $data['user_info']->user_country_name = '';
            $data['user_info']->user_state_name = '';
            $data['user_info']->user_city_name = '';

            if ($data['user_info']->user_country > 0) {
                $country = $this->Userprofile_model->getCountry($data['user_info']->user_country);
                if ($country) {
                    $data['user_info']->user_country_name = $country->country_name;
                }
            } else {
                $data['user_info']->user_country = false;
            }

            if ($data['user_info']->user_state > 0) {
                $state = $this->Userprofile_model->getState($data['user_info']->user_state);
                if ($state) {
                    $data['user_info']->user_state_name = $state->state_name;
                }
            } else {
                $data['user_info']->user_state = false;
            }

            if ($data['user_info']->user_city > 0) {
                $city = $this->Userprofile_model->getCity($data['user_info']->user_city);
                if ($city) {
                    $data['user_info']->user_city_name = $city->city_name;
                }
            } else {
                $data['user_info']->user_city = false;
            }

            $data['banks'] = $this->Userprofile_model->getBanks($only_undeleted = true);

            $data['validation_errors'] = null;
            $this->loadUserInfoForm($data);
        }


    }

    public function postedEditUserInfo()
    {
        if ($this->checkIfOwnProfile($this->session->userdata('user_id')) == true) {
            $data['is_own_profile'] = 'own_profile';
        } else {
            $data['is_own_profile'] = 'others_profile';
        }

        $data['user_info'] = new stdClass();

        $data['user_id'] = $this->input->post('user_id');

        // to get images which can not be loaded from post
        $data['user_info'] = $this->Userprofile_model->getUserInfo($this->input->post('user_id'));

        $data['user_info']->first_name = $this->input->post('first_name');
        $data['user_info']->last_name = $this->input->post('last_name');
        $data['user_info']->company = $this->input->post('company');

        $data['user_info']->email = $this->input->post('email');
        $data['user_info']->user_additional_email = $this->input->post('user_additional_email');

        $data['user_info']->password = $this->input->post('password');
        $data['user_info']->confirm_password = $this->input->post('confirm_password');

        $data['user_info']->phone = $this->input->post('phone');
        $data['user_info']->user_additional_phone = $this->input->post('user_additional_phone');

        $data['user_info']->user_salary = $this->input->post('user_salary');


        if ($this->input->post('user_dob')) {
            $data['user_info']->user_dob = $this->input->post('user_dob');
            $data['user_info']->user_dob_datestring = $this->input->post('user_dob');
        } else {
            $data['user_info']->user_dob = 0;
            $data['user_info']->user_dob_datestring = '';
        }

        if ($this->input->post('subdomain')) {
            $data['user_info']->subdomain = $this->input->post('subdomain');
        } else {
            $data['user_info']->subdomain = null;

        }


        $data['user_info']->user_street_1 = trim($this->input->post('user_street_1'));
        $data['user_info']->user_street_2 = trim($this->input->post('user_street_2'));

        $data['user_info']->user_country = false;
        $data['user_info']->user_state = false;
        $data['user_info']->user_city = false;

        $data['user_info']->user_country_name = '';
        $data['user_info']->user_state_name = '';
        $data['user_info']->user_city_name = '';


        if ($this->input->post('user_country')) {
            $data['user_info']->user_country = $this->input->post('user_country');

            $country = $this->Userprofile_model->getCountry($data['user_info']->user_country);
            if ($country) {
                $data['user_info']->user_country_name = $country->country_name;
            }
        }

        if ($this->input->post('user_state')) {
            $data['user_info']->user_state = $this->input->post('user_state');

            $state = $this->Userprofile_model->getState($data['user_info']->user_state);
            if ($state) {
                $data['user_info']->user_state_name = $state->state_name;
            }
        }

        if ($this->input->post('user_city')) {
            $data['user_info']->user_city = $this->input->post('user_city');

            $city = $this->Userprofile_model->getCity($data['user_info']->user_city);
            if ($city) {
                $data['user_info']->user_city_name = $city->city_name;
            }
        }

        $data['banks'] = $this->Userprofile_model->getBanks($only_undeleted = true);
        if ($this->input->post('user_bank')) {
            $data['user_info']->user_bank = $this->input->post('user_bank');
        } else {
            $data['user_info']->user_bank = '';
        }

        $data['user_info']->user_bank_account_no = $this->input->post('user_bank_account_no');

        $data['subdomain'] = $this->prepareSubdomain($this->input->post('subdomain'));

        $subdomain_exists = false;
        if (!empty($data['subdomain'])) {
            $subdomain_exists = $this->Employer_model->subdomainExistForOther($data['subdomain'], $data['user_id']);
        }

        if ($subdomain_exists) {
            $this->session->set_flashdata('subdomain_error', 'Subdomain already exist');
            redirect('user_profile_module/edit_user_info/' . $data['user_id']);
        }


        if ($this->validateForm() == 'validated') {

            /*uploading image files starts : error checking*/
            if (($_FILES['user_profile_image']['name']) != '') {

                $field_name = 'user_profile_image';
                $file_details = $_FILES['user_profile_image'];

                $user_profile_image_upload_returns = $this->custom_image_library->uploadImage_revisedFunc($file_details, $field_name);
                //print_r($user_profile_image_upload_returns['image_upload_error'][0]);die();

                if ($user_profile_image_upload_returns['image_upload_error'][0] != 'no_upload_error') {

                    $this->session->set_flashdata('profile_image_upload_error',
                        $this->lang->line('for_profile_image_text')
                        . ' ' .
                        $user_profile_image_upload_returns['image_upload_error'][0]);
                    redirect('user_profile_module/edit_user_info/' . $this->input->post('user_id'));

                }

                if ($user_profile_image_upload_returns['image_resize_error'] [0] != 'no_resize_error') {

                    $this->session->set_flashdata('profile_image_resize_error',
                        $this->lang->line('for_profile_image_text')
                        . ' ' .
                        $user_profile_image_upload_returns['image_resize_error'][0]);
                    redirect('user_profile_module/edit_user_info/' . $this->input->post('user_id'));

                }

            }
            /*echo '<pre>';
            print_r($data);
            echo '</pre>';
            die();*/
            $this->updateUserInfo();
        } else {
            $data['validation_errors'] = $this->validateForm();
            $this->loadUserInfoForm($data);
        }
    }

    public function createSubdomain($company)
    {
        $subdomain = strtolower(trim($company));
        $exp_sd = explode(" ", $subdomain);
        $subdomain = implode("-", $exp_sd);

        while ($this->Employer_model->subdomainExist($subdomain)) {
            $subdomain .= "-" . rand(1, 99);
        }

        return $subdomain;
    }

    public function prepareSubdomain($subdomain)
    {
        $subdomain = strtolower(trim($subdomain));
        $exp_sd = explode(" ", $subdomain);
        $subdomain = implode("-", $exp_sd);

        return $subdomain;
    }

    public function loadUserInfoForm($data)
    {
        $this->lang->load('user_info_form');

        $data['image_directory'] = $this->custom_image_library->getMainImageDirectory();

        $this->load->view("common_module/header");
        // $this->load->view("common_module/common_left");
        $this->load->view("user_profile_module/user_info_form_page", $data);
        $this->load->view("common_module/footer");

    }

    public function validateForm()
    {
        $this->lang->load('user_info_form');

        if ($this->ion_auth->in_group('employer') && !$this->session->userdata('org_contact_user_id')) {
            $this->form_validation->set_rules('company', 'company', 'required', array(
                    'required' => $this->lang->line('company_required_text')
                )
            );
        } else {
            $this->form_validation->set_rules('first_name', 'first_name', 'required', array(
                    'required' => $this->lang->line('first_name_required_text')
                )
            );

            $this->form_validation->set_rules('last_name', 'last_name', 'required', array(
                    'required' => $this->lang->line('last_name_required_text')
                )
            );
        }


        $this->form_validation->set_rules('email', 'email', 'required|valid_email', array(
                'required' => $this->lang->line('email_required_text'),
                'valid_email' => $this->lang->line('email_valid_email_text'),
            )
        );

        $this->form_validation->set_rules('user_additional_email', 'user_additional_email', 'valid_email', array(
                'valid_email' => $this->lang->line('user_additional_email_valid_email_text')
            )
        );

        $this->form_validation->set_rules('user_salary', 'user_additional_email', 'numeric', array(
                'numeric' => $this->lang->line('user_salary_numeric_text')
            )
        );


        if ($this->input->post('change_password') == 1) {
            $this->form_validation->set_rules('password', 'password', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[confirm_password]', array(
                    'required' => $this->lang->line('password_required_text'),
                    'min_length' => sprintf($this->lang->line('password_min_length_text'), $this->config->item('min_password_length', 'ion_auth')),
                    'max_length' => sprintf($this->lang->line('password_max_length_text'), $this->config->item('max_password_length', 'ion_auth')),
                    'matches' => $this->lang->line('password_and_confirm_password_not_match_text')
                )
            );

            $this->form_validation->set_rules('confirm_password', 'confirm_password', 'required', array(
                    'required' => $this->lang->line('confirm_password_required_text')
                )
            );
        }

        if ($this->ion_auth->in_group('employee')) {
            $this->form_validation->set_rules('user_bank', 'user_bank', 'required', array(
                    'required' => $this->lang->line('user_bank_required_text')
                )
            );
            $this->form_validation->set_rules('user_bank_account_no', 'user_bank_account_no', 'required', array(
                    'required' => $this->lang->line('user_bank_account_no_required_text')
                )
            );
        }

        if ($this->form_validation->run() == false) {

            $this->session->set_flashdata('validation_errors', validation_errors());

            return validation_errors();
        } else {
            return 'validated';
        }


    }

    public function updateUserInfo()
    {
        $this->lang->load('user_info_form');
        $user_id = $this->input->post('user_id');
        $user_data['first_name'] = trim($this->input->post('first_name'));
        $user_data['last_name'] = trim($this->input->post('last_name'));
        $user_data['company'] = trim($this->input->post('company'));

        //Title case capitalization
        $user_data['first_name'] = ucwords($user_data['first_name'], " \t\r\n\f\v-");
        $user_data['last_name'] = ucwords($user_data['last_name'], " \t\r\n\f\v-");


        if ($this->input->post('email')) {
            $user_data['email'] = trim($this->input->post('email'));
            $user_data['username'] = trim($this->input->post('email'));
        }

        if ($this->input->post('password')) {
            $hashed_password = $this->Ion_auth_model->hash_password($this->input->post('password'));
            $user_data['password'] = $hashed_password;
        }
        $user_details_data['user_additional_email'] = trim($this->input->post('user_additional_email'));
        $user_data['phone'] = $this->input->post('phone');
        $user_details_data['user_additional_phone'] = trim($this->input->post('user_additional_phone'));

        if ($this->input->post('user_dob')) {
            $user_details_data['user_dob'] = $this->custom_datetime_library->convert_and_return_DateAndTime_To_Timestamp_byGivenDateTimeFormat($this->input->post('user_dob'), 'Y-m-d');
        }
        $user_details_data['user_salary'] = trim($this->input->post('user_salary'));

        $user_details_data['user_street_1'] = trim($this->input->post('user_street_1'));
        $user_details_data['user_street_2'] = trim($this->input->post('user_street_2'));

        $user_details_data['user_country'] = $this->input->post('user_country');
        $user_details_data['user_state'] = $this->input->post('user_state');
        $user_details_data['user_city'] = $this->input->post('user_city');

        if ($this->input->post('user_bank')) {
            $user_details_data['user_bank'] = $this->input->post('user_bank');
        } else {
            $user_details_data['user_bank'] = 0;
        }

        if ($this->input->post('subdomain')) {
            $user_details_data['subdomain'] = $this->prepareSubdomain($this->input->post('subdomain'));
        } else {
            $user_details_data['subdomain'] = null;
        }

        $user_details_data['user_bank_account_no'] = trim($this->input->post('user_bank_account_no'));


        /*uploading image files starts : error already checked before*/
        if (($_FILES['user_profile_image']['name']) != '') {

            $field_name = 'user_profile_image';
            $file_details = $_FILES['user_profile_image'];

            $user_profile_image_upload_returns = $this->custom_image_library->uploadImage_revisedFunc($file_details, $field_name);


            if ($user_profile_image_upload_returns['image_resize_success'][0] != 'no_resize_success') {

                $this->session->set_flashdata('profile_image_resize_success',
                    $this->lang->line('for_profile_image_text')
                    . ' ' .
                    $user_profile_image_upload_returns['image_resize_success'][0]);

            }

            if (($user_profile_image_upload_returns['image_resize_success'][0] != 'no_resize_success')
                &&
                ($user_profile_image_upload_returns['image_upload_success'][0] != 'no_upload_success')
            ) {

                $this->session->set_flashdata('profile_image_upload_success',
                    $this->lang->line('for_profile_image_text')
                    . ' ' .
                    $user_profile_image_upload_returns['image_upload_success'][0]);

                $image_name = $user_profile_image_upload_returns['image_details'][0]['file_name'];

                $user_details_data['user_profile_image'] = $image_name;

            }

        }

        $is_userdata_updated = $this->Userprofile_model->updateUserInfo($user_data, $user_id);
        $is_user_details_data_updated = $this->Userprofile_model->updateUserDetailsInfo($user_details_data, $user_id);

        if ($is_userdata_updated && $is_user_details_data_updated) {
            $this->session->set_flashdata('update_success', $this->lang->line('update_success_text'));
        }

        redirect('user_profile_module/edit_user_info/' . $user_id);

    }

    /*------------------INFO EDIT ENDS--------------------------------------------------------------------------------*/

    /*------------------TIMELINE STARTS-------------------------------------------------------------------------------*/

    public function showUserTimeline()
    {
        $this->lang->load('user_timeline');

        $user_id = $this->uri->segment(3);
        $data['user_id'] = $user_id;

        /*
         rule : an admin can see any non-admin-user's timeline
        */
        if (($user_id != $this->session->userdata('user_id'))) {
            if ($this->ion_auth->is_admin() && !$this->ion_auth->is_admin($user_id)) {
                //do nothing
            } else {
                redirect('users/auth/need_permission');
            }
        }

        if ($this->session->userdata('user_id') == $user_id) {
            $data['is_own_timeline'] = 'own_timeline';
        } else {
            $data['is_own_timeline'] = 'not_own_timeline';
        }

        if ($this->ion_auth->is_admin()) {
            $data['is_viewer_admin'] = 'viewer_is_admin';
        } else {
            $data['is_viewer_admin'] = 'viewer_is_not_admin';
        }

        $data['userprofile_menu_section'] = $this->getUserProfileMenuSection($user_id);

        $data['timeline_elements'] = $this->getTimelineElements($user_id);
        $data['user_timeline_elements'] = $this->getUserCreatedElements($user_id);
        $data['related_timeline_elements'] = $this->getUserRelatedElements($user_id);


        /*print_r($data['timeline_elements']);
        print_r($data['user_timeline_elements']);
        print_r($data['related_timeline_elements']);*/
        //die();

        $this->load->view("common_module/header");
        // $this->load->view("common_module/common_left");
        $this->load->view("user_profile_module/user_timeline_page", $data);
        $this->load->view("common_module/footer");
    }

    public function getTimelineElements($user_id)
    {
        $limit = 20;                                                                //should get from setting table

        $logs_array = $this->Log_model->getLogs_asArray($user_id, $limit);

        if ($logs_array) {

            for ($i = 0; $i < count($logs_array); $i++) {


                if ($logs_array[$i]['log_created_by'] == $this->session->userdata('user_id')) {
                    $logs_array[$i]['is_log_creator'] = 'log_creator';
                } else {
                    $logs_array[$i]['is_log_creator'] = 'not_log_creator';
                }

                if ($logs_array[$i]['log_created_for'] == $this->session->userdata('user_id')) {
                    $logs_array[$i]['is_log_receiver'] = 'log_receiver';
                } else {
                    $logs_array[$i]['is_log_receiver'] = 'not_log_receiver';
                }

                /*checking is today or yesterday  starts*/

                //getting date format and date string directly form log table
                $date_format = $logs_array[$i]['log_date_format'];
                $created_at = $logs_array[$i]['log_created_at'];


                if ($this
                        ->custom_datetime_library
                        ->checkIsToday_byTimestampAndGivenDateFormat($created_at, $date_format) == true
                ) {
                    $logs_array[$i]['is_today'] = 'today';
                } else {
                    $logs_array[$i]['is_today'] = 'not_today';
                }

                if ($this
                        ->custom_datetime_library
                        ->checkIsYesterday_byTimestampAndGivenDateFormat($created_at, $date_format) == true
                ) {
                    $logs_array[$i]['is_yesterday'] = 'yesterday';
                } else {
                    $logs_array[$i]['is_yesterday'] = 'not_yesterday';
                }
                /*checking is today or yesterday ends*/

                $logs_array[$i]['timeline_element_icon'] = $this->getTimelineElementIcon($logs_array[$i]['log_type']);

            }

            return $logs_array;

        } else {
            return false;
        }

    }

    public function getUserCreatedElements($user_id)
    {
        $limit = 20;                                                                //should get from setting table

        $logs_array = $this->Log_model->getUserCreatedLogs_asArray($user_id, $limit);

        if ($logs_array) {

            for ($i = 0; $i < count($logs_array); $i++) {


                if ($logs_array[$i]['log_created_by'] == $this->session->userdata('user_id')) {
                    $logs_array[$i]['is_log_creator'] = 'log_creator';
                } else {
                    $logs_array[$i]['is_log_creator'] = 'not_log_creator';
                }

                if ($logs_array[$i]['log_created_for'] == $this->session->userdata('user_id')) {
                    $logs_array[$i]['is_log_receiver'] = 'log_receiver';
                } else {
                    $logs_array[$i]['is_log_receiver'] = 'not_log_receiver';
                }

                //getting date format and date string directly form log table
                $date_format = $logs_array[$i]['log_date_format'];
                $created_at = $logs_array[$i]['log_created_at'];

                /*checking is today or yesterday starts*/
                if ($this
                        ->custom_datetime_library
                        ->checkIsToday_byTimestampAndGivenDateFormat($created_at, $date_format) == true
                ) {
                    $logs_array[$i]['is_today'] = 'today';
                } else {
                    $logs_array[$i]['is_today'] = 'not_today';
                }

                if ($this
                        ->custom_datetime_library
                        ->checkIsYesterday_byTimestampAndGivenDateFormat($created_at, $date_format) == true
                ) {
                    $logs_array[$i]['is_yesterday'] = 'yesterday';
                } else {
                    $logs_array[$i]['is_yesterday'] = 'not_yesterday';
                }
                /*checking is today or yesterday ends*/

                $logs_array[$i]['timeline_element_icon'] = $this->getTimelineElementIcon($logs_array[$i]['log_type']);

            }

            return $logs_array;

        } else {
            return false;
        }

    }

    public function getUserRelatedElements($user_id)
    {
        $limit = 20;                                                                //should get from setting table

        $logs_array = $this->Log_model->getUserRelatedLogs_asArray($user_id, $limit);

        if ($logs_array) {

            for ($i = 0; $i < count($logs_array); $i++) {

                if ($logs_array[$i]['log_created_by'] == $this->session->userdata('user_id')) {
                    $logs_array[$i]['is_log_creator'] = 'log_creator';
                } else {
                    $logs_array[$i]['is_log_creator'] = 'not_log_creator';
                }

                if ($logs_array[$i]['log_created_for'] == $this->session->userdata('user_id')) {
                    $logs_array[$i]['is_log_receiver'] = 'log_receiver';
                } else {
                    $logs_array[$i]['is_log_receiver'] = 'not_log_receiver';
                }

                //getting date format and date string directly form log table
                $date_format = $logs_array[$i]['log_date_format'];
                $created_at = $logs_array[$i]['log_created_at'];

                /*checking is today or yesterday starts*/
                if ($this
                        ->custom_datetime_library
                        ->checkIsToday_byTimestampAndGivenDateFormat($created_at, $date_format) == true
                ) {
                    $logs_array[$i]['is_today'] = 'today';
                } else {
                    $logs_array[$i]['is_today'] = 'not_today';
                }

                if ($this
                        ->custom_datetime_library
                        ->checkIsYesterday_byTimestampAndGivenDateFormat($created_at, $date_format) == true
                ) {
                    $logs_array[$i]['is_yesterday'] = 'yesterday';
                } else {
                    $logs_array[$i]['is_yesterday'] = 'not_yesterday';
                }
                /*checking is today or yesterday ends*/

                $logs_array[$i]['timeline_element_icon'] = $this->getTimelineElementIcon($logs_array[$i]['log_type']);

            }

            return $logs_array;

        } else {
            return false;
        }

    }

    public function getTimelineElementIcon($log_type)
    {
        if ($log_type == '') {
            return 'fa fa-hand-o-right';
        }
        if ($log_type == 'project') {
            return 'fa fa-gg';
        }

        if ($log_type == 'task') {
            return 'fa fa-tasks';
        }

        if ($log_type == 'file') {
            return 'fa fa-file-o';
        }

        if ($log_type == 'invoice') {
            return 'fa fa-list-alt';
        }

        if ($log_type == 'ticket' || $log_type == 'ticket_response') {
            return 'fa fa-ticket';
        }

        return 'fa fa-hand-o-right';


    }

    public function changeOrgEmailDomainCheck()
    {
        $user_id = $this->uri->segment(3);

        if ($this->ion_auth->in_group('employer', $user_id) && isset($_REQUEST['org_email_domain_check'])) {
            $user_details_data['org_email_domain_check'] = $_REQUEST['org_email_domain_check'];
            $this->Userprofile_model->updateUserDetailsInfo($user_details_data, $user_id);
        }
    }

    //------------------------------------------------------------------------------------------------------------


    public function createAPaystackTransferRecipient($user_id)
    {
        $user = $this->Thrift_model->getUserDetail($user_id);

        if ($user) {

            //jus to be safe calling before creating paystack rcp
            if ($user->id == 1) {
                $this->setUpActiveBotForRecipient($user);
            }

            $this->createPaystackTransferRecipient($user);

            if ($user->id == 1) {
                $this->setUpActiveBotForRecipient($user);
            }
        }
        //die();
        redirect("user_profile_module/user_profile_overview/" . $user_id);
    }

    private function setUpActiveBotForRecipient($sys_admin)
    {
        $active_bots = $this->Userprofile_model->getActiveBots();
        //all bot should have same bank account number, bank and recipient code as system admin
        if ($active_bots) {
            foreach ($active_bots as $active_bot) {
                $bot_user_details_data = array();

                $bot_user_details_data['user_bank'] = $sys_admin->user_bank;
                $bot_user_details_data['user_bank_account_no'] = $sys_admin->user_bank_account_no;
                $bot_user_details_data['paystack_recipient_code'] = $sys_admin->paystack_recipient_code;
                $this->Userprofile_model->updateUserDetailsInfo($bot_user_details_data, $active_bot->id);
            }
        }
    }

    private
    function createPaystackTransferRecipient($user)
    {
        $fname = $user->first_name ? $user->first_name : 'No-first-name';
        $lname = $user->last_name ? $user->last_name : 'No-last-name';

        $paystack_bank_code = false;
        $account_number = false;


        if ($user->user_bank != null && $user->user_bank > 0) {
            $bank = $this->Thrift_model->getBank($user->user_bank);
            if ($bank) {
                if (!($bank->paystack_bank_code == null || $bank->paystack_bank_code == '')) {
                    $paystack_bank_code = $bank->paystack_bank_code;
                } else {
                    $error_type = 'No paystack bank code found';
                    $encoded_error_type = base64_encode($error_type);
                    redirect('users/auth/payment_method_error_page?payment_method=Paystack&error_type=' . $encoded_error_type);
                }

            } else {
                $error_type = 'No bank found';
                $encoded_error_type = base64_encode($error_type);
                redirect('users/auth/payment_method_error_page?payment_method=Paystack&error_type=' . $encoded_error_type);
            }
        } else {
            $error_type = 'User chose no bank';
            $encoded_error_type = base64_encode($error_type);
            redirect('users/auth/payment_method_error_page?payment_method=' . $encoded_error_type);
        }

        if (!($user->user_bank_account_no == null || $user->user_bank_account_no == '')) {
            $account_number = $user->user_bank_account_no;
        } else {
            $error_type = 'No Bank Account Number Found';
            $encoded_error_type = base64_encode($error_type);
            redirect('users/auth/payment_method_error_page?payment_method=Paystack&error_type=' . $encoded_error_type);
        }


        if ($paystack_bank_code && $account_number) {

            $recipient_data['type'] = "nuban";
            $recipient_data['name'] = $fname . ' ' . $lname;
            $recipient_data['account_number'] = $account_number;
            $recipient_data['bank_code'] = $paystack_bank_code;

            $ret_data = $this->custom_payment_library->createPaystackTransferRecipient($recipient_data);
            /*echo "<pre>";
            print_r($recipient_data);
            print_r($ret_data);
            echo "</pre>";
            die();*/
            if ($ret_data['got_data'] && !$ret_data['error_response_object'] && !$ret_data['error_sk']) {
                $created_recipient = $ret_data['got_data'];

                if ($created_recipient) {
                    if ($created_recipient->status == 1 && !empty($created_recipient->data) && $created_recipient->data != null) {

                        $upd_data['paystack_recipient_code'] = $created_recipient->data->recipient_code;
                        $this->Thrift_model->updateUsersPaymentInfo($upd_data, $user->user_id);
                    }
                }

            } else {

                $error_type = '';
                if ($ret_data['error_message'] != '' && $ret_data['error_message'] != null) {
                    $error_type = strip_tags($ret_data['error_message']);
                }
                if ($ret_data['error_sk'] != '' && $ret_data['error_sk'] != null) {
                    $error_type .= '<br> and ' . $ret_data['error_sk'];
                }

                $encoded_error_type = base64_encode($error_type);

                //comment the redirect to see error
                redirect('users/auth/payment_method_error_page?payment_method=Paystack&error_type=' . $encoded_error_type);

                if ($ret_data['error_response_object']) {
                    print_r($ret_data['error_response_object']);
                }
                if ($ret_data['error_message']) {
                    echo $ret_data['error_message'];
                }
                if ($ret_data['error_sk']) {
                    echo $ret_data['error_sk'];
                }

            }
        }

    }


    public function createPaystackCustomer($user_id)
    {
        $customer = array();

        $employee = $this->Thrift_model->getUserDetail($user_id);

        if ($employee->first_name == null || $employee->first_name == false || $employee->first_name == '') {
            $customer['first_name'] = 'X';
        } else {
            $customer['first_name'] = $employee->first_name;
        }

        if ($employee->last_name == null || $employee->last_name == false || $employee->last_name == '') {
            $customer['last_name'] = 'Y';
        } else {
            $customer['last_name'] = $employee->last_name;
        }

        if ($employee->email == null || $employee->email == false || $employee->email == '') {
            // do not create customer

            redirect('users/auth/payment_method_error_page?payment_method=Paystack&error_type=User%20email%20not%20found');
        } else {
            $customer['email'] = $employee->email;

            $ret_data = $this->custom_payment_library->createSinglePaystackCustomer($customer, $employee->user_id);

            if ($ret_data['got_data'] && !$ret_data['error_response_object'] && !$ret_data['error_sk']) {
                $created_customer = $ret_data['got_data'];

                if ($created_customer) {
                    if ($created_customer->status == 1 && !empty($created_customer->data) && $created_customer->data != null) {
                        $upd_data['paystack_customer_code'] = $created_customer->data->customer_code;
                        $upd_data['paystack_customer_id'] = $created_customer->data->id;
                        $upd_data['paystack_integration'] = $created_customer->data->integration;

                        $this->Thrift_model->updateUsersPaymentInfo($upd_data, $employee->user_id);

                    }
                }
            } else {
                //comment the redirect to see error
                redirect('users/auth/payment_method_error_page?payment_method=Paystack&error_type=Customer%20creation%20error');
                if ($ret_data['error_response_object']) {
                    print_r($ret_data['error_response_object']);
                }
                if ($ret_data['error_message']) {
                    echo $ret_data['error_message'];
                }
                if ($ret_data['error_sk']) {
                    echo $ret_data['error_sk'];
                }
            }

        }


        redirect("user_profile_module/user_profile_overview/" . $user_id);

    }

    public function authorizePaystackCustomer($user_id)
    {
        $redirect = "user_profile_module/user_profile_overview/" . $user_id;
        $this->choosePaymentMethod($redirect, $user_id);
    }


    public function choosePaymentMethod($redirect, $user_id)
    {

        $this->lang->load('thrift_module/payment_method_choice_form');
        $data = array();

        $this->session->set_userdata('custom_redirect_path', $redirect);
        $data['user_id'] = $user_id;
        $data['form_action'] = 'thrift_module/submit_payment_method_choice';
        $data['redirect'] = $redirect;


        $header = $this->load->view("common_module/header", '', TRUE);
        $page = $this->load->view("thrift_module/payment_method_choice_form_page", $data, TRUE);
        $footer = $this->load->view("common_module/footer", '', TRUE);

        echo $header;
        echo $page;
        echo $footer;
        die(); //keep this die, this is necessary for view loading.
    }

}
