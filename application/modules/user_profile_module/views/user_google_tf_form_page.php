<!-- Content Wrapper. Contains page content -->

<style>
    @import url(http://fonts.googleapis.com/css?family=Open+Sans:400,700,300);

    .form-control, .thumbnail {
        border-radius: 2px;
    }

    .btn-danger {
        background-color: #B73333;
    }

    /* File Upload */
    .fake-shadow {
        box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
    }

    .fileUpload {
        position: relative;
        overflow: hidden;
    }

    .fileUpload #user_profile_image-id {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        padding: 0;
        font-size: 33px;
        cursor: pointer;
        opacity: 0;
        filter: alpha(opacity=0);
    }

    .fileUpload #user_cover_image-id {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        padding: 0;
        font-size: 33px;
        cursor: pointer;
        opacity: 0;
        filter: alpha(opacity=0);
    }

    .user_profile_image-preview {
        max-width: 25%;
    }

    .user_cover_image-preview {
        max-width: 40%;
    }
    .dn{
        display:none;
    }
</style>


<div class="container">
    <div class="row">
        <div class="col-xl-12">
            <div class="page-title-box">
                <h4 class="page-title float-left">
                    <?php echo lang('page_title_text') ?>
                </h4>
                <ol class="breadcrumb float-right">
                    <li class="breadcrumb-item"><a href=""><?php echo lang('breadcrumb_home_text') ?></a></li>
                    <li class="breadcrumb-item"><a
                                href="user_profile_module/user_profile_overview/<?= $user_id; ?>"><?php echo lang('breadcrumb_section_text') ?></a>
                    </li>
                    <li class="breadcrumb-item active"><?php echo lang('breadcrumb_page_text') ?></li>
                </ol>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <?php if ($this->session->flashdata('employer_insert')) { ?>
                    <div class="text-center alert alert-success alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <strong><?php echo $this->session->flashdata('employer_insert') ?></strong>
                    </div>
                <?php } ?>

                <?php if ($this->session->flashdata('employer_insert_error')) { ?>
                    <div class="text-center alert alert-danger alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <strong><?php echo $this->session->flashdata('employer_insert_error') ?></strong>
                    </div>
                <?php } ?>

                <?php if ($this->session->flashdata('employer_exist_error')) { ?>
                    <div class="text-center alert alert-danger alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <strong><?php echo $this->session->flashdata('employer_exist_error') ?></strong>
                    </div>
                <?php } ?>

                <h4 class="header-title m-t-0 m-b-30"><?php echo lang('employee_add_form_header_text') ?></h4>
                <div class="row">
                    <div class="col-lg-4 col-md-6">
                        <div class="box box-primary">
                            <div class="box-body box-profile box-widget widget-user-2">
                                <div class="widget-user-header  custom-user-background">
                                    <img class="profile-user-img img-responsive img-circle" style="max-width:100%;"
                                        <?php if ($user_info->user_profile_image == '' || $user_info->user_profile_image == null) { ?>
                                            src="<?php echo base_url() . 'base_demo_images/user_profile_image_demo.png' ?>"
                                        <?php } else { ?>
                                            src="<?php echo $this->config->item('pg_upload_source_path') ?>image/<?= $user_info->user_profile_image ?>"
                                        <?php } ?>
                                         alt="User profile picture">
                                </div>
                                <hr>
                                <ul class="">
                                    <h5 class="text-muted text-center">
                                        <?php if ($this->ion_auth->in_group('employer') && !$this->session->userdata('org_contact_user_id')) { ?>
                                            <strong>
                                                <?php echo $user_info->company; ?>
                                            </strong>
                                        <?php } else { ?>
                                            <strong>
                                                <?php echo $user_info->first_name . ' ' . $user_info->last_name ?>
                                            </strong>
                                        <?php } ?>
                                    </h5>
                                </ul>
                                <hr>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-6">
                        <!-- general form elements -->
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title"><?php echo lang('box_title_text') ?></h3>
                                <br>
                                <div class=" col-md-offset-2 col-md-8" style="color: darkred;font-size: larger">
                                    <?php if ($validation_errors) {
                                        echo $validation_errors;
                                        echo '<br>';
                                    }
                                    ?>
                                </div>
                                <div class="col-md-2"></div>
                                <div class=" col-md-offset-2 col-md-8" style="color: darkgreen;font-size: larger">
                                    <!--demo-->

                                    <?php if ($this->session->flashdata('update_success')) {
                                        echo $this->session->flashdata('update_success');
                                        echo '<br>';
                                    }
                                    ?>

                                </div>
                                <div class="col-md-2"></div>
                            </div>
                            <!-- /.box-header -->

                            <!-- form start -->
                            <form <?php if ($user_info->google_tf_auth_status == 0) { ?> style="display: none;" <?php } ?>
                                    action="<?php echo base_url() . 'user_profile_module/google_tf_auth/' . $user_id ?>"
                                    role="form"
                                    id="" method="post" enctype="multipart/form-data">
                                <div class="box-body">
                                    <input type="hidden" name="user_id" value="<?php echo $user_id ?>">

                                    <div class="form-group">
                                        <label for="google_tf_auth_status"><?php echo lang('label_google_tf_auth_status') ?></label>

                                        <select name="google_tf_auth_status" id="google_tf_auth_status">
                                            <option value="0">Register again</option>
                                            <?php $display_deactivate_option = true;
                                                if($user_info->google_tf_auth_forced == 1){
                                                    $display_deactivate_option = false;
                                                }
                                            ?>
                                            <?php if($display_deactivate_option) { ?>
                                            <option value="1" <?php if ($user_info->google_tf_auth_status == 1) {
                                                echo " selected ";
                                            }; ?>>Deactivate
                                            </option>
                                            <?php } ?>
                                            <option value="2" <?php if ($user_info->google_tf_auth_status == 2) {
                                                echo " selected ";
                                            }; ?>>Activate
                                            </option>
                                        </select>
                                    </div>


                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer">
                                    <button type="submit" id="btnsubmit" class="btn btn-primary">
                                        <?php echo lang('button_submit_text') ?>
                                    </button>
                                </div>
                            </form>

                            <form id="google_tf_qr_validate_form" <?php if ($user_info->google_tf_auth_status != 0) { ?> style="display: none;" <?php } ?>
                                  role="form"
                                  method="post" enctype="multipart/form-data">
                                <div class="box-body">
                                    <h4>Application Authentication</h4>

                                    <p>
                                        Please download and install Google authenticate app on your phone, and scan
                                        following QR code to configure your device.
                                    </p>

                                    <div class="form-group">
                                        <img src="<?php echo $qr_code; ?>">
                                    </div>
                                    <form method="post" action="user_profile_module/post_qr_code">

                                        <div style="color: green" class="tf_auth_success"></div>
                                        <div style="color: red" class="tf_auth_error"></div>
                                        <div class="form-group">
                                            <label for="code">Enter Authentication Code:</label>
                                            <input type="hidden" name="user_id" value="<?php echo $user_id ?>">
                                            <input type="text" name="code" placeholder="6 Digit Code"
                                                   class="form-control">
                                            <input type="hidden" name="secret" value="<?= $secret ?>"
                                                   class="form-control">
                                        </div>

                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer">
                                    <button type="submit" id="qr_submit" class="btn btn-primary">
                                        <?php echo lang('button_validate_text') ?>
                                    </button>
                                    &nbsp;
                                        <a style="display: none" class="btn btn-primary tf_auth_refresh" href="<?php echo base_url() . 'user_profile_module/google_tf_auth/' . $user_id ?>">Continue Setup</a>

                                </div>
                            </form>
                        </div>
                    </div>
                </div><!-- end row -->
            </div>
        </div><!-- end col -->
    </div>
</div>

<script>

    $("#qr_submit").on('click', function (e) {
        e.preventDefault();
        var datastring = $("#google_tf_qr_validate_form").serialize();
        $.ajax({
            type: "POST",
            url: 'user_profile_module/google_tf_qr_validate',
            data: datastring,
            dataType: "json",
            success: function (data) {
                //var obj = jQuery.parseJSON(data); if the dataType is not specified as json uncomment this
                // do what ever you want with the server response
                /*console.log(data);alert('kkk');
                var parsed_data = jQuery.parseJSON(data);*/

                console.log(data.res);
                console.log(data.message);

                if (data.res == "suc") {
                    $(".tf_auth_refresh").show();
                    $(".tf_auth_error").html('');
                    $(".tf_auth_success").html(data.message);
                } else {
                    $(".tf_auth_refresh").hide();
                    $(".tf_auth_error").html(data.message);
                    $(".tf_auth_success").html('');
                }


            },
            error: function () {
                $(".tf_auth_refresh").hide();
                $(".tf_auth_success").html('');
                $(".tf_auth_error").html('Error');
            }
        });
    });


</script>
