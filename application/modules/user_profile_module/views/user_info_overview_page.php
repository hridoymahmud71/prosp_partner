<div class="container">
    <div class="row">
        <div class="col-xl-12">
            <div class="page-title-box">
                <h4 class="page-title float-left">
                    <?php if ($is_own_profile == 'own_profile') {
                        echo lang('page_title_my_profile_text');
                    } else {
                        echo lang('page_title_user_profile_text');
                    }
                    ?>
                    <small>
                        <?php echo lang('page_subtitle_text'); ?>
                    </small>
                </h4>
                <ol class="breadcrumb float-right">
                    <li class="breadcrumb-item"><a href="common_module"><?php echo lang('breadcrumb_home_text') ?></a>
                    </li>
                    <li class="breadcrumb-item"><a
                                href="user_profile_module/user_profile_overview/<?= $user_id; ?>"><?php echo lang('breadcrumb_section_text') ?></a>
                    </li>
                    <li class="breadcrumb-item active"><?php echo lang('breadcrumb_page_text') ?></li>
                </ol>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <div class="row">
                    <div class="col-lg-4 col-md-6">
                        <div class="box box-primary">
                            <div class="box-body box-profile box-widget widget-user-2">
                                <div class="widget-user-header  custom-user-background">
                                    <img class="profile-user-img img-responsive img-circle" style="max-width:100%;"
                                        <?php if ($user_info->user_profile_image == '' || $user_info->user_profile_image == null) { ?>
                                            src="<?php echo base_url() . 'base_demo_images/user_profile_image_demo.png' ?>"
                                        <?php } else { ?>
                                            src="<?php echo $this->config->item('pg_upload_source_path') ?>image/<?= $user_info->user_profile_image ?>"
                                        <?php } ?>
                                         alt="User profile picture">
                                    <?php if ($is_own_profile == 'others_profile') { ?>
                                        <!--<a href="#" class="btn btn-warning btn-block">
                                            <i class="fa fa-envelope fa-pull-right" aria-expanded="false"></i>
                                            <b>Message</b>
                                        </a>-->
                                    <?php } ?>
                                </div>
                                <hr>
                                <ul class="">
                                    <h5 class="text-muted text-center">
                                        <?php if ($this->ion_auth->in_group('employer', $user_id) && !$this->session->userdata('org_contact_user_id')) { ?>
                                            <strong>
                                                <?php echo $user_info->company; ?>
                                            </strong>
                                        <?php } else { ?>
                                            <strong>
                                                <?php echo $user_info->first_name . ' ' . $user_info->last_name ?>
                                            </strong>
                                        <?php } ?>
                                    </h5>
                                </ul>
                                <hr>
                            </div>
                        </div>
                        <!--userprofile_menu_section ends -->

                        <!--paystack section for admin-->
                        <?php if (
                            $this->ion_auth->in_group('superadmin')
                            &&
                            ($this->ion_auth->in_group('superadmin', $user_info->user_id)
                                ||
                                ($this->ion_auth->in_group('admin', $user_info->user_id)
                                    && $user_info->is_user_bot == 1)
                            )
                        ) { ?>
                            <div class="card m-b-20 card-body">
                                <h4 class="card-title"><?= lang('paystack_section_title_text') ?></h4>
                                <p class="card-text"><?= lang('paystack_section_description_text') ?></p>

                                <a href="user_profile_module/create_a_paystack_transfer_recipient/<?= $user_info->user_id ?>"
                                   class="btn btn-primary m-t-10"><?= lang('paystack_create_recipient_button_text') ?></a>
                                <a href="user_profile_module/create_paystack_customer/<?= $user_info->user_id ?>"
                                   class="btn btn-primary m-t-10"><?= lang('paystack_create_paystack_customer_button_text') ?></a>
                                <a href="user_profile_module/authorize_paystack_customer/<?= $user_info->user_id ?>"
                                   class="btn btn-primary m-t-10"><?= lang('paystack_authorize_paystack_customer_button_text') ?></a>

                            </div>
                        <?php } ?>

                        <!--paystack section for thrifter-->
                        <?php if (
                            $this->ion_auth->in_group('employee')
                            &&
                            $this->session->userdata('user_id') == $user_info->user_id

                        ) { ?>
                            <div class="card m-b-20 card-body">
                                <h4 class="card-title"><?= lang('paystack_section_title_text') ?></h4>
                                <p class="card-text"><?= lang('paystack_section_description_text') ?></p>

                                <a href="user_profile_module/authorize_paystack_customer/<?= $user_info->user_id ?>"
                                   class="btn btn-primary m-t-10"><?= lang('paystack_authorize_paystack_customer_button_text') ?></a>

                            </div>
                        <?php } ?>
                    </div>
                    <!-- /.col -->
                    <div class="col-lg-8 col-md-6">
                        <div class="box-header with-border">
                            <div class="text-center">
                                <h3 class="box-title"><?php echo lang('about_me_box_title_text') ?></h3>
                            </div>

                            <div class="pull-right">

                                <?php if ($is_own_profile == 'own_profile') { ?>

                                    <?php if ($this->ion_auth->is_admin()) { ?>

                                        <a class="btn btn-primary"
                                           href="<?php echo base_url() . 'user_profile_module/google_tf_auth/' . $user_id ?>">Configure
                                            Two-Factor Authentication
                                            &nbsp;<span class="icon"><i class="fa fa-key"></i></span>
                                        </a>

                                        &nbsp;
                                    <?php } ?>

                                    <a class="btn btn-primary"
                                       href="<?php echo base_url() . 'user_profile_module/edit_user_info/' . $user_id ?>">Edit
                                        Info
                                        &nbsp<span class="icon"><i class="fa fa-pencil-square-o"></i></span>
                                    </a>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="box box-primary">
                            <br>
                            <br>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="card">
                                    <div class="card-header">
                                        <div class="col-md-12 m-b-30">
                                            <div class="col-md-4 pull-left">
                                                <strong><i class="fa fa-info-circle margin-r-5"></i><span
                                                            class="m-l-5"><?php echo lang('about_section_basic_info_text'); ?></span></strong>
                                            </div>
                                        </div>
                                    </div>
                                    <ul class="list-group list-group-flush">
                                        <?php if ($this->ion_auth->is_admin() && $this->ion_auth->in_group('employer', $user_info->user_id)) { ?>
                                            <li class="list-group-item">
                                                <div class="col-md-12 m-b-30">
                                                    <div class="col-md-4 pull-left">
                                                        <strong><i class="fa fa-cog margin-r-5"></i><span
                                                                    class="m-l-5"><?php echo lang('about_email_domain_check_text') ?></span></strong>
                                                    </div>
                                                    <div class="col-md-8 pull-right">
                                                        <input type="checkbox" id="email_domain_check_switch"
                                                               class="" <?php if ($user_info->org_email_domain_check == 1) {
                                                            echo " checked ";
                                                        } ?> />
                                                    </div>
                                                </div>
                                            </li>
                                        <?php } ?>
                                        <li class="list-group-item">
                                            <div class="col-md-12 m-b-30">
                                                <div class="col-md-4 pull-left">
                                                    <strong><i class="fa fa-user margin-r-5"></i><span
                                                                class="m-l-5"><?php echo lang('about_section_name_text') ?></span></strong>
                                                </div>
                                                <div class="col-md-8 pull-right">
                                                    <?php if ($this->ion_auth->in_group('employer', $user_info->user_id)) { ?>
                                                        <?php echo lang('unavailable_text') ?>
                                                    <? } else { ?>
                                                        <?php echo $user_info->first_name . ' ' . $user_info->last_name ?>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="list-group-item">
                                            <div class="col-md-12 m-b-30">
                                                <div class="col-md-4 pull-left">
                                                    <strong><i class="fa fa-briefcase margin-r-5"></i><span
                                                                class="m-l-5"><?php echo lang('about_section_company_text') ?></span></strong>
                                                </div>
                                                <div class="col-md-8 pull-right">
                                                    <?php

                                                    if ($user_info->company != '') {
                                                        echo $user_info->company;
                                                    } else {
                                                        echo lang('not_found_no_company_text');
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="list-group-item">
                                            <div class="col-md-12 m-b-30">
                                                <div class="col-md-4 pull-left">
                                                    <strong><i class="fa fa-clock-o margin-r-5"></i><span
                                                                class="m-l-5"><?php echo lang('about_section_dob_text') ?></span></strong>
                                                </div>
                                                <div class="col-md-8 pull-right">
                                                    <?php echo $user_info->user_dob_datestring; ?>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="list-group-item">
                                            <div class="col-md-12 m-b-30">
                                                <div class="col-md-4 pull-left">
                                                    <strong><i class="fa fa-envelope-square margin-r-5"></i><span
                                                                class="m-l-5"><?php echo lang('about_section_email_text') ?></span></strong>
                                                </div>
                                                <div class="col-md-8 pull-right">
                                                    <?php
                                                    if ($user_info->email != '') {
                                                        echo $user_info->email;
                                                    } else {
                                                        echo lang('not_found_no_email_text');
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="list-group-item">
                                            <div class="col-md-12 m-b-30">
                                                <div class="col-md-4 pull-left">
                                                    <strong><i class="fa fa-envelope-square margin-r-5"></i><span
                                                                class="m-l-5"><?php echo lang('additional_email_text') ?></span></strong>
                                                </div>
                                                <div class="col-md-8 pull-right">
                                                    <?php
                                                    if ($user_info->user_additional_email != '') {
                                                        echo $user_info->user_additional_email;
                                                    } else {
                                                        echo lang('not_found_no_email_text');
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </li>
                                        <?php if ($this->ion_auth->in_group('employer', $user_id)) { ?>
                                            <li class="list-group-item">
                                                <div class="col-md-12 m-b-30">
                                                    <div class="col-md-4 pull-left">
                                                        <strong><i class="fa fa-globe margin-r-5"></i><span
                                                                    class="m-l-5"><?php echo lang('subdomain_text') ?></span></strong>
                                                    </div>
                                                    <div class="col-md-8 pull-right">
                                                        <?php
                                                        if ($user_info->subdomain) {
                                                            echo $user_info->subdomain;
                                                        } else {
                                                            echo lang('not_found_no_subdomain_text');
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            </li>
                                        <?php } ?>
                                        <li class="list-group-item">
                                            <div class="col-md-12 m-b-30">
                                                <div class="col-md-4 pull-left">
                                                    <strong><i class="fa fa-mobile-phone margin-r-5"></i><span
                                                                class="m-l-5"><?php echo lang('about_section_phone_text') ?></span></strong>
                                                </div>
                                                <div class="col-md-8 pull-right">
                                                    <?php
                                                    if ($user_info->phone != '') {
                                                        echo $user_info->phone;
                                                    } else {
                                                        echo lang('not_found_no_phone_number_text');
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="list-group-item">
                                            <div class="col-md-12 m-b-30">
                                                <div class="col-md-4 pull-left">
                                                    <strong><i class="fa fa-mobile-phone margin-r-5"></i><span
                                                                class="m-l-5"><?php echo lang('additional_phone_text') ?></span></strong>
                                                </div>
                                                <div class="col-md-8 pull-right">
                                                    <?php
                                                    if ($user_info->user_additional_phone != '') {
                                                        echo $user_info->user_additional_phone;
                                                    } else {
                                                        echo lang('not_found_no_phone_number_text');
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>

                                <?php if ($is_own_profile == 'own_profile' || $is_admin == 'admin' || $is_viewer_user_employer == 'user_employer') { ?>
                                    <br>
                                    <div class="card">
                                        <div class="card-header">
                                            <div class="col-md-12 m-b-30">
                                                <div class="col-md-4 pull-left">
                                                    <strong><i class="fa fa-home margin-r-5"></i><span
                                                                class="m-l-5"><?php echo lang('about_section_address_text'); ?></span></strong>
                                                </div>
                                            </div>
                                        </div>
                                        <ul class="list-group list-group-flush">
                                            <li class="list-group-item">
                                                <div class="col-md-12 m-b-30">
                                                    <div class="col-md-4 pull-left">
                                                        <strong><i class="fa fa-arrow-right"></i><span
                                                                    class="m-l-5"><?php echo lang('about_section_street1_text'); ?></span></strong>
                                                    </div>
                                                    <div class="col-md-8 pull-right">
                                                        <?= $user_info->user_street_1 ?>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="list-group-item">
                                                <div class="col-md-12 m-b-30">
                                                    <div class="col-md-4 pull-left">
                                                        <strong><i class="fa fa-arrow-right"></i><span
                                                                    class="m-l-5"><?php echo lang('about_section_street2_text'); ?></span></strong>
                                                    </div>
                                                    <div class="col-md-8 pull-right">
                                                        <?= $user_info->user_street_2 ?>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="list-group-item">
                                                <div class="col-md-12 m-b-30">
                                                    <div class="col-md-4 pull-left">
                                                        <strong><i class="fa fa-arrow-right"></i><span
                                                                    class="m-l-5"><?php echo lang('about_section_city_text'); ?></span></strong>
                                                    </div>
                                                    <div class="col-md-8 pull-right">
                                                        <?= $user_info->user_city_name ?>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="list-group-item">
                                                <div class="col-md-12 m-b-30">
                                                    <div class="col-md-4 pull-left">
                                                        <strong><i class="fa fa-arrow-right"></i><span
                                                                    class="m-l-5"><?php echo lang('about_section_state_text'); ?></span></strong>
                                                    </div>
                                                    <div class="col-md-8 pull-right">
                                                        <?= $user_info->user_state_name ?>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="list-group-item">
                                                <div class="col-md-12 m-b-30">
                                                    <div class="col-md-4 pull-left">
                                                        <strong><i class="fa fa-arrow-right"></i><span
                                                                    class="m-l-5"><?php echo lang('about_section_country_text'); ?></span></strong>
                                                    </div>
                                                    <div class="col-md-8 pull-right">
                                                        <?= $user_info->user_country_name ?>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                <?php } ?>

                                <?php if (
                                    ($this->ion_auth->in_group('employee', $user_id) && ($is_own_profile == 'own_profile' || $is_admin == 'admin' || $is_viewer_user_employer == 'user_employer'))
                                    ||
                                    $this->ion_auth->in_group('superadmin')
                                ) { ?>
                                    <br>
                                    <div class="card">
                                        <div class="card-header">
                                            <div class="col-md-12 m-b-30">
                                                <div class="col-md-4 pull-left">
                                                    <strong><i class="fa fa-money margin-r-5"></i><span
                                                                class="m-l-5"><?php echo lang('about_section_financial_info_text'); ?></span></strong>
                                                </div>
                                            </div>
                                        </div>
                                        <?php if ($this->ion_auth->in_group('employee', $user_id)) { ?>
                                        <ul class="list-group list-group-flush">
                                            <li class="list-group-item">
                                                <div class="col-md-12 m-b-30">
                                                    <div class="col-md-4 pull-left">
                                                        <strong><i class="fa fa-arrow-right"></i><span
                                                                    class="m-l-5"><?php echo lang('about_employee_id_number_text'); ?></span></strong>
                                                    </div>
                                                    <div class="col-md-8 pull-right">
                                                        <?= $user_info->user_ofc_id != null && $user_info->user_ofc_id != '' ? $user_info->user_ofc_id : lang('unavailable_text') ?>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="list-group-item">
                                                <div class="col-md-12 m-b-30">
                                                    <div class="col-md-4 pull-left">
                                                        <strong><i class="fa fa-arrow-right"></i><span
                                                                    class="m-l-5"><?php echo lang('about_section_bvn_text'); ?></span></strong>
                                                    </div>
                                                    <div class="col-md-8 pull-right">
                                                        <?= $user_info->user_bvn != null && $user_info->user_bvn != '' ? $user_info->user_bvn : lang('unavailable_text') ?>
                                                    </div>
                                                </div>
                                            </li>
                                            <?php } ?>
                                            <li class="list-group-item">
                                                <div class="col-md-12 m-b-30">
                                                    <div class="col-md-4 pull-left">
                                                        <strong><i class="fa fa-arrow-right"></i><span
                                                                    class="m-l-5"><?php echo lang('about_section_bank_text'); ?></span></strong>
                                                    </div>
                                                    <div class="col-md-8 pull-right">
                                                        <?= $user_info->bank_name ?>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="list-group-item">
                                                <div class="col-md-12 m-b-30">
                                                    <div class="col-md-4 pull-left">
                                                        <strong><i class="fa fa-arrow-right"></i><span
                                                                    class="m-l-5"><?php echo lang('about_section_account_number_text'); ?></span></strong>
                                                    </div>
                                                    <div class="col-md-8 pull-right">
                                                        <?= $user_info->user_bank_account_no != null && $user_info->user_bank_account_no != '' ? $user_info->user_bank_account_no : lang('unavailable_text') ?>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                <?php } ?>

                                <?php if (($is_admin == 'admin' && $this->ion_auth->in_group('employee', $user_id)) || $this->ion_auth->in_group('superadmin')) { ?>
                                    <br>
                                    <div class="card">
                                        <div class="card-header">
                                            <div class="col-md-12 m-b-30">
                                                <div class="col-md-4 pull-left">
                                                    <strong><i class="fa fa-credit-card margin-r-5"></i><span
                                                                class="m-l-5"><?php echo lang('about_section_payment_info_text'); ?></span></strong>
                                                </div>
                                            </div>
                                        </div>
                                        <ul class="list-group list-group-flush">
                                            <li class="list-group-item">
                                                <div class="col-md-12 m-b-30">
                                                    <div class="col-md-4 pull-left">
                                                        <strong><i class="fa fa-arrow-right"></i><span
                                                                    class="m-l-5"><?php echo lang('about_section_user_chosen_payment_method_text'); ?></span></strong>
                                                    </div>
                                                    <div class="col-md-8 pull-right">
                                                        <?= $user_info->user_chosen_payment_method != null && $user_info->user_chosen_payment_method != '' ? $user_info->user_chosen_payment_method : lang('unavailable_text') ?>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="list-group-item">
                                                <div class="col-md-12 m-b-30">
                                                    <div class="col-md-4 pull-left">
                                                        <strong><i class="fa fa-arrow-right"></i><span
                                                                    class="m-l-5"><?php echo lang('about_section_paystack_authorization_code_text'); ?></span></strong>
                                                    </div>
                                                    <div class="col-md-8 pull-right">
                                                        <?= $user_info->paystack_authorization_code != null && $user_info->paystack_authorization_code != '' ? $user_info->paystack_authorization_code : lang('unavailable_text') ?>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="list-group-item">
                                                <div class="col-md-12 m-b-30">
                                                    <div class="col-md-4 pull-left">
                                                        <strong><i class="fa fa-arrow-right"></i><span
                                                                    class="m-l-5"><?php echo lang('about_section_paystack_customer_code_text'); ?></span></strong>
                                                    </div>
                                                    <div class="col-md-8 pull-right">
                                                        <?= $user_info->paystack_customer_code != null && $user_info->paystack_customer_code != '' ? $user_info->paystack_customer_code : lang('unavailable_text') ?>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="list-group-item">
                                                <div class="col-md-12 m-b-30">
                                                    <div class="col-md-4 pull-left">
                                                        <strong><i class="fa fa-arrow-right"></i><span
                                                                    class="m-l-5"><?php echo lang('about_section_paystack_recipient_code_text'); ?></span></strong>
                                                    </div>
                                                    <div class="col-md-8 pull-right">
                                                        <?= $user_info->paystack_recipient_code != null && $user_info->paystack_recipient_code != '' ? $user_info->paystack_recipient_code : lang('unavailable_text') ?>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                <?php } ?>
                            </div>

                            <?php if ($user_info->thrift_list) { ?>

                                <!-- <pre>
                                    <?php /*print_r($user_info->thrift_list)*/ ?>
                                </pre>-->
                                <br>
                                <div class="card">
                                    <div class="card-header">
                                        <div class="col-md-12 m-b-30">
                                            <div class="col-md-4 pull-left">
                                                <strong><i class="fa fa-star margin-r-5"></i><span
                                                            class="m-l-5">Thrifts</span></strong>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="box-body table-responsive">
                                            <table id="thrift-table"
                                                   class="table table-bordered table-hover  ">
                                                <thead>
                                                <tr>
                                                    <th>Thrift Group ID</th>
                                                    <th>Product</th>
                                                    <th>Contribution amount</th>
                                                    <th>Completion</th>
                                                    <th>Members</th>
                                                    <th>Status</th>
                                                    <th>Open?</th>
                                                    <th>Started</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                $sign = $this->custom_settings_library->getASettingsValue('çurrency_settings', 'currency_sign');

                                                ?>
                                                <?php foreach ($user_info->thrift_list as $a_thrift) { ?>

                                                    <?php
                                                    $product = $this->Thrift_model->getProduct($a_thrift->thrift_group_product_id);
                                                    $product_name = $product->product_name;
                                                    ?>

                                                    <tr>
                                                        <td>
                                                            <a href="thrift_module/view_thrift/<?= $a_thrift->thrift_group_id ?>"><?= $a_thrift->thrift_group_number ?></a>
                                                        </td>
                                                        <td>
                                                            <?= $product_name ?>
                                                        </td>
                                                        <td>
                                                            <?= !empty($a_thrift->thrift_group_product_price) && is_numeric($a_thrift->thrift_group_product_price) ? $sign . number_format($a_thrift->thrift_group_product_price, 2, '.', ',') : "" ?>
                                                        </td>
                                                        <td data-sort="<?= $a_thrift->thrift_group_current_cycle ?>">
                                                            <?= '(' . $a_thrift->thrift_group_current_cycle . '/' . $a_thrift->thrift_group_term_duration . ')'; ?>
                                                        </td>
                                                        <td data-sort="<?= $a_thrift->thrift_group_member_count ?>">
                                                            <?= '(' . $a_thrift->thrift_group_member_count . '/' . $a_thrift->thrift_group_member_limit . ')'; ?>
                                                        </td>
                                                        <td data-sort="<?= $a_thrift->thrift_group_activation_status ?>">
                                                            <?= $a_thrift->thrift_group_activation_status == 1 ? "Active" : "Inactive" ?>
                                                        </td>
                                                        <td data-sort="<?= $a_thrift->thrift_group_open ?>">
                                                            <?= $a_thrift->thrift_group_open == 1 ? "Open" : "Closed" ?>
                                                        </td>
                                                        <td>
                                                            <?= $a_thrift->thrift_group_start_date == 0 ? "Not started" : $this->custom_datetime_library->convert_and_return_TimestampToDateAndTime($a_thrift->thrift_group_start_date) ?>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                    <!-- /.col -->
                </div>
            </div>
        </div>
    </div>
</div>

<!--this css style is holding datatable inside the box-->
<style>
    table.dataTable thead > tr > th {
        padding-right:.75rem !important;
    }

</style>

<script>


    $(document).ready(function () {
        $('#email_domain_check_switch').on('change', function () {
            if (this.checked) {
                $.post("user_profile_module/change_org_email_domain_check/<?=$user_id;?>", {org_email_domain_check: "1"});
            } else {
                $.post("user_profile_module/change_org_email_domain_check/<?=$user_id;?>", {org_email_domain_check: "0"});
            }

        });


        $('#thrift-table').DataTable();
    })

</script>
