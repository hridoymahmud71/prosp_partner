<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BankController extends MX_Controller
{
    function __construct()
    {
        parent::__construct();

        if ($this->session->userdata('user_id') == null) {
            redirect('');
        }
        //$this->load->model('Contact_model');
        $this->load->model('user/Ion_auth_model');
        $this->load->model('Bank_model');

        // application/settings_module/library/custom_settings_library*
        $this->load->library('settings_module/custom_settings_library');
        $this->load->library('custom_datetime_library');
        // require_once(FCPATH.'external_libraries/excel_reader.php');
        $this->load->model('bank_module/bank_model');
    }

    public function all_bank_info()
    {
        $this->lang->load('all_bank_info');

        $data['all_bank'] = $this->bank_model->get_all_bank_list();

        $this->load->view("common_module/header");
        // $this->load->view("common_module/common_left");
        $this->load->view("all_bank_view_page",$data);
        $this->load->view("common_module/footer");
    }

    public function add_bank_info()
    {
        $this->lang->load('add_bank_info');

        $this->load->view("common_module/header");
        // $this->load->view("common_module/common_left");
        $this->load->view("add_bank_page");
        $this->load->view("common_module/footer");
    }

    public function insert_bank_info()
    {
        $data['bank_name']=$this->input->post('bank_name');
        $data['bank_address']=$this->input->post('bank_address');
        $data['bank_website']=$this->input->post('bank_website');
        $data['bank_email']=$this->input->post('bank_email');
        $data['bank_phone']=$this->input->post('bank_phone');

        $this->form_validation->set_rules('bank_name', 'First name', 'required');
        $this->form_validation->set_rules('bank_address', 'Phone number', 'required');
        $this->form_validation->set_rules('bank_email', 'Office ID', 'required');
        $this->form_validation->set_rules('bank_phone', 'Salary', 'required');;

        if($this->form_validation->run() == FALSE)
        {
            $this->session->set_flashdata('bank_detail_err','employer Name required');
            redirect('bank_module/add_bank_info');
        }

        $this->bank_model->insert_bank_detail($data);
        $this->session->set_flashdata('bank_detail_insert','Bank information Successfully Inserted');
        redirect('bank_module/add_bank_info');
        
    }

    public function edit_bank_info()
    {
        $this->lang->load('edit_bank_info');

        $bank_id = $this->uri->segment(3);
        $data['bank_info'] = $this->bank_model->get_bank_info($bank_id);

        $this->load->view("common_module/header");
        // $this->load->view("common_module/common_left");
        $this->load->view("edit_bank_page",$data);
        $this->load->view("common_module/footer");

    }

    public function update_bank_info()
    {
        $bank_id = $this->input->post('bank_id');

        $data['bank_name']=$this->input->post('bank_name');
        $data['bank_address']=$this->input->post('bank_address');
        $data['bank_website']=$this->input->post('bank_website');
        $data['bank_email']=$this->input->post('bank_email');
        $data['bank_phone']=$this->input->post('bank_phone');

        $this->form_validation->set_rules('bank_name', 'First name', 'required');
        $this->form_validation->set_rules('bank_address', 'Phone number', 'required');
        $this->form_validation->set_rules('bank_email', 'Office ID', 'required');
        $this->form_validation->set_rules('bank_phone', 'Salary', 'required');;

        if($this->form_validation->run() == FALSE)
        {
            $this->session->set_flashdata('bank_detail_err','employer Name required');
            redirect('bank_module/edit_bank_info/'.$bank_id);
        }

        $this->bank_model->update_bank_detail($data, $bank_id);
        $this->session->set_flashdata('bank_detail_update','Bank information Successfully Updated');
        redirect('bank_module/all_bank_info');   
    }

    public function delete_bank_info()
    {
        $bank_id = $this->uri->segment(3);
        $data['bank_deletion_status']=1;
        $this->bank_model->update_bank_status($data,$bank_id);
        $this->session->set_flashdata('delete_bank','Bank Information Successfully Removed');
        redirect('bank_module/all_bank_info');  
    }

    public function all_state_info()
    {
        $this->lang->load('all_state_info');

        $data['state_info'] = $this->bank_model->get_state_info();

        $this->load->view("common_module/header");
        // $this->load->view("common_module/common_left");
        $this->load->view("all_state_info",$data);
        $this->load->view("common_module/footer");
    }

    public function state_status_active()
    {
        $state_id = $this->uri->segment(3);
        $data['state_status']=1;
        $this->bank_model->update_state_status($data,$state_id);
        $this->session->set_flashdata('state_status_change','Status Successfully Changed');
        redirect('bank_module/all_state_info');
    }

    public function state_status_deactive()
    {
        $state_id = $this->uri->segment(3);
        $data['state_status']=0;
        $this->bank_model->update_state_status($data,$state_id);
        $this->session->set_flashdata('state_status_change','Status Successfully Changed');
        redirect('bank_module/all_state_info');
    }

    public function insert_state_info()
    {
        $data['state_name'] = $this->input->post('state_name');
        $data['state_capital_name'] = $this->input->post('state_capital_name');

        $this->form_validation->set_rules('state_name', 'State name', 'required');

        if($this->form_validation->run() == FALSE)
        {
            $this->session->set_flashdata('state_insert_err','State Name required');
            redirect('bank_module/all_state_info');
        }

        $this->bank_model->insert_state_name($data);
        $this->session->set_flashdata('state_inserted','State Successfully Inserted');
        redirect('bank_module/all_state_info');        
    }

    public function update_state_info()
    {
        
    }

    public function delete_state_info()
    {
        $state_id = $this->uri->segment(3);
        $data['state_deletion_status']=1;
        $this->bank_model->update_state_status($data,$state_id);
        $this->session->set_flashdata('delete_state','State Information Successfully Removed');
        redirect('bank_module/all_state_info');  
    }

    public function bank_status_active()
    {
        $bank_id = $this->uri->segment(3);
        $data['bank_active_status']=1;
        $this->bank_model->update_bank_status($data,$bank_id);
        $this->session->set_flashdata('status_change','Status Successfully Changed');
        redirect('bank_module/all_bank_info');
    }

    public function bank_status_deactive()
    {
        $bank_id = $this->uri->segment(3);
        $data['bank_active_status']=0;
        $this->bank_model->update_bank_status($data,$bank_id);
        $this->session->set_flashdata('status_change','Status Successfully Changed');
        redirect('bank_module/all_bank_info');
    }
}