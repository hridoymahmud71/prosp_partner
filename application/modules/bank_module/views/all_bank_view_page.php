
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="page-title-box">
                        <h4 class="page-title float-left">
                            <?php echo lang('page_title_text') ?>
                        </h4>
                        <ol class="breadcrumb float-right">
                            <li class="breadcrumb-item"><a href="product_module/all_product_info"><?php echo lang('breadcrum_home_text') ?></a></li>
                            <li class="breadcrumb-item active"><?php echo lang('breadcrumb_page_add_text') ?></li>
                        </ol>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            <div class="row">
                <div class="col-12">
                    <h4 class="header-title m-t-0 m-b-30"></h4>
                    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 col-xl-12">
                        <div class="page-title-box">
                            <?php if($this->session->flashdata('status_change')){?>
                            <div class="text-center alert alert-success alert-dismissible fade show" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <strong><?php echo lang('status_success_change_text') ?></strong>
                            </div>
                            <?php }?>

                            <?php if($this->session->flashdata('delete_bank')){?>
                            <div class="text-center alert alert-success alert-dismissible fade show" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <strong><?php echo lang('bank_delete_text') ?></strong>
                            </div>
                            <?php }?>

                            <?php if($this->session->flashdata('bank_detail_update')){?>
                            <div class="text-center alert alert-success alert-dismissible fade show" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <strong><?php echo lang('data_update_success_change_text') ?></strong>
                            </div>
                            <?php }?>
                            <h4 class="page-title float-left"><?php echo lang('page_form_title_text')?></h4>

                            <!-- Main content -->
                            <section class="content">
                                <div>
                                    <div class="col-xs-12">
                                        <div class="box box-primary">
                                            <div class="box-body">
                                                <table id="datatable" class="table table-striped table-bordered table-hover table-responsive">
                                                    <thead>
                                                    <tr>
                                                        <th><?=lang('column_bank_name_text');?></th>
                                                        <th><?=lang('column_address_text');?></th>
                                                        <th><?=lang('column_website_text');?></th>
                                                        <th><?=lang('column_email_text');?></th>
                                                        <th><?=lang('column_phone_text');?></th>
                                                        <th><?=lang('column_status_text');?></th>
                                                        <th><?=lang('column_actions_text');?></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php foreach($all_bank as $row){?>
                                                        <tr>
                                                            <td><?=$row->bank_name;?></td>
                                                            <td><?=$row->bank_address;?></td>
                                                            <td><?=$row->bank_website;?></td>
                                                            <td><?=$row->bank_phone;?></td>
                                                            <td><?=$row->bank_email;?></td>
                                                            <td>
                                                                <?php if($row->bank_active_status==1) { ?>
                                                                    <span class="label label-primary"><?php echo "Active" ?></span>
                                                                        &nbsp;
                                                                        <a title="Deactivate" href="bank_module/bank_status_deactive/<?=$row->bank_id;?>">
                                                                            <span class="label label-danger"><i class="fa fa-times" aria-hidden="true"></i></span>
                                                                        </a>
                                                                <?php } else {?>
                                                                 <span class="label label-primary"><?php echo "Inactive" ?></span>
                                                                        &nbsp;
                                                                        <a title="Active" href="bank_module/bank_status_active/<?=$row->bank_id;?>">
                                                                            <span class="label label-danger"><i class="fa fa-check" aria-hidden="true"></i></span>
                                                                        </a>
                                                                <?php }?>
                                                            </td>
                                                            <td>
                                                                <!-- &nbsp;
                                                                <a title="<?php echo lang('tooltip_view_text') ?>" style="color: #2b2b2b"
                                                                   href=""
                                                                   class=""><i class="fa fa-eye fa-lg"
                                                                               aria-hidden="true"></i>
                                                                </a> -->

                                                                &nbsp;
                                                                <a title="<?php echo lang('tooltip_edit_text') ?>" style="color: #2b2b2b"
                                                                   href="bank_module/edit_bank_info/<?=$row->bank_id;?>"
                                                                   class=""><i class="fa fa-pencil-square-o fa-lg"
                                                                               aria-hidden="true"></i>
                                                                </a>
                                                                &nbsp;

                                                                <a title="<?php echo lang('tooltip_delete_text') ?>" id="" style="color: #2b2b2b"
                                                                   href="bank_module/delete_bank_info/<?=$row->bank_id;?>"
                                                                   class="confirmation">
                                                                <i id="remove" class="fa fa-trash-o fa-lg"
                                                                   aria-hidden="true">
                                                                </i>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                        <?php }?>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <!-- /.box-body -->
                                        </div>
                                        <!-- /.box -->
                                    </div>
                                    <!-- /.col -->
                                </div>
                                <!-- /.row -->
                            </section>
                            <!-- /.content -->
                            <div class="clearfix"></div>
                        </div>
                    </div><!-- end col -->
                </div><!-- end col -->
            </div>
            <!-- end row -->
        </div> <!-- container -->




<script type="text/javascript">
    $(document).ready(function() {
        var table = $('#datatable').DataTable({
            scrollablex:true;
        });
    });
</script>



<script>
    $('.confirmation').click(function (e) {
        var href = $(this).attr('href');

        swal({
                title: "<?php echo lang('swal_title')?>",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "<?php echo lang('swal_confirm_button_text')?>",
                cancelButtonText: "<?php echo lang('swal_cancel_button_text')?>",
                closeOnConfirm: true,
                closeOnCancel: true
            },
            function (isConfirm) {
                if (isConfirm) {
                    window.location.href = href;
                }
            });

        return false;
    });
</script>