
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="page-title-box">
                        <h4 class="page-title float-left">
                            <?php echo lang('page_title_text') ?>
                            <!-- <small><?php echo lang('page_subtitle_add_text')?></small> -->
                        </h4>
                        <ol class="breadcrumb float-right">
                            <li class="breadcrumb-item"><a href="employer_module/add_employee_info"><?php echo lang('breadcrum_home_text') ?></a></li>
                            <li class="breadcrumb-item active"><?php echo lang('breadcrumb_page_add_text') ?></li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <?php if($this->session->flashdata('bank_detail_insert')){?>
            <div class="text-center alert alert-success alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <strong><?php echo lang('success_bank_upload_text')?></strong>
            </div>
            <?php }?>

            <?php if($this->session->flashdata('bank_detail_err')){?>
            <div class="text-center alert alert-danger alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <strong><?php echo lang('bank_upload_err_text') ?></strong>
            </div>
            <?php }?>
            <div class="row">
                <div class="col-8">
                    <div class="card-box">
                            <h4 class="header-title m-t-0 m-b-30"><?php echo lang('page_form_title_text') ?></h4>
                        <div class="row">
                            <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 col-xl-12">

                                <form role="form" action="bank_module/insert_bank_info" method="post" data-parsley-validate novalidate>
                                    <div class="form-group row">
                                        <label for="bank_name" class="col-sm-4 form-control-label"><?php echo lang('bank_name_text');?><span class="text-danger">*</span></label>
                                        <div class="col-sm-7">
                                            <input type="text" required parsley-type="bank_name" class="form-control" name="bank_name" 
                                                   id="bank_name" placeholder="<?php echo lang('bank_name_text');?>">
                                            <?php if($this->session->flashdata('bank_detail_err')){?>
                                                <label class="text-danger" for="confirm2"><?php echo lang('field_mandatory_text');?></label>
                                            <?php }?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="bank_email" class="col-sm-4 form-control-label"><?php echo lang('bank_email_text');?><span class="text-danger">*</span></label>
                                        <div class="col-sm-7">
                                            <input type="bank_email" required parsley-type="bank_email" class="form-control" name="bank_email"
                                                   id="bank_email" placeholder="<?php echo lang('bank_email_text');?>">
                                            <?php if($this->session->flashdata('bank_detail_err')){?>
                                                <label class="text-danger" for="confirm2"><?php echo lang('field_mandatory_text');?></label>
                                            <?php }?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="bank_website" class="col-sm-4 form-control-label"><?php echo lang('bank_website_text');?></label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="bank_website"
                                                   id="bank_website" placeholder="<?php echo lang('bank_website_text');?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="bank_phone" class="col-sm-4 form-control-label"><?php echo lang('bank_phone_text');?><span class="text-danger">*</span></label>
                                        <div class="col-sm-7">
                                            <input type="text" required parsley-type="bank_phone" class="form-control" name="bank_phone"
                                                   id="bank_phone" placeholder="<?php echo lang('bank_phone_text');?>">
                                            <?php if($this->session->flashdata('bank_detail_err')){?>
                                                <label class="text-danger" for="confirm2"><?php echo lang('field_mandatory_text');?></label>
                                            <?php }?>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="bank_address" class="col-sm-4 form-control-label"><?php echo lang('bank_address_text');?><span class="text-danger">*</span></label>
                                        <div class="col-sm-7">
                                            <input type="text" required parsley-type="bank_address" class="form-control" name="bank_address"
                                                   id="bank_address" placeholder="<?php echo lang('bank_address_text');?>">
                                            <?php if($this->session->flashdata('bank_detail_err')){?>
                                                <label class="text-danger" for="confirm2"><?php echo lang('field_mandatory_text');?></label>
                                            <?php }?>
                                        </div>
                                    </div>

                                    <div class="form-group text-center m-b-0">
                                        <button class="btn btn-primary waves-effect waves-light" type="submit">
                                            <?php echo lang('file_submit_text');?>
                                        </button>
                                        <button type="reset" class="btn btn-secondary waves-effect m-l-5">
                                            <?php echo lang('modal_cancel_text');?>
                                        </button>
                                    </div>
                                </form>
                            </div><!-- end row -->
                        </div>
                    </div><!-- end col -->
                </div>
            </div>
        </div>
