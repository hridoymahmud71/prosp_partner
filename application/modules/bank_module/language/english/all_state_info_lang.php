<?php

/*page texts*/
$lang['page_title_text'] = 'State Information';
$lang['breadcrumb_home_text'] = 'State';
$lang['breadcrumb_section_text'] = 'All States';
$lang['page_subtitle_text'] = 'Add State Information';
$lang['page_all_subtitle_text'] = 'All State Information';

// BANK COLUMN



// all BANK page

$lang['status_success_change_text'] = 'Status Successfully Changed';
$lang['data_update_success_change_text'] = 'State information Successfully Updated';
$lang['state_delete_text'] = 'State Information Successfully Removed';
$lang['error_state_insert_text'] = 'State Information was not inserted. Input a Valid State Name';

$lang['swal_title'] = 'Are you sure to delete this State Information ?';



// all state page

$lang['label_state_name_text'] = 'State Name';
$lang['label_state_branch_name_text'] = 'State Capital Name';


$lang['label_state_name_text'] = 'State Name';
$lang['input_submit_text'] = 'Submit';

$lang['page_state_name_text']='State name';
$lang['page_state_capital_text']='State Capital';
$lang['page_state_status_text']='Status';
$lang['page_state_action_text']='Action';

$lang['success_state_insert_text']='State information Successfully Inserted';

?>