<?php

$lang['approve_subject_text'] = 'Your account has been approved';
$lang['approve_message_text'] = 'Your account has been approved.Click the link below to login';
$lang['login_text'] = 'Login';

$lang['disapprove_subject_text'] = 'Your account has been disapproved';
$lang['disapprove_message_text'] = 'Your account has been disapproved.';
$lang['disapprove_by_system_message_text'] = 'Your account has been disapproved by the system.';
$lang['disapprove_by_organization_message_text'] = 'Your account has been disapproved by the organization.';

