<?php

$lang['page_title_text'] = 'Contact Person';


$lang['page_subtitle_add_text'] = 'Add Contact Person';
$lang['page_subtitle_edit_text'] = 'Edit Contact Person';

$lang['breadcrum_home_text'] = 'Organization';
$lang['breadcrumb_page_add_text'] = 'New Contact Person';
$lang['breadcrumb_page_edit_text'] = 'Edit Contact Person';

$lang['employee_form_header_text'] = "Contact Person Information";

/*page texts*/
$lang['employer_first_name_text'] = 'First Name';

$lang['employer_last_name_text'] = 'Last Name';

$lang['employer_phone_text'] = 'Phone';

$lang['employer_email_text'] = 'Email';

$lang['employer_website_text'] = 'Website';

$lang['employer_organization_text'] = 'Organization Name';

$lang['file_submit_text'] = 'SUBMIT';
$lang['modal_cancel_text'] = 'RESET';


$lang['edit_contact_person_text'] = 'Edit Contact Person';
$lang['my_contact_person_list_text'] = 'See My Contacts';

/*flash*/
$lang['contact_create_success_text'] = 'Contact Person Created Successfully';
$lang['contact_update_success_text'] = 'Contact Person Updated Successfully';



?>