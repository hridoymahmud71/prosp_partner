<?php

$lang['activation_subject_text'] = 'Your account is activated';
$lang['activation_message_text'] = 'Your account is activated. Click the link below to login';

$lang['deactivation_subject_text'] = 'Your account is deactivated';
$lang['deactivation_message_text'] = 'Your account is deactivated. You would not be able to log in';


$lang['deletion_subject_text'] = 'Your account is deleted';
$lang['deletion_message_text'] = 'Your account is deleted. You would not be able to log in';





