<!-- <div class="content-page"> -->
<!-- Start content -->
<!-- <div class="content"> -->
<div class="container">
    <div class="row">
        <div class="col-xl-12">
            <div class="page-title-box">
                <h4 class="page-title float-left">
                    <?php echo lang('page_title_text') ?>
                </h4>
                <ol class="breadcrumb float-right">
                    <li class="breadcrumb-item"><a
                            href="product_module/all_product_info"><?php echo lang('breadcrumb_home_text') ?></a>
                    </li>
                    <li class="breadcrumb-item active"><?php echo lang('breadcrumb_section_text') ?></li>
                </ol>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>

    <?php if ($this->session->flashdata('success')) { ?>
        <section class="content mt-0">
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert"
                                aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <h4 class="alert-heading"><?php echo lang('successfull_text') ?></h4>
                        <p>
                            <?php
                            if ($this->session->flashdata('add_success')) {
                                echo lang('user_add_success_text');
                            }
                            if ($this->session->flashdata('update_success')) {
                                echo lang('update_success_text');
                            }
                            if ($this->session->flashdata('activate_success')) {
                                echo lang('activate_success_text');
                            }
                            if ($this->session->flashdata('deactivate_success')) {
                                echo lang('dectivate_success_text');
                            }
                            if ($this->session->flashdata('delete_success')) {
                                echo lang('delete_success_text');
                            }
                            if ($this->session->flashdata('employee_insert')) {
                                echo lang('add_employee_text');
                            }

                            if ($this->session->flashdata('password_send_success')) {
                                echo lang('password_send_success_text');
                            }
                            ?>
                        </p>
                        <?php if (!$this->session->flashdata('delete_success')) { ?>
                            <a href="<?php echo base_url()
                                . 'user_profile_module/user_profile_overview/' . $this->session->flashdata('flash_user_id') ?>">
                                <?php echo lang('see_user_text'); ?>
                            </a>
                        <?php } ?>
                        &nbsp;
                        <?php if ($this->session->flashdata('add_success')) { ?>
                            <a href="<?php echo base_url() . 'employer_module/edit_organization_contact_person/' . $this->session->flashdata('flash_user_id') ?>">
                                <?php echo lang('edit_user_text'); ?>
                            </a>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>
    <?php } ?>

    <!-- end row -->
    <div class="row">
        <div class="col-12">
            <h4 class="header-title m-t-0 m-b-30"></h4>
            <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 col-xl-12">
                <div class="page-title-box">
                    <h4 class="page-title float-left">

                        <?php if ($which_list == 'my_employee_info') { ?>
                            <?php echo lang('page_employer_subtitle_text') ?>
                        <?php } elseif ($which_list == 'all_employee_info' || $which_list == 'employers_employee_info') { ?>
                            <?php echo lang('page_admin_subtitle_text') ?>
                        <?php } ?>

                    </h4>
                    <?php if ($this->ion_auth->is_admin() || $this->ion_auth->in_group('employer')) { ?>
                        <ol class="breadcrumb float-right">
                            <a class="btn btn-primary"
                               href="employer_module/add_organization_contact_person"><?php echo lang('add_button_text') ?>
                                &nbsp;<span class="icon"><i class="fa fa-plus"></i></span>
                            </a>
                        </ol>
                    <?php } ?>

                    <!-- Main content -->
                    <section class="content">
                        <div>
                            <div class="col-xs-12">
                                <div class="box box-primary">
                                    <div class="box-header">
                                        <div>
                                            <table onkeyup="get_details()"
                                                   style="width: 67%; margin: 0 auto 2em auto;margin-top: 10%;"
                                                   cellspacing="1" cellpadding="3" border="0">
                                                <tbody>
                                                <tr>
                                                    <td align="center">
                                                        <label><?php echo lang('column_firstname_text') ?></label>
                                                    </td>
                                                    <td align="center">
                                                        <input class="column_filter form-control" id="fname"
                                                               type="text">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center">
                                                        <label for=""><?php echo lang('column_lastname_text') ?></label>
                                                    </td>
                                                    <td align="center">
                                                        <input class="column_filter form-control" id="lname"
                                                               type="text">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center">
                                                        <label for=""><?php echo lang('column_email_text') ?></label>
                                                    </td>
                                                    <td align="center">
                                                        <input class="column_filter form-control" id="email"
                                                               type="text">
                                                    </td>
                                                </tr>
                                                <?php if ($this->ion_auth->is_admin()) { ?>
                                                    <tr>
                                                        <td align="center">
                                                            <label for=""><?php echo lang('column_organization_text') ?></label>
                                                        </td>
                                                        <td align="center">
                                                            <input type="hidden" name="employer" id="employer">
                                                            <select style="width: 100%;"
                                                                    class="form-control select2 myselect">
                                                                <option value=""><?php echo lang('column_org_select_text'); ?></option>
                                                                <?php foreach ($all_info as $row) { ?>
                                                                    <option value="<?= $row->id ?>"><?= $row->company; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                                <tr>
                                                    <td align="center">
                                                        <label for=""><?php echo lang('column_status_text') ?></label>
                                                    </td>
                                                    <td align="center">
                                                        <input type="hidden" name="status_chk" id="status_chk">
                                                        <select class="form-control" id="get_status">
                                                            <option value=""><?php echo lang('option_all_text') ?></option>
                                                            <option value="1"><?php echo lang('option_active_text') ?></option>
                                                            <option value="0"><?php echo lang('option_inactive_text') ?></option>
                                                        </select>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <!-- /.box-header -->
                                    <div class="box-body">
                                        <!-- <table id="user-table" class="table table-bordered table-hover table-responsive ">
                                                    <thead>
                                                    <tr>
                                                        <th><?php echo lang('column_firstname_text') ?></th>
                                                        <th><?php echo lang('column_lastname_text') ?></th>
                                                        <th><?php echo lang('column_email_text') ?></th>
                                                        <th><?php echo lang('column_status_text') ?></th>
                                                        <th><?php echo lang('column_created_on_text') ?></th>
                                                        <th><?php echo lang('column_actions_text') ?></th>
                                                    </tr>
                                                    </thead>
                                                </table> -->
                                        <table id="datatable"
                                               class="table table-striped table-bordered table-hover table-responsive">
                                            <thead>
                                            <tr>
                                                <th><?= lang('column_member_id_text'); ?></th>
                                                <th><?= lang('column_firstname_text'); ?></th>
                                                <th><?= lang('column_lastname_text'); ?></th>
                                                <th><?= lang('column_email_text'); ?></th>
                                                <th><?= lang('column_organization_text'); ?></th>
                                                <th><?= lang('column_status_text'); ?></th>
                                                <!-- <th><?= lang('column_created_on_text'); ?></th> -->
                                                <th><?= lang('column_actions_text'); ?></th>
                                            </tr>
                                            </thead>
                                            <tbody id="tr_data"></tbody>
                                        </table>
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                                <!-- /.box -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </section>
                    <!-- /.content -->
                    <div class="clearfix"></div>
                </div>
            </div><!-- end col -->
        </div><!-- end col -->
    </div>
    <!-- end row -->
</div> <!-- container -->
<!-- </div> content -->
<!-- </div> -->

<!--clearing the extra arrow-->
<style>
    table.dataTable thead .sorting:before, table.dataTable thead .sorting_asc:before, table.dataTable thead .sorting_desc:before, table.dataTable thead .sorting_asc_disabled:before, table.dataTable thead .sorting_desc_disabled:before {
        right: unset;
    }
</style>


<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#datatable').DataTable();
        $('.myselect').select2();
        $('.myselect').on("select2:select", function (e) {
            var value = $(e.currentTarget).find("option:selected").val();
            $('#employer').val(value);
            get_details();
        });
        $("#get_status").change(function () {
            var value = $(this).val();
            $('#status_chk').val(value);
            get_details();
        });
        table.clear().draw();
        get_details();
    });

    function get_details() {
        var fname = $('#fname').val();
        fname = fname.trim();
        var lname = $('#lname').val();
        lname = lname.trim();
        var email = $('#email').val();
        email = email.trim();
        var employer = $('#employer').val();
        var status_chk = $('#status_chk').val();
        // var path_info = "<?php echo base_url() . 'employer_module/get_employee_by_ajax'?>"
        $.ajax({
            url: "<?php echo base_url() . 'employer_module/get_contact_list_by_ajax' ?>",
            data: {
                fname: fname,
                lname: lname,
                email: email,
                employer: employer,
                status_chk: status_chk
            },
            type: "POST",
            datatype: "JSON",
            success: function (data) {


                var data_table = $('#datatable').DataTable();
                var all_data = JSON.parse(data);

                data_table.clear().draw();
                $.each(all_data, function (i, item) {
                    getConfirm();

                    if (item.active == 1) {
                        var status = 'Active';
                        var anchor_detail = '</span><?php if($this->ion_auth->is_admin() || $this->ion_auth->in_group('employer')){?>&nbsp;&nbsp;<a href="<?php echo base_url();?>employer_module/deactivate_contact/' + item.id + '"><span class="label label-danger"><i class="fa fa-times" aria-hidden="true"></i></span></a><?php }?>';
                    }
                    if (item.active == 0) {
                        var status = 'Inactive';
                        var anchor_detail = '</span><?php if($this->ion_auth->is_admin() || $this->ion_auth->in_group('employer')){?>&nbsp;&nbsp;<a href="<?php echo base_url();?>employer_module/activate_contact/' + item.id + '"><span class="label label-success"><i class="fa fa-check" aria-hidden="true"></i></span></a><?php }?>';
                    }


                    data_table.row.add([
                        '<td>' + item.mem_id_num + '</td>',
                        '<td>' + item.first_name + '</td>',
                        '<td>' + item.last_name + '</td>',
                        '<td>' + item.email + '</td>',
                        '<td>' + item.company + '</td>',
                        '<td><span class="label label-primary">' + status +anchor_detail+ '</td>',
                        // '<td>'+item.created_on+'</td>',
                        '<td>' +
                        '<a title="<?php echo $this->lang->line('tooltip_view_text');?>" style="color:#2b2b2b;" href="<?php echo base_url();?>user_profile_module/user_profile_overview/' + item.id + '"><i class="fa fa-eye fa-lg" aria-hidden="true"></i></a>&nbsp;&nbsp;' +
                        '<a title="<?php echo $this->lang->line('tooltip_edit_text');?>" style="color:#2b2b2b;" href="<?php echo base_url();?>employer_module/edit_organization_contact_person/' + item.id + '"><i class="fa fa-pencil-square-o fa-lg" aria-hidden="true"></i></a>&nbsp;&nbsp;' +
                        <?php if($this->ion_auth->is_admin() || $this->ion_auth->in_group('employer')) {?>
                        '<a class="delete_confirmation" title="<?php echo $this->lang->line('tooltip_send_password_text');?>" style="color:#2b2b2b;" href="<?php echo base_url();?>employer_module/delete_contact/' + item.id + '"><i class="fa fa-trash-o fa-lg" aria-hidden="true"></i></a>' +
                        <?php } ?>
                        '</td>'
                    ]).draw();
                });
            }
        });

    }


</script>


<!--this css style is holding datatable inside the box-->
<style>
    #user-table {
        table-layout: fixed;
        width: 100% !important;
    }

    #user-table td,
    #user-table th {
        width: auto !important;
        white-space: normal;
        text-overflow: ellipsis;
        overflow: hidden;
    }
</style>


<script>
    function getConfirm() {
        $('.delete_confirmation').click(function (e) {

            e.preventDefault();

            var href = $(this).attr('href');

            swal({
                    title: "<?= lang('swal_delete_title_text')?>",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "<?= lang('swal_delete_confirm_button_text')?>",
                    cancelButtonText: "<?= lang('swal_delete_cancel_button_text')?>",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        window.location.href = href;
                    }
                });

            return false;
        });


        $('.password_change_confirmation').click(function (e) {

            e.preventDefault();

            var href = $(this).attr('href');

            swal({
                    title: "<?= lang('swal_password_title_text')?>",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "<?= lang('swal_password_confirm_button_text')?>",
                    cancelButtonText: "<?= lang('swal_password_cancel_button_text')?>",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        window.location.href = href;
                    }
                });

            return false;
        });

    }


</script>


