<!-- Content Wrapper. Contains page content -->
<!-- <div class="content-page"> -->
<!-- Start content -->
<!-- <div class="content"> -->
<div class="container">
    <div class="row">
        <div class="col-xl-12">
            <div class="page-title-box">
                <h4 class="page-title float-left">
                    <?php echo lang('page_title_text') ?>
                    <!-- <small><?php echo lang('page_subtitle_add_text') ?></small> -->
                </h4>
                <ol class="breadcrumb float-right">
                    <li class="breadcrumb-item"><a
                                href="employer_module/add_employee_info"><?php echo lang('breadcrum_home_text') ?></a>
                    </li>
                    <li class="breadcrumb-item active"><?php echo lang('breadcrumb_page_add_text') ?></li>
                </ol>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <?php if ($this->session->flashdata('employer_update')) { ?>
                    <div class="text-center alert alert-success alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <strong><?php echo lang('employer_success_update_text'); ?></strong>
                    </div>
                <?php } ?>

                <?php if ($this->session->flashdata('employer_update_error')) { ?>
                    <div class="text-center alert alert-danger alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <strong><?php echo lang('employer_update_error_text'); ?></strong>
                    </div>
                <?php } ?>
                <?php if ($this->session->flashdata('subdomain_error')) { ?>
                    <div class="text-center alert alert-danger alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <strong><?php echo lang('subdomain_error_text'); ?></strong>
                    </div>
                <?php } ?>

                <h4 class="header-title m-t-0 m-b-30"><?php echo lang('employee_add_form_header_text') ?></h4>
                <div class="row">
                    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 col-xl-8">
                        <form role="form" action="employer_module/update_employer_info" method="post"
                              data-parsley-validate novalidate>
                            <div class="form-group row">
                                <label for="company"
                                       class="col-sm-4 form-control-label"><?php echo lang('employer_company_text'); ?>
                                    <span class="text-danger">*</span></label>
                                <div class="col-sm-7">
                                    <input type="text" required parsley-type="company" class="form-control"
                                           name="company" value="<?= $emp_info[0]->company; ?>"
                                           id="company" placeholder="<?php echo lang('employer_company_text'); ?>">
                                </div>
                            </div>
                            <!--<div class="form-group row">
                                        <label for="first_name" class="col-sm-4 form-control-label"><?php /*echo lang('employer_first_name_text');*/ ?><span class="text-danger">*</span></label>
                                        <div class="col-sm-7">
                                            <input type="text" required parsley-type="first_name" class="form-control" name="first_name" value="<? /*=$emp_info[0]->first_name;*/ ?>"
                                                   id="first_name" placeholder="<?php /*echo lang('employer_first_name_text');*/ ?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="last_name" class="col-sm-4 form-control-label"><?php /*echo lang('employer_last_name_text');*/ ?></label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="last_name" value="<? /*=$emp_info[0]->last_name;*/ ?>"
                                                   id="last_name" placeholder="<?php /*echo lang('employer_last_name_text');*/ ?>">
                                        </div>
                                    </div>-->

                            <div class="form-group row">
                                <label for="phone"
                                       class="col-sm-4 form-control-label"><?php echo lang('employer_phone_text'); ?>
                                    <span class="text-danger">*</span></label>
                                <div class="col-sm-7">
                                    <input type="text" required parsley-type="phone" class="form-control" name="phone"
                                           value="<?= $emp_info[0]->phone; ?>"
                                           id="phone" placeholder="<?php echo lang('employer_phone_text'); ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="email"
                                       class="col-sm-4 form-control-label"><?php echo lang('employer_email_text'); ?></label>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" value="<?= $emp_info[0]->email; ?>"
                                           id="email" placeholder="<?php echo lang('employer_email_text'); ?>" readonly>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="user_website"
                                       class="col-sm-4 form-control-label"><?php echo lang('employer_website_text'); ?></label>
                                <div class="col-sm-7">
                                    <input type="text" parsley-type="user_website" class="form-control"
                                           name="user_website" value="<?= $emp_info[0]->user_website; ?>"
                                           id="user_website" placeholder="<?php echo lang('employer_website_text'); ?>">
                                </div>
                            </div>

                            <?php if ($this->ion_auth->in_group('admin') && !$this->ion_auth->in_group('analyst')) {
                                $subdomain_readonly = "";
                            } else {
                                $subdomain_readonly = " readonly  ";
                            }

                            ?>
                            <div class="form-group row">
                                <label for="user_website"
                                       class="col-sm-4 form-control-label"><?php echo lang('employer_subdomain_text'); ?></label>
                                <div class="col-sm-7">
                                    <input type="text" parsley-type="user_website"
                                           class="form-control" <?= $subdomain_readonly ?>
                                           name="subdomain" value="<?= $emp_info[0]->subdomain; ?>"
                                           id="subdomain"
                                           placeholder="<?php echo lang('employer_subdomain_text'); ?>">
                                </div>
                            </div>


                            <input type="hidden" name="id" value="<?= $emp_info[0]->id; ?>">

                            <div class="form-group text-center m-b-0">
                                <button class="btn btn-primary waves-effect waves-light" type="submit">
                                    <?php echo lang('file_submit_text'); ?>
                                </button>
                                <button type="reset" class="btn btn-secondary waves-effect m-l-5">
                                    <?php echo lang('modal_cancel_text'); ?>
                                </button>
                            </div>
                        </form>
                    </div><!-- end col -->
                </div><!-- end row -->
            </div>
        </div><!-- end col -->
    </div>
</div>
<!-- </div> -->
<!-- </div> -->

