<!-- <div class="content-page"> -->
<!-- Start content -->
<!-- <div class="content"> -->
<div class="container">
    <div class="row">
        <div class="col-xl-12">
            <div class="page-title-box">
                <h4 class="page-title float-left">
                    <?php echo lang('page_title_text') ?>
                </h4>
                <ol class="breadcrumb float-right">
                    <li class="breadcrumb-item"><a
                                href="product_module/all_product_info"><?php echo lang('breadcrumb_home_text') ?></a>
                    </li>
                    <li class="breadcrumb-item"><a
                                href="users/auth/show_user_groups"><?php echo lang('breadcrumb_section_text') ?></a>
                    </li>
                    <li class="breadcrumb-item active"><?php echo lang('breadcrumb_page_text') ?></li>
                </ol>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!-- end row -->
    <div class="row">
        <div class="col-12">
            <h4 class="header-title m-t-0 m-b-30"></h4>
            <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 col-xl-12">
                <div class="page-title-box">
                    <?php if ($this->session->flashdata('group_add_success')) { ?>
                        <br>
                        <div class="col-md-6">
                            <div class="panel panel-success copyright-wrap" id="add-success-panel">
                                <div class="panel-heading"><?php echo lang('successfull_text') ?>
                                    <button type="button" class="close" data-target="#add-success-panel"
                                            data-dismiss="alert"><span
                                                aria-hidden="true">&times;</span><span class="sr-only">Close</span>

                                    </button>
                                </div>
                                <div class="panel-body"><?php echo lang('add_successfull_text') ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <?php if ($this->session->flashdata('group_update_success')) { ?>
                        <br>
                        <div class="col-md-6">
                            <div class="panel panel-success copyright-wrap" id="update-success-panel">
                                <div class="panel-heading"><?php echo lang('successfull_text') ?>
                                    <button type="button" class="close" data-target="#update-success-panel"
                                            data-dismiss="alert"><span
                                                aria-hidden="true">&times;</span><span class="sr-only">Close</span>

                                    </button>
                                </div>
                                <div class="panel-body"><?php echo lang('update_successfull_text') ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <!-- Main content -->
                    <section class="content">
                        <div class="row">

                            <div class="col-lg-offset-2 col-lg-8 col-md-offset-2 col-md-8">
                                <!-- general form elements -->
                                <div class="box box-primary">
                                    <div class="box-header with-border">
                                        <h5 class="box-title"><?php echo lang('page_subtitle_text') ?></h5>

                                        <div class=" col-md-offset-2 col-md-8" style="color: darkred;font-size: larger">
                                            <?php if ($this->session->flashdata('validation_errors')) {
                                                echo $this->session->flashdata('validation_errors');
                                            }
                                            ?>
                                            <?php if ($this->session->flashdata('noting_to_update')) {
                                                echo $this->session->flashdata('noting_to_update');
                                            }
                                            ?>
                                        </div>
                                        <div class="col-md-2"></div>

                                        <div class=" col-md-offset-2 col-md-12"
                                             style="color: darkgreen;font-size: larger">

                                            <?php if ($this->session->flashdata('update_success')) { ?>
                                                <div class="text-center alert alert-success alert-dismissible fade show"
                                                     role="alert">
                                                    <button type="button" class="close" data-dismiss="alert"
                                                            aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                    </button><?php echo lang('update_success_text') ?>
                                                </div>
                                            <?php } ?>
                                        </div>
                                        <div class="col-md-2"></div>
                                    </div>
                                    <!-- /.box-header -->
                                    <!-- form start -->
                                    <!-- form start -->
                                    <form action="<?php echo base_url() . 'settings_module/update_prosperisgold_settings' ?>"
                                          role="form"
                                          id="" method="post" enctype="multipart/form-data">
                                        <div class="box-body">

                                            <hr>
                                            <div style="font-size: larger;color: #2b2b2b">
                                                <?php echo lang('mailchimp_separator_lang') ?>
                                            </div>
                                            <hr>

                                            <div class="form-group">
                                                <label for="mailchimp_api_key"><?php echo lang('label_mailchimp_api_key_text') ?>
                                                </label>

                                                <input type="text" name="mailchimp_api_key" class="form-control"
                                                       id="mailchimp_api_key"
                                                       value="<?php
                                                       if ($all_prosperisgold_settings) {
                                                           foreach ($all_prosperisgold_settings as $a_prosperisgold_settings) {
                                                               if (($a_prosperisgold_settings->settings_key) == 'mailchimp_api_key')
                                                                   echo $a_prosperisgold_settings->settings_value;
                                                           }
                                                       }
                                                       ?>"
                                                       placeholder="<?php echo lang('placeholder_mailchimp_api_key_text') ?>">
                                            </div>
                                            <div class="form-group">
                                                <label for="thrift_percentage"><?php echo lang('label_mailchimp_thrifter_list_name_text') ?>
                                                </label>

                                                <input type="text" name="mailchimp_thrifter_list_name" class="form-control"
                                                       id="mailchimp_thrifter_list_name"
                                                       value="<?php
                                                       if ($all_prosperisgold_settings) {
                                                           foreach ($all_prosperisgold_settings as $a_prosperisgold_settings) {
                                                               if (($a_prosperisgold_settings->settings_key) == 'mailchimp_thrifter_list_name')
                                                                   echo $a_prosperisgold_settings->settings_value;
                                                           }
                                                       }
                                                       ?>"
                                                       placeholder="<?php echo lang('placeholder_mailchimp_thrifter_list_name_text') ?>">
                                            </div>

                                            <br>
                                            <hr>
                                            <div style="font-size: larger;color: #2b2b2b">
                                                <?php echo lang('thrift_separator_lang') ?>
                                            </div>
                                            <hr>
                                            <div class="form-group">
                                                <label for="thrift_percentage"><?php echo lang('label_stop_new_thrift_text') ?>
                                                    &nbsp;
                                                    <span>
                                                                <i class="fa fa-question-circle" aria-hidden="true"
                                                                   data-toggle="tooltip" data-placement="top"
                                                                   title="<?= lang('tooltip_stop_new_thrift_text') ?>"></i>
                                                            </span>
                                                </label>
                                                <div>
                                                    <input type="checkbox" name="stop_new_thrift" class=""
                                                           id="stop_new_thrift"
                                                        <?php
                                                        if ($all_prosperisgold_settings) {
                                                            foreach ($all_prosperisgold_settings as $a_prosperisgold_settings) {
                                                                if (($a_prosperisgold_settings->settings_key) == 'stop_new_thrift') {
                                                                    if ($a_prosperisgold_settings->settings_value == 'stop') {
                                                                        echo ' checked="checked" ';
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        ?>>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="thrift_warning_1_title"><?php echo lang('label_thrift_warning_1_title_text') ?>
                                                    &nbsp;
                                                            <span>
                                                                <i class="fa fa-question-circle" aria-hidden="true"
                                                                   data-toggle="tooltip" data-placement="top"
                                                                   title="<?= lang('tooltip_thrift_warning_1_title_text') ?>">

                                                                </i>
                                                            </span>
                                                </label>

                                                <input type="text" name="thrift_warning_1_title" class="form-control"
                                                       id="thrift_warning_1_title"
                                                       value="<?php
                                                       if ($all_prosperisgold_settings) {
                                                           foreach ($all_prosperisgold_settings as $a_prosperisgold_settings) {
                                                               if (($a_prosperisgold_settings->settings_key) == 'thrift_warning_1_title')
                                                                   echo $a_prosperisgold_settings->settings_value;
                                                           }
                                                       }
                                                       ?>"
                                                       placeholder="<?php echo lang('placeholder_thrift_warning_1_title_text') ?>">
                                            </div>
                                            <div class="form-group">
                                                <label for="thrift_warning_1_message"><?php echo lang('label_thrift_warning_1_message_text') ?>
                                                    &nbsp;
                                                    <span>
                                                                <i class="fa fa-question-circle" aria-hidden="true"
                                                                   data-toggle="tooltip" data-placement="top"
                                                                   title="<?= lang('tooltip_thrift_warning_1_message_text') ?>">

                                                                </i>
                                                            </span>
                                                </label>

                                                <input type="text" name="thrift_warning_1_message" class="form-control"
                                                       id="thrift_warning_1_message"
                                                       value="<?php
                                                       if ($all_prosperisgold_settings) {
                                                           foreach ($all_prosperisgold_settings as $a_prosperisgold_settings) {
                                                               if (($a_prosperisgold_settings->settings_key) == 'thrift_warning_1_message')
                                                                   echo $a_prosperisgold_settings->settings_value;
                                                           }
                                                       }
                                                       ?>"
                                                       placeholder="<?php echo lang('placeholder_thrift_warning_1_title_message') ?>">
                                            </div>
                                            <div class="form-group">
                                                <label for="paystack_fees"><?php echo lang('label_paystack_fees_text') ?>
                                                    &nbsp;
                                                    <span>
                                                                <i class="fa fa-question-circle" aria-hidden="true"
                                                                   data-toggle="tooltip" data-placement="top"
                                                                   title="<?= lang('tooltip_paystack_fees_text') ?>"></i>
                                                            </span>
                                                </label>

                                                <input type="text" name="paystack_fees" class="form-control"
                                                       id="paystack_fees"
                                                       value="<?php
                                                       if ($all_prosperisgold_settings) {
                                                           foreach ($all_prosperisgold_settings as $a_prosperisgold_settings) {
                                                               if (($a_prosperisgold_settings->settings_key) == 'paystack_fees')
                                                                   echo $a_prosperisgold_settings->settings_value;
                                                           }
                                                       }
                                                       ?>"
                                                       placeholder="<?php echo lang('placeholder_paystack_fees_text') ?>">
                                            </div>

                                            <div class="form-group">
                                                <label for="prosperisgold_loan_fees"><?php echo lang('label_prosperisgold_loan_fees_text') ?>
                                                    &nbsp;
                                                    <span>
                                                                <i class="fa fa-question-circle" aria-hidden="true"
                                                                   data-toggle="tooltip" data-placement="top"
                                                                   title="<?= lang('tooltip_prosperisgold_loan_fees_text') ?>"></i>
                                                            </span>
                                                </label>

                                                <input type="text" name="prosperisgold_loan_fees" class="form-control"
                                                       id="prosperisgold_loan_fees"
                                                       value="<?php
                                                       if ($all_prosperisgold_settings) {
                                                           foreach ($all_prosperisgold_settings as $a_prosperisgold_settings) {
                                                               if (($a_prosperisgold_settings->settings_key) == 'prosperisgold_loan_fees')
                                                                   echo $a_prosperisgold_settings->settings_value;
                                                           }
                                                       }
                                                       ?>"
                                                       placeholder="<?php echo lang('placeholder_prosperisgold_loan_fees_text') ?>">
                                            </div>

                                            <div class="form-group">
                                                <label for="thrift_percentage"><?php echo lang('label_thrift_percentage_text') ?>
                                                    &nbsp;
                                                    <span>
                                                                <i class="fa fa-question-circle" aria-hidden="true"
                                                                   data-toggle="tooltip" data-placement="top"
                                                                   title="<?= lang('tooltip_thrift_percentage_text') ?>"></i>
                                                            </span>
                                                </label>

                                                <input type="text" name="thrift_percentage" class="form-control"
                                                       id="thrift_percentage"
                                                       value="<?php
                                                       if ($all_prosperisgold_settings) {
                                                           foreach ($all_prosperisgold_settings as $a_prosperisgold_settings) {
                                                               if (($a_prosperisgold_settings->settings_key) == 'thrift_percentage')
                                                                   echo $a_prosperisgold_settings->settings_value;
                                                           }
                                                       }
                                                       ?>"
                                                       placeholder="<?php echo lang('placeholder_thrift_percentage_text') ?>">
                                            </div>

                                            <div class="form-group">
                                                <label for="thrift_percentage"><?php echo lang('label_allow_inter_org_thrift_text') ?>
                                                    &nbsp;
                                                    <span>
                                                                <i class="fa fa-question-circle" aria-hidden="true"
                                                                   data-toggle="tooltip" data-placement="top"
                                                                   title="<?= lang('tooltip_allow_inter_org_thrift_text') ?>"></i>
                                                            </span>
                                                </label>
                                                <div>
                                                    <input type="checkbox" name="allow_inter_org_thrift" class=""
                                                           id="allow_inter_org_thrift"
                                                        <?php
                                                        if ($all_prosperisgold_settings) {
                                                            foreach ($all_prosperisgold_settings as $a_prosperisgold_settings) {
                                                                if (($a_prosperisgold_settings->settings_key) == 'allow_inter_org_thrift') {
                                                                    if ($a_prosperisgold_settings->settings_value == 'allowed') {
                                                                        echo ' checked="checked" ';
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        ?>>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="thrift_percentage"><?php echo lang('label_auto_approve_thrifter_account_text') ?>
                                                    &nbsp;
                                                    <span>
                                                                <i class="fa fa-question-circle" aria-hidden="true"
                                                                   data-toggle="tooltip" data-placement="top"
                                                                   title="<?= lang('tooltip_auto_approve_thrifter_account_text') ?>"></i>
                                                            </span>
                                                </label>
                                                <div>
                                                    <input type="checkbox" name="auto_approve_thrifter_account" class=""
                                                           id="auto_approve_thrifter_account"
                                                        <?php
                                                        if ($all_prosperisgold_settings) {
                                                            foreach ($all_prosperisgold_settings as $a_prosperisgold_settings) {
                                                                if (($a_prosperisgold_settings->settings_key) == 'auto_approve_thrifter_account') {
                                                                    if ($a_prosperisgold_settings->settings_value == 'on') {
                                                                        echo ' checked="checked" ';
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        ?>>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="custom_thrift_start_delay"><?php echo lang('label_custom_thrift_start_delay_text') ?>
                                                    &nbsp;
                                                    <span>
                                                                <i class="fa fa-question-circle" aria-hidden="true"
                                                                   data-toggle="tooltip" data-placement="top"
                                                                   title="<?= lang('tooltip_custom_thrift_start_delay_text') ?>"></i>
                                                            </span>
                                                </label>

                                                <input type="text" name="custom_thrift_start_delay" class="form-control"
                                                       id="custom_thrift_start_delay"
                                                       value="<?php
                                                       if ($all_prosperisgold_settings) {
                                                           foreach ($all_prosperisgold_settings as $a_prosperisgold_settings) {
                                                               if (($a_prosperisgold_settings->settings_key) == 'custom_thrift_start_delay')
                                                                   echo $a_prosperisgold_settings->settings_value;
                                                           }
                                                       }
                                                       ?>"
                                                       placeholder="<?php echo lang('placeholder_custom_thrift_start_delay_text') ?>">
                                            </div>

                                            <div class="form-group">
                                                <label for="custom_thrift_max_start_time_from_delay"><?php echo lang('label_custom_thrift_max_start_time_from_delay_text') ?>
                                                    &nbsp;
                                                    <span>
                                                                <i class="fa fa-question-circle" aria-hidden="true"
                                                                   data-toggle="tooltip" data-placement="top"
                                                                   title="<?= lang('tooltip_custom_thrift_max_start_time_from_delay_text') ?>"></i>
                                                            </span>
                                                </label>

                                                <input type="text" name="custom_thrift_max_start_time_from_delay"
                                                       class="form-control"
                                                       id="custom_thrift_max_start_time_from_delay"
                                                       value="<?php
                                                       if ($all_prosperisgold_settings) {
                                                           foreach ($all_prosperisgold_settings as $a_prosperisgold_settings) {
                                                               if (($a_prosperisgold_settings->settings_key) == 'custom_thrift_max_start_time_from_delay')
                                                                   echo $a_prosperisgold_settings->settings_value;
                                                           }
                                                       }
                                                       ?>"
                                                       placeholder="<?php echo lang('placeholder_custom_thrift_max_start_time_from_delay_text') ?>">
                                            </div>

                                            <div class="form-group">
                                                <label for="loan_thrift_start_delay"><?php echo lang('label_loan_thrift_start_delay_text') ?>
                                                    &nbsp;
                                                    <span>
                                                                <i class="fa fa-question-circle" aria-hidden="true"
                                                                   data-toggle="tooltip" data-placement="top"
                                                                   title="<?= lang('tooltip_loan_thrift_start_delay_text') ?>"></i>
                                                            </span>
                                                </label>

                                                <input type="text" name="loan_thrift_start_delay" class="form-control"
                                                       id="loan_thrift_start_delay"
                                                       value="<?php
                                                       if ($all_prosperisgold_settings) {
                                                           foreach ($all_prosperisgold_settings as $a_prosperisgold_settings) {
                                                               if (($a_prosperisgold_settings->settings_key) == 'loan_thrift_start_delay')
                                                                   echo $a_prosperisgold_settings->settings_value;
                                                           }
                                                       }
                                                       ?>"
                                                       placeholder="<?php echo lang('placeholder_loan_thrift_start_delay_text') ?>">
                                            </div>

                                            <div class="form-group">
                                                <label for="loan_thrift_max_start_time_from_delay"><?php echo lang('label_loan_thrift_max_start_time_from_delay_text') ?>
                                                    &nbsp;
                                                    <span>
                                                                <i class="fa fa-question-circle" aria-hidden="true"
                                                                   data-toggle="tooltip" data-placement="top"
                                                                   title="<?= lang('tooltip_loan_thrift_max_start_time_from_delay_text') ?>"></i>
                                                            </span>
                                                </label>

                                                <input type="text" name="loan_thrift_max_start_time_from_delay"
                                                       class="form-control"
                                                       id="loan_thrift_max_start_time_from_delay"
                                                       value="<?php
                                                       if ($all_prosperisgold_settings) {
                                                           foreach ($all_prosperisgold_settings as $a_prosperisgold_settings) {
                                                               if (($a_prosperisgold_settings->settings_key) == 'loan_thrift_max_start_time_from_delay')
                                                                   echo $a_prosperisgold_settings->settings_value;
                                                           }
                                                       }
                                                       ?>"
                                                       placeholder="<?php echo lang('placeholder_loan_thrift_max_start_time_from_delay_text') ?>">
                                            </div>

                                            <div class="form-group">
                                                <label for="individual_thrift_start_delay"><?php echo lang('label_individual_thrift_start_delay_text') ?>
                                                    &nbsp;
                                                    <span>
                                                                <i class="fa fa-question-circle" aria-hidden="true"
                                                                   data-toggle="tooltip" data-placement="top"
                                                                   title="<?= lang('tooltip_individual_thrift_start_delay_text') ?>"></i>
                                                            </span>
                                                </label>

                                                <input type="text" name="individual_thrift_start_delay"
                                                       class="form-control"
                                                       id="individual_thrift_start_delay"
                                                       value="<?php
                                                       if ($all_prosperisgold_settings) {
                                                           foreach ($all_prosperisgold_settings as $a_prosperisgold_settings) {
                                                               if (($a_prosperisgold_settings->settings_key) == 'individual_thrift_start_delay')
                                                                   echo $a_prosperisgold_settings->settings_value;
                                                           }
                                                       }
                                                       ?>"
                                                       placeholder="<?php echo lang('placeholder_individual_thrift_start_delay_text') ?>">
                                            </div>

                                            <div class="form-group">
                                                <label for="individual_thrift_max_start_time_from_delay"><?php echo lang('label_individual_thrift_max_start_time_from_delay_text') ?>
                                                    &nbsp;
                                                    <span>
                                                                <i class="fa fa-question-circle" aria-hidden="true"
                                                                   data-toggle="tooltip" data-placement="top"
                                                                   title="<?= lang('tooltip_individual_thrift_max_start_time_from_delay_text') ?>"></i>
                                                            </span>
                                                </label>

                                                <input type="text" name="individual_thrift_max_start_time_from_delay"
                                                       class="form-control"
                                                       id="individual_thrift_max_start_time_from_delay"
                                                       value="<?php
                                                       if ($all_prosperisgold_settings) {
                                                           foreach ($all_prosperisgold_settings as $a_prosperisgold_settings) {
                                                               if (($a_prosperisgold_settings->settings_key) == 'individual_thrift_max_start_time_from_delay')
                                                                   echo $a_prosperisgold_settings->settings_value;
                                                           }
                                                       }
                                                       ?>"
                                                       placeholder="<?php echo lang('placeholder_individual_thrift_max_start_time_from_delay_text') ?>">
                                            </div>

                                            <div class="form-group">
                                                <label for="individual_thrift_minimum_payment_number"><?php echo lang('label_individual_thrift_minimum_payment_number_text') ?>
                                                    &nbsp;
                                                    <span>
                                                                <i class="fa fa-question-circle" aria-hidden="true"
                                                                   data-toggle="tooltip" data-placement="top"
                                                                   title="<?= lang('tooltip_individual_thrift_minimum_payment_number_text') ?>"></i>
                                                            </span>
                                                </label>

                                                <input type="text" name="individual_thrift_minimum_payment_number"
                                                       class="form-control"
                                                       id="individual_thrift_minimum_payment_number"
                                                       value="<?php
                                                       if ($all_prosperisgold_settings) {
                                                           foreach ($all_prosperisgold_settings as $a_prosperisgold_settings) {
                                                               if (($a_prosperisgold_settings->settings_key) == 'individual_thrift_minimum_payment_number')
                                                                   echo $a_prosperisgold_settings->settings_value;
                                                           }
                                                       }
                                                       ?>"
                                                       placeholder="<?php echo lang('placeholder_individual_thrift_minimum_payment_number_text') ?>">
                                            </div>

                                            <div class="form-group">
                                                <label for="individual_thrift_maximum_payment_number"><?php echo lang('label_individual_thrift_maximum_payment_number_text') ?>
                                                    &nbsp;
                                                    <span>
                                                                <i class="fa fa-question-circle" aria-hidden="true"
                                                                   data-toggle="tooltip" data-placement="top"
                                                                   title="<?= lang('tooltip_individual_thrift_maximum_payment_number_text') ?>"></i>
                                                            </span>
                                                </label>

                                                <input type="text" name="individual_thrift_maximum_payment_number"
                                                       class="form-control"
                                                       id="individual_thrift_maximum_payment_number"
                                                       value="<?php
                                                       if ($all_prosperisgold_settings) {
                                                           foreach ($all_prosperisgold_settings as $a_prosperisgold_settings) {
                                                               if (($a_prosperisgold_settings->settings_key) == 'individual_thrift_maximum_payment_number')
                                                                   echo $a_prosperisgold_settings->settings_value;
                                                           }
                                                       }
                                                       ?>"
                                                       placeholder="<?php echo lang('placeholder_individual_thrift_maximum_payment_number_text') ?>">
                                            </div>

                                            <div class="form-group">
                                                <label for="thrift_threshold_time"><?php echo lang('label_thrift_threshold_time_text') ?>
                                                    &nbsp;
                                                    <span>
                                                                <i class="fa fa-question-circle" aria-hidden="true"
                                                                   data-toggle="tooltip" data-placement="top"
                                                                   title="<?= lang('tooltip_thrift_threshold_time_text') ?>"></i>
                                                            </span>
                                                </label>

                                                <input type="text" name="thrift_threshold_time" class="form-control"
                                                       readonly
                                                       id="thrift_threshold_time"
                                                       value="<?php
                                                       if ($all_prosperisgold_settings) {
                                                           foreach ($all_prosperisgold_settings as $a_prosperisgold_settings) {
                                                               if (($a_prosperisgold_settings->settings_key) == 'thrift_threshold_time')
                                                                   echo $a_prosperisgold_settings->settings_value;
                                                           }
                                                       }
                                                       ?>"
                                                       placeholder="<?php echo lang('placeholder_thrift_threshold_time_text') ?>">
                                            </div>

                                        </div>
                                        <!-- /.box-body -->

                                        <br>
                                        <hr>
                                        <div style="font-size: larger;color: #2b2b2b">
                                            <?php echo lang('color_separator_lang') ?>
                                        </div>
                                        <hr>


                                        <!--admin st -->
                                        <div class="form-group">
                                            <label><?php echo lang('label_office_chosen_color_text') ?></label>
                                            <input type="text"
                                                   class="colorpicker-default form-control colorpicker-element"
                                                   name="office_chosen_color" value="<?php
                                            $col = '#ffffff';
                                            if ($all_prosperisgold_settings) {
                                                foreach ($all_prosperisgold_settings as $a_prosperisgold_settings) {
                                                    if ($a_prosperisgold_settings->settings_key == 'office_chosen_color'){
                                                        echo $a_prosperisgold_settings->settings_value;
                                                        $col = $a_prosperisgold_settings->settings_value;
                                                    }

                                                }
                                            }
                                            ?>">
                                            
                                        </div>
                                        <div class="form-group">
                                            <label><?php echo lang('label_office_chosen_color_deeper_text') ?></label>
                                            <input type="text"
                                                   class="colorpicker-default form-control colorpicker-element"
                                                   name="office_chosen_color_deeper" value="<?php
                                            $col = '#ffffff';
                                            if ($all_prosperisgold_settings) {
                                                foreach ($all_prosperisgold_settings as $a_prosperisgold_settings) {
                                                    if ($a_prosperisgold_settings->settings_key == 'office_chosen_color_deeper'){
                                                        echo $a_prosperisgold_settings->settings_value;
                                                        $col = $a_prosperisgold_settings->settings_value;
                                                    }
                                                }
                                            }
                                            ?>">
                                            
                                        </div>
                                        <!--admin en -->

                                        <!--partner st -->
                                        <div class="form-group">
                                            <label><?php echo lang('label_partner_chosen_color_text') ?></label>
                                            <input type="text"
                                                   class="colorpicker-default form-control colorpicker-element"
                                                   name="partner_chosen_color" value="<?php
                                            $col = '#ffffff';
                                            if ($all_prosperisgold_settings) {
                                                foreach ($all_prosperisgold_settings as $a_prosperisgold_settings) {
                                                    if ($a_prosperisgold_settings->settings_key == 'partner_chosen_color'){
                                                        echo $a_prosperisgold_settings->settings_value;
                                                        $col = $a_prosperisgold_settings->settings_value;
                                                    }
                                                }
                                            }
                                            ?>">
                                            
                                        </div>
                                        <div class="form-group">
                                            <label><?php echo lang('label_partner_chosen_color_deeper_text') ?></label>
                                            <input type="text"
                                                   class="colorpicker-default form-control colorpicker-element"
                                                   name="partner_chosen_color_deeper" value="<?php
                                            $col = '#ffffff';
                                            if ($all_prosperisgold_settings) {
                                                foreach ($all_prosperisgold_settings as $a_prosperisgold_settings) {
                                                    if ($a_prosperisgold_settings->settings_key == 'partner_chosen_color_deeper'){
                                                        echo $a_prosperisgold_settings->settings_value;
                                                        $col = $a_prosperisgold_settings->settings_value;
                                                    }
                                                }
                                            }
                                            ?>">
                                            
                                        </div>
                                        <!--partner en -->

                                        <!--thrift st -->
                                        <div class="form-group">
                                            <label><?php echo lang('label_thrift_chosen_color_text') ?></label>
                                            <input type="text"
                                                   class="colorpicker-default form-control colorpicker-element"
                                                   name="thrift_chosen_color" value="<?php
                                            $col = '#ffffff';
                                            if ($all_prosperisgold_settings) {
                                                foreach ($all_prosperisgold_settings as $a_prosperisgold_settings) {
                                                    if ($a_prosperisgold_settings->settings_key == 'thrift_chosen_color'){
                                                        echo $a_prosperisgold_settings->settings_value;
                                                        $col = $a_prosperisgold_settings->settings_value;
                                                    }
                                                }
                                            }
                                            ?>">
                                            
                                        </div>
                                        <div class="form-group">
                                            <label><?php echo lang('label_thrift_chosen_color_deeper_text') ?></label>
                                            <input type="text"
                                                   class="colorpicker-default form-control colorpicker-element"
                                                   name="thrift_chosen_color_deeper" value="<?php
                                            $col = '#ffffff';
                                            if ($all_prosperisgold_settings) {
                                                foreach ($all_prosperisgold_settings as $a_prosperisgold_settings) {
                                                    if ($a_prosperisgold_settings->settings_key == 'thrift_chosen_color_deeper'){
                                                        echo $a_prosperisgold_settings->settings_value;
                                                        $col = $a_prosperisgold_settings->settings_value;
                                                    }
                                                }
                                            }
                                            ?>">
                                            
                                        </div>
                                        <!--thrift en -->

                                        <!--trustee st -->
                                        <div class="form-group">
                                            <label><?php echo lang('label_trustee_chosen_color_text') ?></label>
                                            <input type="text"
                                                   class="colorpicker-default form-control colorpicker-element"
                                                   name="trustee_chosen_color" value="<?php
                                            $col = '#ffffff';
                                            if ($all_prosperisgold_settings) {
                                                foreach ($all_prosperisgold_settings as $a_prosperisgold_settings) {
                                                    if ($a_prosperisgold_settings->settings_key == 'trustee_chosen_color'){
                                                        echo $a_prosperisgold_settings->settings_value;
                                                        $col = $a_prosperisgold_settings->settings_value;
                                                    }
                                                }
                                            }
                                            ?>">
                                            
                                        </div>
                                        <div class="form-group">
                                            <label><?php echo lang('label_trustee_chosen_color_deeper_text') ?></label>
                                            <input type="text"
                                                   class="colorpicker-default form-control colorpicker-element"
                                                   name="trustee_chosen_color_deeper" value="<?php
                                            $col = '#ffffff';
                                            if ($all_prosperisgold_settings) {
                                                foreach ($all_prosperisgold_settings as $a_prosperisgold_settings) {
                                                    if ($a_prosperisgold_settings->settings_key == 'trustee_chosen_color_deeper'){
                                                        echo $a_prosperisgold_settings->settings_value;
                                                        $col = $a_prosperisgold_settings->settings_value;
                                                    }
                                                }
                                            }
                                            ?>">
                                            
                                        </div>
                                        <!--trustee en -->


                                        <div class="box-footer">
                                            <button type="submit" id=""
                                                    class="btn btn-primary"><?php echo lang('button_submit_text') ?>
                                            </button>
                                        </div>
                                    </form>
                                </div>
                                <!-- /.box -->
                            </div>


                        </div>
                        <!-- /.row -->
                    </section>
                    <!-- /.content -->
                    <div class="clearfix"></div>
                </div>
            </div><!-- end col -->
        </div><!-- end col -->
    </div>
    <!-- end row -->
</div>
<!-- </div> -->
<!-- </div> -->

<script>
    $(function () {

        $('#thrift_threshold_time').timepicker({
            minuteStep: 15,
            icons: {
                up: 'zmdi zmdi-chevron-up',
                down: 'zmdi zmdi-chevron-down'
            }
        });

        $('.colorpicker-default').colorpicker({
            format: 'hex'
        });

    });

</script>

