<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Author: Mahmudur Rahman
 * Web Dev: RS Soft
 *
 * */

/*
 *   This Library depends on the Settings Model
 *   Remember to create a settings model if there is none
 *
 * */

class Custom_settings_library
{

    public $CI;

    public function __construct()
    {

        $this->CI = &get_instance();

        $this->CI->load->model('settings_module/Settings_model');

    }

    //if a specific settings exist
    public function ifSettingsExist($a_settings_code, $a_settings_key)
    {
        //this function returns bool
        return $this->CI->Settings_model->ifSettingsExist($a_settings_code, $a_settings_key);
    }

    //if a type of settings exist
    public function ifSettingsTypeExist($a_settings_code)
    {
        //this function returns bool
        return $this->CI->Settings_model->ifSettingsTypeExist($a_settings_code);
    }

    public function addSettings($a_settings_code, $a_settings_key, $a_settings_value)
    {
        $this->CI->Settings_model->addSettings($a_settings_code, $a_settings_key, $a_settings_value);
    }

    public function updateSettings($a_settings_code, $a_settings_key, $a_settings_value)
    {
        $this->CI->Settings_model->updateSettings($a_settings_code, $a_settings_key, $a_settings_value);

    }

    //get a specific settings
    public function getASettings($a_settings_code, $a_settings_key)
    {
        if ($this->ifSettingsExist($a_settings_code, $a_settings_key) == true) {

            //returns a row
            return $this->CI->Settings_model->getASetting($a_settings_code, $a_settings_key);
        }

    }

    public function getASettingsValue($a_settings_code, $a_settings_key)
    {
        if ($this->ifSettingsExist($a_settings_code, $a_settings_key) == true) {

            //returns a objet of specific settings value
            $row = $this->CI->Settings_model->getASettingsValue($a_settings_code, $a_settings_key);
            if ($row) {
                //return specific settings value
                return $row->settings_value;
            }
        }
    }

    public function getSettings($a_settings_code)
    {
        // returns array of settings
        return $this->CI->Settings_model->getSettings($a_settings_code);
    }

    //------------------------------------------------------------------------------------------------------------------

    //if a specific settings exist
    public function ifUserSettingsExist($a_settings_code, $a_settings_key,$user_id)
    {
        //this function returns bool
        return $this->CI->Settings_model->ifUserSettingsExist($a_settings_code, $a_settings_key,$user_id);
    }

    //if a type of settings exist
    public function ifUserSettingsTypeExist($a_settings_code,$user_id)
    {
        //this function returns bool
        return $this->CI->Settings_model->ifUserSettingsTypeExist($a_settings_code,$user_id);
    }

    public function addUserSettings($a_settings_code, $a_settings_key, $a_settings_value,$user_id)
    {
        $this->CI->Settings_model->addUserSettings($a_settings_code, $a_settings_key, $a_settings_value,$user_id);
    }

    public function updateUserSettings($a_settings_code, $a_settings_key, $a_settings_value,$user_id)
    {
        $this->CI->Settings_model->updateUserSettings($a_settings_code, $a_settings_key, $a_settings_value,$user_id);

    }

    //get a specific settings
    public function getAUserSettings($a_settings_code, $a_settings_key, $user_id)
    {
        if ($this->ifuserSettingsExist($a_settings_code, $a_settings_key, $user_id) == true) {

            //returns a row
            return $this->CI->Settings_model->getAUserSettings($a_settings_code, $a_settings_key, $user_id);
        }

    }

    public function getAUserSettingsValue($a_settings_code, $a_settings_key, $user_id)
    {
        if ($this->ifUserSettingsExist($a_settings_code, $a_settings_key, $user_id) == true) {

            //returns a objet of specific settings value
            $row = $this->CI->Settings_model->getAUserSettingsValue($a_settings_code, $a_settings_key, $user_id);
            if ($row) {
                //return specific settings value
                return $row->settings_value;
            }
        }
    }

    public function getUserSettings($a_settings_code, $user_id)
    {
        // returns array of settings
        return $this->CI->Settings_model->getUserSettings($a_settings_code, $user_id);
    }
}