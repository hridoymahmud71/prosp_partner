<?php

/*page texts*/
$lang['page_title_text'] = 'Date & Time Settings';
$lang['page_subtitle_text'] = 'Edit and update Date, Time settings here';
$lang['box_title_text'] = 'Currency Settings Form';


$lang['breadcrumb_home_text'] = 'Home';
$lang['breadcrumb_section_text'] = 'Settings';
$lang['breadcrumb_page_text'] = 'Datetime Settings';

$lang['button_submit_text'] = 'Update Datetime Settings';

/*Datetime settings form texts*/
$lang['label_time_zone_text'] = 'Time Zone';
$lang['label_gmt_text'] = 'Difference to Greenwich time';


$lang['label_date_format_text'] = 'Date Format';
$lang['label_time_format_text'] = 'Time Format';

$lang['12_hrs_format'] = '(12 Hrs Format)';
$lang['24_hrs_format'] = '(24 Hrs Format)';

$lang['short_month_name_text'] = '(short month name)';
$lang['without_leading_zero'] = '(without leading zero)';

/*validation error texts*/


/*success messages*/
$lang['update_success_text'] = 'Successfully Updated Datetime Settings';

