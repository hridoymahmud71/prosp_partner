<?php

class Timezone_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function getTimezones()
    {
        $this->db->select('*');
        $this->db->from('zone');

        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;

    }
}