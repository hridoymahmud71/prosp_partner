<?php

class Currency_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function getCurrencies()
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_currency');
        $this->db->where('currency_deletion_status!=', 1);

        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    public function getActivatedCurrencies()
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_currency');
        $this->db->where('currency_status!=', 0);

        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    public function getACurrency($currency_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_currency');
        $this->db->where('currency_id', $currency_id);


        $query_result = $this->db->get();
        $row = $query_result->row();
        return $row;
    }


    public function addCurrency($data)
    {
        $this->db->insert('rspm_tbl_currency', $data);
        return true;
    }

    public function updateCurrency($data, $currency_id)
    {
        $this->db->where('currency_id', $currency_id);
        $this->db->update('rspm_tbl_currency', $data);

        return true;
    }

    public function activateCurrency($currency_id)
    {
        $this->db->set('currency_status', 1);
        $this->db->where('currency_id', $currency_id);
        $this->db->update('rspm_tbl_currency');

        return true;
    }

    public function deactivateCurrency($currency_id)
    {
        $this->db->set('currency_status', 0);
        $this->db->where('currency_id', $currency_id);
        $this->db->update('rspm_tbl_currency');

        return true;
    }

    public function deleteCurrency($currency_id)
    {
        $this->db->set('currency_deletion_status', 1);
        $this->db->where('currency_id', $currency_id);
        $this->db->update('rspm_tbl_currency');

        return true;
    }

    public function checkIfExists($field_name, $field_value, $currency_id)
    {
        $this->db->select('*');
        $this->db->from('rspm_tbl_currency');
        $this->db->where($field_name, $field_value);
        $this->db->where('currency_id!=', $currency_id);


        $query_result = $this->db->get();
        $num_rows = $query_result->num_rows();

        if ($num_rows > 0) {
            return true;
        } else {
            return false;
        }
    }

}