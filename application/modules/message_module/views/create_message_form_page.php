<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->

        <div class="container">

            <div class="row">
                <div class="col-xl-12">
                    <div class="page-title-box">
                        <h4 class="page-title float-left"><?= lang('page_title_text') ?></h4>

                        <ol class="breadcrumb float-right">
                            <li class="breadcrumb-item"><a href="/"><?= lang('breadcrum_home_text') ?></a></li>
                            <li class="breadcrumb-item active"><?= lang('breadcrum_page_text') ?></li>
                        </ol>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->

            <?php if ($this->session->flashdata('message_sent_error') ) { ?>
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <strong><?= lang('unsuccessful_text') ?></strong>
                    <?php

                    if ($this->session->flashdata('validation_errors')) {
                        echo $this->session->flashdata('validation_errors');
                    }
                    ?>

                    <?php
                    if ($this->session->flashdata('dont_have_employer_text')) {
                        echo $this->session->flashdata('dont_have_employer_text');
                    }
                    ?>

                    <?php
                    if ($this->session->flashdata('message_text_required_text')) {
                        echo $this->session->flashdata('message_text_required_text');
                    }
                    ?>

                </div>
            <? } ?>

            <div class="row">
                <div class="col-12">
                    <div class="card-box">

                        <h4 class="header-title m-t-0 m-b-30"><?= lang('box_title_text') ?></h4>

                        <div class="row">
                            <div class="col-md-12">
                                <form id="#msg-form" action="<?= $form_action ?>" method="post"
                                      enctype="multipart/form-data">
                                    <fieldset class="form-group w-50">
                                        <label for=""><?= lang('select_send_to_whom_label_text') ?></label>
                                        <select name="to_whom" style="height: auto" class="form-control"
                                                id="">
                                            <option value="to_admin"><?= lang('to_whom_select_option_admin') ?></option>
                                            <?php if ($this->ion_auth->in_group('employee') && $has_employer) { ?>
                                                <option value="to_employer"><?= lang('to_whom_select_option_employer') ?></option>
                                            <?php } ?>
                                        </select>
                                    </fieldset>
                                    <fieldset class="form-group w-100">
                                        <label for=""><?= lang('message_description_text') ?></label>
                                        <textarea id="message_text" class="form-control" name="message_text"
                                                  rows="3"><?=$message?></textarea>
                                    </fieldset>

                                    <button type="submit"
                                            class="btn btn-primary"><?= lang('submit_btn_text') ?></button>
                                </form>
                            </div><!-- end col -->


                        </div><!-- end row -->
                    </div>
                </div><!-- end col -->
            </div>


        </div> <!-- container -->


<!-- End content-page -->


<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->


<script>
    $(function () {

        tinymce.init({
            selector: '#message_text',
            height: 250,
            menubar: false,
            plugins: [
                'advlist autolink lists link charmap print preview anchor textcolor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime table contextmenu paste code help'
            ],
            toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
        });



    });


</script>