<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
 *  Created By,
 *  Mahmudur Rahman
 *  Web Dev, RS Soft
 *
 * */

class MessageController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('user_id') == null) {
            redirect('/');
        }

        $this->load->model('Message_model');

        // application/libraries
        $this->load->library('custom_datetime_library');
        $this->load->library('settings_module/custom_settings_library');


    }

    public function inbox()
    {
        $this->lang->load('message_inbox');

        $data = array();

        $this->load->view("common_module/header");
        //$this->load->view("common_module/common_left");
        $this->load->view("message_module/inbox_page", $data);
        $this->load->view("common_module/footer");
    }

    public function getInboxMessageListByAjax()
    {
        $this->lang->load('message_inbox');
        $messages = array();

        $sender_id = false;
        $reciever_id = $this->session->userdata('user_id');

        $requestData = $_REQUEST;
        //print_r($requestData);

        $columns[0] = 'message_created_at';
        $columns[1] = 'message_sender_id';
        $columns[2] = 'message';
        $columns[3] = 'message_archived_by_reciever';
        $columns[4] = 'reciever_read';
        $columns[5] = 'actions';

        $common_filter_value = false;
        $order_column = false;

        $specific_filters = array();
        $specific_filters = false;

        if (!empty($requestData['columns'][0]['search']['value'])) {
            $specific_filters['message_created_at'] = $requestData['columns'][0]['search']['value'];
        }

        if (!empty($requestData['columns'][1]['search']['value'])) {
            $specific_filters['message_sender_id'] = $requestData['columns'][1]['search']['value'];
        }

        if (!empty($requestData['columns'][2]['search']['value'])) {
            $specific_filters['message'] = $requestData['columns'][2]['search']['value'];
        }

        if (!empty($requestData['columns'][3]['search']['value'])) {
            $specific_filters['message_archived_by_reciever'] = $requestData['columns'][3]['search']['value'];
        }

        if (!empty($requestData['columns'][4]['search']['value'])) {
            $specific_filters['reciever_read'] = $requestData['columns'][4]['search']['value'];
        }


        if (!empty($requestData['search']['value'])) {
            $common_filter_value = $requestData['search']['value'];
        }

        if ($specific_filters == true || !empty($specific_filters)) {
            $common_filter_value = false;       //either search with specific filters or with common filter
        }

        $order['column'] = $columns[$requestData['order'][0]['column']];
        $order['by'] = $requestData['order'][0]['dir'];


        $limit['start'] = $requestData['start'];
        $limit['length'] = $requestData['length'];

        $totalData = $this->Message_model->countInboxMessages(false, false, $sender_id, $reciever_id);

        if ($common_filter_value == true || $specific_filters == true) {

            $totalFiltered = $this->Message_model->countInboxMessages($common_filter_value, $specific_filters, $sender_id, $reciever_id);
        } else {
            $totalFiltered = $totalData; // when there is no search parameter then total number rows = total number filtered rows.
        }


        $messages = $this->Message_model->getInboxMessages($common_filter_value, $specific_filters, $order, $limit, $sender_id, $reciever_id);

        if ($messages == false || empty($messages) || $messages == null) {
            $messages = false;
        }

        $last_query = $this->db->last_query();


        $this->load->library('custom_datetime_library');

        if ($messages) {
            $i = 0;
            foreach ($messages as $msg) {

                /*message starts*/
                $text = $msg->message;
                //$messages[$i]->message = substr(str_replace(array("&nbsp;", "\r\n", "\r", "\n", "\t", "'", "\"", "&quot;"), '', strip_tags($msg->message)), 0, 15) . '...';
                if (str_word_count($msg->message, 0) > 5) {
                    $words = str_word_count($msg->message, 2);
                    $pos = array_keys($words);
                    $text = substr($msg->message, 0, $pos[5]) . '...';
                }
                $messages[$i]->message = $text;
                /*message ends*/

                /*date time starts*/
                $messages[$i]->cr_on = new stdClass();
                $messages[$i]->cr_on->timestamp = $msg->message_created_at;

                $messages[$i]->rd_on = new stdClass();
                $messages[$i]->rd_on->timestamp = $msg->reciever_read_at;


                if ($msg->message_created_at == 0) {
                    $messages[$i]->cr_on->display = $this->lang->line('creation_time_unknown_text');
                } else {
                    $messages[$i]->cr_on->display =
                        $this->custom_datetime_library
                            ->convert_and_return_TimestampToDateAndTime($msg->message_created_at);
                }

                /*date time ends*/

                $messages[$i]->sender = new stdClass();
                $messages[$i]->sender->int = $msg->message_sender_id;

                $sender = $this->Message_model->getUser($msg->message_sender_id);

                if ($this->ion_auth->is_admin($msg->message_sender_id)) {
                    $messages[$i]->sender->display = 'Prosperis';
                } else if ($this->ion_auth->in_group('employer', $msg->message_sender_id)) {
                    $messages[$i]->sender->display = $sender->company;
                } else {
                    $messages[$i]->sender->display = $sender->first_name . ' ' . $sender->last_name;
                }

                /*archive - dearchive starts*/
                $messages[$i]->arc = new stdClass();
                $messages[$i]->arc->int = $msg->message_archived_by_reciever;

                if ($msg->message_archived_by_reciever == 1) {

                    $status_span = '<span class = "label label-primary">' . $this->lang->line('status_archive_text') . '</span>';

                    $status_tooltip = $this->lang->line('tooltip_dearchive_text');
                    $status_url = base_url() . 'message_module/dearchive_message/by_reciever/' . $msg->message_id;
                    $status_anchor_span = '<span class="label label-danger"><i class="fa fa-times" aria-hidden="true"></i></span>';
                    $status_anchor =
                        '<a ' . ' title="' . $status_tooltip . '"' . ' href="' . $status_url . '">' . $status_anchor_span . '</a>';

                } else {
                    $status_span = '<span class = "label label-default">' . $this->lang->line('status_non_archive_text') . '</span>';

                    $status_tooltip = $this->lang->line('tooltip_archive_text');
                    $status_url = base_url() . 'message_module/archive_message/by_reciever/' . $msg->message_id;
                    $status_anchor_span = '<span class="label label-success"><i class="fa fa-check" aria-hidden="true"></i></span>';
                    $status_anchor =
                        '<a ' . ' title="' . $status_tooltip . '"' . ' href="' . $status_url . '">' . $status_anchor_span . '</a>';
                }

                $messages[$i]->arc->html = $status_span . '&nbsp; &nbsp;' . $status_anchor;
                /*archive - dearchive ends*/

                /*read - unread starts*/
                $messages[$i]->rd = new stdClass();
                $messages[$i]->rd->int = $msg->reciever_read;

                if ($msg->reciever_read == 1) {

                    $read_status_span = '<span class = "label label-primary">' . $this->lang->line('status_read_text') . '</span>';

                    $read_status_tooltip = $this->lang->line('tooltip_mark_unread_text');
                    $read_status_url = base_url() . 'message_module/mark_message_unread/' . $msg->message_id;
                    $read_status_anchor_span = '<span class="label label-danger"><i class="fa fa-times" aria-hidden="true"></i></span>';
                    $read_status_anchor =
                        '<a ' . ' title="' . $read_status_tooltip . '"' . ' href="' . $read_status_url . '">' . $read_status_anchor_span . '</a>';

                } else {
                    $read_status_span = '<span class = "label label-default">' . $this->lang->line('status_unread_text') . '</span>';

                    $read_status_tooltip = $this->lang->line('tooltip_mark_read_text');
                    $read_status_url = base_url() . 'message_module/mark_message_read/' . $msg->message_id;
                    $read_status_anchor_span = '<span class="label label-success"><i class="fa fa-check" aria-hidden="true"></i></span>';
                    $read_status_anchor =
                        '<a ' . ' title="' . $read_status_tooltip . '"' . ' href="' . $read_status_url . '">' . $read_status_anchor_span . '</a>';
                }

                $messages[$i]->rd->html = $read_status_span . '&nbsp; &nbsp;' . $read_status_anchor;
                /*read - unread ends*/


                /*action starts*/


                $view_tooltip = $this->lang->line('tooltip_view_text');
                $view_url = base_url() . 'message_module/view_message/' . $msg->message_id;
                $view_anchor =
                    '<a ' . ' title="' . $view_tooltip . '" ' . ' href="' . $view_url . '" ' . ' style="color:#2b2b2b" ' . '>'
                    . '<i class="fa fa-eye fa-lg" aria-hidden="true"></i>'
                    . '</a>';


                $delete_tooltip = $this->lang->line('tooltip_delete_text');
                $delete_url = base_url() . 'message_module/delete_message/by_reciever/' . $msg->message_id;
                $delete_anchor = '<a ' . ' title="' . $delete_tooltip . '" ' . ' href="' . $delete_url . '" ' . ' class="confirmation" ' . ' style="color:#2b2b2b" ' . '>'
                    . '<i class="fa fa-trash-o fa-lg" aria-hidden="true">'
                    . '</a>';


                $messages[$i]->action = $view_anchor;
                /*action ends*/


                $i++;

            }
        }

        /*echo '<pre>';
        print_r($messages);die();*/


        $json_data['draw'] = intval($requestData['draw']);
        /* $totalData: for every request/draw by clientside ,
         they send a number as a parameter, when they recieve a response/data they first check the draw number,
         so we are sending same number in draw.*/
        $json_data['recordsTotal'] = intval($totalData); // total number of records after searching, if there is no searching then totalFiltered = totalData
        $json_data['recordsFiltered'] = intval($totalFiltered);

        //$messages = $this->removeKeys($messages); // converting to numeric indices.
        $json_data['data'] = $messages;

        // checking requests in console.log() for testing starts;
        /*$json_data['last_query'] = $last_query;
        $json_data['common_filter_value'] = $common_filter_value;
        $json_data['specific_filters'] = $specific_filters;
        $json_data['order_column'] = $order['column'];
        $json_data['order_by'] = $order['by'];
        $json_data['limit_length'] = $limit['length'];
        $json_data['limit_start'] = $limit['start'];*/
        // checking requests in console.log() for testing ends;

        echo(json_encode($json_data));

    }

    public function outbox()
    {
        $this->lang->load('message_outbox');

        $data = array();

        $this->load->view("common_module/header");
        //$this->load->view("common_module/common_left");
        $this->load->view("message_module/outbox_page", $data);
        $this->load->view("common_module/footer");
    }

    public function getOutboxMessageListByAjax()
    {
        $this->lang->load('message_outbox');
        $messages = array();

        $sender_id = $this->session->userdata('user_id');

        $requestData = $_REQUEST;
        //print_r($requestData);

        $columns[0] = 'message_created_at';
        $columns[1] = 'message';
        $columns[2] = 'message_archived_by_sender';
        $columns[3] = 'actions';

        $common_filter_value = false;
        $order_column = false;

        $specific_filters = array();
        $specific_filters = false;

        if (!empty($requestData['columns'][0]['search']['value'])) {
            $specific_filters['message_created_at'] = $requestData['columns'][0]['search']['value'];
        }

        if (!empty($requestData['columns'][1]['search']['value'])) {
            $specific_filters['message'] = $requestData['columns'][1]['search']['value'];
        }

        if (!empty($requestData['columns'][2]['search']['value'])) {
            $specific_filters['message_archived_by_sender'] = $requestData['columns'][2]['search']['value'];
        }


        if (!empty($requestData['search']['value'])) {
            $common_filter_value = $requestData['search']['value'];
        }

        if ($specific_filters == true || !empty($specific_filters)) {
            $common_filter_value = false;       //either search with specific filters or with common filter
        }

        $order['column'] = $columns[$requestData['order'][0]['column']];
        $order['by'] = $requestData['order'][0]['dir'];


        $limit['start'] = $requestData['start'];
        $limit['length'] = $requestData['length'];

        $totalData = $this->Message_model->countOutboxMessages(false, false, $sender_id);

        if ($common_filter_value == true || $specific_filters == true) {

            $totalFiltered = $this->Message_model->countOutboxMessages($common_filter_value, $specific_filters, $sender_id);
        } else {
            $totalFiltered = $totalData; // when there is no search parameter then total number rows = total number filtered rows.
        }


        $messages = $this->Message_model->getOutboxMessages($common_filter_value, $specific_filters, $order, $limit, $sender_id);

        if ($messages == false || empty($messages) || $messages == null) {
            $messages = false;
        }

        $last_query = $this->db->last_query();


        $this->load->library('custom_datetime_library');


        if ($messages) {
            $i = 0;
            foreach ($messages as $msg) {


                /*message starts*/

                $text = $msg->message;
                //$messages[$i]->message = substr(str_replace(array("&nbsp;", "\r\n", "\r", "\n", "\t", "'", "\"", "&quot;"), '', strip_tags($msg->message)), 0, 15) . '...';
                if (str_word_count($msg->message, 0) > 5) {
                    $words = str_word_count($msg->message, 2);
                    $pos = array_keys($words);
                    $text = substr($msg->message, 0, $pos[5]) . '...';
                }
                $messages[$i]->message = $text;

                /*message ends*/

                /*date time starts*/
                $messages[$i]->cr_on = new stdClass();
                $messages[$i]->cr_on->timestamp = $msg->message_created_at;


                if ($msg->message_created_at == 0) {
                    $messages[$i]->cr_on->display = $this->lang->line('creation_time_unknown_text');
                } else {
                    $messages[$i]->cr_on->display =
                        $this->custom_datetime_library
                            ->convert_and_return_TimestampToDateAndTime($msg->message_created_at);
                }

                /*date time ends*/

                /*archive - dearchive starts*/
                $messages[$i]->arc = new stdClass();
                $messages[$i]->arc->int = $msg->message_archived_by_sender;

                if ($msg->message_archived_by_sender == 1) {

                    $status_span = '<span class = "label label-primary">' . $this->lang->line('status_archive_text') . '</span>';

                    $status_tooltip = $this->lang->line('tooltip_dearchive_text');
                    $status_url = base_url() . 'message_module/dearchive_message/by_sender/' . $msg->message_id;
                    $status_anchor_span = '<span class="label label-danger"><i class="fa fa-times" aria-hidden="true"></i></span>';
                    $status_anchor =
                        '<a ' . ' title="' . $status_tooltip . '"' . ' href="' . $status_url . '">' . $status_anchor_span . '</a>';

                } else {
                    $status_span = '<span class = "label label-default">' . $this->lang->line('status_non_archive_text') . '</span>';

                    $status_tooltip = $this->lang->line('tooltip_archive_text');
                    $status_url = base_url() . 'message_module/archive_message/by_sender/' . $msg->message_id;
                    $status_anchor_span = '<span class="label label-success"><i class="fa fa-check" aria-hidden="true"></i></span>';
                    $status_anchor =
                        '<a ' . ' title="' . $status_tooltip . '"' . ' href="' . $status_url . '">' . $status_anchor_span . '</a>';
                }


                $messages[$i]->arc->html = $status_span . '&nbsp; &nbsp;' . $status_anchor;

                /*archive - dearchive ends*/

                /*action starts*/


                $view_tooltip = $this->lang->line('tooltip_view_text');
                $view_url = base_url() . 'message_module/view_message/' . $msg->message_id;
                $view_anchor =
                    '<a ' . ' title="' . $view_tooltip . '" ' . ' href="' . $view_url . '" ' . ' style="color:#2b2b2b" ' . '>'
                    . '<i class="fa fa-eye fa-lg" aria-hidden="true"></i>'
                    . '</a>';


                $delete_tooltip = $this->lang->line('tooltip_delete_text');
                $delete_url = base_url() . 'message_module/delete_message/by_sender/' . $msg->message_id;
                $delete_anchor = '<a ' . ' title="' . $delete_tooltip . '" ' . ' href="' . $delete_url . '" ' . ' class="confirmation" ' . ' style="color:#2b2b2b" ' . '>'
                    . '<i class="fa fa-trash-o fa-lg" aria-hidden="true">'
                    . '</a>';

                $messages[$i]->action = $view_anchor;
                /*action ends*/


                $i++;

            }
        }


        $json_data['draw'] = intval($requestData['draw']);
        /* $totalData: for every request/draw by clientside ,
         they send a number as a parameter, when they recieve a response/data they first check the draw number,
         so we are sending same number in draw.*/
        $json_data['recordsTotal'] = intval($totalData); // total number of records after searching, if there is no searching then totalFiltered = totalData
        $json_data['recordsFiltered'] = intval($totalFiltered);

        //$messages = $this->removeKeys($messages); // converting to numeric indices.
        $json_data['data'] = $messages;

        // checking requests in console.log() for testing starts;
        /*$json_data['last_query'] = $last_query;
        $json_data['common_filter_value'] = $common_filter_value;
        $json_data['specific_filters'] = $specific_filters;
        $json_data['order_column'] = $order['column'];
        $json_data['order_by'] = $order['by'];
        $json_data['limit_length'] = $limit['length'];
        $json_data['limit_start'] = $limit['start'];*/
        // checking requests in console.log() for testing ends;

        echo(json_encode($json_data));

    }


    public function sendMessage()
    {
        $this->lang->load('create_message_form');


        if ($this->input->post()) {
            $this->createMessage();
        }

        $vdata = array();

        $vdata['form_action'] = 'message_module/send_message';
        $vdata['message'] = '';

        $vdata['has_employer'] = false;
        if ($this->ion_auth->in_group('employee')) {
            $my_info = $this->Message_model->getUser($this->session->userdata('user_id'));
            if ($my_info->user_employer_id > 0 && $my_info->user_employer_id!= "" && $my_info->user_employer_id != null) {
                $vdata['has_employer'] = true;
            }
        }

        if (isset($_REQUEST['payment_method'])) {
            if (!($_REQUEST['payment_method'] == null || $_REQUEST['payment_method'] == '')) {
                $vdata['message'] .= 'Payment Method : '.$_REQUEST['payment_method'].'<br>';
            }
        }

        if (isset($_REQUEST['error_type'])) {
            if (!($_REQUEST['error_type'] == null || $_REQUEST['error_type'] == '')) {
                $vdata['message'] .= 'Error Type : ' . $_REQUEST['error_type'].'<br>';
            }
        }

        $this->load->view("common_module/header");
        //$this->load->view("common_module/common_left");
        $this->load->view("message_module/create_message_form_page", $vdata);
        $this->load->view("common_module/footer");
    }

    public function createMessage()
    {
        $to_whom = $this->input->post('to_whom');
        $message_text = $this->input->post('message_text');

        if (trim($message_text) == '' || empty(str_replace(array("&nbsp;", "\r\n", "\r", "\n", "\t", " "), '', strip_tags($message_text)))) {
            $this->session->set_flashdata('message_sent_error', 'message_sent_error');
            $this->session->set_flashdata('message_text_required_text', $this->lang->line('message_text_required_text'));

            redirect('message_module/send_message');
        }


        $this->form_validation->set_rules('message_text', 'message_text', 'required', array(
                'required' => $this->lang->line('message_text_required_text')
            )
        );


        if ($this->form_validation->run() == false) {

            $this->session->set_flashdata('message_sent_error', 'message_sent_error');
            $this->session->set_flashdata('validation_errors', validation_errors());

            redirect('message_module/send_message');
        }


        $message_reciever_ids = array();


        if ($to_whom == 'to_admin') {
            $message_reciever_ids[] = 1;

            /*$active_non_bot_admin_ids = $this->Message_model->getActiveNonBotAdminsIds();
            if ($active_non_bot_admin_ids) {
                foreach ($active_non_bot_admin_ids as $active_non_bot_admin_id) {
                    $message_reciever_ids[] = $active_non_bot_admin_id->user_id;
                }
            } else {
                $message_reciever_ids[] = 1;
            }*/


        } else if ($to_whom == 'to_employer') {

            if ($this->ion_auth->in_group('employee')) {

                $my_info = $this->Message_model->getUser($this->session->userdata('user_id'));

                if ($my_info->user_employer_id && $my_info->user_employer_id > 0) {
                    $message_reciever_ids[] = $my_info->user_employer_id;
                } else {
                    $this->session->set_flashdata('message_sent_error', 'message_sent_error');
                    $this->session->set_flashdata('dont_have_employer_text', $this->lang->line('dont_have_employer_text'));
                    redirect('message_module/send_message');
                }

            } else {
                $this->session->set_flashdata('message_sent_error', 'message_sent_error');
                $this->session->set_flashdata('dont_have_employer_text', $this->lang->line('dont_have_employer_text'));
                redirect('message_module/send_message');
            }

        }


        $p_data['message_sender_id'] = $this->session->userdata('user_id');
        $p_data['message'] = $message_text;
        $p_data['message_number'] = $this->getMessageNumber();
        $p_data['message_to_whom'] = $to_whom;
        $p_data['message_created_at'] = $this->custom_datetime_library->getCurrentTimestamp();
        $p_data['message_deleted_by_sender'] = 0;
        $p_data['message_archived_by_sender'] = 0;

        $inserted_message_id = $this->Message_model->insertMessage($p_data);


        if (!empty($message_reciever_ids)) {

            foreach ($message_reciever_ids as $message_reciever_id) {
                $pr_data['message_id'] = $inserted_message_id;
                $pr_data['message_reciever_id'] = $message_reciever_id;
                $pr_data['reciever_read'] = 0;
                $pr_data['reciever_read_at'] = 0;
                $pr_data['message_deleted_by_reciever'] = 0;
                $pr_data['message_archived_by_reciever'] = 0;

                $this->Message_model->insertMessageReciever($pr_data);

                //notify reciever through email
                $this->notifyNewMessage($inserted_message_id, $message_reciever_id);
            }

            if ($to_whom == 'to_admin' && $message_reciever_id == 1) {

                $active_non_bot_admin_ids = $this->Message_model->getActiveNonBotAdminsIds();
                if ($active_non_bot_admin_ids) {
                    foreach ($active_non_bot_admin_ids as $active_non_bot_admin_id) {
                        if ($active_non_bot_admin_id->user_id != 1) { //already notified superadmin
                            $this->notifyNewMessage($inserted_message_id, $active_non_bot_admin_id->user_id);
                        }
                    }
                }
            }

        }


        $this->session->set_flashdata('message_sent_success', 'message_sent_success');
        $this->session->set_flashdata('message_sent_success_text', 'message_sent_success_text');
        $this->session->set_flashdata('flash_inserted_message_id', $inserted_message_id);

        redirect('message_module/outbox');

    }

    private function notifyNewMessage($inserted_message_id, $message_reciever_id)
    {
        $user = $this->Message_model->getUser($message_reciever_id);

        if ($user) {

            $username = $user->first_name . ' ' . $user->last_name;
            if ($this->ion_auth->in_group('employer', $message_reciever_id)) {
                $username = $user->company;
            } else {
                $username = $user->first_name . ' ' . $user->last_name;
            }

            $mail_data['to'] = $user->email;
            //$mail_data['to'] = 'mahmud@sahajjo.com';

            $template = $this->Message_model->getEmailTempltateByType('new_message');

            if ($template) {
                $subject = $template->email_template_subject;

                $template_message = $template->email_template;

                /*-------*/
                $base_url = $this->getBaseUrl($message_reciever_id);

                $actual_link = $base_url . 'message_module/view_message/' . $inserted_message_id;

                $find = array("{{username}}", "{{actual_link}}");
                $replace = array($username, $actual_link);
                $message = str_replace($find, $replace, $template_message);

                /*--------*/


                $mail_data['subject'] = $subject;
                $mail_data['message'] = $message;

                $this->sendEmail($mail_data);
            }


        }


    }

    private function sendEmail($mail_data)
    {
        $site_name = $this->custom_settings_library->getASettingsValue('general_settings', 'site_name');
        $site_email = $this->custom_settings_library->getASettingsValue('general_settings', 'site_email');

        if (!$site_name) {
            $site_name = 'Prosperis';
        }

        if (!$site_email) {
            $site_email = 'prosperis@info.com';
        }

        try {

            $this->email->from($site_email, $site_name);
            $this->email->to($mail_data['to']);


            $this->email->subject($mail_data['subject']);
            $this->email->message(PROSPERIS_MAIL_TOP. $mail_data['message'].PROSPERIS_MAIL_BOTTOM);
            $this->email->set_mailtype("html");

            /* echo '<hr>';
             echo '<br>';
             echo $mail_data['subject'].'<br>';
             echo $mail_data['message'].'<br>';
             echo '<hr>';
             echo '<br>';*/

            @$this->email->send();

            /*$headers = 'From: '.$site_email.'\r\n';
            mail(mail_data['to'],$mail_data['subject'],$mail_data['message'],$headers);*/

        } catch (Exception $e) {
            // echo $e->getMessage();
        }
    }


    public function getMessageNumber()
    {
        $num = 'MSG';
        $num .= $this->alphaNum(10, false, false);

        $exists = $this->Message_model->doesMessageNumberExist($num);

        if ($exists) {

            $this->getMessageNumber();

        } else {

            return $num;

        }

    }

    private function alphaNum($length = false, $only_alphabets = false, $only_integers = false)
    {
        if (!$length) {
            $length = 8;
        }

        $alphabets = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $integers = '0123456789';
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';

        if ($only_alphabets) {
            $characters = $alphabets;
        }

        if ($only_integers) {
            $characters = $integers;
        }

        $randomString = '';

        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }

        return $randomString;
    }

    public function viewMessage()
    {
        $seen_by_sender = false;
        $seen_by_reciever = false;

        $this->lang->load('message_details_page');

        $message_id = $this->uri->segment(3);

        if ($this->input->post()) {
            $this->createMessageComment($message_id);
        }

        $curr_ts = $this->custom_datetime_library->getCurrentTimestamp();

        $vdata = array();

        $message = false;
        $message_sender = false;

        $comments_with_commenter = false;


        $vdata['form_action'] = 'message_module/view_message/' . $message_id;

        $message = $this->Message_model->getMessage($message_id);


        if ($message) {

            /*------------------------------------------*/
            if ($message->message_sender_id == $this->session->userdata('user_id')) {
                $seen_by_sender = true;
            }

            $message_recievers_ids = $this->Message_model->getMessagRecieversIds($message->message_id);
            $flatten_message_recievers_ids = array();

            if ($message_recievers_ids) {
                foreach ($message_recievers_ids as $message_recievers_id) {
                    $flatten_message_recievers_ids[] = $message_recievers_id['message_reciever_id'];
                }
            }

            $mark_read_by_other_admins = false;
            if (!empty($flatten_message_recievers_ids)) {
                if (in_array($this->session->userdata('user_id'), $flatten_message_recievers_ids)) {
                    $seen_by_reciever = true;
                } else if ($message->message_to_whom == 'to_admin' && in_array(1, $flatten_message_recievers_ids) && $this->ion_auth->is_admin()) {
                    $seen_by_reciever = true;
                    $mark_read_by_other_admins = true;
                }
            }

            /*------------------------------------------*/

            if ($seen_by_reciever) {

                $message_reciever = $this->Message_model->getMessagReciever($message_id, $this->session->userdata('user_id'));

                if ($message_reciever || $mark_read_by_other_admins) {

                    if ($message_reciever) {
                        if ($message_reciever->message_deleted_by_reciever == 1) {
                            redirect('users/auth/does_not/exist');
                        }
                    }

                    //echo $mark_read_by_other_admins;
                    $upd_data['reciever_read'] = 1;
                    $upd_data['reciever_read_at'] = $curr_ts;
                    $this->Message_model->recieverReadMessage($message_id, $this->session->userdata('user_id'), $upd_data, $mark_read_by_other_admins);

                    $cmt_data['reciever_read_comment'] = 1;
                    $this->Message_model->readComment($message_id,$cmt_data);

                }
            }

            if ($seen_by_sender) {

                $cmt_data['sender_read_comment'] = 1;
                $this->Message_model->readComment($message_id,$cmt_data);

                if ($message->message_deleted_by_sender == 1) {
                    redirect('users/auth/does_not/exist');
                }
            }


            $message->created_at_datetimestring =
                $this
                    ->custom_datetime_library
                    ->convert_and_return_TimestampToDateAndTime($message->message_created_at);


            $message_sender = $this->Message_model->getUser($message->message_sender_id);

            $comments_with_commenter = $this->Message_model->getMessageCommentsWithCommenter($message->message_id);

            if ($comments_with_commenter) {

                $i = 0;
                foreach ($comments_with_commenter as $cc) {

                    $comments_with_commenter[$i]->commented_at_datetimestring =
                        $this
                            ->custom_datetime_library
                            ->convert_and_return_TimestampToDateAndTime($cc->commented_at);


                    $i++;
                }

            }

        }


        $vdata['message'] = $message;
        $vdata['message_sender'] = $message_sender;
        $vdata['comments_with_commenter'] = $comments_with_commenter;


        $this->load->view("common_module/header");
        //$this->load->view("common_module/common_left");
        $this->load->view("message_module/message_details_page", $vdata);
        $this->load->view("common_module/footer");
    }

    public function createMessageComment($message_id)
    {

        $comment_text = $this->input->post('comment_text');


        if (trim($comment_text) == '' || empty(str_replace(array("&nbsp;", "\r\n", "\r", "\n", "\t", " "), '', strip_tags($comment_text)))) {
            $this->session->set_flashdata('comment_sent_error', 'comment_sent_error');
            $this->session->set_flashdata('comment_text_required_text', $this->lang->line('comment_text_required_text'));

            redirect('message_module/view_message/' . $message_id);
        }


        $this->form_validation->set_rules('comment_text', 'comment_text', 'required', array(
                'required' => $this->lang->line('comment_text_required_text')
            )
        );


        if ($this->form_validation->run() == false) {

            $this->session->set_flashdata('comment_sent_error', 'comment_sent_error');
            $this->session->set_flashdata('validation_errors', validation_errors());

            redirect('message_module/view_message/' . $message_id);
        }

        $p_data['message_id'] = $message_id;
        $p_data['comment_by'] = $this->session->userdata('user_id');
        $p_data['comment'] = $comment_text;
        $p_data['commented_at'] = $this->custom_datetime_library->getCurrentTimestamp();

        $inserted_comment_id = $this->Message_model->insertMessageComment($p_data);

        /*for notifying <starts>*/
        $sender_nt_del = $this->Message_model->getMessageSenderIdWhoDidNotDeleteMessage($message_id);


        if ($sender_nt_del) {
            //do not need to notify if sender is commenter
            if ($sender_nt_del->message_sender_id != $this->session->userdata('user_id')) {
                /*echo '<script>alert("sender")</script>';*/
                $this->notifyMessageComment($message_id, $inserted_comment_id, $sender_nt_del->message_sender_id);
            }
        }

        $recievers_nt_del = $this->Message_model->getMessageRecieversIdsWhoDidNotDeletedMessage($message_id);

        if ($recievers_nt_del) {

            foreach ($recievers_nt_del as $reciever_nt_del) {

                //do not need to notify if reciever is commenter
                if ($reciever_nt_del->message_reciever_id != $this->session->userdata('user_id')) {
                    /*echo '<script>alert("reciever")</script>';*/
                    $this->notifyMessageComment($message_id, $inserted_comment_id, $reciever_nt_del->message_reciever_id);
                }

            }


        }
        /*for notifying <ends>*/


        $this->session->set_flashdata('comment_sent_success', 'comment_sent_success');
        $this->session->set_flashdata('comment_sent_success_text', $this->lang->line('comment_sent_success_text'));


        redirect('message_module/view_message/' . $message_id);

    }

    private function notifyMessageComment($message_id, $inserted_comment_id, $member_id)
    {
        $user = $this->Message_model->getUser($member_id);

        if ($user) {

            $username = $user->first_name . ' ' . $user->last_name;
            if ($this->ion_auth->in_group('employer', $member_id)) {
                $username = $user->company;
            } else {
                $username = $user->first_name . ' ' . $user->last_name;
            }

            $mail_data['to'] = $user->email;
            //$mail_data['to'] = 'mahmud@sahajjo.com';

            $template = $this->Message_model->getEmailTempltateByType('new_message_comment');

            if ($template) {
                $subject = $template->email_template_subject;

                $template_message = $template->email_template;

                /*-------*/
                $base_url = $this->getBaseUrl($member_id);

                $actual_link = $base_url . 'message_module/view_message/' . $message_id . '#comment-' . $inserted_comment_id;

                $find = array("{{username}}", "{{actual_link}}");
                $replace = array($username, $actual_link);
                $message = str_replace($find, $replace, $template_message);

                /*--------*/


                $mail_data['subject'] = $subject;
                $mail_data['message'] = $message;

                $this->sendEmail($mail_data);
            }


        }


    }

    private function getBaseUrl($id)
    {
        $base_url = '';
        if ($this->ion_auth->in_group('admin', $id)) {
            $base_url = $this->config->item('office_base_url');
        } else if ($this->ion_auth->in_group('employer', $id) || $this->ion_auth->in_group('organization_contact', $id)) {
            $base_url = $this->config->item('partner_base_url');
        } else if ($this->ion_auth->in_group('employee', $id)) {
            $base_url = $this->Utility_model->makeThriftUrlWithEmployerSubdomain($this->config->item('thrift_base_url'), $id);
        } else if ($this->ion_auth->in_group('trustee', $id)) {
            $base_url = $this->config->item('trustee_base_url');
        }

        return $base_url;
    }


    public function archiveMessage()
    {
        $by_whom = $this->uri->segment(3);
        $message_id = $this->uri->segment(4);

        if ($by_whom == 'by_sender') {

            $this->Message_model->archiveOutboxMessage($message_id);

            $this->session->set_flashdata('archive_success', 'archive_success');
            redirect('message_module/outbox');

        } else if ($by_whom = 'by_reciever') {

            $user_id = $this->session->userdata('user_id');

            $message = $this->Message_model->getMessage($message_id);
            $mark_message_as_other_admins = false;
            if ($message) {
                if ($message->message_to_whom == 'to_admin') {
                    $mark_message_as_other_admins = true;
                }
            }

            $this->Message_model->archiveInboxMessage($message_id, $user_id,$mark_message_as_other_admins);

            $this->session->set_flashdata('archive_success', 'archive_success');
            redirect('message_module/inbox');
        }


    }

    public function dearchiveMessage()
    {
        $by_whom = $this->uri->segment(3);
        $message_id = $this->uri->segment(4);

        if ($by_whom == 'by_sender') {

            $this->Message_model->dearchiveOutboxMessage($message_id);

            $this->session->set_flashdata('dearchive_success', 'dearchive_success');
            redirect('message_module/outbox');

        } else if ($by_whom = 'by_reciever') {

            $user_id = $this->session->userdata('user_id');

            $message = $this->Message_model->getMessage($message_id);
            $mark_message_as_other_admins = false;
            if ($message) {
                if ($message->message_to_whom == 'to_admin') {
                    $mark_message_as_other_admins = true;
                }
            }

            $this->Message_model->dearchiveInboxMessage($message_id, $user_id,$mark_message_as_other_admins);

            $this->session->set_flashdata('dearchive_success', 'dearchive_success');
            redirect('message_module/inbox');
        }
    }

    public function markMessageAsRead()
    {
        $message_id = $this->uri->segment(3);
        $user_id = $this->session->userdata('user_id');

        $message = $this->Message_model->getMessage($message_id);
        $mark_message_as_other_admins = false;
        if ($message) {
            if ($message->message_to_whom == 'to_admin') {
                $mark_message_as_other_admins = true;
            }
        }

        $this->Message_model->markInboxMessageAsRead($message_id, $user_id,$mark_message_as_other_admins);

        $this->session->set_flashdata('read_success', 'read_success');
        redirect('message_module/inbox');
    }

    public function markMessageAsUnead()
    {
        $message_id = $this->uri->segment(3);
        $user_id = $this->session->userdata('user_id');

        $message = $this->Message_model->getMessage($message_id);
        $mark_message_as_other_admins = false;
        if ($message) {
            if ($message->message_to_whom == 'to_admin') {
                $mark_message_as_other_admins = true;
            }
        }

        $this->Message_model->markInboxMessageAsUnRead($message_id, $user_id,$mark_message_as_other_admins);

        $this->session->set_flashdata('unread_success', 'unread_success');
        redirect('message_module/inbox');
    }


}