<?php


$lang['page_title_text'] = 'Send Message';
$lang['breadcrum_home_text'] = 'Home';
$lang['breadcrum_page_text'] = 'Send Message';


$lang['box_title_text'] = 'Write Message';

$lang['select_send_to_whom_label_text'] = 'To Whom';
$lang['to_whom_select_option_admin'] = 'Prosperis Support';
$lang['to_whom_select_option_employer'] = 'My Organization Contact';

$lang['message_description_text'] = 'Message';
$lang['submit_btn_text'] = 'Send';



//validation

$lang['unsuccessful_text'] = 'Unsuccessful !';
$lang['message_text_required_text'] = 'Message cannot be empty!';
$lang['dont_have_employer_text'] = 'You do not have an organization!';




