<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');


class Message_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }


    public function getUser($user_id)
    {
        $this->db->select('*');
        $this->db->from('users as u');
        $this->db->where('u.id', $user_id);
        $this->db->join('rspm_tbl_user_details as ud', 'u.id = ud.user_id');

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    public function insertMessage($ins_data)
    {
        $this->db->insert('pg_message', $ins_data);

        return $this->db->insert_id();
    }

    public function insertMessageReciever($ins_data)
    {
        $this->db->insert('pg_message_reciever', $ins_data);
    }

    public function insertMessageComment($ins_data)
    {
        $this->db->insert('pg_message_comment', $ins_data);

        return $this->db->insert_id();
    }

    public function doesMessageNumberExist($num)
    {
        $this->db->select('*');
        $this->db->from('pg_message');
        $this->db->where('message_number', $num);

        $query = $this->db->get();
        $num_rows = $query->num_rows();

        if ($num_rows > 0) {
            return true;
        } else {
            return false;
        }

    }

    public function getMessage($message_id)
    {
        $this->db->select('*');
        $this->db->from('pg_message');
        $this->db->where('message_id', $message_id);


        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    public function getMessageCommentsWithCommenter($message_id)
    {
        $this->db->select('*');
        $this->db->from('pg_message_comment mc');
        $this->db->where('mc.message_id', $message_id);
        $this->db->join('users as u', 'mc.comment_by=u.id', 'left');

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }


    public function countInboxMessages($common_filter_value = false, $specific_filters = false, $sender_id, $reciever_id)
    {
        $adm = false;
        if ($this->ion_auth->is_admin()) {
            $adm = true;
        }

        $this->db->select('*');
        $this->db->from('pg_message as m');

        if ($sender_id) {
            $this->db->where('m.message_sender_id=', $sender_id);
        }

        $this->db->join('pg_message_reciever as mr', 'm.message_id=mr.message_id', 'left');

        $this->db->where('mr.message_deleted_by_reciever!=', 1);

        if ($adm) {
            $this->db->where('mr.message_reciever_id', 1);
        } else {
            $this->db->where('mr.message_reciever_id', $reciever_id);
        }

        if ($common_filter_value != false) {

            $this->db->like('m.message', $common_filter_value);
        }

        if ($specific_filters != false) {
            foreach ($specific_filters as $column_name => $filter_value) {

                if ($column_name == 'message') {
                    $this->db->like('m.' . $column_name, $filter_value);
                }


                if ($column_name == 'message_archived_by_reciever') {
                    if ($filter_value == 'yes') {
                        $this->db->where('mr.message_archived_by_reciever', 1);
                    } else {
                        $this->db->where('mr.message_archived_by_reciever!=', 1);
                    }

                }

                if ($column_name == 'reciever_read') {
                    if ($filter_value == 'yes') {
                        $this->db->where('mr.reciever_read', 1);
                    } else {
                        $this->db->where('mr.reciever_read!=', 1);
                    }

                }
            }


        }


        $query = $this->db->get();

        $num_rows = $query->num_rows();
        return $num_rows;
    }

    public function getInboxMessages($common_filter_value = false, $specific_filters = false, $order, $limit, $sender_id, $reciever_id)
    {
        $adm = false;
        if ($this->ion_auth->is_admin()) {
            $adm = true;
        }

        $this->db->select('*');
        $this->db->from('pg_message as m');

        if ($sender_id) {
            $this->db->where('m.message_sender_id=', $sender_id);
        }

        $this->db->join('pg_message_reciever as mr', 'm.message_id=mr.message_id', 'left');
        $this->db->where('mr.message_deleted_by_reciever!=', 1);

        if ($adm) {
            $this->db->where('mr.message_reciever_id', 1);
        } else {
            $this->db->where('mr.message_reciever_id', $reciever_id);
        }


        if ($common_filter_value != false) {

            $this->db->like('m.message', $common_filter_value);
        }

        if ($specific_filters != false) {
            foreach ($specific_filters as $column_name => $filter_value) {

                if ($column_name == 'message') {
                    $this->db->like('m.' . $column_name, $filter_value);
                }


                if ($column_name == 'message_archived_by_reciever') {
                    if ($filter_value == 'yes') {
                        $this->db->where('mr.message_archived_by_reciever', 1);
                    } else {
                        $this->db->where('mr.message_archived_by_reciever!=', 1);
                    }

                }

                if ($column_name == 'reciever_read') {
                    if ($filter_value == 'yes') {
                        $this->db->where('mr.reciever_read', 1);
                    } else {
                        $this->db->where('mr.reciever_read!=', 1);
                    }

                }
            }


        }


        if ($order['column'] == "message_archived_by_reciever" || $order['column'] == "reciever_read") {
            $this->db->order_by('mr.' . $order['column'], $order['by']);
        } else {
            $this->db->order_by('m.' . $order['column'], $order['by']);
        }

        $this->db->limit($limit['length'], $limit['start']);

        $query = $this->db->get();

        $result = $query->result();


        return $result;
    }


    public function countOutboxMessages($common_filter_value = false, $specific_filters = false, $sender_id)
    {
        $this->db->select('*');
        $this->db->from('pg_message as m');
        $this->db->where('m.message_deleted_by_sender!=', 1);
        $this->db->where('m.message_sender_id=', $sender_id);


        if ($common_filter_value != false) {
            /*$this->db->group_start();
            $this->db->like('m.message', $common_filter_value);
            $this->db->group_end();*/

            $this->db->like('m.message', $common_filter_value);
        }

        if ($specific_filters != false) {
            foreach ($specific_filters as $column_name => $filter_value) {

                if ($column_name == 'message') {
                    $this->db->like('m.' . $column_name, $filter_value);
                }


                if ($column_name == 'message_archived_by_sender') {
                    if ($filter_value == 'yes') {
                        $this->db->where('m.message_archived_by_sender', 1);
                    } else {
                        $this->db->where('m.message_archived_by_sender!=', 1);
                    }

                }
            }


        }


        $query = $this->db->get();

        $num_rows = $query->num_rows();


        return $num_rows;
    }

    public function getOutboxMessages($common_filter_value = false, $specific_filters = false, $order, $limit, $sender_id)
    {

        $this->db->select('*');
        $this->db->from('pg_message as m');
        $this->db->where('m.message_deleted_by_sender!=', 1);
        $this->db->where('m.message_sender_id=', $sender_id);


        if ($common_filter_value != false) {
            /*$this->db->group_start();
            $this->db->like('m.message', $common_filter_value);
            $this->db->group_end();*/

            $this->db->like('m.message', $common_filter_value);
        }

        if ($specific_filters != false) {
            foreach ($specific_filters as $column_name => $filter_value) {

                if ($column_name == 'message') {
                    $this->db->like('m.' . $column_name, $filter_value);
                }


                if ($column_name == 'message_archived_by_sender') {
                    if ($filter_value == 'yes') {
                        $this->db->where('m.message_archived_by_sender', 1);
                    } else {
                        $this->db->where('m.message_archived_by_sender!=', 1);
                    }

                }
            }


        }


        $this->db->order_by('m.' . $order['column'], $order['by']);
        $this->db->limit($limit['length'], $limit['start']);

        $query = $this->db->get();

        $result = $query->result();


        return $result;
    }


    public function getMessagRecieversIds($message_id)
    {
        $this->db->select('message_reciever_id');
        $this->db->from('pg_message_reciever');
        $this->db->where('message_id', $message_id);

        $query = $this->db->get();
        $result_array = $query->result_array();
        return $result_array;
    }

    public function getMessagReciever($message_id, $reciever_id)
    {
        $this->db->select('*');
        $this->db->from('pg_message_reciever');
        $this->db->where('message_id', $message_id);
        $this->db->where('message_reciever_id', $reciever_id);

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    public function recieverReadMessage($message_id, $message_reciever_id, $upd_data, $mark_read_by_other_admins)
    {
        $this->db->where('message_id', $message_id);
        if (!$mark_read_by_other_admins) {
            $this->db->where('message_reciever_id', $message_reciever_id);
        }

        $this->db->update('pg_message_reciever', $upd_data);
    }

    public function readComment($message_id, $upd_data)
    {
        $this->db->where('message_id', $message_id);
        $this->db->update('pg_message_comment', $upd_data);
    }


    public function archiveOutboxMessage($message_id)
    {
        $this->db->set('message_archived_by_sender', 1);
        $this->db->where('message_id', $message_id);
        $this->db->update('pg_message');
    }

    public function dearchiveOutboxMessage($message_id)
    {
        $this->db->set('message_archived_by_sender', 0);
        $this->db->where('message_id', $message_id);
        $this->db->update('pg_message');
    }

    public function archiveInboxMessage($message_id, $user_id, $mark_message_as_other_admins)
    {
        $this->db->set('message_archived_by_reciever', 1);
        $this->db->where('message_id', $message_id);
        if (!$mark_message_as_other_admins) {
            $this->db->where('message_reciever_id', $user_id);
        }

        $this->db->update('pg_message_reciever');

    }

    public function dearchiveInboxMessage($message_id, $user_id, $mark_message_as_other_admins)
    {
        $this->db->set('message_archived_by_reciever', 0);
        $this->db->where('message_id', $message_id);
        if (!$mark_message_as_other_admins) {
            $this->db->where('message_reciever_id', $user_id);
        }
        $this->db->update('pg_message_reciever');
    }

    public function markInboxMessageAsRead($message_id, $user_id, $mark_message_as_other_admins)
    {
        $this->db->set('reciever_read', 1);
        $this->db->where('message_id', $message_id);
        if (!$mark_message_as_other_admins) {
            $this->db->where('message_reciever_id', $user_id);
        }

        $this->db->update('pg_message_reciever');
    }

    public function markInboxMessageAsUnread($message_id, $user_id, $mark_message_as_other_admins)
    {
        $this->db->set('reciever_read', 0);
        $this->db->where('message_id', $message_id);
        if (!$mark_message_as_other_admins) {
            $this->db->where('message_reciever_id', $user_id);
        }
        $this->db->update('pg_message_reciever');
    }

    public function getEmailTempltateByType($type)
    {
        $this->db->select('*');
        $this->db->from('tbl_email_template');
        $this->db->where('email_template_type', $type);

        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    public function getActiveNonBotAdminsIds()
    {
        $this->db->select('u.id as user_id');
        $this->db->from('users as u');

        $this->db->where('u.active', 1);
        $this->db->where('u.is_user_bot', 0);
        $this->db->where('u.deletion_status !=', 1);

        $this->db->join('users_groups as ug', 'u.id=ug.user_id');
        $this->db->where('ug.group_id =', 1);

        $query = $this->db->get();

        //echo $this->db->last_query();die();

        $result = $query->result();
        return $result;
    }

    public function getMessageSenderIdWhoDidNotDeleteMessage($message_id)
    {
        $this->db->select('m.message_sender_id');
        $this->db->from('pg_message as m');
        $this->db->where('m.message_deleted_by_sender!=', 1);
        $this->db->where('m.message_id', $message_id);

        $query = $this->db->get();

        //echo $this->db->last_query();die();

        $row = $query->row();
        return $row;
    }

    public function getMessageRecieversIdsWhoDidNotDeletedMessage($message_id)
    {
        $this->db->select('mr.message_reciever_id');
        $this->db->from('pg_message_reciever as mr');
        $this->db->where('mr.message_deleted_by_reciever!=', 1);
        $this->db->where('mr.message_id', $message_id);

        $query = $this->db->get();

        //echo $this->db->last_query();die();

        $result = $query->result();
        return $result;
    }


}