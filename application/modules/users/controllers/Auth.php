<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 *  Modified By,
 *  Mahmudur Rahman
 *  Web Dev, RS Soft
 *
 * */

class Auth extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        /* extra line <starts> */
        $this->Utility_model->setTargetInACookie();
        $this->Utility_model->checkMaintenanceMode();
        /* extra line <ends> */

        $this->load->database();
        $this->load->library(array('ion_auth', 'form_validation'));

        $this->load->library('custom_log_library');

        $this->load->helper(array('url', 'language'));

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

        $this->lang->load('auth');
        $this->lang->load('users');

        $this->load->model('Custom_auth_model');
        $this->load->library('settings_module/custom_settings_library');


        /*for test  purpose : remove code in between*/
        set_time_limit(0);
        ini_set('MAX_EXECUTION_TIME', 3600);
        /*for test  purpose : remove code in between*/

    }

    // redirect if needed, otherwise display the user list
    public function index()
    {
        if (!$this->ion_auth->logged_in()) {
            // redirect them to the login page
            /*redirect('auth/login', 'refresh');*/

            /*Redirect above is changed for HMVC Conversion
            see:  http://dmitriykravchuk.co.za/blog/2015/10/30/codeigniter-3-hmvc-ion-auth/
            */
            redirect('users/auth/login', 'refresh');
        }

        if (!$this->ion_auth->is_admin()) {      // remove this if if you want to enable this for non-admins

            // redirect them to the home page because they must be an administrator to view this

            //originally
            //return show_error('You must be an administrator to view this page.');

            redirect('users/auth/need_permission');
        } else {
            // set the flash data error message if there is one
            $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');


            // application/libraries
            // this segment us added for customized datetime
            $this->load->library('custom_datetime_library');

            //original demo
            //$this->_render_page('users/auth/index', $this->data);

            //print_r($this->data);die();
            $this->data['group_list'] = $this->Custom_auth_model->getGroups();

            $this->load->view("common_module/header");
            //$this->load->view("common_module/common_left");
            $this->load->view("users/auth/custom_folder/users_page", $this->data);
            $this->load->view("common_module/footer");

        }
    }


    // log the user in
    public function login()
    {
        $this->data['title'] = $this->lang->line('login_heading');

        //validate form input
        $this->form_validation->set_rules('identity', str_replace(':', '', $this->lang->line('login_identity_label')), 'required',
            array(
                'required' => $this->lang->line('identity_required')
            )
        );

        $this->form_validation->set_rules('password', str_replace(':', '', $this->lang->line('login_password_label')), 'required',
            array(
                'required' => $this->lang->line('password_required')
            )
        );

        if ($this->form_validation->run() == true) {

            // check to see if the user is logging in
            // check for "remember me"
            $remember = (bool)$this->input->post('remember');

            if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), $remember)) {
                //if the login is successful
                //redirect them back to the home page


                //only employer can pass through here
                if (!($this->ion_auth->in_group('employer') || $this->ion_auth->in_group('organization_contact'))) {
                    $this->session->set_flashdata('message', $this->lang->line('only_employer_can_login_text'));
                    redirect('users/auth/login');
                }

                $activity_by = '';
                if ($this->ion_auth->in_group('employer') == 'employer') {
                    $activity_by = 'employer';
                } else if ($this->ion_auth->in_group('organization_contact')) {
                    $activity_by = 'user';
                }

                $this->session->set_flashdata('message', $this->ion_auth->messages());

                /*creating log starts*/
                $this->custom_log_library->createALog
                (
                    $this->session->userdata('user_id'),                                    //1.    $created_by
                    '',                                                                     //2.    $created_for
                    'login_logout',                                                         //3.    $type
                    '',                                                                     //4.    $type_id
                    'logged_in',                                                            //5.    $activity
                    $activity_by,                                                           //6.    $activity_by
                    'admin',                                                                //7.    $activity_for
                    '',                                                                     //8.    $sub_type
                    '',                                                                     //9.    $sub_type_id
                    '',                                                                     //10.   $super_type
                    '',                                                                     //11.   $super_type_id
                    '',                                                                     //12.   $other_information
                    ''                                                                      //13.   $change_list
                );
                /*creating log ends*/


                /*while organization cantact is logging in ...
                replace its sesssion fields with its organization and
                add its session fileds as org_contact_%something%
                */
                if ($this->ion_auth->in_group('organization_contact')) {
                    $org_contact = $this->Custom_auth_model->getUserDetails($this->session->userdata('user_id'));
                    $org = $this->Custom_auth_model->getUserDetails($org_contact->user_employer_id);

                    if ($org) {

                        if ($org->active != 1 || $org->deletion_status != 0) {
                            $this->session->set_flashdata('message', 'Organization is inactive or deleted');
                            redirect('users/auth/login');
                        }

                        $_SESSION['org_contact_identity'] = $_SESSION['identity'];
                        $_SESSION['identity'] = $org->email; //assuming identitiy is email and not username

                        $_SESSION['org_contact_email'] = $_SESSION['email'];
                        $_SESSION['email'] = $org->email;

                        $_SESSION['org_contact_user_id'] = $_SESSION['user_id'];
                        $_SESSION['user_id'] = $org->id;
                    } else {
                        $this->session->set_flashdata('message', 'Organization is not found');
                        redirect('users/auth/login');
                    }
                }

                /*if cookie has a path redirect there <starts>*/
                $cookie_name = 'redirect_path_after_login';
                if (isset($_COOKIE[$cookie_name])) {

                    $rediect_from_cookie = $_COOKIE[$cookie_name];
                    unset($_COOKIE[$cookie_name]);
                    setcookie($cookie_name, '', time() - 36000, '/');

                    redirect($rediect_from_cookie);
                }
                /*if cookie has a path redirect there <ends>*/

                redirect('common_module', 'refresh');
            } else {
                // if the login was un-successful
                // redirect them back to the login page
                $this->session->set_flashdata('message', $this->ion_auth->errors());
                redirect('users/auth/login', 'refresh'); // use redirects instead of loading views for compatibility with MY_Controller libraries
            }
        } else {
            // the user is not logging in so display the login page
            // set the flash data error message if there is one
            $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

            //set email or username in flash so that
            $this->session->set_flashdata('identity', $this->input->post('identity'));

            $this->data['identity'] = array('name' => 'identity',
                'id' => 'identity',
                'type' => 'text',
                'value' => $this->form_validation->set_value('identity'),
            );
            $this->data['password'] = array('name' => 'password',
                'id' => 'password',
                'type' => 'password',
            );

            $settings_code = 'general_settings';
            $partner_site_logo = $this->custom_settings_library->getASettingsValue($settings_code, 'partner_site_logo');

            if ($partner_site_logo == '' || $partner_site_logo == null || $partner_site_logo == false) {
                $this->data['site_logo'] = $this->custom_settings_library->getASettingsValue($settings_code, 'site_logo');
            } else {
                $this->data['site_logo'] = $partner_site_logo;
            }

            /*$this->_render_page('users/auth/login', $this->data);*/

            $this->_render_page('users/auth/custom_folder/login_page', $this->data);
        }
    }

    // log the user out
    public function logout()
    {
        if (!$this->ion_auth->logged_in()) {
            redirect('users/auth/login', 'refresh');
        }
        $this->data['title'] = "Logout";

        /*creating log starts*/
        $this->custom_log_library->createALog
        (
            $this->session->userdata('user_id'),                                    //1.    $created_by
            '',                                                                     //2.    $created_for
            'login_logout',                                                         //3.    $type
            '',                                                                     //4.    $type_id
            'logged_out',                                                           //5.    $activity
            'employer',                                                             //6.    $activity_by
            'admin',                                                                //7.    $activity_for
            '',                                                                     //8.    $sub_type
            '',                                                                     //9.    $sub_type_id
            '',                                                                     //10.   $super_type
            '',                                                                     //11.   $super_type_id
            '',                                                                     //12.   $other_information
            ''                                                                      //13.   $change_list
        );
        /*creating log ends*/

        // log the user out
        $logout = $this->ion_auth->logout();

        // redirect them to the login page
        $this->session->set_flashdata('message', $this->ion_auth->messages());
        redirect('users/auth/login', 'refresh');
    }

    // change password
    public function change_password()
    {
        $this->form_validation->set_rules('old', $this->lang->line('change_password_validation_old_password_label'), 'required');
        $this->form_validation->set_rules('new', $this->lang->line('change_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
        $this->form_validation->set_rules('new_confirm', $this->lang->line('change_password_validation_new_password_confirm_label'), 'required');

        if (!$this->ion_auth->logged_in()) {
            redirect('users/auth/login', 'refresh');
        }

        $user = $this->ion_auth->user()->row();

        if ($this->form_validation->run() == false) {
            // display the form
            // set the flash data error message if there is one
            $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

            $this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
            $this->data['old_password'] = array(
                'name' => 'old',
                'id' => 'old',
                'type' => 'password',
            );
            $this->data['new_password'] = array(
                'name' => 'new',
                'id' => 'new',
                'type' => 'password',
                'pattern' => '^.{' . $this->data['min_password_length'] . '}.*$',
            );
            $this->data['new_password_confirm'] = array(
                'name' => 'new_confirm',
                'id' => 'new_confirm',
                'type' => 'password',
                'pattern' => '^.{' . $this->data['min_password_length'] . '}.*$',
            );
            $this->data['user_id'] = array(
                'name' => 'user_id',
                'id' => 'user_id',
                'type' => 'hidden',
                'value' => $user->id,
            );

            // render
            $this->_render_page('users/auth/change_password', $this->data);
        } else {
            $identity = $this->session->userdata('identity');

            $change = $this->ion_auth->change_password($identity, $this->input->post('old'), $this->input->post('new'));

            if ($change) {
                //if the password was successfully changed
                $this->session->set_flashdata('message', $this->ion_auth->messages());
                $this->logout();
            } else {
                $this->session->set_flashdata('message', $this->ion_auth->errors());
                redirect('users/auth/change_password', 'refresh');
            }
        }
    }

    // forgot password
    public function forgot_password()
    {
        /*extra line st*/
        $settings_code = 'general_settings';
        $partner_site_logo = $this->custom_settings_library->getASettingsValue($settings_code, 'partner_site_logo');

        if ($partner_site_logo == '' || $partner_site_logo == null || $partner_site_logo == false) {
            $this->data['site_logo'] = $this->custom_settings_library->getASettingsValue($settings_code, 'site_logo');
        } else {
            $this->data['site_logo'] = $partner_site_logo;
        }

        /*extra line en*/

        // setting validation rules by checking whether identity is username or email
        if ($this->config->item('identity', 'ion_auth') != 'email') {
            $this->form_validation->set_rules('identity', $this->lang->line('forgot_password_identity_label'), 'required');
        } else {
            $this->form_validation->set_rules('identity', $this->lang->line('forgot_password_validation_email_label'), 'required|valid_email');
        }


        if ($this->form_validation->run() == false) {
            $this->data['type'] = $this->config->item('identity', 'ion_auth');
            // setup the input
            $this->data['identity'] = array('name' => 'identity',
                'id' => 'identity',
            );

            if ($this->config->item('identity', 'ion_auth') != 'email') {
                $this->data['identity_label'] = $this->lang->line('forgot_password_identity_label');
            } else {
                $this->data['identity_label'] = $this->lang->line('forgot_password_email_identity_label');
            }

            // set any errors and display the form
            $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
            //$this->_render_page('users/auth/forgot_password', $this->data);
            $this->_render_page('users/auth/custom_folder/forgot_password_page', $this->data);
        } else {
            $identity_column = $this->config->item('identity', 'ion_auth');
            $identity = $this->ion_auth->where($identity_column, $this->input->post('identity'))->users()->row();

            if (empty($identity)) {

                if ($this->config->item('identity', 'ion_auth') != 'email') {
                    $this->ion_auth->set_error('forgot_password_identity_not_found');
                } else {
                    $this->ion_auth->set_error('forgot_password_email_not_found');
                }

                $this->session->set_flashdata('message', $this->ion_auth->errors());
                redirect("users/auth/forgot_password", 'refresh');
            }

            // run the forgotten password method to email an activation code to the user
            $forgotten = $this->ion_auth->forgotten_password($identity->{$this->config->item('identity', 'ion_auth')});

            if ($forgotten) {
                // if there were no errors
                $this->session->set_flashdata('message', $this->ion_auth->messages());
                redirect("users/auth/login", 'refresh'); //we should display a confirmation page here instead of the login page
            } else {
                $this->session->set_flashdata('message', $this->ion_auth->errors());
                //redirect("users/auth/forgot_password", 'refresh');
                redirect("users/auth/forgot_password", 'refresh');
            }
        }
    }

    // reset password - final step for forgotten password
    public function reset_password($code = NULL)
    {
        if (!$code) {
            show_404();
        }

        /*extra line st*/
        $settings_code = 'general_settings';
        $partner_site_logo = $this->custom_settings_library->getASettingsValue($settings_code, 'partner_site_logo');

        if ($partner_site_logo == '' || $partner_site_logo == null || $partner_site_logo == false) {
            $this->data['site_logo'] = $this->custom_settings_library->getASettingsValue($settings_code, 'site_logo');
        } else {
            $this->data['site_logo'] = $partner_site_logo;
        }

        /*extra line en*/

        $user = $this->ion_auth->forgotten_password_check($code);

        if ($user) {
            // if the code is valid then display the password reset form

            //original rules
            //$this->form_validation->set_rules('new', $this->lang->line('reset_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
            //$this->form_validation->set_rules('new_confirm', $this->lang->line('reset_password_validation_new_password_confirm_label'), 'required');
            //re-written rule
            $this->form_validation->set_rules('new', $this->lang->line('reset_password_validation_new_password_label'), 'required|callback_validate_password');
            $this->form_validation->set_rules('new_confirm', $this->lang->line('reset_password_validation_new_password_confirm_label'), 'required|matches[new]');


            if ($this->form_validation->run() == false) {
                // display the form

                // set the flash data error message if there is one
                $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

                $this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
                $this->data['new_password'] = array(
                    'name' => 'new',
                    'id' => 'new',
                    'type' => 'password',
                    'pattern' => '^.{' . $this->data['min_password_length'] . '}.*$',
                );
                $this->data['new_password_confirm'] = array(
                    'name' => 'new_confirm',
                    'id' => 'new_confirm',
                    'type' => 'password',
                    'pattern' => '^.{' . $this->data['min_password_length'] . '}.*$',
                );

                $this->data['user_id'] = array(
                    'name' => 'user_id',
                    'id' => 'user_id',
                    'type' => 'hidden',
                    'value' => $user->id,
                );
                $this->data['csrf'] = $this->_get_csrf_nonce();
                $this->data['code'] = $code;

                // render
                //$this->_render_page('users/auth/reset_password', $this->data);
                $this->_render_page('users/auth/custom_folder/reset_password_page', $this->data);
            } else {
                // do we have a valid request?

                //if ($this->_valid_csrf_nonce() === FALSE || $user->id != $this->input->post('user_id')) { //orginally
                if ($user->id != $this->input->post('user_id')) {
                    // something fishy might be up
                    $this->ion_auth->clear_forgotten_password_code($code);

                    show_error($this->lang->line('error_csrf'));

                } else {
                    // finally change the password
                    $identity = $user->{$this->config->item('identity', 'ion_auth')};

                    $change = $this->ion_auth->reset_password($identity, $this->input->post('new'));

                    if ($change) {
                        // if the password was successfully changed
                        $this->session->set_flashdata('message', $this->ion_auth->messages());
                        redirect("users/auth/login", 'refresh');
                    } else {
                        $this->session->set_flashdata('message', $this->ion_auth->errors());
                        //redirect('users/auth/reset_password/' . $code, 'refresh');
                        redirect('users/auth/reset_password/' . $code, 'refresh');

                    }
                }
            }
        } else {
            // if the code is invalid then send them back to the forgot password page
            $this->session->set_flashdata('message', $this->ion_auth->errors());
            redirect("users/auth/forgot_password", 'refresh');
        }
    }

    function validate_password($str)
    {
        if (preg_match("/^(?=.*[A-Z])(?=.*\d.*)(?=.*\W.*)[a-zA-Z0-9\S]{8,32}$/", $str) !== 0) {
            return true;
        } else {
            $this->form_validation->set_message("validate_password", $this->lang->line('strong_password_text'));
            return false;
        }
    }


    private function getMemIdNum($f_name, $l_name)
    {
        if (!$f_name) {
            $f_name = 'X';
        } else {
            $f_name = ucfirst($f_name[0]);
        }

        if (!$l_name) {
            $l_name = 'X';
        } else {
            $l_name = ucfirst($l_name[0]);
        }

        $mem_id_num = 'M' . $this->alphaNum(5, false, true) . $f_name . $l_name;

        $exists = $this->Custom_auth_model->checkIfMemIdNumExists($mem_id_num);

        if ($exists) {
            $x = $this->getMemIdNum($f_name, $l_name);
        } else {
            return $mem_id_num;
        }

        return $x;


    }

    private function alphaNum($length = false, $only_alphabets = false, $only_integers = false)
    {
        if (!$length) {
            $length = 8;
        }

        $alphabets = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $integers = '0123456789';
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';

        if ($only_alphabets) {
            $characters = $alphabets;
        }

        if ($only_integers) {
            $characters = $integers;
        }

        $randomString = '';

        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }

        return $randomString;
    }

    public
    function getEmployersBySelect2()
    {

        if (isset($_REQUEST['keyword'])) {
            $keyword = $_REQUEST['keyword'];
        } else {
            $keyword = '';
        }

        if (isset($_REQUEST['page'])) {
            $page = $_REQUEST['page'];
        } else {
            $page = 1;
        }


        $limit = 10;

        $total_count = $this->Custom_auth_model->countTotalEmployerBySelect2($keyword);
        $offset = ($page - 1) * $limit;

        $end_count = $offset + $limit;
        $more_pages = $total_count > $end_count;

        $employers = $this->Custom_auth_model->getTotalEmployerBySelect2($keyword, $limit, $offset);

        $last_query = $this->db->last_query();

        $json_data = array();
        $items = array();


        if ($employers) {

            foreach ($employers as $an_employer) {
                $p = array();
                $p['id'] = $an_employer->id;
                $p['text'] =
                    $an_employer->company;

                $items = $p;
                $json_data['items'][] = $items;
            }
        } else {
            $p = array();
            $p['id'] = '';
            $p['text'] = 'No Employer Found';

            $items = $p;
            $json_data['items'][] = $items;
        }

        //echo '<pre>';print_r($json_data);die();


        $json_data['total_count'] = $total_count;
        $json_data['more_pages'] = $more_pages;
        $json_data['last_query'] = $last_query;

        echo json_encode($json_data);

    }

    /*custom func starts*/
    public
    function getGroupName($group_id)
    {
        $group_info = $this->ion_auth_model->group($group_id)->row();

        if ($group_info) {
            return $group_info->name;
        }
    }

    /*custom func starts*/


    public
    function _get_csrf_nonce()
    {
        $this->load->helper('string');
        $key = random_string('alnum', 8);
        $value = random_string('alnum', 20);
        $this->session->set_flashdata('csrfkey', $key);
        $this->session->set_flashdata('csrfvalue', $value);

        return array($key => $value);
    }

    public
    function _valid_csrf_nonce()
    {
        $csrfkey = $this->input->post($this->session->flashdata('csrfkey'));
        if ($csrfkey && $csrfkey == $this->session->flashdata('csrfvalue')) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public
    function _render_page($view, $data = null, $returnhtml = false)//I think this makes more sense
    {

        $this->viewdata = (empty($data)) ? $this->data : $data;

        $view_html = $this->load->view($view, $this->viewdata, $returnhtml);

        if ($returnhtml) return $view_html;//This will return html on 3rd argument being true
    }


    /*--------------------------------------------------------------------------------------------------------------------*/
    /*
     * custom functions below
     * author: Mahmudur Rahman
     * Web Dev : RS Soft
    */


    public
    function showNeedPermission()
    {
        if (!$this->ion_auth->logged_in()) {

            redirect('users/auth/login', 'refresh');
        }

        $this->lang->load('need_permission');
        $this->load->view("common_module/header");
        //$this->load->view("common_module/common_left");
        $this->load->view("users/auth/custom_folder/need_permission_page");
        $this->load->view("common_module/footer");
    }

    public
    function showDoesNotExist()
    {
        if (!$this->ion_auth->logged_in()) {

            redirect('users/auth/login', 'refresh');
        }

        $this->lang->load('does_not_exist');
        $this->load->view("common_module/header");
        //$this->load->view("common_module/common_left");
        $this->load->view("users/auth/custom_folder/does_not_exist_page");
        $this->load->view("common_module/footer");
    }

    public
    function showPaymentMethodErrorPage()
    {
        if (!$this->ion_auth->logged_in()) {

            redirect('users/auth/login', 'refresh');
        }
        $this->lang->load('payment_method_error');

        $data = array();

        $data['payment_method'] = $this->lang->line('unknown_text');
        $data['error_type'] = $this->lang->line('unknown_text');

        if (isset($_REQUEST['payment_method'])) {
            if (!($_REQUEST['payment_method'] == null || $_REQUEST['payment_method'] == '')) {
                $data['payment_method'] = $_REQUEST['payment_method'];
            }
        }

        if (isset($_REQUEST['error_type'])) {
            if (!($_REQUEST['error_type'] == null || $_REQUEST['error_type'] == '')) {
                $data['error_type'] = $_REQUEST['error_type'];
            }
        }


        $this->load->view("common_module/header");
        //$this->load->view("common_module/common_left");
        $this->load->view("users/auth/custom_folder/payment_method_error_page", $data);
        $this->load->view("common_module/footer");
    }

    public
    function showMaintenancePage()
    {
        $data = array();

        $html = 'Site Under Maintenance';

        $site_maintenance_html = $this->custom_settings_library->getASettingsValue('general_settings', 'site_maintenance_html');

        if($site_maintenance_html != '' && $site_maintenance_html != false){
            $html = $site_maintenance_html;
        }

        $data['html'] = $html;


        $this->load->view("users/auth/custom_folder/maintenance_page",$data);

    }






}
