
<?php if ($custom_product_invited_members) { ?>
    <style>
        table.minimalistBlack {
            border: 3px solid #FFFFFF;
            width: 100%;
            text-align: left;
            border-collapse: collapse;
        }
        table.minimalistBlack td, table.minimalistBlack th {
            border: 0px solid #000000;
            padding: 5px 5px;
        }
        table.minimalistBlack tbody td {
            font-size: 13px;
        }
        table.minimalistBlack thead {
        }
        table.minimalistBlack thead th {
            font-size: 15px;
            font-weight: bold;
            color: #000000;
            text-align: left;
        }
        table.minimalistBlack tfoot td {
            font-size: 14px;
        }

        .fixed {
            table-layout: fixed;
        }
    </style>
            <table id="" class="minimalistBlack fixed">
                <thead>
                <tr>
                    <th>Members</th>
                    <th>Status</th>
                    <th>Order</th>
                </tr>
                </thead>


                <tbody>
                <tr>
                    <td>Prosperis Gold</td>
                    <td>System</td>
                    <td>1</td>
                </tr>
                <?php foreach ($custom_product_invited_members as $cpim) { ?>
                    <tr>
                        <td><?= $cpim->name ?></td>
                        <td>
                            <?php if ($cpim->cpi_is_invitor == 1) {
                                echo "Initiator";
                            } else {
                                echo $cpim->status;
                            } ?>
                        </td>
                        <td><?= $cpim->cpi_inv_order ?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
<?php } ?>