<?php

$lang['page_title_text'] = 'Payment Method Choice';

$lang['breadcrum_home_text'] = 'Home';
$lang['breadcrum_page_text'] = 'Payment Method Choice';

$lang['box_title_text'] = 'Choose a payment method';

//form
$lang['paystack_text'] = 'Paystack';

$lang['payment_method_description_text'] = 'You need to select a payment method before thrifting';
$lang['choose_payment_method_text'] = 'Choose a payment method';

$lang['payment_method_one_time_auth_text'] = 'Please note a tiny one time authorization amount may be required';
$lang['flutterwave_text'] = 'Flutterwave';

$lang['submit_btn_text'] = 'Submit';

//validation

//flash
$lang['successful_text'] = 'Successful!';
$lang['unsuccessful_text'] = 'Unsuccessful!';









