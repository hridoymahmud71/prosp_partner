<?php

class Utility_model extends CI_Model
{

    public function __construct()
    {
        // parent::__construct();
        $this->load->helper('cookie');
    }

    public function setTargetInACookie()
    {
        //called form third party mx_loader

        $items = array(
            'login',
            'logout',
            'forgot_password',
            'reset_password',
            'load_style',
            'maintenance',
            'need_permission',
            'does_not_exist',
            'thrifter_registration',
            'insert_thrifter_registration',
            'do_verifiy_user',
            'get_employers_by_select2'
        );
		
        $segs = $this->uri->segment_array();

        $cookie_name = 'redirect_path_after_login';

        if ($this->session->userdata('user_id') == null) {

            if (!$this->in_array_any($items, $segs)) {

                $route = implode('/', $segs);
                setcookie($cookie_name, $route, time() + 3600, '/');
            }
        }

        if (in_array('logout', $segs)) {
            $this->deleteCookie($cookie_name);
        }

    }

    function in_array_any($needles, $haystack)
    {
        return count(array_intersect($needles, $haystack));
    }


    public function deleteCookie($cookie_name)
    {
        if (isset($_COOKIE[$cookie_name])) {
            unset($_COOKIE[$cookie_name]);
            setcookie($cookie_name, '', time() - 3600, '/');
        }

    }

    public function checkMaintenanceMode()
    {
        /*
         //by cron job
         $route['thrift_module/auto_assign_bot_admins'] = 'ThriftController/autoAssignBotAdmins';
         $route['thrift_module/auto_thrift_group_payment'] = 'ThriftController/autoThriftGroupPayment';
         $route['thrift_module/auto_start_custom_thrift_group'] = 'ThriftController/autoStartCustomThriftGroup';
         $route['thrift_module/send_reminder_to_thrifters'] = 'ThriftController/sendReminderToThrifters';
         //webhook
         $route['thrift_module/get_paystack_events_by_webhook'] = 'ThriftController/getPaystackEventsByWebhook';
        */

        $exeption_items =
            array('auto_assign_bot_admins',
                'auto_thrift_group_payment',
                'auto_start_custom_thrift_group',
                'send_reminder_to_thrifters',
                'get_paystack_events_by_webhook',
                'maintenance');

        $segs = $this->uri->segment_array();

        $exeption = false;
        if ($this->in_array_any($exeption_items, $segs)) {
            $exeption = true;
        }

        $this->load->library('settings_module/custom_settings_library');

        $under_maintenance = false;
        $partner_site_under_maintenance = $this->custom_settings_library->getASettingsValue('general_settings', 'partner_site_under_maintenance');
        if ($partner_site_under_maintenance) {
            if ($partner_site_under_maintenance == 'yes') {
                $under_maintenance = true;
            }
        }

        if (!$exeption && $under_maintenance) {
            redirect('users/auth/maintenance');
        }


    }

    //------------------------------------------------------------------------------------------------------------------

    public function getUserIdBySubdmain($subdomain)
    {

        $user_id = 0;

        $this->db->select('user_id');
        $this->db->from('rspm_tbl_user_details');
        $this->db->where('subdomain', $subdomain);

        $row = $this->db->get()->row();

        if ($row) {
            $user_id = $row->user_id;
        }

        return $user_id;
    }

    //----------------------------------------------lll
    public function getSubdomainByUserId($user_id)
    {
        $subdomain = "";

        $this->db->select('subdomain');
        $this->db->from('rspm_tbl_user_details');
        $this->db->where('user_id', $user_id);
        $row = $this->db->get()->row();

        if ($row) {
            $subdomain = $row->subdomain;
        }

        return $subdomain;
    }

    public function getEmployerSubdomain($user_id)
    {
        $employer_subdomain = "";

        $employer_id = $this->getUserEmployerId($user_id);

        if ($employer_id > 0) {
            $employer_subdomain = $this->getSubdomainByUserId($employer_id);
        }

        return $employer_subdomain;

    }

    public function makeThriftUrlWithEmployerSubdomain($url, $user_id)
    {
        $employer_subdomain = $this->getEmployerSubdomain($user_id);

        return $this->makeUrlWithSubdomain($url, $employer_subdomain);
    }

    public function makeUrlWithSubdomain($url, $subdomain)
    {
        $exploded_url = explode('//', $url);

        $join = $subdomain != "" ? "{$subdomain}." : "";

        return "{$exploded_url[0]}//{$join}{$exploded_url[1]}";
    }



    public function getUserEmployerId($user_id)
    {
        $user_employer_id = 0;

        $this->db->select('user_employer_id');
        $this->db->from('rspm_tbl_user_details');
        $this->db->where('user_id', $user_id);

        $row = $this->db->get()->row();

        if ($row) {
            $user_employer_id = $row->user_employer_id;
        }

        return $user_employer_id;
    }

    //----------------------------------------------lll

    public function getIndividualLogo($subdomain, $user_id)
    {
        if (!$user_id) {
            $user_id = $this->getUserIdBySubdmain($subdomain);
        }

        $individual_logo = $this->custom_settings_library->getAUserSettingsValue('individual_settings', 'individual_logo', $user_id);

        if ($individual_logo == null || $individual_logo == '') {
            $individual_logo = false;
        }

        return $individual_logo;
    }

    public function getSubdomain()
    {
        $url = current_url();
        $subdomain = "";
        $parsedUrl = parse_url($url);

        $host = explode('.', $parsedUrl['host']);

        $subdomains = array_slice($host, 0, count($host) - 2);

        if (!empty($subdomains)) {
            if (isset($subdomains[0])) {
                $subdomain = $subdomains[0];
            }
        }

        return $subdomain;
    }

}