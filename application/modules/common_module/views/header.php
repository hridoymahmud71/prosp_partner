<!DOCTYPE html>
<base href="<?php echo base_url() ?>">
<?php
$this->lang->load('common_left');
?>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta name="author" content="Coderthemes">
    <!-- App Favicon -->
    <link rel="shortcut icon" href="assets/custom_asset/favicon.ico">

    <!-- App title -->
    <?php if ($this->ion_auth->is_admin()) { ?>

        <title>Administration – Prosperis Gold</title>

    <?php } else if ($this->ion_auth->in_group('employee')) { ?>

        <title>Thrifting – Prosperis Gold</title>

    <?php } else if ($this->ion_auth->in_group('employer')) { ?>

        <title>Partners – Prosperis Gold</title>

    <?php } else if ($this->ion_auth->in_group('trustee')) { ?>

        <title>Trustee – Prosperis Gold</title>

    <?php } else { ?>

        <title>Prosperis</title>

    <?php } ?>

    <link rel="stylesheet" href="assets/backend_assets/css/main.css">
    <link rel="stylesheet" href="<?php echo base_url() . 'style_module/load_style/partner' ?>">
    <!-- Tour css -->
    <link href="assets/backend_assets/plugins/hopscotch/css/hopscotch.min.css" rel="stylesheet" type="text/css">


    <!-- Switchery css -->
    <!--    <link href="assets/backend_assets/plugins/switchery/switchery.min.css" rel="stylesheet"/>-->
    <!-- Custom box css -->
    <!--    <link href="assets/backend_assets/plugins/custombox/css/custombox.min.css" rel="stylesheet">-->
    <!-- Bootstrap CSS -->
    <!--    <link href="assets/backend_assets/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>-->
    <!-- App CSS -->
    <!--    <link href="assets/backend_assets/css/style.css" rel="stylesheet" type="text/css"/>-->


    <?php if ($this->ion_auth->is_admin()) { ?>

        <!--        <link href="assets/backend_assets/css/menu_office.css" rel="stylesheet" type="text/css"/>-->

    <?php } else if ($this->ion_auth->in_group('employee')) { ?>

        <!--        <link href="assets/backend_assets/css/menu_thrift.css" rel="stylesheet" type="text/css"/>-->

    <?php } else if ($this->ion_auth->in_group('employer')) { ?>

        <!--        <link href="assets/backend_assets/css/menu_partners.css" rel="stylesheet" type="text/css"/>-->

    <?php } else if ($this->ion_auth->in_group('trustee')) { ?>

        <!--        <link href="assets/backend_assets/css/menu_trustee.css" rel="stylesheet" type="text/css"/>-->

    <?php } else { ?>

        <!--        <link href="assets/backend_assets/css/menu.css" rel="stylesheet" type="text/css"/>-->

    <?php } ?>

    <!--    <link href="assets/backend_assets/css/helper.css" rel="stylesheet" type="text/css"/>-->
    <!-- Modernizr js -->
    <script src="assets/backend_assets/js/modernizr.min.js"></script>
    <!--Morris Chart CSS -->
    <!--    <link rel="stylesheet" href="assets/backend_assets/plugins/morris/morris.css">-->


    <!-- DataTables type="text/css"/>  -->
    <!--    <link href="assets/backend_assets/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css"/>-->

    <!-- Responsive datatable examples -->
    <!--    <link href="assets/backend_assets/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css"/>-->

    <!--jquery-->
    <script src="assets/backend_assets/js/jquery.min.js"></script>

    <!--bootstrap datepicker-->
    <!--    <link href="assets/backend_assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">-->

    <!--dropify-->
    <!--    <link href="assets/backend_assets/plugins/fileuploads/css/dropify.min.css" rel="stylesheet" type="text/css" />-->

    <!--select 2-->
    <script src="assets/backend_assets/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
    <!--    <link href="assets/backend_assets/plugins/multiselect/css/multi-select.css" rel="stylesheet" type="text/css"/>-->
    <!--    <link href="assets/backend_assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>-->

    <!--swal-->
    <!--    <link href="assets/backend_assets/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css"/>-->

    <!--daterangepicker-->
    <!--    <link href="assets/backend_assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">-->

    <!--tinymce-->
    <script src="assets/backend_assets/plugins/tinymce/js/tinymce/tinymce.min.js"></script>
    <script src="assets/backend_assets/plugins/tinymce/js/tinymce/jquery.tinymce.min.js"></script>

    <!--timepicker css-->
    <!--    <link href="assets/backend_assets/plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">-->

    <!--color picker-->
    <!--    <link href="assets/backend_assets/plugins/mjolnic-bootstrap-colorpicker/css/bootstrap-colorpicker.min.css" rel="stylesheet">-->


</head>
<body class="fixed-left">
<!-- Navigation Bar-->
<header id="topnav">
    <div class="topbar-main">
        <div class="container">

            <!-- LOGO -->
            <div class="topbar-left">
                <a href="index.php" class="logo">
                    <img id="img_logo" style="height:25px;">
                    <span id='site_name'></span>
                    <!--                    <i class="zmdi zmdi-group-work icon-c-logo"></i>-->
                    <!--                    <span>Uplon</span>-->
                </a>
            </div>
            <!-- End Logo container-->


            <div class="menu-extras navbar-topbar">

                <ul class="list-inline float-right mb-0">

                    <li class="list-inline-item">
                        <!-- Mobile menu toggle-->
                        <a class="navbar-toggle">
                            <div class="lines">
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                        </a>
                        <!-- End mobile menu toggle-->
                    </li>

                    <li id="noti" class="list-inline-item dropdown notification-list"></li>

                    <li id="tour-profile" class="list-inline-item dropdown notification-list">
                        <a class="nav-link dropdown-toggle waves-effect waves-light nav-user" data-toggle="dropdown"
                           href="#" role="button"
                           aria-haspopup="false" aria-expanded="false">
                            <img src="<?= base_url() ?>/base_demo_images/user_profile_image_demo.png" alt="user"
                                 id="user_image" class="rounded-circle">
                        </a>
                        <div class="dropdown-menu dropdown-menu-right profile-dropdown " aria-labelledby="Preview">
                            <!-- item-->
                            <div class="dropdown-item noti-title">
                                <h5 class="text-overflow">
                                    <small id="user_fullname">
                                        <!-- --><?php /*echo $this->session->userdata('email') */ ?>

                                        <?php if ($this->ion_auth->in_group('employer') && !$this->session->userdata('org_contact_user_id')) { ?>
                                            <?php echo $this->session->userdata('company') ?>
                                        <?php } else { ?>
                                            <?php echo $this->session->userdata('first_name') . ' ' . $this->session->userdata('last_name') ?>
                                        <?php } ?>
                                    </small>
                                </h5>
                            </div>

                            <!-- item-->
                            <a href="user_profile_module/user_profile_overview/<?= $this->session->userdata('org_contact_user_id') ? $this->session->userdata('org_contact_user_id') : $this->session->userdata('user_id') ?>"
                               class="dropdown-item notify-item">
                                <i class="zmdi zmdi-account-circle"></i> <span>Profile</span>
                            </a>

                            <!-- item-->
                            <a href="<?php echo base_url() . 'users/auth/logout' ?>" class="dropdown-item notify-item">
                                <i class="zmdi zmdi-power"></i> <span>Logout</span>
                            </a>

                        </div>
                    </li>

                </ul>

            </div> <!-- end menu-extras -->
            <div class="clearfix"></div>

        </div> <!-- end container -->
    </div>
    <!-- end topbar-main -->


    <div class="navbar-custom">
        <div class="container">
            <div id="navigation">
                <!-- Navigation Menu-->
                <ul class="navigation-menu">
                    <li>
                        <a href="index.php"><i class="zmdi zmdi-view-dashboard"></i> <span> Dashboard </span> </a>
                    </li>

                    <?php if ($this->ion_auth->is_admin() || $this->ion_auth->in_group('employee')) { ?>
                        <li id="tour-thrift-menu" class="has-submenu">
                            <a href="#"><i class="zmdi zmdi-collection-item"></i>
                                <span><?php echo lang('cl_menu_thrift_text') ?></span> </a>
                            <ul class="submenu">
                                <?php if ($this->ion_auth->is_admin()) { ?>
                                    <li>
                                        <a href="thrift_module/show_thrifts/all"><?php echo lang('cl_menu_all_thrift_text') ?></a>
                                    </li>

                                    <li>
                                        <a href="thrift_module/show_issue/all_issue/payment_issue/all"><?php echo lang('cl_menu_all_payment_issue_text') ?></a>
                                    </li>

                                    <li>
                                        <a href="thrift_module/show_issue/all_issue/payment_recieve_issue/all"><?php echo lang('cl_menu_all_payment_recieve_issue_text') ?></a>
                                    </li>
                                <?php } ?>

                                <?php if ($this->ion_auth->in_group('employee')) { ?>
                                    <li>
                                        <a href="thrift_module/show_thrifts/my"><?php echo lang('cl_menu_my_thrift_text') ?></a>
                                    </li>

                                    <li>
                                        <a href="thrift_module/custom_product_list/created"><?php echo lang('cl_menu_custom_product_create_list_text') ?></a>
                                    </li>

                                    <li>
                                        <a href="thrift_module/custom_product_list/recieved"><?php echo lang('cl_menu_custom_product_recieve_list_text') ?></a>
                                    </li>

                                    <li>
                                        <a href="thrift_module/add_custom_product"><?php echo lang('cl_menu_custom_product_add_text') ?></a>
                                    </li>

                                    <li>
                                        <a href="thrift_module/add_individual_product"><?php echo lang('cl_menu_individual_product_add_text') ?></a>
                                    </li>

                                    <li>
                                        <a href="thrift_module/show_issue/employee_issue_as_employee/payment_issue/all"><?php echo lang('cl_menu_my_payment_issue_text') ?></a>
                                    </li>

                                    <li>
                                        <a href="thrift_module/show_issue/employee_issue_as_employee/payment_recieve_issue/all"><?php echo lang('cl_menu_my_payment_recieve_issue_text') ?></a>
                                    </li>
                                <?php } ?>

                            </ul>
                        </li>
                    <?php } ?>

                    <?php if ($this->ion_auth->in_group('employee')) { ?>
                        <li id="tour-thrift-menu" class="has-submenu">
                            <a href="#"><i class="zmdi zmdi-collection-item"></i>
                                <span><?php echo lang('cl_menu_loan_text') ?></span></a>
                            <ul class="submenu">
                                <?php if ($this->ion_auth->in_group('employee')) { ?>
                                    <li>
                                        <a href="thrift_module/loan_product_list/created"><?php echo lang('cl_menu_loan_product_create_list_text') ?></a>
                                    </li>

                                    <li>
                                        <a href="thrift_module/loan_product_list/recieved"><?php echo lang('cl_menu_loan_product_recieve_list_text') ?></a>
                                    </li>

                                    <li>
                                        <a href="thrift_module/add_loan_product"><?php echo lang('cl_menu_loan_product_add_text') ?></a>
                                    </li>
                                <?php } ?>

                            </ul>
                        </li>
                    <?php } ?>


                    <?php if (!$this->ion_auth->in_group('employee') && !$this->ion_auth->in_group('trustee')) { ?>
                        <li class="has-submenu">
                            <?php if ($this->ion_auth->is_admin()) { ?>
                                <a href="#"><i class="zmdi zmdi-collection-item"></i>
                                    <span><?php echo lang('cl_menu_employer_dashboard_text') ?></span> </a>
                            <?php } ?>
                            <?php if ($this->ion_auth->in_group('employer')) { ?>
                                <a href="#"><i class="zmdi zmdi-collection-item"></i>
                                    <span><?php echo lang('cl_menu_employer_member_dashboard_text') ?></span> </a>
                            <?php } ?>
                            <ul class="submenu">
                                <?php if ($this->ion_auth->is_admin()) { ?>
                                    <!--<li>
                                        <a href="employer_module/contact_person_list/all"><?php /*echo lang('cl_menu_all_contact_person_text') */ ?></a>
                                    </li>-->
                                    <li>
                                        <a href="employer_module/all_unapproved_employee_info"><?php echo lang('cl_menu_all_unapproved_employee_text') ?></a>
                                    </li>
                                    <li>
                                        <a href="employer_module/all_employee_info"><?php echo lang('cl_menu_all_employee_text') ?></a>
                                    </li>
                                    <li>
                                        <a href="employer_module/add_employee_info"><?php echo lang('cl_menu_add_employee_text') ?></a>
                                    </li>
                                    <li>
                                        <a href="employer_module/add_employer_info"><?php echo lang('cl_menu_add_employer_text') ?></a>
                                    </li>
                                    <li>
                                        <a href="employer_module/all_employer_info"><?php echo lang('cl_menu_all_employer_view_text') ?></a>
                                    </li>
                                <?php } ?>
                                <?php if ($this->ion_auth->in_group('employer')) { ?>
                                    <?php if (!$this->session->userdata('org_contact_user_id')) { ?>
                                        <li>
                                            <a href="employer_module/contact_person_list/my"><?php echo lang('cl_menu_my_contact_person_text') ?></a>
                                        </li>
                                    <?php } ?>
                                    <li>
                                        <a href="employer_module/my_unapproved_employee_info"><?php echo lang('cl_menu_my_unapproved_employee_member_text') ?></a>
                                    </li>
                                    <li>
                                        <a href="employer_module/my_employee_info"><?php echo lang('cl_menu_my_employee_member_text') ?></a>
                                    </li>
                                    <li>
                                        <a href="employer_module/add_employee_info"><?php echo lang('cl_menu_add_employee_member_text') ?></a>
                                    </li>
                                <?php } ?>

                            </ul>
                        </li>
                    <?php } ?>
                    <?php if ($this->ion_auth->is_admin() && !$this->ion_auth->in_group('analyst')) { ?>
                        <li class="has-submenu">
                            <a href="#"><i class="zmdi zmdi-collection-item"></i>
                                <span><?php echo lang('cl_menu_product_text') ?></span> </a>
                            <ul class="submenu">
                                <li>
                                    <a href="product_module/all_product_info"><?php echo lang('cl_menu_all_product_text') ?></a>
                                </li>
                                <li>
                                    <a href="product_module/add_product_info"><?php echo lang('cl_menu_add_product_text') ?></a>
                                </li>
                            </ul>
                        </li>
                    <?php } ?>


                    <li class="has-submenu">
                        <a href="#"><i class="zmdi zmdi-collection-item"></i>
                            <?php if ($this->ion_auth->in_group('employee')) { ?>
                                <span><?php echo lang('cl_menu_report_and_statement_text') ?></span>
                            <?php } else { ?>
                                <span><?php echo lang('cl_menu_report_text') ?></span>
                            <?php } ?>
                        </a>
                        <ul class="submenu">
                            <?php if ($this->ion_auth->is_admin() || $this->ion_auth->in_group('trustee')) { ?>
                                <li>
                                    <a href="report_module/all_employer"><?php echo lang('cl_menu_all_employer_text') ?></a>
                                </li>
                                <li>
                                    <a href="report_module/employee/all"><?php echo lang('cl_menu_all_employee_text') ?></a>
                                </li>

                                <li>
                                    <a href="report_module/show_report/all_report/payment_report/all"><?php echo lang('cl_menu_all_payment_report_text') ?></a>
                                </li>
                                <li>
                                    <a href="report_module/show_report/all_report/payment_recieve_report/all"><?php echo lang('cl_menu_payment_recieve_report_text') ?></a>
                                </li>

                                <li>
                                    <a href="report_module/all_report/payment_report/years/all"><?php echo lang('cl_menu_payment_report_by_year_text') ?></a>
                                </li>
                                <li>
                                    <a href="report_module/all_report/payment_report/months/all_year"><?php echo lang('cl_menu_payment_report_by_month_text') ?></a>
                                </li>

                                <li>
                                    <a href="report_module/all_report/payment_recieve_report/years/all"><?php echo lang('cl_menu_payment_recieve_report_by_year_text') ?></a>
                                </li>
                                <li>
                                    <a href="report_module/all_report/payment_recieve_report/months/all_year"><?php echo lang('cl_menu_payment_recieve_report_by_month_text') ?></a>
                                </li>

                            <?php } ?>
                            <?php if ($this->ion_auth->in_group('employer')) { ?>
                                <li>
                                    <a href="report_module/employee/my"><?php echo lang('cl_menu_my_employee_text') ?></a>
                                </li>

                                <li>
                                    <a href="report_module/show_report/employer_report_as_employer/payment_report/all"><?php echo lang('cl_menu_my_payment_report_text') ?></a>
                                </li>
                                <li>
                                    <a href="report_module/show_report/employer_report_as_employer/payment_recieve_report/all"><?php echo lang('cl_menu_my_payment_recieve_report_text') ?></a>
                                </li>

                                <li>
                                    <a href="report_module/employer_report_as_employer/payment_report/years/all"><?php echo lang('cl_menu_payment_report_by_year_text') ?></a>
                                </li>
                                <li>
                                    <a href="report_module/employer_report_as_employer/payment_report/months/all_year"><?php echo lang('cl_menu_payment_report_by_month_text') ?></a>
                                </li>

                                <li>
                                    <a href="report_module/employer_report_as_employer/payment_recieve_report/years/all"><?php echo lang('cl_menu_payment_recieve_report_by_year_text') ?></a>
                                </li>
                                <li>
                                    <a href="report_module/employer_report_as_employer/payment_recieve_report/months/all_year"><?php echo lang('cl_menu_payment_recieve_report_by_month_text') ?></a>
                                </li>
                            <?php } ?>

                            <?php if ($this->ion_auth->in_group('employee')) { ?>
                                <li>
                                    <a href="report_module/show_report/employee_report_as_employee/payment_report/all"><?php echo lang('cl_menu_my_payment_report_text') ?></a>
                                </li>
                                <li>
                                    <a href="report_module/show_report/employee_report_as_employee/payment_recieve_report/all"><?php echo lang('cl_menu_my_payment_recieve_report_text') ?></a>
                                </li>

                                <li>
                                    <a href="report_module/employee_report_as_employee/payment_report/years/all"><?php echo lang('cl_menu_payment_report_by_year_text') ?></a>
                                </li>
                                <li>
                                    <a href="report_module/employee_report_as_employee/payment_report/months/all_year"><?php echo lang('cl_menu_payment_report_by_month_text') ?></a>
                                </li>

                                <li>
                                    <a href="report_module/employee_report_as_employee/payment_recieve_report/years/all"><?php echo lang('cl_menu_payment_recieve_report_by_year_text') ?></a>
                                </li>
                                <li>
                                    <a href="report_module/employee_report_as_employee/payment_recieve_report/months/all_year"><?php echo lang('cl_menu_payment_recieve_report_by_month_text') ?></a>
                                </li>
                            <?php } ?>
                        </ul>
                    </li>

                    <li class="has-submenu">
                        <a href="#"><i class="zmdi zmdi-album"></i>
                            <span><?php echo lang('cl_menu_message_text') ?></span> </a>
                        <ul class="submenu">
                            <li>
                                <a href="message_module/send_message"><?php echo lang('cl_menu_message_sub_menu_send_message_text') ?></a>
                            </li>
                            <li>
                                <a href="message_module/outbox"><?php echo lang('cl_menu_message_sub_menu_outbox_text') ?></a>
                            </li>
                            <li>
                                <a href="message_module/inbox"><?php echo lang('cl_menu_message_sub_menu_inbox_text') ?></a>
                            </li>

                        </ul>
                    </li>

                    <?php if ($this->ion_auth->is_admin()) { ?>
                        <li class="has-submenu">
                            <a href="#"><i class="zmdi zmdi-format-list-bulleted"></i>
                                <span><?php echo lang('cl_menu_users_text') ?></span> </a>
                            <ul class="submenu">
                                <li><a href="users/auth"><?php echo lang('cl_menu_users_sub_menu_users_text') ?></a>
                                </li>
                                <li>
                                    <a href="users/auth/create_user"><?php echo lang('cl_menu_users_sub_menu_add_user_text') ?></a>
                                </li>
                                <!-- <li><a href="users/auth/show_user_groups"><?php echo lang('cl_menu_users_sub_menu_groups_text') ?></a></li> -->
                            </ul>
                        </li>
                    <?php } ?>

                    <?php if ($this->ion_auth->is_admin() || $this->ion_auth->in_group('employer')) { ?>
                        <li class="has-submenu">
                            <a href="#"><i
                                        class="zmdi zmdi-collection-text"></i><span> <?php echo lang('cl_menu_settings_text') ?> </span>
                            </a>
                            <ul class="submenu">
                                <?php if ($this->ion_auth->in_group('superadmin')) { ?>
                                    <li>
                                        <a href="settings_module/general_settings"><?php echo lang('cl_menu_settings_sub_menu_general_settings_text') ?></a>
                                    </li>
                                    <li>
                                        <a href="settings_module/contact_settings"><?php echo lang('cl_menu_settings_sub_menu_contact_settings_text') ?></a>
                                    </li>
                                    <li>
                                        <a href="settings_module/currency_settings"><?php echo lang('cl_menu_settings_sub_menu_currency_settings_text') ?></a>
                                    </li>
                                    <li>
                                        <a href="settings_module/datetime_settings"><?php echo lang('cl_menu_settings_sub_menu_datetime_settings_text') ?></a>
                                    </li>
                                    <li>
                                        <a href="settings_module/prosperisgold_settings"><?php echo lang('cl_menu_settings_sub_menu_system_settings_text') ?></a>
                                    </li>
                                    <li>
                                        <a href="settings_module/payment_settings"><?php echo lang('cl_menu_settings_sub_menu_payment_settings_text') ?></a>
                                    </li>
                                    <li>
                                        <a href="email_template_module/view_templates"><?php echo lang('cl_menu_settings_sub_menu_email_templates_text') ?></a>
                                    </li>
                                <?php } ?>
                                <?php if ($this->ion_auth->is_admin()) { ?>
                                    <li>
                                        <a href="bank_module/all_bank_info"><?php echo lang('cl_menu_settings_sub_menu_all_bank_text') ?></a>
                                    </li>
                                    <li>
                                        <a href="bank_module/add_bank_info"><?php echo lang('cl_menu_settings_sub_menu_add_bank_text') ?></a>
                                    </li>
                                <?php } ?>
                                <?php if($this->ion_auth->in_group('employer')) { ?>
                                    <li>
                                        <a href="settings_module/individual_settings"><?php echo lang('cl_menu_settings_sub_menu_individual_settings_text') ?></a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </li>
                    <?php } ?>

                    <?php if ($this->ion_auth->is_admin()) { ?>
                        <li class="has-submenu">
                            <a href="#"><i class="zmdi zmdi-collection-item"></i>
                                <span><?php echo lang('cl_menu_log_text') ?></span> </a>
                            <ul class="submenu">
                                <li>
                                    <a href="log_module/view_log"><?php echo lang('cl_menu_view_log_text') ?></a>
                                </li>
                            </ul>
                        </li>
                    <?php } ?>

                    <!-- && false is hidng the module-->
                    <?php if ($this->ion_auth->is_admin() && false) { ?>
                        <li class="has-submenu">
                            <a href="#"><i class="zmdi zmdi-collection-item"></i>
                                <span><?php echo lang('cl_menu_content') ?></span> </a>
                            <ul class="submenu">
                                <li>
                                    <a href="content_module"><?php echo lang('cl_menu_content_faq') ?></a>
                                </li>
                                <li>
                                    <a href="content_module/all_news"><?php echo lang('cl_menu_content_news') ?></a>
                                </li>
                                <li>
                                    <a href="content_module/all_pages"><?php echo lang('cl_menu_content_pages') ?></a>
                                </li>
                            </ul>
                        </li>
                    <?php } ?>


                </ul>
                <!-- End navigation menu  -->
            </div>
        </div>
    </div>
</header>
<!-- End Navigation Bar-->


<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->

<div class="wrapper">
    <!--<div class="container">-->


    <script type="text/javascript">
        $(function () {

            $.ajax({
                url: '<?php echo base_url() . 'common_module/get_site_detail'?>',
                type: 'GET',
                success: function (data) {
                    var all_data = JSON.parse(data);

                    $('#site_name').html('');

                    if (all_data.site_logo) {
                        $('#img_logo').attr('src', '<?= $this->config->item('pg_upload_source_path') ?>' + 'image/' + all_data.site_logo);
                    }


                    if (!all_data.user_image[0]['user_profile_image'] || all_data.user_image[0]['user_profile_image'] == '' || all_data.user_image[0]['user_profile_image'] == null) {
                        $('#user_image').attr('src', '<?=base_url()?>' + 'base_demo_images/user_profile_image_demo.png');
                    }
                    else {
                        $('#user_image').attr('src', '<?= $this->config->item('pg_upload_source_path') ?>' + 'image/' + all_data.user_image[0]['user_profile_image']);
                    }


                }
            });

        });
    </script>

    <script type="text/javascript">
        $(function () {

            $.ajax({
                url: '<?php echo base_url() . 'common_module/get_notifications_by_ajax'?>',
                type: 'GET',
                success: function (noti) {
                    if (noti) {
                        $('#noti').html(noti);
                    }
                }
            });

        });
    </script>

    <!--check if user is active-->
    <script>
        $(function () {

            $.ajax({
                url: '<?php echo base_url() . 'common_module/check_if_user_is_active_with_ajax'?>',
                type: 'GET',
                success: function (res) {
                    // nothing to write
                }
            });

        });
    </script>

