<!--<div class="content-page">-->


<?php if ($this->session->flashdata('thrift_error')) { ?>
    <section class="content mt-0">
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert"
                            aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="alert-heading"><?php echo lang('unsuccessful_text') ?></h4>

                    <?php if ($this->session->flashdata('thrift_error_exceed_limit_text')) { ?>
                        <p>
                            <?php echo lang('thrift_error_exceed_limit_text') ?>
                        </p>
                    <?php } ?>

                    <?php if ($this->session->flashdata('thrift_error_only_employee_allowed_text')) { ?>
                        <p>
                            <?php echo lang('thrift_error_only_employee_allowed_text') ?>
                        </p>
                    <?php } ?>

                </div>
            </div>
        </div>
    </section>
<?php } ?>

<!-- Start content -->
<!--    <div class="content">-->
<div class="container">
    <div class="row">
        <div class="col-xl-12">
            <div class="page-title-box">
                <h4 class="page-title float-left">
                    <?php if ($is_admin == 'admin') echo lang('page_subtitle_admin_dashboard_text') ?>
                    <?php if ($is_trustee == 'trustee') echo lang('page_subtitle_trustee_dashboard_text') ?>
                    <?php if ($is_employer == 'employer') echo lang('page_subtitle_employer_dashboard_text') ?>
                    <?php if ($is_employee == 'employee') echo lang('page_subtitle_employee_dashboard_text') ?>
                </h4>

                <ol class="breadcrumb float-right">
                    <li class="breadcrumb-item"><a href=""><?php echo lang('breadcrum_home_text') ?></a></li>
                    <li class="breadcrumb-item active"><?php echo lang('breadcrum_page_text') ?></li>
                </ol>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!-- end row -->

    <?php if ($is_employer == 'employer') { ?>
        <hr>
        <div class="row">
            <div class="col-xs-12 col-md-6 col-lg-6 col-xl-3">
                <div class="card-box tilebox-one">
                    <i class="icon-people float-right text-muted"></i>
                    <h6 class="text-muted text-uppercase m-b-20"><?php echo lang('my_employees_text'); ?></h6>
                    <h2 class="m-b-20"
                        data-plugin="counterup"><?= $my_employees_count ? $my_employees_count : 0 ?></h2>
                    <a class="label label-warning"
                       href="employer_module/my_employee_info"><?php echo lang('see_my_employees_text'); ?></a>
                </div>
            </div>
            <div class="col-xs-12 col-md-6 col-lg-6 col-xl-3">
                <div class="card-box tilebox-one">
                    <i class="icon-support float-right text-muted"></i>
                    <h6 class="text-muted text-uppercase m-b-20"><?php echo lang('my_employees_thrifts_text') ?></h6>
                    <h2 class="m-b-20"
                        data-plugin="counterup"><?= $my_employees_running_thrift_group_count ? $my_employees_running_thrift_group_count : 0 ?></h2>
                    <a class="label label-warning"
                       href="thrift_module/show_thrifts/my_employees"><?php echo lang('see_employees_thrifts_text'); ?></a>
                </div>
            </div>
            <div class="col-xs-12 col-md-6 col-lg-6 col-xl-3">
                <div class="card-box tilebox-one">
                    <i class="icon-rocket float-right text-muted"></i>
                    <h6 class="text-muted text-uppercase m-b-20"><?php echo lang('thrift_percet_text') ?></h6>
                    <h2 class="m-b-20">
                        <?= $my_employee_percentage[0]['user_employer_thrift_percentage'] ?>
                        <span>%</span>
                    </h2>
                    <a class="label label-warning" style="cursor:pointer;" data-toggle="modal"
                       data-target="#percent_changing_modal"><?php echo lang('change_overall_percentage') ?></a>
                </div>

                <!-- Modal -->
                <div class="modal fade" id="percent_changing_modal" tabindex="-1" role="dialog"
                     aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <form action="common_module/update_employee_overall_percentage" method="post">
                                <div class="modal-header">
                                    <h5 class="modal-title"
                                        id="exampleModalLabel"><?php echo lang('change_overall_percentage') ?></h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group row">
                                        <label for="user_employer_id"
                                               class="col-sm-4 form-control-label"><?php echo lang('percentage_number_text') ?></label>
                                        <div class="col-sm-7">
                                            <input type="number" min="1" max="100" class="form-control"
                                                   name="user_employer_thrift_percentage"
                                                   placeholder="<?php echo lang('percentage_number_placeholder') ?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal"><?php echo lang('cancel_button_text') ?></button>
                                    <button type="submit"
                                            class="btn btn-primary"><?php echo lang('submit_button_text') ?></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-6 col-lg-6 col-xl-3">
                <div class="card-box tilebox-one">
                    <i class="icon-support float-right text-muted"></i>
                    <h6 class="text-muted text-uppercase m-b-20"><?php echo lang('member_management_text') ?></h6>
                    <h2 class="m-b-20"
                        data-plugin=""><?= $employer_pending_approval_count ? $employer_pending_approval_count : 0 ?>&nbsp;<?php echo lang('pending_approvals_text') ?></h2>
                    <a class="label label-warning"
                       href="employer_module/my_unapproved_employee_info"><?php echo lang('view_request_text'); ?></a>
                </div>
            </div>
        </div>
    <?php } ?>
    <!-- end row -->

    <div class="row" <?php if ($is_employer != 'employer') { ?> style="display: none" <?php } ?>>
        <div class="col-xs-12 col-md-9" > <!-- <div class="col-md-12 col-xs-12 col-sm-12"> -->
            <div class="card-box">
                <div class="p-20">
                    <canvas id="participating-employees-per-month" height="300" ></canvas>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-3">
            <div class="card-box">
                <h4 class="header-title m-t-0 m-b-20"><?= lang('inbox_text') ?> (<?= $count_messages ?>)</h4>
                <a href="message_module/inbox" type="button"
                   class="btn btn-info btn-rounded waves-effect waves-light"><?= lang('view_all_text') ?></a>

                <?php if ($messages_and_comments) { ?>
                <div class="inbox-widget nicescroll"
                     style="height: 250px;
                                 tabindex=" 5000>

                    <?php foreach ($messages_and_comments as $messages_and_comment) { ?>
                        <a href="<?= $messages_and_comment->url ?>">
                            <div class="inbox-item">
                                <!--<div class="inbox-item-img"><img src="assets/images/users/avatar-1.jpg"
                                                                 class="rounded-circle" alt=""></div>-->
                                <p class="inbox-item-author"><?= $messages_and_comment->sender ?></p>
                                <p class="inbox-item-text"><?= $messages_and_comment->text ?></p>
                                <p class="inbox-item-date"><?= $messages_and_comment->datestring ?></p>
                            </div>
                        </a>
                    <?php } ?>
                </div>
                <?php } ?>

                <?php if (!$messages_and_comments || empty($messages_and_comments)) { ?>
                    <p><?= lang('no_unread_messages_text') ?></p>
                <?php } ?>

            </div>

        </div><!-- end col-->
    </div>


</div>


<!-- Chart JS -->
<script src="assets/backend_assets/plugins/chart.js/chart.min.js"></script>

<!--Morris Chart-->
<script src="assets/backend_assets/plugins/morris/morris.min.js"></script>
<script src="assets/backend_assets/plugins/raphael/raphael-min.js"></script>


<script type="text/javascript">


    var canvas = document.getElementById('participating-employees-per-month');
    var data = {
        labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
        datasets: [
            {
                label: "Participating Employees",
                backgroundColor: "rgba(27,185,154,0.3)",
                borderColor: "#1bb99a",
                borderWidth: 1,
                hoverBackgroundColor: "rgba(27,185,154,0.6)",
                hoverBorderColor: "#1bb99a",
                data: <?php echo $bar_chrt;?>
            }
        ]
    };

    var emp_part_month = <?php echo $bar_chrt;?>;

    var option = {
        responsive: true,
        maintainAspectRatio: false,
        scales: {
            yAxes: [{
                ticks: {
                    max: Math.max(...emp_part_month
    ) ,
    min: 0,
        stepSize
    :
    5
    },
    stacked: true,
        gridLines
    :
    {
        display: true,
            color
    :
        "rgba(255,99,132,0.2)"
    }
    }],
    xAxes: [{
        gridLines: {
            display: false
        }
    }]
    }
    }
    ;

    var myBarChart = Chart.Bar(canvas, {
        data: data,
        options: option
    });


</script>