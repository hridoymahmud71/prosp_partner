<?php

$lang['page_title_text'] = 'Dashboard';

$lang['page_subtitle_admin_dashboard_text'] = 'Administrative Dashboard';

$lang['page_subtitle_trustee_dashboard_text'] = 'Trustee Dashboard';
$lang['page_subtitle_employer_dashboard_text'] = 'Partner Dashboard';
$lang['page_subtitle_employee_dashboard_text'] = 'Thrifter Dashboard';


$lang['breadcrum_home_text'] = 'Dashboard';
$lang['breadcrum_page_text'] = 'Dashboard View';

$lang['go_text'] = 'Go';

$lang['admin_section_text'] = 'Admin\'s Section';
$lang['my_section_text'] = 'My Section';

/*settings*/
$lang['settings_text'] = 'Settings';
$lang['general_settings_text'] = 'General Settings';
$lang['contact_settings_text'] = 'Contact Settings';
$lang['image_settings_text'] = 'Image Settings';
$lang['file_settings_text'] = 'File Settings';
$lang['currency_settings_text'] = 'Currency Settings';
$lang['datetime_settings_text'] = 'Date & Time Settings';
$lang['email_settings_text'] = 'Email Settings';


/*user*/
$lang['number_of_active_users_text'] = 'Active Users';
$lang['number_of_active_admins_text'] = 'Active Admins';

$lang['number_of_active_trustees_text'] = 'Active Trustees';
$lang['number_of_active_employers_text'] = 'Active Partners';
$lang['number_of_active_employees_text'] = 'Active Thrifters';

$lang['number_of_active_products_text'] = 'Active Products';

$lang['number_of_thrift_groups_text'] = 'Thrift Groups';

$lang['see_all_users_text'] = 'See All Users';


/*thrift*/
$lang['number_of_running_thifts_text']='Active Running Thrifts';
$lang['number_of_active_thrifts_volume_text']='Active Thrifts Volume';
$lang['number_of_total_thrifts_volume_text']='Average Thrifts Volume';
$lang['number_of_member_active_thrift_text']='Members in Active Thrift';



/*product*/

$lang['months_text'] = 'month(s)';
$lang['start_thrift_text'] = 'Start Now';
$lang['product_details_text'] = 'Product Details';


/*file*/
$lang['files_text'] = 'File(s)';
$lang['all_files_text'] = 'All Files';
$lang['my_files_text'] = 'My Files';

$lang['see_all_files_text'] = 'See All Files';
$lang['see_my_files_text'] = 'See My Files';

/*trustee-employer-employee*/

$lang['see_all_employers_text'] = 'See All Organizations';
$lang['see_all_employees_text'] = 'See All Thrifters';
$lang['see_all_trustees_text'] = 'See All Trustees';

$lang['all_employers_text'] = 'All Organizations';
$lang['all_employees_text'] = 'All Thrifters';
$lang['all_trustees_text'] = 'All Trustees';


// Employers dashboard

$lang['my_employees_text'] = 'My Employees';
$lang['see_my_employees_text'] = 'See My Thrifters';
$lang['my_employees_thrifts_text'] = 'My members\' Thrifts';
$lang['see_employees_thrifts_text'] = 'See My members\' Thrifts';

$lang['thrift_engaged_text'] = 'Thrift Engaged';
$lang['see_thrift_engaged_text'] = 'See all Thrift Engaged';

$lang['thrift_percet_text'] = 'Thrift Percentage';

$lang['my_employee_rating_text'] = 'Overall Ratings';
$lang['change_employee_rating_text'] = 'See Thrifter\'s Ratings';

$lang['member_management_text'] = 'Member Management';
$lang['pending_approvals_text'] = 'Pending approval(s)';
$lang['view_request_text'] = 'View Requests';



$lang['change_overall_percentage'] = 'Change overall percentage';
$lang['percentage_number_text'] = 'Percentage Number';
$lang['percentage_number_placeholder'] = 'example - 40';
$lang['submit_button_text'] = 'Save changes';
$lang['cancel_button_text'] = 'Close';


// Employees dashboard

$lang['my_available_products_text'] = 'Available Products';
$lang['see_available_products_text'] = 'See All Products';

$lang['employee_total_thrift_engaged_text'] = 'Total Thrift Engaged';
$lang['see_thrift_engaged_text'] = 'See all Thrift Engaged';

$lang['employee_personal_rating_text'] = 'Overall Ratings';
$lang['see_employee_profile_text'] = 'View my profile';

$lang['my_running_thrift_text'] = 'My Running Thrifts';
$lang['see_my_thrifts_text'] = 'See My Thrifts';


$lang['total_disbursment_text'] = 'Total Disbursement';
$lang['concluded_thrifts_text'] = 'Concluded Thrifts';
$lang['active_thrifts_text'] = 'Active Thrifts';
$lang['my_ratings_text'] = 'My Ratings';

$lang['next_payment_text'] = 'Next Payment';
$lang['next_disbursement_text'] = 'Next Disbursement';

$lang['quick_menu_text'] = 'Quick Menu';
$lang['review_my_thrift_text'] = 'Review My Thrift';
$lang['invite_colleagues_text'] = 'Invite Colleagues';
$lang['update_my_profile_text'] = 'Update My Profile';
$lang['view_my_statements_text'] = 'View My Statement';

$lang['not_joined_thrift_yet_text'] = 'You have not joined any thrifts yet';
$lang['view_all_thrifts_text'] = 'View all thrifts';

$lang['thrift_invitations_text'] = 'You have new thrift invitations';


/*trustee*/
$lang['number_of_last_month_payment_text'] = 'Last Month\'s Payment';
$lang['number_of_this_month_payment_text'] = 'This Month\'s Payment';
$lang['number_of_next_month_payment_text'] = 'Next Month\'s Payment';
$lang['number_of_total_paid_text'] = 'Total Paid';



$lang['performance_stat_text'] = 'Performance Statistic';
$lang['organization_trends_monthly_text'] = 'Organization Trends Monthly';
$lang['recent_thrift_text'] = 'Recent Thrifts';
$lang['from_previous_month_text'] = 'From Previous Month';
$lang['join_thrift_group_text'] = 'Join a thrift group';

/*custom product*/
$lang['custom_product_name_text'] = 'Group Thrift';
$lang['custom_product_start_text'] = 'Start a private thrift group';

$lang['individual_product_name_text'] = 'Personal Thrift';
$lang['individual_product_start_text'] = 'Start an individual savings';

$lang['collections_text'] = 'Collections';
$lang['disbursments_text'] = 'Disbursements';


/*flash*/
$lang['unsuccessful_text'] = 'Unsuccessful';
$lang['thrift_error_only_employee_allowed_text'] = 'Only Thrifters are allowed to join thrift';
$lang['thrift_error_exceed_limit_text'] = 'Exceeds thrift limit';



/*inbox*/
$lang['inbox_text'] = 'Inbox';
$lang['view_all_text'] = 'View All';
$lang['no_unread_messages_text'] = 'No Unread Messages';


/*extra*/
$lang['unavailable_text'] = 'Unavailable';



?>