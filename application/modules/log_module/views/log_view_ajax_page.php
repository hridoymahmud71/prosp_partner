
<?php if ($timeline_elements) { ?>

    <?php foreach ($timeline_elements as $a_timeline_element) {
        ?>
        <article class="timeline-item log-item">
            <div class="timeline-desk">
                <div class="panel">
                    <div class="timeline-box">
                        <span class="arr"></span>
                        <span class="timeline-icon <?php if ($a_timeline_element['is_today'] == 'today') {
                            echo ' bg-success ';
                        } elseif ($a_timeline_element['is_yesterday'] == 'yesterday') {
                            echo ' bg-warning ';
                        } else {
                            echo ' bg-danger ';
                        } ?>
                                                    ">
                                            <i class="zmdi zmdi-circle"></i>
                                        </span>
                        <h4 class="<?php if ($a_timeline_element['is_today'] == 'today') {
                            echo ' text-success ';
                        } elseif ($a_timeline_element['is_yesterday'] == 'yesterday') {
                            echo ' text-warning ';
                        } else {
                            echo ' text-danger ';
                        } ?>
                                                    ">
                            <?php if ($a_timeline_element['is_today'] == 'today') {
                                echo lang('today_text');
                            } elseif ($a_timeline_element['is_yesterday'] == 'yesterday') {
                                echo lang('yesterday_text');
                            } else {
                                echo $a_timeline_element['log_created_at_date'];
                            } ?>
                        </h4>
                        <p class="timeline-date text-muted">
                            <small><?php echo $a_timeline_element['log_created_at_time'] ?></small>
                        </p>
                        <p>  <?php echo $a_timeline_element['log_title_message'] ?> </p>

                        <?php
                        /* if $a_timeline_element['log_type'] == %_settings ' */
                        if ( strpos($a_timeline_element['log_type'], '_settings') !== false ) { ?>
                            <hr>
                            <p>
                                <a href="<?php echo base_url() . $a_timeline_element['log_type_route'] ?>"
                                   class="btn btn-primary btn-xs">
                                    <?php echo lang('go_text') ?>
                                </a>
                            </p>
                        <?php } ?>

                    </div>
                </div>
            </div>
        </article>
    <?php } ?>
<?php } ?>

