<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->

<div class="container">

    <div class="row">
        <div class="col-xl-12">
            <div class="page-title-box">
                <h4 class="page-title float-left">
                    <?php
                    if ($thrift_group_product) {
                        echo $thrift_group_product->product_name;
                    }
                    echo ' Thrift - ';
                    if ($thrift) {
                        echo $thrift->thrift_group_number;
                    }
                    ?>
                </h4>

                <ol class="breadcrumb float-right">
                    <li class="breadcrumb-item"><a href="#"><?= lang('breadcrum_thrift_text') ?></a></li>
                    <li class="breadcrumb-item active"><?= lang('breadcrum_thrift_details_text') ?></li>
                </ol>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!-- end row -->

    <div class="row">
        <div class="col-12">
            <div class="card-box">

                <div class="row m-t-0">
                    <div class="col-sm-12 col-xs-12 col-md-12 col-lg-6 m-t-20">
                        <h4 class="header-title m-t-0"><?= lang('thrift_details_text') ?></h4>
                        <p class="text-muted font-13 m-b-10">
                        </p>

                        <div class="p-20">
                            <table class="table table-sm">
                                <thead>
                                <tr>
                                    <th><?= lang('key_text') ?></th>
                                    <th><?= lang('val_text') ?></th>

                                </tr>
                                </thead>
                                <tbody>

                                <?php if ($thrift) { ?>
                                    <tr>
                                        <th scope="row"><?= lang('thrift_group_number_text') ?></th>
                                        <td><?= $thrift->thrift_group_number ?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row"><?= lang('thrift_group_member_count_text') ?></th>
                                        <td><?= $thrift->thrift_group_member_count ?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row"><?= lang('thrift_group_member_limit_text') ?></th>
                                        <td><?= $thrift->thrift_group_member_limit > 0 ? $thrift->thrift_group_member_limit : lang('unavailable_text') ?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row"><?= lang('thrift_group_term_duration_text') ?></th>
                                        <td><?= $thrift->thrift_group_term_duration > 0 ? $thrift->thrift_group_term_duration : lang('unavailable_text') ?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row"><?= lang('thrift_group_current_cycle_text') ?></th>
                                        <td><?= $thrift->thrift_group_current_cycle ?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row"><?= lang('thrift_group_activation_status_text') ?></th>
                                        <td>
                                            <?php
                                            if ($thrift->thrift_group_activation_status == 1) {
                                                echo lang('yes_text');
                                            } else {
                                                echo lang('no_text');
                                            }
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row"><?= lang('thrift_group_open_text') ?></th>
                                        <td>
                                            <?php
                                            if ($thrift->thrift_group_open == 1) {
                                                echo lang('yes_text');
                                            } else {
                                                echo lang('no_text');
                                            }
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row"><?= lang('thrift_group_creation_date_text') ?></th>
                                        <td><?= $thrift->thrift_group_creation_datestring ?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row"><?= lang('thrift_group_start_date_text') ?></th>
                                        <td><?= $thrift->thrift_group_start_datestring ?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row"><?= lang('thrift_group_end_date_text') ?></th>
                                        <td><?= $thrift->thrift_group_end_datestring ?></td>
                                    </tr>

                                    <tr>
                                        <th scope="row"><?= lang('thrift_group_this_month_payment_text') ?></th>
                                        <td><?= $thrift->thrift_group_this_month_payment ?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row"><?= lang('thrift_group_this_month_disbursement_text') ?></th>
                                        <td><?= $thrift->thrift_group_this_month_disbursement ?></td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <?php if ($custom_product_invited_members) { ?>
                        <div class="col-sm-12 col-xs-12 col-md-12 col-lg-6 m-t-20">
                            <div class="card-box table-responsive">
                                <h4 class="m-t-0 header-title"><b><?= lang('invited_members_text') ?></b></h4>


                                <table id="member-table" class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th><?= lang('members_text') ?></th>
                                        <th><?= lang('status_text') ?></th>
                                    </tr>
                                    </thead>


                                    <tbody>
                                    <?php if ($custom_product_invitor) { ?>
                                        <tr>
                                            <td><?= $custom_product_invitor ?></td>
                                            <td><?= lang('initiator_text') ?></td>
                                        </tr>
                                    <?php } ?>
                                    <?php foreach ($custom_product_invited_members as $cpim) { ?>
                                        <tr>
                                            <td><?= $cpim->name ?></td>
                                            <td><?= $cpim->status ?></td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    <?php } ?>


                </div>
                <!-- end row -->

                <div class="row m-t-50">

                    <div class="col-12 m-t-20">
                        <h4 class="header-title m-t-0"><?= lang('member_details_text') ?></h4>
                        <p class="text-muted font-13 m-b-10">

                        </p>

                        <div class="p-20">
                            <div class="">
                                <table id="" class="table table-bordered table-responsive table-striped">
                                    <thead>
                                    <tr>
                                        <th><?= lang('disbursement_order_text') ?></th>
                                        <th><?= lang('mem_id_num_text') ?></th>
                                        <!--<th><? /*= lang('member_name_text') */ ?></th>-->
                                        <th><?= lang('employer_name_text') ?></th>
                                        <th><?= lang('member_join_date_text') ?></th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    <?php if ($thrift_members) { ?>
                                        <?php foreach ($thrift_members as $tm) { ?>
                                            <tr>

                                                <td><?= $tm->thrift_group_member_number ?></td>
                                                <th scope="row"><?= $tm->mem_id_num ?></th>
                                                <!--<th scope="row"><? /*= $tm->member_full_name */ ?></th>-->
                                                <td><?= $tm->employer_company ?></td>
                                                <td><?= $tm->thrift_group_member_join_datestring ?></td>
                                            </tr>
                                        <?php } ?>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- end row -->


                <div class="row m-t-50">

                    <div class="col-12 m-t-20">
                        <h4 class="header-title m-t-0"><?= lang('payments_and_disbursement_text') ?></h4>
                        <p class="text-muted font-13 m-b-10">

                        </p>

                        <div class="p-20">
                            <div class="">
                                <table id="payment-table" class="table table-bordered table-responsive table-striped">
                                    <thead>
                                    <tr>
                                        <th><?= lang('cycle_number_text') ?></th>
                                        <th><?= lang('payee_or_payer_text') ?></th>
                                        <th><?= lang('mem_id_num_text') ?></th>
                                        <!--<th><? /*= lang('member_name_text') */ ?></th>-->
                                        <th><?= lang('payment_amount_text') ?></th>
                                        <th><?= lang('thrift_order_text') ?></th>
                                        <!--<th><? /*= lang('payment_paid_text') */ ?></th>
                                                <th><? /*= lang('payment_recieved_text') */ ?></th>-->
                                        <th><?= lang('payment_date_text') ?></th>
                                        <th><?= lang('payment_status_text') ?></th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    <?php if ($combined_payment_tables) { ?>
                                        <?php foreach ($combined_payment_tables as $cpt) { ?>
                                            <tr>

                                                <td><?= $cpt->thrift_group_payment_cycle_number ?></td>
                                                <th scope="row"><?= $cpt->thrift_group_payee_or_payer ?></th>
                                                <th scope="row"><?= $cpt->mem_id_num ?></th>
                                                <!--<th scope="row"><? /*= $cpt->member_full_name */ ?></th>-->
                                                <td><?= $cpt->thrift_group_payment_amount_text ?></td>
                                                <td><?= $cpt->thrift_group_member_number ?></td>
                                                <!--<td>
                                                            <?php /*if ($cpt->thrift_group_is_payment_paid == 1) {
                                                                echo lang('yes_text');
                                                            } else {
                                                                echo lang('no_text');
                                                            }
                                                            */ ?>
                                                        </td>
                                                        <td>
                                                            <?php /*if ($cpt->thrift_group_is_payment_recieved == 1) {
                                                                echo lang('yes_text');
                                                            } else {
                                                                echo lang('no_text');
                                                            }
                                                            */ ?>
                                                        </td>-->
                                                <td data-sort="<?= $cpt->thrift_group_payment_date ?>"><?= $cpt->thrift_group_payment_datestring ?></td>
                                                <td>
                                                    <?php if ($cpt->thrift_group_payee_or_payer_another_var == 'payer' && $cpt->thrift_group_is_payment_paid == 1) {
                                                        echo lang('payment_completed_text');
                                                    } else if ($cpt->thrift_group_payee_or_payer_another_var == 'payee' && $cpt->thrift_group_is_payment_recieved == 1) {
                                                        echo lang('payment_completed_text');
                                                    } else {
                                                        echo lang('payment_scheduled_text');
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- end row -->

            </div>
        </div><!-- end col-->

    </div>
    <!-- end row -->


</div> <!-- container -->


<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->

<style>
    table.dataTable thead .sorting:before, table.dataTable thead .sorting_asc:before, table.dataTable thead .sorting_desc:before, table.dataTable thead .sorting_asc_disabled:before, table.dataTable thead .sorting_desc_disabled:before {
        right: unset;
    }
</style>

<script>
    $(function () {

        $('#payment-table').dataTable();

    });
</script>