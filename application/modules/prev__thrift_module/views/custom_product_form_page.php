<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->


<!--clearing the extra arrow-->
<style>
    table.dataTable thead .sorting:before, table.dataTable thead .sorting_asc:before, table.dataTable thead .sorting_desc:before, table.dataTable thead .sorting_asc_disabled:before, table.dataTable thead .sorting_desc_disabled:before {
        right: unset;
    }
</style>
<div class="container">

    <div class="row">
        <div class="col-xl-12">
            <div class="page-title-box">

                <?php if ($which_form == 'add') { ?>
                    <h4 class="page-title float-left"><?= lang('page_title_add_text') ?></h4>
                <?php } ?>

                <?php if ($which_form == 'edit') { ?>
                    <h4 class="page-title float-left"><?= lang('page_title_edit_text') ?></h4>
                <?php } ?>

                <?php if ($which_form == 'view') { ?>
                    <h4 class="page-title float-left"><?= lang('page_title_view_text') ?></h4>
                <?php } ?>

                <ol class="breadcrumb float-right">
                    <li class="breadcrumb-item"><a href="/"><?= lang('breadcrum_home_text') ?></a></li>
                    <li class="breadcrumb-item active"><?= lang('breadcrum_page_text') ?></li>
                </ol>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!-- end row -->

    <!--<pre>
        <?php /*print_r($cp_inv_mems) */ ?>
    </pre>
    <pre>
        <?php /*print_r($cp_acc_mems) */ ?>
    </pre>
    <pre>
        <?php /*print_r($cp_dec_mems) */ ?>
    </pre>-->

    <?php if ($which_form == 'view' && $cp_inv_mems && !empty($cp_inv_mems)) { ?>
        <?php if (in_array($this->session->userdata('user_id'), $cp_inv_mems)) { ?>
            <div class="row">
                <div class="col-md-12">

                    <div class="card m-b-20">
                        <div class="card-body">
                            <h4 class="card-title"><?= lang('invitation_card_title_text') ?></h4>
                            <h6 class="card-subtitle text-muted">
                                <?= lang('invitation_card_invited_by_text') ?>:
                                <?php if ($custom_product_invitation) { ?>
                                    <a href="<?= base_url() . 'user_profile_module/user_profile_overview/' . $custom_product_invitation->cpi_created_by ?>">
                                        <?= $invitor_name ?>
                                    </a>
                                <?php } ?>
                            </h6>
                            <hr>
                            <h6 class="card-subtitle text-muted">
                                <?= lang('invitation_card_status_text') ?> :
                                <?php
                                $p = false;
                                $a = false;
                                $d = false;
                                if (!(in_array($this->session->userdata('user_id'), $cp_acc_mems)
                                    || in_array($this->session->userdata('user_id'), $cp_dec_mems))
                                ) {
                                    echo lang('pending_text');
                                    $p = true;
                                } else if (in_array($this->session->userdata('user_id'), $cp_acc_mems)
                                    && !in_array($this->session->userdata('user_id'), $cp_dec_mems)
                                ) {
                                    echo lang('accepted_text');
                                    $a = true;
                                } else if (!in_array($this->session->userdata('user_id'), $cp_acc_mems)
                                    && in_array($this->session->userdata('user_id'), $cp_dec_mems)
                                ) {
                                    echo lang('declined_text');
                                    $d = true;
                                }

                                ?>

                            </h6>
                        </div>
                        <div class="card-body">
                            <p class="card-text"><?= lang('invitation_card_description_text') ?></p>
                            <?php if ($cpi_id) { ?>

                                <?php if ($p) { ?>
                                    <a href="thrift_module/accept_custom_product/<?= $cpi_id ?>"
                                       class="card-link"><?= lang('invitation_card_accept_text') ?></a>
                                    <a href="thrift_module/decline_custom_product/<?= $cpi_id ?>"
                                       class="card-link"><?= lang('invitation_card_decline_text') ?></a>
                                <?php } else if ($a) { ?>
                                    <a href="thrift_module/decline_custom_product/<?= $cpi_id ?>"
                                       class="card-link"><?= lang('invitation_card_decline_text') ?></a>
                                <?php } else if ($d) { ?>
                                    <a href="thrift_module/accept_custom_product/<?= $cpi_id ?>"
                                       class="card-link"><?= lang('invitation_card_accept_text') ?></a>
                                <?php } ?>


                            <?php } ?>
                        </div>
                    </div>

                </div><!-- end col -->
            </div>
        <?php } ?>
    <?php } ?>

    <?php if ($which_form != 'add' && $custom_product_invited_members) { ?>
        <div class="row">
            <div class="col-12">
                <div class="card-box table-responsive">
                    <h4 class="m-t-0 header-title"><b><?= lang('member_box_title_text') ?></b></h4>


                    <table id="member-table" class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th><?= lang('members_text') ?></th>
                            <th><?= lang('status_text') ?></th>
                        </tr>
                        </thead>


                        <tbody>

                        <?php if ($invitor_name) { ?>
                            <?php if ($invitor_name != '') { ?>
                                <tr>
                                    <td><?= $invitor_name ?></td>
                                    <td><?= lang('initiator_text') ?></td>
                                </tr>
                            <?php } ?>
                        <?php } ?>
                        <?php foreach ($custom_product_invited_members as $cpim) { ?>
                            <tr>
                                <td><?= $cpim->name ?></td>
                                <td><?= $cpim->status ?></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div> <!-- end row -->
    <?php } ?>

    <?php if ($this->session->flashdata('successful')) { ?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <strong><?= lang('successful_text') ?></strong>
            <?php

            if ($this->session->flashdata('custom_product_create_success')) {
                echo lang('custom_product_create_success');
            }

            if ($this->session->flashdata('custom_product_update_success')) {
                echo lang('custom_product_update_success');
            }
            ?>

            <?php if ($this->session->flashdata('flash_cpi_id')) { ?>
                <a href="<?php echo base_url() . 'thrift_module/custom_product_thrift/view/' . $this->session->flashdata('flash_cpi_id') ?>">
                    <?php echo lang('view_custom_product_thrift_text'); ?>
                </a>
            <?php } ?>

        </div>
    <? } ?>

    <?php if ($this->session->flashdata('unsuccessful')) { ?>
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <strong><?= lang('unsuccessful_text') ?></strong>
            <?php

            if ($this->session->flashdata('validation_errors')) {
                echo $this->session->flashdata('validation_errors');
            }

            if ($this->session->flashdata('atleast_need_one_colleague')) {
                echo lang('atleast_need_one_colleague_text');
            }
            ?>

            <?php if ($this->session->flashdata('thrift_error_no_employer_text')) { ?>

                <?php echo lang('thrift_error_no_employer_text') ?>

            <?php } ?>

            <?php if ($this->session->flashdata('flash_thrift_percentage_error')) { ?>
                <?php echo sprintf($this->lang->line('flash_thrift_percentage_error_text'), $this->session->flashdata('flash_thrift_percentage_error')); ?>
            <?php } ?>

        </div>
    <? } ?>


    <div class="row">
        <div class="col-12">
            <div class="card-box">

                <h4 class="header-title m-t-0 m-b-30">
                    <?php if ($which_form == 'add') { ?>
                        <?= lang('box_title_add_text') ?>
                    <?php } ?>

                    <?php if ($which_form == 'edit') { ?>
                        <?= lang('box_title_edit_text') ?>
                    <?php } ?>

                    <?php if ($which_form == 'view') { ?>
                        <?= lang('box_title_view_text') ?>
                    <?php } ?>
                </h4>


                <div class="row">
                    <div class="col-md-12">
                        <form id="#custom_produc_-form" action="<?= $form_action ?>" method="post"
                              enctype="multipart/form-data">

                            <?php if ($which_form != 'view') { ?>
                                <fieldset class="form-group w-50">
                                    <label for="select_employer"><?php echo lang('select_colleagues_text') ?></label>
                                    <select required multiple="multiple" class="form-control select_colleagues select2"
                                            style="width: 100%" name="select_colleagues[]"
                                            id="">
                                        <?php if ($custom_product_accepted_members) { ?>

                                            <?php foreach ($custom_product_accepted_members as $cpam) { ?>

                                                <option value="<?= $cpam->cpi_inv_to ?>"
                                                        selected="selected"><?= $cpam->name ?></option>
                                            <?php } ?>

                                        <?php } ?>

                                        <?php $selected_colleagues = false ?>
                                        <?php if ($this->session->flashdata('flash_selected_ids')) {
                                            $sc = $this->session->flashdata('flash_selected_ids');

                                            if ($sc && !empty($sc)) {
                                                $selected_colleagues = $this->Thrift_model->getUsers($sc);
                                            }
                                        } ?>

                                        <?php if ($selected_colleagues) { ?>

                                            <?php foreach ($selected_colleagues as $a_sc) { ?>

                                                <option value="<?= $a_sc->id ?>"
                                                        selected="selected"><?= $a_sc->first_name ? $a_sc->first_name : '' ?> <?= $a_sc->last_name ? $a_sc->last_name : '' ?>
                                                    (<?= $a_sc->email ? $a_sc->email : '' ?>)
                                                </option>
                                            <?php } ?>

                                        <?php } ?>
                                    </select>
                                </fieldset>
                            <?php } ?>

                            <fieldset class="form-group">
                                <label><?= lang('thrift_amount_text') ?> <?= lang('per_person_per_month_text') ?></label>
                                <div class="input-group">
                                    <span class="input-group-addon"><?= $currency_sign ?> </span>
                                    <input <?php if ($which_form == 'view') { ?> readonly="readonly" <?php } ?>
                                            name="product_price" class="form-control" style="line-height:1.5"
                                            placeholder="<?= lang('thrift_amount_text') ?>"
                                            type="number"
                                            min="0.01" step="0.01"
                                            required
                                            value="<?= $custom_product_invitation ? $custom_product_invitation->cpi_product_price : '' ?><?= $this->session->flashdata('flash_product_price') ? $this->session->flashdata('flash_product_price') : '' ?>">
                                </div>
                            </fieldset>

                            <fieldset class="form-group">
                                <label><?= lang('set_start_date_text') ?></label>
                                <div>
                                    <div class="input-group">
                                        <span class="input-group-addon bg-custom b-0"><i
                                                    class="icon-calender"></i></span>
                                        <input readonly="readonly" type="text" name="start_date" class="form-control"
                                               placeholder="<?= lang('set_start_date_text') ?>" id="start_date"
                                               required
                                               value="<?php
                                               if ($custom_product_invitation) {
                                                   if ($custom_product_invitation->cpi_start_date != 0) {
                                                       echo $custom_product_invitation->cpi_start_datestring;
                                                   }
                                               }
                                               ?><?= $this->session->flashdata('flash_start_date') ? $this->session->flashdata('flash_start_date') : '' ?>"
                                        >

                                    </div><!-- input-group -->
                                </div>
                            </fieldset>

                            <?php if ($which_form == 'edit') { ?>
                                <br><br>
                                <hr>
                                <fieldset class="form-group row">

                                    <div style="padding-left: 1%">
                                        <div>
                                            <input id="start_thrift" type="checkbox" name="start_thrift">
                                            <label for="">
                                                <?= lang('start_thrift_long_text') ?>
                                            </label>
                                        </div>
                                    </div>
                                </fieldset>
                                <hr>
                            <?php } ?>

                            <?php if ($which_form == 'add') { ?>
                                <button type="submit"
                                        class="btn btn-primary"><?= lang('submit_btn_create_thrift_text') ?></button>
                            <?php } ?>

                            <?php if ($which_form == 'edit') { ?>
                                <button type="submit"
                                        class="btn btn-primary"><?= lang('submit_btn_update_text') ?></button>
                            <?php } ?>

                        </form>
                    </div><!-- end col -->


                </div><!-- end row -->
            </div>
        </div><!-- end col -->
    </div>


</div> <!-- container -->


<!-- placeholder: "<?= lang('select_colleagues_text') ?>", -->

<!-- End content-page -->


<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->

<script>
    $(function () {


        $('.select_colleagues').select2({


            allowClear: true,

            "language": {
                "noResults": function () {
                    var btn_html = "<?= lang('no_colleaguse_found_text')?>";
                    var div = document.createElement("div");
                    div.innerHTML = btn_html;
                    return div;
                }
            },

            minimumInputLength: 1,
            ajax: {
                url: '<?= base_url() ?>thrift_module/get_colleagues_by_select2',
                dataType: 'json',
                cache: true,
                delay: 1000,
                allowClear: true,


                data: function (params) {

                    return {
                        choose_from_colleagues: '<?= $choose_from_colleagues ? 'yes' : 'no'?>',
                        keyword: params.term, // search term
                        page: params.page,
                    };
                },
                processResults: function (data, params) {

                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;

                    //console.log(data.items);

                    return {
                        results: data.items,
                        pagination: {
                            more: data.more_pages
                        }
                    };
                },

                /*templateResult: formatRepo,
                 templateSelection: formatRepoSelection,*/

                escapeMarkup: function (markup) {
                    return markup;
                } // let our custom formatter work


            }

        });

        $('#select_colleagues').val('selected').trigger('change');




        <?php if($which_form != 'view') { ?>

        $('#start_date').datepicker({
            autoclose: true,
            disableEntry: true,
            format: "yyyy-mm-dd",
            todayHighlight: true,
            startDate: "<?= $custom_thrift_start_delay?>",
            endDate: "<?= $custom_thrift_end_delay?>"
        });

        <?php } ?>

        $('#member-table').DataTable();


    });
</script>
