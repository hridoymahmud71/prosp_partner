<?php

$lang['page_title_text'] = 'Private Thrifting';
$lang['breadcrum_home_text'] = 'Home';
$lang['breadcrum_page_text'] = 'Invitations sent';


$lang['box_title_text'] = 'Invitations sent';

$lang['add_custom_product_text'] = 'New Private Thrift';


//ok
$lang['successful_text'] = 'Successful !';
$lang['thrift_delete_success_text'] = 'Successfully deleted the thrift group and corresponding invitations';

/*Column names of the table*/
$lang['toggle_column_text'] = 'Toggle Columns';

$lang['option_all_text'] = 'All';
$lang['option_archived_text'] = 'Yes';
$lang['option_non_archived_text'] = 'No';


$lang['column_created_at_text'] = 'Time';
$lang['column_sender_text'] = 'Sender';
$lang['column_cpi_number_text'] = 'CPI Number';
$lang['column_thrift_group_number_text'] = 'Thrift Group ID';


$lang['column_sent_to_text'] = 'Sent To';
$lang['column_accepted_by_text'] = 'Accepted By';
$lang['column_pending_text'] = 'Pending';


$lang['column_product_price_text'] = 'Monthly Amount';

$lang['column_actions_text'] = 'Actions';

$lang['status_archive_text'] = 'Archived';
$lang['status_non_archive_text'] = 'Not Archived';

/*swal texts*/
$lang['swal_title_text'] = 'Are You Sure To Delete This Product ?';
$lang['swal_confirm_button_text'] = 'yes delete this Product';
$lang['swal_cancel_button_text'] = 'No, keep this Product';




$lang['archive_success_text'] = 'Message archived';
$lang['dearchive_success_text'] = 'Message removed from archive';

$lang['read_success_text'] = 'Message marked as read';
$lang['unread_success_text'] = 'Message marked as unread';

$lang['creation_time_unknown_text'] = 'Unknown';

/*tooltip text*/
$lang['tooltip_archive_text'] = 'Make Message Acrchived';
$lang['tooltip_dearchive_text'] = 'Make Message Non Archived';

$lang['tooltip_view_text'] = 'View Custom Product ';
$lang['tooltip_edit_text'] = 'Edit Custom Product ';
$lang['tooltip_delete_text'] = 'Delete Custom Product ';

/*loading*/
$lang['loading_text'] = 'Loading . . .';


