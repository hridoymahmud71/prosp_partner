<?php


$lang['page_title_add_text'] = 'Private Thrifting';
$lang['page_title_edit_text'] = 'Private Thrifting';
$lang['page_title_view_text'] = 'Private Thrifting';

$lang['breadcrum_home_text'] = 'Home';
$lang['breadcrum_page_text'] = 'Private Thrifting';


$lang['box_title_add_text'] = 'Enter Thrift Details';
$lang['box_title_edit_text'] = 'Edit Details';
$lang['box_title_view_text'] = 'View Details';

/*invitation*/
$lang['invitation_card_title_text'] = 'Private Thrift Invitation';
$lang['invitation_card_invited_by_text'] = 'Invited By';
$lang['invitation_card_status_text'] = 'Status';


$lang['invitation_card_description_text'] = 'You are invited to join this thrift';
$lang['invitation_card_accept_text'] = 'Accept';
$lang['invitation_card_decline_text'] = 'Decline';


/*member table*/
$lang['member_box_title_text'] = 'Invited Members';
$lang['members_text'] = 'Members';
$lang['status_text'] = 'Status';


//form
$lang['select_colleagues_text'] = 'Select colleagues/members';
$lang['no_colleaguse_found_text'] = 'No colleague/member is found';


$lang['thrift_amount_text'] = 'Thrift Amount';
$lang['per_person_per_month_text'] = 'Per Person/Per Month';

$lang['set_start_date_text'] = 'Set start date';



//chk
$lang['start_thrift_text'] = 'Start Thrift';
$lang['start_thrift_long_text'] = 'Start a thrift with this custom product';



$lang['submit_btn_add_text'] = 'Add';
$lang['submit_btn_create_thrift_text'] = 'Create Thrift';
$lang['submit_btn_update_text'] = 'Update';
//validation

$lang['atleast_need_one_colleague_text'] = 'At least one colleague/member need to be selected';

$lang['initiator_text'] = 'Initiator';
$lang['accepted_text'] = 'Accepted';
$lang['pending_text'] = 'Pending';
$lang['declined_text'] = 'Declined';

//flash
$lang['successful_text'] = 'Successful !';
$lang['unsuccessful_text'] = 'Unsuccessful !';

$lang['view_custom_product_thrift_text'] = 'See here';

$lang['custom_product_create_success'] = 'Successfully created custom product';
$lang['custom_product_update_success'] = 'Successfully updated custom product';

$lang['flash_thrift_percentage_error_text'] = 'Your monthly salary must be greater than %s%% of the monthly contribution amount to join this thrift';

$lang['thrift_error_no_employer_text'] = 'You do not belong to any organization';











