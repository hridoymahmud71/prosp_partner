<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ThriftController extends MX_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->library('email');

        $this->load->library('settings_module/custom_settings_library');
        $this->load->library('custom_datetime_library');
        $this->load->library('custom_log_library');
        $this->load->library('payment_module/custom_payment_library');

        $this->load->model('thrift_module/Thrift_model');

        set_time_limit(400);
    }

    public function ggbb()
    {
        $st = '1517305915';
        $dt = $this->custom_datetime_library
            ->convert_and_return_TimestampToDateAndTime($st);

        echo $dt;
    }

    private function inter_org_thrift_allowed()
    {
        $allow_inter_org_thrift = $this->custom_settings_library->getASettingsValue('prosperisgold_settings', 'allow_inter_org_thrift');

        if ($allow_inter_org_thrift) {
            if ($allow_inter_org_thrift == 'allowed') {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private function is_payment_method_on()
    {
        $bool = false;

        $payment_method_check = $this->custom_settings_library->getASettingsValue('payment_settings', 'payment_method_check');

        if ($payment_method_check) {
            if ($payment_method_check == 'on') {
                $bool = true;
            }
        }

        return $bool;

    }

    private function chkIfEmployeeIsPaystackCustomer()
    {
        $user_id = $this->session->userdata('user_id');

        $user = $this->Thrift_model->getEmployee($user_id);

        if ($user) {

            if (!($user->paystack_customer_code == null || $user->paystack_customer_code == '')) {
                return true;
            }

        }

        return false;
    }

    private function createPaystackCustomer()
    {
        $customer = array();

        $user_id = $this->session->userdata('user_id');

        $employee = $this->Thrift_model->getEmployee($user_id);

        if ($employee->first_name == null || $employee->first_name == false || $employee->first_name == '') {
            $customer['first_name'] = 'X';
        } else {
            $customer['first_name'] = $employee->first_name;
        }

        if ($employee->last_name == null || $employee->last_name == false || $employee->last_name == '') {
            $customer['last_name'] = 'Y';
        } else {
            $customer['last_name'] = $employee->last_name;
        }

        if ($employee->email == null || $employee->email == false || $employee->email == '') {
            // do not create customer

            redirect('users/auth/payment_method_error_page?payment_method=Paystack&error_type=User%20email%20not%20found');
        } else {
            $customer['email'] = $employee->email;

            $ret_data = $this->custom_payment_library->createSinglePaystackCustomer($customer, $employee->user_id);

            if ($ret_data['got_data'] && !$ret_data['error_response_object'] && !$ret_data['error_sk']) {
                $created_customer = $ret_data['got_data'];

                if ($created_customer) {
                    if ($created_customer->status == 1 && !empty($created_customer->data) && $created_customer->data != null) {
                        $upd_data['paystack_customer_code'] = $created_customer->data->customer_code;
                        $upd_data['paystack_customer_id'] = $created_customer->data->id;
                        $upd_data['paystack_integration'] = $created_customer->data->integration;

                        $this->Thrift_model->updateEmployeesPaymentInfo($upd_data, $employee->user_id);

                    }
                }
            } else {
                //comment the redirect to see error
                redirect('users/auth/payment_method_error_page?payment_method=Paystack&error_type=Customer%20creation%20error');
                if ($ret_data['error_response_object']) {
                    print_r($ret_data['error_response_object']);
                }
                if ($ret_data['error_message']) {
                    echo $ret_data['error_message'];
                }
                if ($ret_data['error_sk']) {
                    echo $ret_data['error_sk'];
                }
            }

        }
    }


    private function chkChosenPaymentMethod()
    {
        $user_id = $this->session->userdata('user_id');

        $user = $this->Thrift_model->getEmployee($user_id);

        if ($user) {

            if ($user->paystack_recipient_code == null || $user->paystack_recipient_code == '') {
                $this->createPaystackTransferRecipient($user);
            }

            if ($user->user_chosen_payment_method == 'paystack' || $user->user_chosen_payment_method == 'flutterwave') {
                if ($user->user_chosen_payment_method == 'paystack') {

                    if ($this->chkIfEmployeeIsPaystackCustomer() == false) {
                        //customer already gets created while getting authorized, this may never be hit
                        $this->createPaystackCustomer();
                    }

                    if (!($user->paystack_authorization_code == null || $user->paystack_authorization_code == '')) {
                        return true;
                    }
                }
            }
        }

        return false;

    }

    public function choosePaymentMethod($redirect)
    {

        $this->lang->load('payment_method_choice_form');
        $data = array();

        if ($this->session->has_userdata('custom_redirect_path')) {
            $this->session->unset_userdata('custom_redirect_path');
        }

        $this->session->set_userdata('custom_redirect_path', $redirect);
        $data['form_action'] = 'thrift_module/submit_payment_method_choice';


        $header = $this->load->view("common_module/header", '', TRUE);
        $page = $this->load->view("thrift_module/payment_method_choice_form_page", $data, TRUE);
        $footer = $this->load->view("common_module/footer", '', TRUE);

        echo $header;
        echo $page;
        echo $footer;
        die(); //keep this die, this is necessary for view loading.
    }

    public function submitPaymentMethodChoice()
    {
        $user = $this->Thrift_model->getEmployee($this->session->userdata('user_id'));


        if ($user) {
            if ($user->email != null && $user->email != '') {
                $paystack_transaction_data = array();
                $paystack_transaction_data['email'] = $user->email;
                $paystack_transaction_data['amount'] = 50;
                $paystack_transaction_data['callback_url'] = base_url() . 'thrift_module/ThriftController/paymentCallbackUrl';

                if ($this->input->post('user_chosen_payment_method') == 'paystack') {
                    $ret_data = $this->custom_payment_library->initializePaystackTransaction($paystack_transaction_data);

                    if ($ret_data['got_data'] && !$ret_data['error_response_object'] && !$ret_data['error_sk']) {
                        $initilized_payment = $ret_data['got_data'];

                        if ($initilized_payment) {
                            if ($initilized_payment->status == true && !empty($initilized_payment->data) && $initilized_payment->data != null) {

                                redirect($initilized_payment->data->authorization_url);

                            }
                        }
                    } else {
                        //comment the redirect to see error
                        redirect('users/auth/payment_method_error_page?payment_method=Paystack&error_type=Transaction%20initialization%20error');

                        if ($ret_data['error_response_object']) {
                            print_r($ret_data['error_response_object']);
                        }
                        if ($ret_data['error_message']) {
                            echo $ret_data['error_message'];
                        }
                        if ($ret_data['error_sk']) {
                            echo $ret_data['error_sk'];
                        }
                    }


                }


            }
        }


    }

    public function paymentCallbackUrl()
    {
        $reference = false;


        if (isset($_REQUEST['reference'])) {
            $reference = $_REQUEST['reference'];
        }


        if ($reference) {

            $paystack_verification_data = array();
            $paystack_verification_data['reference'] = $reference;

            $ret_data = $this->custom_payment_library->verifyPaystackTransaction($paystack_verification_data);

            if ($ret_data['got_data'] && !$ret_data['error_response_object'] && !$ret_data['error_sk']) {
                $verified_payment = $ret_data['got_data'];

                if ($verified_payment) {
                    if ($verified_payment->status == true && !empty($verified_payment->data) && $verified_payment->data != null) {

                        $upd_data['user_chosen_payment_method'] = 'paystack';
                        $upd_data['paystack_customer_code'] = $verified_payment->data->customer->customer_code;
                        $upd_data['paystack_customer_id'] = $verified_payment->data->customer->id;
                        $upd_data['paystack_authorization_code'] = $verified_payment->data->authorization->authorization_code;

                        $this->Thrift_model->updateEmployeesPaymentInfo($upd_data, $this->session->userdata('user_id'));

                        if ($this->session->has_userdata('custom_redirect_path')) {
                            $redirect = $this->session->userdata('custom_redirect_path');
                            $this->session->unset_userdata('custom_redirect_path');
                            redirect($redirect);
                        } else {
                            redirect('/');
                        }

                    }
                }
            } else {
                //comment the redirect to see error
                redirect('users/auth/payment_method_error_page?payment_method=Paystack&error_type=Transaction%20verification%20error');

                if ($ret_data['error_response_object']) {
                    print_r($ret_data['error_response_object']);
                }
                if ($ret_data['error_message']) {
                    echo $ret_data['error_message'];
                }
                if ($ret_data['error_sk']) {
                    echo $ret_data['error_sk'];
                }
            }


        }
    }


    /* -------------------- Logic for new thrift List <starts> -----------------------------------------------------  */
    public function startThrift()
    {
        if ($this->session->userdata('user_id') == null) {
            redirect('/');
        }

        $this->lang->load('thrift');

        if (!$this->ion_auth->in_group('employee')) {

            $this->session->set_flashdata('thrift_error', 'thrift_error');
            $this->session->set_flashdata('thrift_error_only_employee_allowed_text', 'thrift_error_only_employee_allowed_text');

            redirect('users/auth/does_not_exist');  // redirect to does not exist
        }

        $product_id = $this->uri->segment(3);

        if ($this->is_payment_method_on()) {

            if ($this->chkChosenPaymentMethod() == false) {

                $this->choosePaymentMethod('thrift_module/start_thrift/' . $product_id);
            }
        }


        $product = $this->Thrift_model->getProduct($product_id);

        $employee_id = $this->session->userdata('user_id');

        $employee = $this->Thrift_model->getEmployee($employee_id);

        $employer_id = $employee->user_employer_id;

        $employer = null;
        if ($employer_id != 0 && $employer_id != null) {

            $employer = $this->Thrift_model->getEmployer($employer_id);

        }

        $joined_thrift_group_id = 0;

        //must have employer
        if ($employee && $employer && $product) {

            if ($this->checkThriftCondition($employee, $employer, $product) == true) {

                if ($this->inter_org_thrift_allowed()) {
                    $available_thrift_groups = $this->Thrift_model->getIncompleteThriftGroups($product_id, $colleagues = false);

                } else {

                    $my_id = $this->session->userdata('user_id');
                    $my_details = $this->Thrift_model->getEmployee($my_id);
                    $employer_id = false;

                    if ($my_details->user_employer_id != null || $my_details->user_employer_id > 0) {
                        $employer_id = $my_details->user_employer_id;
                    }

                    if ($employer_id) {

                        $colleagues_obj = $this->Thrift_model->getEmployeeColleaguesIds($my_id, $employer_id);
                        $colleagues = false;

                        if ($colleagues_obj) {
                            foreach ($colleagues_obj as $a_col_obj) {
                                $colleagues[] = $a_col_obj->user_id;
                            }
                        }

                        $available_thrift_groups = $this->Thrift_model->getIncompleteThriftGroups($product_id, $colleagues);

                    } else {
                        $available_thrift_groups = null;
                    }


                }

                if ($available_thrift_groups) {

                    $available_thrift_group_id = $available_thrift_groups[0]->thrift_group_id;

                    $is_employee_already_in_thrift_group =
                        $this->Thrift_model->checkIfMemberAlreadyInThriftGroup($available_thrift_group_id, $employee_id);


                    if ($is_employee_already_in_thrift_group) {

                        $created_thrift_group_id = $this->createThriftGroup($product); //return $thrift_group_id

                        $joined_thrift_group_id = $this->joinThriftGroup($created_thrift_group_id, $employee_id);

                    } else {

                        $joined_thrift_group_id = $this->joinThriftGroup($available_thrift_group_id, $employee_id);

                    }


                } else {
                    /*echo 'hold on';
                    die();*/

                    $created_thrift_group_id = $this->createThriftGroup($product); //return $thrift_group_id

                    $joined_thrift_group_id = $this->joinThriftGroup($created_thrift_group_id, $employee_id);

                }

                $this->session->set_flashdata('thrift_success', 'thrift_success');
                $this->session->set_flashdata('thrift_success_text', 'thrift_success_text');

                if ($joined_thrift_group_id != 0) {
                    $this->session->set_flashdata('flash_thrift_group_id', $joined_thrift_group_id);
                }

                redirect('thrift_module/show_thrifts/my');

            } else {

                $this->session->set_flashdata('thrift_error', 'thrift_error');
                $this->session->set_flashdata('thrift_error_exceed_limit_text', 'thrift_error_exceed_limit_text');

                redirect('thrift_module/show_thrifts/my');
            }


        } else {
            $this->session->set_flashdata('thrift_error', 'thrift_error');
            $this->session->set_flashdata('thrift_error_no_employer_text', 'thrift_error_no_employer_text');

            redirect('thrift_module/show_thrifts/my');
        }


    }


    private function checkThriftCondition($employee, $employer, $product)
    {

        $return_flag = false;

        //check conditions

        $thrift_percentage = 50;
        $employee_salary = 0.00;
        $bought_product_value = 0;

        $max_product_buy_limit_amount = 0.00;

        $system_thrift_percentage = $this->custom_settings_library->getASettingsValue('prosperisgold_settings', 'thrift_percentage');
        $user_employee_thrift_percentage = $employee->user_employee_thrift_percentage;

        $user_employer_thrift_percentage = null;
        if($employer){
            $user_employer_thrift_percentage = $employer->user_employer_thrift_percentage;
        }


        if (!is_numeric($system_thrift_percentage) || $system_thrift_percentage == 0 || $system_thrift_percentage == '') {
            $system_thrift_percentage = null;
        }

        if (!is_numeric($user_employee_thrift_percentage) || $user_employee_thrift_percentage == 0 || $user_employee_thrift_percentage == '') {
            $user_employee_thrift_percentage = null;
        }

        if (!is_numeric($user_employer_thrift_percentage) || $user_employer_thrift_percentage == 0 || $user_employer_thrift_percentage == '') {
            $user_employer_thrift_percentage = null;
        }


        $bought_product_value = $this->getAlreadyBoughtProductValue($employee->id);


        if ($employee->user_salary) {

            if (is_numeric($employee->user_salary) && $employee->user_salary > 0) {

                $employee_salary = $employee->user_salary;

            }

        }


        if ($user_employer_thrift_percentage && $employer) {
            $thrift_percentage = $user_employer_thrift_percentage;
        } elseif ($user_employee_thrift_percentage && $employer) {
            $thrift_percentage = $user_employer_thrift_percentage;
        } elseif ($system_thrift_percentage) {
            $thrift_percentage = $system_thrift_percentage;
        }


        $max_product_buy_limit_amount = ($employee_salary * ($thrift_percentage / 100)) - $bought_product_value;

        if ($max_product_buy_limit_amount > $product->product_price) {
            $return_flag = true;
        }

        if ($return_flag == false) {
            $this->session->set_flashdata('flash_thrift_percentage_error', $thrift_percentage);
        }


        return $return_flag;

    }


    private function getAlreadyBoughtProductValue($employee_id)
    {
        $bought_product_value = 0.00;

        $running_joined_thrifts = $this->Thrift_model->getRunningJoinedThrifts($employee_id);

        if ($running_joined_thrifts) {

            foreach ($running_joined_thrifts as $a_running_joined_thrift) {

                $bought_product_value += $a_running_joined_thrift->thrift_group_product_price;
            }

        }

        return $bought_product_value;
    }


    private function createThriftGroup($product)
    {
        $ins_data['thrift_group_number'] = $this->generateThriftGroupNumber();
        $ins_data['thrift_group_product_id'] = $product->product_id;
        $ins_data['thrift_group_product_price'] = $product->product_price;
        $ins_data['thrift_group_term_duration'] = $product->product_term_duration;
        $ins_data['thrift_group_member_count'] = 0;
        $ins_data['thrift_group_member_limit'] = $product->product_term_duration;
        $ins_data['thrift_group_activation_status'] = 1;
        $ins_data['thrift_group_open'] = 1;
        $ins_data['thrift_group_creation_date'] = $this->custom_datetime_library->getCurrentTimestamp();

        $ins_data['thrift_group_threshold_date'] =
            strtotime('today 11:59 PM', $this->custom_datetime_library->getCurrentTimestamp());   //when auto bot assign starts

        $threshold_time = $this->getThresholdTime();
        if ($threshold_time) {
            $threshold = 'today ' . $threshold_time;
            $ins_data['thrift_group_threshold_date'] =
                strtotime($threshold, $this->custom_datetime_library->getCurrentTimestamp()); //when auto bot assign starts
        }

        //testcheck
        // remove this. It is only for testing
        //$ins_data['thrift_group_threshold_date'] = $this->custom_datetime_library->getCurrentTimestamp();

        $ins_data['thrift_group_start_date'] = 0;
        $ins_data['thrift_group_end_date'] = 0;

        $thrift_group_id = $this->Thrift_model->insertThriftGroup($ins_data);

        if ($this->is_payment_method_on()) {
            $this->createNewPaystackPlan($product->product_price, $thrift_group_id, $product->product_name);
        }

        /*creating log starts*/
        $this->custom_log_library->createALog
        (
            $this->session->userdata('user_id'),                                    //1.    $created_by
            '',                                                                     //2.    $created_for
            'thrift_group',                                                         //3.    $type
            $thrift_group_id,                                                       //4.    $type_id
            'created',                                                              //5.    $activity
            'employee',                                                             //6.    $activity_by
            '',                                                                     //7.    $activity_for
            '',                                                                     //8.    $sub_type
            '',                                                                     //9.    $sub_type_id
            '',                                                                     //10.   $super_type
            '',                                                                     //11.   $super_type_id
            '',                                                                     //12.   $other_information
            ''                                                                      //13.   $change_list
        );
        /*creating log ends*/

        $this->joinThriftGroup($thrift_group_id, $admin_id = 1); //add admin to thrift as first member

        return $thrift_group_id;

    }

    public function getCurrencySign()
    {
        $sign = $this->custom_settings_library->getASettingsValue('çurrency_settings', 'currency_sign');

        if ($sign) {
            return $sign;
        } else {
            return '';
        }
    }

    public function getThresholdTime()
    {
        $thrift_threshold_time = $this->custom_settings_library->getASettingsValue('prosperisgold_settings', 'thrift_threshold_time');

        if ($thrift_threshold_time) {
            if ($thrift_threshold_time == '') {
                return false;
            } else {
                return $thrift_threshold_time;
            }
        } else {
            return false;
        }
    }

    public function generateThriftGroupNumber()
    {
        $num = $this->alphaNum(1, true, false);
        $num .= $this->alphaNum(7, false, false);

        $exists = $this->Thrift_model->doesThriftGroupNumberExist($num);

        if ($exists) {

            $this->generateThriftGroupNumber();

        } else {

            return $num;

        }

    }

    public function generateThriftGroupPaymentNumber()
    {
        $num = 'P';
        $num .= $this->alphaNum(10, false, true);

        $exists = $this->Thrift_model->doesThriftGroupPaymentNumberExist($num);

        if ($exists) {

            $this->generateThriftGroupPaymentNumber();

        } else {

            return $num;

        }
    }

    public function generateThriftGroupPaymentRecieveNumber()
    {
        $num = 'D';
        $num .= $this->alphaNum(10, false, true);

        $exists = $this->Thrift_model->doesThriftGroupPaymentRecieveNumberExist($num);

        if ($exists) {

            $this->generateThriftGroupPaymentRecieveNumber();

        } else {

            return $num;

        }
    }

    public function generateThriftGroupCombinePaymentNumber()
    {
        $num = 'CP';
        $num .= $this->alphaNum(18, false, false);

        $exists = $this->Thrift_model->doesThriftGroupCombinePaymentNumberExist($num);

        if ($exists) {

            $this->generateThriftGroupPaymentRecieveNumber();

        } else {

            return $num;

        }

    }

    private function generateCpiNumber()
    {
        $num = 'CPI';
        $num .= $this->alphaNum(22, false, false);

        return $num;
    }

    private function alphaNum($length = false, $only_alphabets = false, $only_integers = false)
    {
        if (!$length) {
            $length = 8;
        }

        $alphabets = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $integers = '0123456789';
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';

        if ($only_alphabets) {
            $characters = $alphabets;
        }

        if ($only_integers) {
            $characters = $integers;
        }

        $randomString = '';

        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }

        return $randomString;
    }

    private function genNum($prefix)
    {
        $time_zone = $this->custom_datetime_library->getTimezone();

        if ($time_zone) {
            date_default_timezone_set($time_zone);
        } else {
            date_default_timezone_set('Europe/London');
        }

        $datetime = date('YmdHis');

        $number = $prefix . $datetime . '_r' . rand(1000, 9999);

        return $number;
    }



    /*the function to assign bot admins to the thrift group if thrift group is not filled for a long time*/
    /*cron job function */
    public function autoAssignBotAdmins()
    {
        $curr_dt = $this->custom_datetime_library->getCurrentTimestamp();

        $available_thrift_group_count = $this->Thrift_model->countAllIncompleteThriftGroupsCrossingThreshold($curr_dt);


        if ($available_thrift_group_count > 0) {

            $available_thrift_groups = $this->Thrift_model->getAllIncompleteThriftGroupsCrossingThreshold($curr_dt);

            if ($available_thrift_groups) {

                foreach ($available_thrift_groups as $atg) {


                    $empty_position_count = $atg->thrift_group_member_limit - $atg->thrift_group_member_count;

                    if ($empty_position_count > 0 && $atg->thrift_group_is_custom_product != 1) {

                        $limit = $empty_position_count;
                        $bot_admins = $this->Thrift_model->getActiveBotAdminsIds($limit);

                        if ($bot_admins) {

                            foreach ($bot_admins as $a_bot_admin) {

                                /*for checking the current status*/
                                $tg_current = $this->Thrift_model->getThriftGroup($atg->thrift_group_id);

                                /*check if the bot is alreardy assigned*/
                                $existence = $this->Thrift_model->checkIfMemberAlreadyInThriftGroup($tg_current->thrift_group_id, $a_bot_admin->user_id);


                                $emp_pos_cnt = $tg_current->thrift_group_member_limit - $tg_current->thrift_group_member_count;

                                if (!$existence && $emp_pos_cnt > 0) {
                                    $this->joinThriftGroup($tg_current->thrift_group_id, $a_bot_admin->user_id);
                                }


                            }

                        }

                    }


                }

            }

        }

    }


    private function joinThriftGroup($thrift_group_id, $member_id)
    {
        $thrift_group_member_count = 0;
        $thrift_group_member_limit = 0;
        $thrift_group_start_date = 0;

        $thrift_group = $this->Thrift_model->getThriftGroup($thrift_group_id);

        if ($thrift_group) {

            if (is_numeric($thrift_group->thrift_group_member_count)) {
                $thrift_group_member_count = $thrift_group->thrift_group_member_count;
            }

            if (is_numeric($thrift_group->thrift_group_member_limit)) {
                $thrift_group_member_limit = $thrift_group->thrift_group_member_limit;
            }

        }


        if ($thrift_group->thrift_group_open == 1) {

            /*insert in pg_thrift_group_members */
            $ins_data['thrift_group_id'] = $thrift_group_id;
            $ins_data['thrift_group_member_id'] = $member_id;
            $ins_data['thrift_group_member_number'] = $thrift_group_member_count + 1;
            $ins_data['thrift_group_member_join_date'] = $this->custom_datetime_library->getCurrentTimestamp();


            /*
             * here a thrift group is complete
             * should be true only once
             *
             * update pg_thrift_group
             */

            $upd_data['thrift_group_member_count'] = $thrift_group_member_count + 1;

            if ($thrift_group_member_limit - 1 == $thrift_group_member_count) {

                $upd_data['thrift_group_open'] = 0;

                $thrift_group_start_date = $this->custom_datetime_library->getCurrentTimestamp(); //when the cycle starts
                $upd_data['thrift_group_start_date'] = $thrift_group_start_date;

            }

            $inserted = false;

            if ($thrift_group_member_limit > $thrift_group_member_count) {
                $inserted = $this->Thrift_model->insertEmployeeInThriftGroup($ins_data); //return bool

                /*creating log starts*/

                $activity_by = '';

                if ($this->ion_auth->is_admin($member_id)) {
                    $activity_by = 'admin';
                } else if ($this->ion_auth->in_group('employee', $member_id)) {
                    $activity_by = 'employee';
                }

                $this->custom_log_library->createALog
                (
                    $member_id,                                                             //1.    $created_by
                    '',                                                                     //2.    $created_for
                    'thrift_group',                                                         //3.    $type
                    $thrift_group_id,                                                       //4.    $type_id
                    'joined',                                                               //5.    $activity
                    $activity_by,                                                           //6.    $activity_by
                    '',                                                                     //7.    $activity_for
                    '',                                                                     //8.    $sub_type
                    '',                                                                     //9.    $sub_type_id
                    '',                                                                     //10.   $super_type
                    '',                                                                     //11.   $super_type_id
                    '',                                                                     //12.   $other_information
                    ''                                                                      //13.   $change_list
                );
                /*creating log ends*/

                //join here ...

                if (!$this->ion_auth->is_admin($member_id)) {
                    $this->sendThriftJoinNotificationEmail($thrift_group_id, $member_id);
                }

            }


            if ($inserted) {
                $this->Thrift_model->updateThriftGroup($upd_data, $thrift_group_id);

                if ($thrift_group_start_date != 0) {

                    //here the thrift group is closed and starts

                    $this->setUpThriftGroupPayments($thrift_group_id, $thrift_group_start_date);

                    if ($this->is_payment_method_on()) {
                        $this->setUpPaystackSubscriptionForEmployees($thrift_group_id);
                    }


                    /*creating log starts*/
                    $this->custom_log_library->createALog
                    (
                        '',                                                                     //1.    $created_by
                        '',                                                                     //2.    $created_for
                        'thrift_group',                                                         //3.    $type
                        $thrift_group_id,                                                       //4.    $type_id
                        'thrift_started',                                                       //5.    $activity
                        '',                                                                     //6.    $activity_by
                        '',                                                                     //7.    $activity_for
                        '',                                                                     //8.    $sub_type
                        '',                                                                     //9.    $sub_type_id
                        '',                                                                     //10.   $super_type
                        '',                                                                     //11.   $super_type_id
                        '',                                                                     //12.   $other_information
                        ''                                                                      //13.   $change_list
                    );
                    /*creating log ends*/

                    $this->notifyThriftStarted($thrift_group_id);

                }

            }


        }

        return $thrift_group_id;

    }

    private function sendThriftJoinNotificationEmail($thrift_group_id, $im)
    {
        $user = $this->Thrift_model->getEmployee($im);

        if ($user) {

            $username = $user->first_name . ' ' . $user->last_name;
            if ($this->ion_auth->in_group('employer', $im)) {
                $username = $user->company;
            }

            $mail_data['to'] = $user->email;

            $template = $this->Thrift_model->getEmailTempltateByType('thrift_join');

            if ($template) {
                $subject = $template->email_template_subject;

                $template_message = $template->email_template;

                /*-------*/
                $base_url = $this->getBaseUrl($im);

                $actual_link = $base_url . 'thrift_module/view_thrift/' . $thrift_group_id;

                $find = array("{{username}}", "{{actual_link}}");
                $replace = array($username, $actual_link);
                $message = str_replace($find, $replace, $template_message);

                /*--------*/
                $mail_data['subject'] = $subject;
                $mail_data['message'] = $message;

                $this->sendEmail($mail_data);
            }

        }
    }

    private function setUpThriftGroupPayments($thrift_group_id, $thrift_group_start_date)
    {
        $thrift_members = $this->Thrift_model->getThriftMembers($thrift_group_id);
        $thrift_group = $this->Thrift_model->getThriftGroup($thrift_group_id);


        if ($thrift_members && $thrift_group) {

            if ($thrift_group->thrift_group_member_limit > 0) {


                for ($i = 1; $i <= $thrift_group->thrift_group_member_limit; $i++) {

                    /*unique number for a pay cycle
                    if 4 member pays and one recieves each will have this number
                    */
                    $combine_payment_number = $this->generateThriftGroupCombinePaymentNumber();


                    //testcheck
                    //change this to +30 days
                    $diff = 30; //change to 30
                    $daycount = $i * $diff;
                    $days_string = $daycount . ' days'; //change to days

                    foreach ($thrift_members as $thrift_member) {

                        /*unique for ch member payment*/
                        $payment_number = $this->generateThriftGroupPaymentNumber();
                        $payment_recieve_number = $this->generateThriftGroupPaymentRecieveNumber();

                        $ins_pay_data = null;
                        $ins_rec_data = null;

                        if ($i != $thrift_member->thrift_group_member_number) {

                            $ins_pay_data['thrift_group_id'] = $thrift_member->thrift_group_id;

                            /*who pays*/
                            $ins_pay_data['thrift_group_payer_member_id'] = $thrift_member->thrift_group_member_id;
                            $ins_pay_data['thrift_group_payer_member_number'] = $thrift_member->thrift_group_member_number;

                            /*whom to pay*/
                            for ($k = 0; $k < count($thrift_members); $k++) {
                                if ($thrift_members[$k]->thrift_group_member_number == $i) {

                                    $ins_pay_data['thrift_group_payee_member_id'] = $thrift_members[$k]->thrift_group_member_id;
                                    $ins_pay_data['thrift_group_payee_member_number'] = $thrift_members[$k]->thrift_group_member_number;

                                }
                            }


                            $ins_pay_data['thrift_group_payment_number'] = $payment_number;
                            $ins_pay_data['thrift_group_combine_payment_number'] = $combine_payment_number;
                            $ins_pay_data['thrift_group_payment_cycle_number'] = $i;
                            $ins_pay_data['thrift_group_payment_amount'] =
                                $thrift_group->thrift_group_product_price ? $thrift_group->thrift_group_product_price : 0.00;
                            $ins_pay_data['thrift_group_is_payment_paid'] = 0;
                            $ins_pay_data['thrift_group_payment_date'] = strtotime($days_string, $thrift_group_start_date);

                        }


                        if ($i == $thrift_member->thrift_group_member_number) {

                            $ins_rec_data['thrift_group_id'] = $thrift_member->thrift_group_id;
                            $ins_rec_data['thrift_group_member_id'] = $thrift_member->thrift_group_member_id;
                            $ins_rec_data['thrift_group_member_number'] = $thrift_member->thrift_group_member_number;
                            $ins_rec_data['thrift_group_payment_recieve_number'] = $payment_recieve_number;
                            $ins_rec_data['thrift_group_combine_payment_number'] = $combine_payment_number;
                            $ins_rec_data['thrift_group_payment_cycle_number'] = $i;

                            $amt = 0.00;
                            if ($thrift_group->thrift_group_product_price) {
                                $amt = $thrift_group->thrift_group_product_price * ($thrift_group->thrift_group_member_count - 1);
                            }

                            $ins_rec_data['thrift_group_payment_recieve_amount'] = $amt;

                            $ins_rec_data['thrift_group_is_payment_recieved'] = 0;
                            $ins_rec_data['thrift_group_payment_date'] = strtotime($days_string, $thrift_group_start_date);

                            if ($this->ion_auth->in_group('employee', $thrift_member->thrift_group_member_id)) {
                                $user = $this->Thrift_model->getEmployee($thrift_member->thrift_group_member_id);
                                if ($user) {
                                    if ($user->user_chosen_payment_method == 'paystack' && $user->paystack_recipient_code != null && $user->paystack_recipient_code != '') {
                                        $ins_rec_data['eligible_for_paystack_payment_transfer'] = 1;
                                    }
                                }
                            }


                        }


                        if ($ins_pay_data) {
                            $this->Thrift_model->insertThriftGroupPaymentSetup($ins_pay_data);
                        }

                        if ($ins_rec_data) {
                            $this->Thrift_model->insertThriftGroupPaymentRecieveSetup($ins_rec_data);
                        }


                        if (
                            $i == $thrift_group->thrift_group_member_limit
                            &&
                            $i == $thrift_member->thrift_group_member_number
                            &&
                            isset($ins_rec_data['thrift_group_payment_date'])
                        ) {


                            $thrift_group_end_date = $ins_rec_data['thrift_group_payment_date'];

                            $this->Thrift_model->updateThriftGroupEndDate($thrift_group_end_date, $thrift_group_id);


                        }


                    }


                }

            }


        }


    }

    public function sendReminderToThrifters()
    {

        $first_reminder_time = 86400; //1 day before
        $second_reminder_time = 259200; //1 day before

        // test check for a 1 hour gap subscription , delete the immediate lines below
        $first_reminder_time = 900; //15 mins before
        $second_reminder_time = 300; //5 mins before

        // test check for a 1 minute gap subscription , delete the immediate lines below
        $first_reminder_time = 30; //30 sec before
        $second_reminder_time = 15; //15 sec before

        $this->sendReminder('payment', $first_reminder_time, 1);
        $this->sendReminder('payment', $first_reminder_time, 1);
        $this->sendReminder('payment_recieve', $first_reminder_time, 1);
        $this->sendReminder('payment_recieve', $first_reminder_time, 1);

        $this->sendReminder('payment', $second_reminder_time, 2);
        $this->sendReminder('payment', $second_reminder_time, 2);
        $this->sendReminder('payment_recieve', $second_reminder_time, 2);
        $this->sendReminder('payment_recieve', $second_reminder_time, 2);

    }

    private function sendReminder($what_reminder, $reminder_time, $reminder_num)
    {
        $eligible_payments = false;
        $eligible_payment_recieves = false;

        if ($what_reminder == 'payment') {
            $eligible_payments = $this->Thrift_model->getEligiblePaymentsForRemainders($reminder_time, $reminder_num);
        }

        if ($what_reminder == 'payment_recieve') {
            $eligible_payment_recieves = $this->Thrift_model->getEligiblePaymentRecievesForRemainders($reminder_time, $reminder_num);
        }

        if ($eligible_payments) {
            foreach ($eligible_payments as $ep) {

                if (!$this->ion_auth->is_admin($ep->thrift_group_payer_member_id)) {
                    $this->sendPaymentReminderEmail($ep);
                }
            }
        }

        if ($eligible_payment_recieves) {
            foreach ($eligible_payment_recieves as $epr) {
                if (!$this->ion_auth->is_admin($epr->thrift_group_member_id)) {
                    $this->sendPaymentRecieveReminderEmail($epr);
                }
            }
        }
    }

    private function sendPaymentReminderEmail($ep)
    {
        $user = $this->Thrift_model->getEmployee($ep->thrift_group_payer_member_id);
        $tg = $this->Thrift_model->getThriftGroup($ep->thrift_group_id);

        if ($user && $tg) {

            $username = $user->first_name . ' ' . $user->last_name;
            if ($this->ion_auth->in_group('employer', $ep->thrift_group_payer_member_id)) {
                $username = $user->company;
            }

            $mail_data['to'] = $user->email;

            $template = $this->Thrift_model->getEmailTempltateByType('payment_notification');

            if ($template) {
                $subject = $template->email_template_subject;

                $template_message = $template->email_template;

                /*-------*/
                $base_url = $this->getBaseUrl($ep->thrift_group_payer_member_id);

                $actual_link = $base_url . 'thrift_module/view_thrift/' . $ep->thrift_group_id;
                $amount_value = $this->getCurrencySign() .
                    number_format($ep->thrift_group_payment_amount, 2, '.', ',');
                $thrift_group_number = $tg->thrift_group_number;
                $date = $this->custom_datetime_library->convert_and_return_TimestampToDate($ep->thrift_group_payment_date);

                //testcheck show only dates ,remove this
                //$date = $this->custom_datetime_library->convert_and_return_TimestampToDateAndTime($ep->thrift_group_payment_date);


                $find = array("{{username}}", "{{actual_link}}", "{{amount_value}}", "{{thrift_group_number}}", "{{date}}");
                $replace = array($username, $actual_link, $amount_value, $thrift_group_number, $date);
                $message = str_replace($find, $replace, $template_message);

                /*--------*/
                $mail_data['subject'] = $subject;
                $mail_data['message'] = $message;

                $this->sendEmail($mail_data);

                $upd_data_payment['thrift_group_reminder_email_count'] = $ep->thrift_group_reminder_email_count + 1;
                $this->Thrift_model->autoUpdateThriftGroupPayment($upd_data_payment, $ep->thrift_group_payment_id);
            }

        }
    }

    private function sendPaymentRecieveReminderEmail($epr)
    {
        $user = $this->Thrift_model->getEmployee($epr->thrift_group_member_id);
        $tg = $this->Thrift_model->getThriftGroup($epr->thrift_group_id);

        if ($user && $tg) {

            $this->sendExtraPaymentReminderEmail($epr, $user, $tg);

            $username = $user->first_name . ' ' . $user->last_name;
            if ($this->ion_auth->in_group('employer', $epr->thrift_group_member_id)) {
                $username = $user->company;
            }

            $mail_data['to'] = $user->email;

            $template = $this->Thrift_model->getEmailTempltateByType('payment_recieve_notification');

            if ($template) {
                $subject = $template->email_template_subject;

                $template_message = $template->email_template;

                /*-------*/
                $base_url = $this->getBaseUrl($epr->thrift_group_member_id);

                $actual_link = $base_url . 'thrift_module/view_thrift/' . $epr->thrift_group_id;
                $amount_value = $this->getCurrencySign() .
                    number_format($epr->thrift_group_payment_recieve_amount, 2, '.', ',');
                $thrift_group_number = $tg->thrift_group_number;
                $date = $this->custom_datetime_library->convert_and_return_TimestampToDate($epr->thrift_group_payment_date);

                //testcheck show only dates ,remove this
                //$date = $this->custom_datetime_library->convert_and_return_TimestampToDateAndTime($epr->thrift_group_payment_date);

                $find = array("{{username}}", "{{actual_link}}", "{{amount_value}}", "{{thrift_group_number}}", "{{date}}");
                $replace = array($username, $actual_link, $amount_value, $thrift_group_number, $date);
                $message = str_replace($find, $replace, $template_message);

                /*--------*/
                $mail_data['subject'] = $subject;
                $mail_data['message'] = $message;

                $this->sendEmail($mail_data);

                $upd_data_payment['thrift_group_reminder_email_count'] = $epr->thrift_group_reminder_email_count + 1;

                $upd_data_payment_recieve['thrift_group_reminder_email_count'] = 1;
                $this->Thrift_model->autoUpdateThriftGroupPaymentRecieve($upd_data_payment_recieve, $epr->thrift_group_payment_recieve_id);
            }
        }
    }

    private function sendExtraPaymentReminderEmail($epr, $user, $tg)
    {
        $username = $user->first_name . ' ' . $user->last_name;
        if ($this->ion_auth->in_group('employer', $epr->thrift_group_member_id)) {
            $username = $user->company;
        }

        $mail_data['to'] = $user->email;

        $template = $this->Thrift_model->getEmailTempltateByType('payment_notification');

        if ($template) {
            $subject = $template->email_template_subject;

            $template_message = $template->email_template;

            /*-------*/
            $base_url = $this->getBaseUrl($epr->thrift_group_member_id);

            $actual_link = $base_url . 'thrift_module/view_thrift/' . $epr->thrift_group_id;
            $amount_value = $this->getCurrencySign() .
                number_format($tg->thrift_group_product_price, 2, '.', ',');
            $thrift_group_number = $tg->thrift_group_number;
            $date = $this->custom_datetime_library->convert_and_return_TimestampToDate($epr->thrift_group_payment_date);

            //testcheck show only dates ,remove this
            //$date = $this->custom_datetime_library->convert_and_return_TimestampToDateAndTime($epr->thrift_group_payment_date);

            $find = array("{{username}}", "{{actual_link}}", "{{amount_value}}", "{{thrift_group_number}}", "{{date}}");
            $replace = array($username, $actual_link, $amount_value, $thrift_group_number, $date);
            $message = str_replace($find, $replace, $template_message);

            /*--------*/
            $mail_data['subject'] = $subject;
            $mail_data['message'] = $message;

            $this->sendEmail($mail_data);
        }
    }



    /*sets payments as paid after crossing the payment date */
    /*cron job function */
    public function autoThriftGroupPayment()
    {
        $current_timestamp = $this->custom_datetime_library->getCurrentTimestamp();
        $unpaid_payments_till_now = $this->Thrift_model->getUnpaidThriftGroupPaymentsTillNow($current_timestamp);
        $unpaid_payment_recieves_till_now = $this->Thrift_model->getUnpaidThriftGroupPaymentRecievesTillNow($current_timestamp);

        if ($unpaid_payments_till_now) {

            foreach ($unpaid_payments_till_now as $an_up) {

                $upd_data_payment['thrift_group_is_payment_paid'] = 1;

                $this->Thrift_model->autoUpdateThriftGroupPayment($upd_data_payment, $an_up->thrift_group_payment_id);

                $activity_by = '';
                if ($this->ion_auth->is_admin($an_up->thrift_group_payer_member_id)) {
                    $activity_by = 'admin';
                } else if ($this->ion_auth->in_group('employee', $an_up->thrift_group_payer_member_id)) {
                    $activity_by = 'employee';
                }

                /*creating log starts*/
                $this->custom_log_library->createALog
                (
                    $an_up->thrift_group_payer_member_id,                                   //1.    $created_by
                    '',                                                                     //2.    $created_for
                    'thrift_payment',                                                       //3.    $type
                    $an_up->thrift_group_payment_id,                                        //4.    $type_id
                    'thrift_payment_paid',                                                  //5.    $activity
                    $activity_by,                                                           //6.    $activity_by
                    '',                                                                     //7.    $activity_for
                    '',                                                                     //8.    $sub_type
                    '',                                                                     //9.    $sub_type_id
                    'thrift_group',                                                         //10.   $super_type
                    $an_up->thrift_group_id,                                                //11.   $super_type_id
                    '',                                                                     //12.   $other_information
                    ''                                                                      //13.   $change_list
                );
                /*creating log ends*/

                $this->notifyThriftPayment($an_up->thrift_group_id, $an_up->thrift_group_payer_member_id, $an_up->thrift_group_payment_amount);


            }

        }

        if ($unpaid_payment_recieves_till_now) {

            foreach ($unpaid_payment_recieves_till_now as $an_ur) {

                $upd_data_payment_recieve['thrift_group_is_payment_recieved'] = 1;

                $this->Thrift_model->autoUpdateThriftGroupPaymentRecieve($upd_data_payment_recieve, $an_ur->thrift_group_payment_recieve_id);

                $this->notifyThriftDisbursement($an_ur->thrift_group_id, $an_ur->thrift_group_member_id, $an_ur->thrift_group_payment_recieve_amount);

                $thrift_group = $this->Thrift_model->getThriftGroup($an_ur->thrift_group_id);

                if ($thrift_group) {


                    if ($thrift_group->thrift_group_current_cycle != null && $thrift_group->thrift_group_current_cycle != null) {

                        $this->transferPaymentToThrifter($an_ur, $thrift_group);

                        //when ends
                        if ($thrift_group->thrift_group_term_duration - 1 == $thrift_group->thrift_group_current_cycle) {
                            $upd_data_group['thrift_group_activation_status'] = 0;

                            $this->increaseUserRatings($thrift_group->thrift_group_id);

                            /*creating log starts*/
                            $this->custom_log_library->createALog
                            (
                                '',                                                                     //1.    $created_by
                                '',                                                                     //2.    $created_for
                                'thrift_group',                                                         //3.    $type
                                $thrift_group->thrift_group_id,                                         //4.    $type_id
                                'thrift_ended',                                                         //5.    $activity
                                '',                                                                     //6.    $activity_by
                                '',                                                                     //7.    $activity_for
                                '',                                                                     //8.    $sub_type
                                '',                                                                     //9.    $sub_type_id
                                '',                                                                     //10.   $super_type
                                '',                                                                     //11.   $super_type_id
                                '',                                                                     //12.   $other_information
                                ''                                                                      //13.   $change_list
                            );
                            /*creating log ends*/

                            $this->notifyThriftEnded($thrift_group->thrift_group_id);

                            $this->disableGroupMembersPaystackSubscription($thrift_group->thrift_group_id);
                        }

                        $upd_data_group['thrift_group_current_cycle'] = $thrift_group->thrift_group_current_cycle + 1;

                        $this->Thrift_model->updateThriftGroup($upd_data_group, $thrift_group->thrift_group_id);


                    }

                }

            }

        }


    }

    private function transferPaymentToThrifter($payment_recieve, $thrift_group)
    {
        if ($this->ion_auth->in_group('employee', $payment_recieve->thrift_group_member_id)) {
            $user = $this->Thrift_model->getEmployee($payment_recieve->thrift_group_member_id);

            if ($user) {

                if ($user->user_chosen_payment_method == 'paystack'
                    && $user->paystack_recipient_code != null && $user->paystack_recipient_code != '') {

                    //while in paystack subscription user pays on the month of receiving , so we re giving him/her money back
                    //multiplied by 100 in kobo
                    $amount = ((int)$payment_recieve->thrift_group_payment_recieve_amount + (int)$thrift_group->thrift_group_product_price) * 100;

                    $transfer_data['recipient'] = $user->paystack_recipient_code;
                    $transfer_data['reason'] = 'Disbursement ' . $payment_recieve->thrift_group_payment_recieve_number;
                    $transfer_data['source'] = 'balance';
                    $transfer_data['amount'] = $amount;

                    $this->initiatePaystackTransfer($transfer_data, $payment_recieve->thrift_group_payment_recieve_id);

                }

            }
        }

    }

    private function notifyThriftPayment($thrift_group_id, $member_id, $amt)
    {
        $user = $this->Thrift_model->getEmployee($member_id);

        if ($user) {

            $username = $user->first_name . ' ' . $user->last_name;
            if ($this->ion_auth->in_group('employer', $member_id)) {
                $username = $user->company;
            }

            $mail_data['to'] = $user->email;

            $template = $this->Thrift_model->getEmailTempltateByType('made_payment');

            if ($template) {
                $subject = $template->email_template_subject;

                $template_message = $template->email_template;

                /*-------*/
                $base_url = $this->getBaseUrl($member_id);

                $actual_link = $base_url . 'thrift_module/view_thrift/' . $thrift_group_id;
                $amount_value = $this->getCurrencySign() .
                    number_format($amt, 2, '.', ',');

                $find = array("{{username}}", "{{actual_link}}", "{{amount_value}}");
                $replace = array($username, $actual_link, $amount_value);
                $message = str_replace($find, $replace, $template_message);

                /*--------*/
                $mail_data['subject'] = $subject;
                $mail_data['message'] = $message;

                $this->sendEmail($mail_data);
            }

        }
    }

    private function notifyThriftDisbursement($thrift_group_id, $member_id, $amt)
    {
        $user = $this->Thrift_model->getEmployee($member_id);

        if ($user) {

            $username = $user->first_name . ' ' . $user->last_name;
            if ($this->ion_auth->in_group('employer', $member_id)) {
                $username = $user->company;
            }

            $mail_data['to'] = $user->email;

            $template = $this->Thrift_model->getEmailTempltateByType('recieved_disbursement');

            if ($template) {
                $subject = $template->email_template_subject;

                $template_message = $template->email_template;

                /*-------*/
                $base_url = $this->getBaseUrl($member_id);

                $actual_link = $base_url . 'thrift_module/view_thrift/' . $thrift_group_id;
                $amount_value = $this->getCurrencySign() .
                    number_format($amt, 2, '.', ',');

                $find = array("{{username}}", "{{actual_link}}", "{{amount_value}}");
                $replace = array($username, $actual_link, $amount_value);
                $message = str_replace($find, $replace, $template_message);

                /*--------*/
                $mail_data['subject'] = $subject;
                $mail_data['message'] = $message;

                $this->sendEmail($mail_data);
            }

        }
    }


    private function increaseUserRatings($thrift_group_id)
    {
        $member_ids = $this->Thrift_model->getThriftMembersIds($thrift_group_id);

        $flaten_mem_ids = array();

        if ($member_ids) {
            foreach ($member_ids as $member_id) {

                $flaten_mem_ids[] = $member_id['thrift_group_member_id'];
            }
        }

        if (!empty($flaten_mem_ids)) {

            $this->Thrift_model->increaseUsersRatings($flaten_mem_ids);

        }


    }



    /* -------------------- Logic for new thrift List <ends> --------------------  */


    /* -------------------- Thrift List <starts> --------------------  */
    public function showThrifts()
    {
        if ($this->session->userdata('user_id') == null) {
            redirect('/');
        }

        $which_list = $this->uri->segment(3);

        /* only admin can see all thrifts */
        if ($which_list == 'all') {
            if (!$this->ion_auth->is_admin()) {
                redirect('users/auth/need_permission');
            }
        }

        /* only admin can see own thrifts */
        if ($which_list == 'my') {
            if (!$this->ion_auth->in_group('employee')) {
                redirect('users/auth/does_not_exist');
            }
        }

        /* only employee can see own emloyees' thrifts */
        if ($which_list == 'my_employees') {
            if (!$this->ion_auth->in_group('employer')) {
                redirect('users/auth/does_not_exist');
            }
        }

        $this->showThriftList($which_list);
    }

    private function showThriftList($which_list)
    {
        $this->lang->load('thrift_list');

        $data = array();
        $data['which_list'] = $which_list;

        $data['products'] = $this->Thrift_model->getAllProducts();

        $this->load->view("common_module/header");
        //$this->load->view("common_module/common_left");
        $this->load->view("thrift_module/thrift_list_page", $data);
        $this->load->view("common_module/footer");

    }

    public function getThriftsByAjax()
    {
        $this->lang->load('thrift_list');

        $thrifts = array();

        $which_list = $_REQUEST['which_list'];

        $user_id = $this->session->userdata('user_id');

        $requestData = $_REQUEST;
        //print_r($requestData);

        $columns[0] = 'thrift_group_number';
        $columns[1] = 'thrift_group_product_id';
        $columns[2] = 'thrift_group_product_price';
        $columns[3] = 'thrift_group_completion';
        $columns[4] = 'thrift_group_member_status';
        $columns[5] = 'thrift_group_activation_status';
        $columns[6] = 'thrift_group_open';
        $columns[7] = 'thrift_group_creation_date';
        $columns[8] = 'thrift_group_start_date';
        $columns[9] = 'actions';

        $common_filter_value = false;
        $order_column = false;

        $specific_filters = array();
        $specific_filters = false;

        if (!empty($requestData['columns'][0]['search']['value'])) {
            $specific_filters['thrift_group_number'] = $requestData['columns'][0]['search']['value'];
        }

        if (!empty($requestData['columns'][1]['search']['value'])) {
            $specific_filters['thrift_group_product_id'] = $requestData['columns'][1]['search']['value'];
        }

        if (!empty($requestData['columns'][2]['search']['value'])) {
            $specific_filters['thrift_group_product_price'] = $requestData['columns'][2]['search']['value'];
        }

        if (!empty($requestData['columns'][3]['search']['value'])) {
            $specific_filters['thrift_group_completion'] = $requestData['columns'][3]['search']['value'];
        }

        if (!empty($requestData['columns'][4]['search']['value'])) {
            $specific_filters['thrift_group_member_status'] = $requestData['columns'][4]['search']['value'];
        }

        if (!empty($requestData['columns'][5]['search']['value'])) {
            $specific_filters['thrift_group_activation_status'] = $requestData['columns'][5]['search']['value'];
        }

        if (!empty($requestData['columns'][6]['search']['value'])) {
            $specific_filters['thrift_group_open'] = $requestData['columns'][6]['search']['value'];
        }


        if (!empty($requestData['search']['value'])) {
            $common_filter_value = $requestData['search']['value'];
        }

        if ($specific_filters == true || !empty($specific_filters)) {
            $common_filter_value = false;       //either search with specific filters or with common filter
        }

        $order['column'] = $columns[$requestData['order'][0]['column']];
        $order['by'] = $requestData['order'][0]['dir'];


        $limit['start'] = $requestData['start'];
        $limit['length'] = $requestData['length'];

        $totalData = $this->Thrift_model->countThrifts(false, false, $which_list, $user_id);

        if ($common_filter_value == true || $specific_filters == true) {

            $totalFiltered = $this->Thrift_model->countThrifts($common_filter_value, $specific_filters, $which_list, $user_id);
        } else {
            $totalFiltered = $totalData; // when there is no search parameter then total number rows = total number filtered rows.
        }


        $thrifts = $this->Thrift_model->getThrifts($common_filter_value, $specific_filters, $order, $limit, $which_list, $user_id);

        if ($thrifts == false || empty($thrifts) || $thrifts == null) {
            $thrifts = false;
        }

        $last_query = $this->db->last_query();


        $this->load->library('custom_datetime_library');


        if ($thrifts) {
            $i = 0;
            foreach ($thrifts as $a_thrift) {

                /*product starts */
                $thrifts[$i]->prod = new stdClass();

                $product = $this->Thrift_model->getProduct($a_thrift->thrift_group_product_id);

                $thrifts[$i]->prod->html = $product->product_name;
                $thrifts[$i]->prod->int = $a_thrift->thrift_group_product_id;


                /* product ends*/

                $cont_amt = $a_thrift->thrift_group_product_price;

                $thrifts[$i]->cont = new stdClass();
                $thrifts[$i]->cont->html = $this->getCurrencySign() .
                    number_format($cont_amt, 2, '.', ',');
                $thrifts[$i]->cont->dec = $cont_amt;
                /*price starts*/

                /*date time starts*/
                $thrifts[$i]->cr_dt = new stdClass();
                $thrifts[$i]->st_dt = new stdClass();

                $thrifts[$i]->cr_dt->timestamp = $a_thrift->thrift_group_creation_date;
                $thrifts[$i]->st_dt->timestamp = $a_thrift->thrift_group_start_date;

                if ($a_thrift->thrift_group_creation_date == 0) {
                    $thrifts[$i]->cr_dt->display = $this->lang->line('creation_time_unknown_text');
                } else {
                    $thrifts[$i]->cr_dt->display =
                        $this->custom_datetime_library
                            ->convert_and_return_TimestampToDateAndTime($a_thrift->thrift_group_creation_date);
                }

                if ($a_thrift->thrift_group_start_date == 0) {
                    $thrifts[$i]->st_dt->display = $this->lang->line('not_started_text');
                } else {
                    $thrifts[$i]->st_dt->display =
                        $this->custom_datetime_library
                            ->convert_and_return_TimestampToDateAndTime($a_thrift->thrift_group_start_date);
                }
                /*date time ends*/

                /*complete-incomplete starts*/
                $thrifts[$i]->comp = new stdClass();
                $thrifts[$i]->comp->int = $a_thrift->thrift_group_current_cycle;
                $thrifts[$i]->comp->html = '(' . $a_thrift->thrift_group_current_cycle . '/' . $a_thrift->thrift_group_term_duration . ')';
                /*complete-incomplete ends*/

                /*full-not full starts*/
                $thrifts[$i]->mem = new stdClass();
                $thrifts[$i]->mem->int = $a_thrift->thrift_group_member_count;
                $thrifts[$i]->mem->html = '(' . $a_thrift->thrift_group_member_count . '/' . $a_thrift->thrift_group_member_limit . ')';
                /*full-not full ends*/

                /*active - inactive starts*/
                $thrifts[$i]->act = new stdClass();
                $thrifts[$i]->act->int = $a_thrift->thrift_group_activation_status;

                if ($a_thrift->thrift_group_activation_status == 1) {

                    $status_span = '<span class = "label label-primary">' . $this->lang->line('status_active_text') . '</span>';

                    $status_tooltip = $this->lang->line('tooltip_deactivate_text');
                    $status_url = base_url() . 'thrift_module/deactivate_thrift' . $a_thrift->thrift_group_id;
                    $status_anchor_span = '<span class="label label-danger"><i class="fa fa-times" aria-hidden="true"></i></span>';
                    $status_anchor =
                        '';

                } else {
                    $status_span = '<span class = "label label-default">' . $this->lang->line('status_inactive_text') . '</span>';

                    $status_tooltip = $this->lang->line('tooltip_activate_text');
                    $status_url = base_url() . 'thrift_module/activate_thrift' . $a_thrift->thrift_group_id;
                    $status_anchor_span = '<span class="label label-success"><i class="fa fa-check" aria-hidden="true"></i></span>';
                    $status_anchor =
                        '';
                }

                $thrifts[$i]->act->html = $status_span . '&nbsp; &nbsp;' . $status_anchor;
                /*active - inactive ends*/

                /*open - close starts*/
                $thrifts[$i]->opn = new stdClass();
                $thrifts[$i]->opn->int = $a_thrift->thrift_group_activation_status;

                if ($a_thrift->thrift_group_open == 1) {

                    $open_status_span = '<span class = "label label-primary">' . $this->lang->line('status_open_text') . '</span>';

                    $open_status_tooltip = $this->lang->line('tooltip_close_text');
                    $open_status_url = base_url() . 'thrift_module/close_thrift' . $a_thrift->thrift_group_id;
                    $open_status_anchor_span = '<span class="label label-danger"><i class="fa fa-times" aria-hidden="true"></i></span>';
                    $open_status_anchor =
                        '';

                } else {
                    $open_status_span = '<span class = "label label-default">' . $this->lang->line('status_close_text') . '</span>';

                    $open_status_tooltip = $this->lang->line('tooltip_open_text');
                    $open_status_url = base_url() . 'thrift_module/open_thrift' . $a_thrift->thrift_group_id;
                    $open_status_anchor_span = '<span class="label label-success"><i class="fa fa-check" aria-hidden="true"></i></span>';
                    $open_status_anchor =
                        '';
                }

                $thrifts[$i]->opn->html = $open_status_span . '&nbsp; &nbsp;' . $open_status_anchor;
                /*open-close ends*/

                /*action starts*/


                $view_tooltip = $this->lang->line('tooltip_view_text');
                $view_url = base_url() . 'thrift_module/view_thrift/' . $a_thrift->thrift_group_id;
                $view_anchor =
                    '<a ' . ' title="' . $view_tooltip . '" ' . ' href="' . $view_url . '" ' . ' style="color:#2b2b2b" ' . '>'
                    . '<i class="fa fa-eye fa-lg" aria-hidden="true"></i>'
                    . '</a>';


                $edit_tooltip = $this->lang->line('tooltip_edit_text');
                $edit_url = base_url() . 'thrift_module/edit_thrift/' . $a_thrift->thrift_group_id;
                $edit_anchor = '<a ' . ' title="' . $edit_tooltip . '" ' . ' href="' . $edit_url . '" ' . ' style="color:#2b2b2b" ' . '>'
                    . '<i class="fa fa-pencil-square-o fa-lg" aria-hidden="true"></i>'
                    . '</a>';

                $delete_tooltip = $this->lang->line('tooltip_delete_text');
                $delete_url = base_url() . 'thrift_module/delete/' . $a_thrift->thrift_group_id;
                $delete_anchor = '<a ' . ' title="' . $delete_tooltip . '" ' . ' href="' . $delete_url . '" ' . ' class="confirmation" ' . ' style="color:#2b2b2b" ' . '>'
                    . '<i class="fa fa-trash-o fa-lg" aria-hidden="true">'
                    . '</a>';

                $thrifts[$i]->action = $view_anchor;
                /*action ends*/

                $i++;

            }
        }


        $json_data['draw'] = intval($requestData['draw']);
        /* $totalData: for every request/draw by clientside ,
         they send a number as a parameter, when they recieve a response/data they first check the draw number,
         so we are sending same number in draw.*/
        $json_data['recordsTotal'] = intval($totalData); // total number of records after searching, if there is no searching then totalFiltered = totalData
        $json_data['recordsFiltered'] = intval($totalFiltered);

        //$thrifts = $this->removeKeys($thrifts); // converting to numeric indices.
        $json_data['data'] = $thrifts;

        // checking requests in console.log() for testing starts;
        //$json_data['last_query'] = $last_query;
        $json_data['common_filter_value'] = $common_filter_value;
        $json_data['specific_filters'] = $specific_filters;
        $json_data['order_column'] = $order['column'];
        $json_data['order_by'] = $order['by'];
        $json_data['limit_length'] = $limit['length'];
        $json_data['limit_start'] = $limit['start'];
        // checking requests in console.log() for testing ends;

        echo(json_encode($json_data));

    }

    /* -------------------- Thrift List <ends> --------------------  */


    public function viewThrift()
    {
        if ($this->session->userdata('user_id') == null) {
            redirect('/');
        }

        $this->lang->load('thrift_view');

        $thrift_group_id = $this->uri->segment(3);

        //write code to check condition if one should be able to view the thrift or not
        if ($this->chkPermissionToViewThrift($thrift_group_id) == false) {
            redirect('users/auth/need_permission');
        }


        $thrift = $this->Thrift_model->getThriftGroup($thrift_group_id);
        $thrift_members = $this->Thrift_model->getThriftMembers($thrift_group_id);
        $thrift_group_payments = $this->Thrift_model->getThriftGroupPayments($thrift_group_id);
        $thrift_group_payment_recieves = $this->Thrift_model->getThriftGroupPaymentRecieves($thrift_group_id);

        $thrift_group_term_duration = false;
        $thrift_group_product = false;
        $combined_payment_tables = false;

        $data = array();

        $data['custom_product_invitor'] = false;
        $data['custom_product_invitation'] = false;
        $data['custom_product_invited_members'] = false;
        $data['custom_product_accepted_members'] = false;

        if ($thrift) {

            $thrift_group_product = $this->Thrift_model->getProduct($thrift->thrift_group_product_id);


            $thrift_group_term_duration = $thrift->thrift_group_term_duration;

            if ($thrift->thrift_group_creation_date == 0) {
                $thrift->thrift_group_creation_datestring = $this->lang->line('unavailable_text');
            } else {
                $thrift->thrift_group_creation_datestring =
                    $this->custom_datetime_library->convert_and_return_TimestampToDateAndTime($thrift->thrift_group_creation_date);

            }

            if ($thrift->thrift_group_start_date == 0) {
                $thrift->thrift_group_start_datestring = $this->lang->line('not_started_text');
            } else {
                $thrift->thrift_group_start_datestring =
                    $this->custom_datetime_library->convert_and_return_TimestampToDateAndTime($thrift->thrift_group_start_date);
            }

            if ($thrift->thrift_group_end_date == 0) {
                $thrift->thrift_group_end_datestring = $this->lang->line('not_started_text');
            } else {
                $thrift->thrift_group_end_datestring =
                    $this->custom_datetime_library->convert_and_return_TimestampToDateAndTime($thrift->thrift_group_end_date);
            }


            //this section was commented as the client wanted a simple calculation , not the actual one
            /*$thrift_group_this_month_payment = $this->Thrift_model->get_this_month_payment($thrift_group_id);
            $thrift_group_this_month_disbursement = $this->Thrift_model->get_this_month_disbursement($thrift_group_id);


            if ($thrift_group_this_month_payment->total_payment_amount && $thrift_group_this_month_payment->total_payment_count != 0) {
                $thrift->thrift_group_this_month_payment =
                    $this->getCurrencySign() .
                    number_format($thrift_group_this_month_payment->total_payment_amount / $thrift_group_this_month_payment->total_payment_count, 2, '.', ',');
            } else {
                $thrift->thrift_group_this_month_payment =
                    $this->getCurrencySign() .
                    number_format(0, 2, '.', ',');
            }

            if ($thrift_group_this_month_disbursement->total_payment_recieve_amount && $thrift_group_this_month_disbursement->total_payment_recieve_count != 0) {
                $thrift->thrift_group_this_month_disbursement =
                    $this->getCurrencySign() .
                    number_format($thrift_group_this_month_disbursement->total_payment_recieve_amount / $thrift_group_this_month_disbursement->total_payment_recieve_count, 2, '.', ',');
            } else {
                $thrift->thrift_group_this_month_disbursement =
                    $this->getCurrencySign() .
                    number_format(0, 2, '.', ',');
            }*/

            $thrift->thrift_group_this_month_payment =
                $this->getCurrencySign() .
                number_format($thrift->thrift_group_product_price, 2, '.', ',');

            if ($thrift->thrift_group_member_count == 0) {
                $thrift->thrift_group_this_month_payment =
                    $this->getCurrencySign() .
                    number_format(0, 2, '.', ',');
            } else {
                $thrift->thrift_group_this_month_disbursement =
                    $this->getCurrencySign() .
                    number_format($thrift->thrift_group_product_price * ($thrift->thrift_group_member_count - 1), 2, '.', ',');
            }


            /*------------------invitation starts----------------------------*/


            $custom_product_invitation = false;
            $custom_product_invited_members = false;
            $custom_product_accepted_members = false;

            $cp_inv_mems = array();
            $cp_acc_mems = array();
            $cp_dec_mems = array();


            $custom_product_invitation = $this->Thrift_model->getCustomProductInvitationByThriftGroup($thrift_group_id);


            if ($custom_product_invitation) {

                $invitor = $this->Thrift_model->getEmployee($custom_product_invitation->cpi_created_by);

                if ($invitor) {
                    $invitor_name = $invitor->first_name . ' ' . $invitor->last_name . ' ' . '(' . $invitor->email . ')';
                    $data['custom_product_invitor'] = $invitor_name;
                } else {
                    $invitor_name = '';
                }

                $custom_product_invited_members = $this->Thrift_model->getCustomProductInvitationMembers($custom_product_invitation->cpi_id, $only_accepted_members = false, $only_declined_members = false);
                $custom_product_accepted_members = $this->Thrift_model->getCustomProductInvitationMembers($custom_product_invitation->cpi_id, $only_accepted_members = true, $only_declined_members = false);
                $custom_product_declined_members = $this->Thrift_model->getCustomProductInvitationMembers($custom_product_invitation->cpi_id, $only_accepted_members = false, $only_declined_members = true);


                $custom_product_invitation->cpi_start_datestring =
                    $this->custom_datetime_library->convert_and_return_TimestampToDateTimeGivenFormat($custom_product_invitation->cpi_start_date, 'Y-m-d');

                if ($custom_product_invited_members) {
                    $i = 0;
                    foreach ($custom_product_invited_members as $a_cpim) {

                        $cp_inv_mems[] = $a_cpim->cpi_inv_to;

                        $mem = $this->Thrift_model->getEmployee($a_cpim->cpi_inv_to);

                        $custom_product_invited_members[$i]->name = $mem->first_name . ' ' . $mem->last_name . ' ' . '(' . $mem->email . ')';


                        if ($a_cpim->cpi_inv_accepted == 1) {
                            $custom_product_invited_members[$i]->status = $this->lang->line('accepted_text');
                        } else if ($a_cpim->cpi_inv_accepted == -1) {
                            $custom_product_invited_members[$i]->status = $this->lang->line('declined_text');
                        } else {
                            $custom_product_invited_members[$i]->status = $this->lang->line('pending_text');

                        }

                        $i++;
                    }
                }

                if ($custom_product_accepted_members) {

                    $i = 0;
                    foreach ($custom_product_accepted_members as $a_cpim) {

                        $cp_acc_mems[] = $a_cpim->cpi_inv_to;

                        $mem = $this->Thrift_model->getEmployee($a_cpim->cpi_inv_to);

                        $custom_product_accepted_members[$i]->name = $mem->first_name . ' ' . $mem->last_name . ' ' . '(' . $mem->email . ')';

                        $i++;
                    }


                }

                if ($custom_product_declined_members) {

                    $i = 0;
                    foreach ($custom_product_declined_members as $a_cpim) {

                        $cp_dec_mems[] = $a_cpim->cpi_inv_to;

                        $i++;
                    }

                }

            }


            $data['custom_product_invitation'] = $custom_product_invitation;
            $data['custom_product_invited_members'] = $custom_product_invited_members;
            $data['custom_product_accepted_members'] = $custom_product_accepted_members;

            /*------------------invitation ends------------------------------*/


        }


        if ($thrift_members) {
            $i = 0;
            foreach ($thrift_members as $thrift_member) {

                $thrift_members[$i]->employer_company = $this->lang->line('unavailable_text');
                $thrift_members[$i]->member_full_name = $this->lang->line('unavailable_text');

                $member_details = $this->Thrift_model->getMember($thrift_member->thrift_group_member_id);

                if ($member_details) {

                    $thrift_members[$i]->mem_id_num = $member_details->mem_id_num;

                    if ($this->ion_auth->is_admin($member_details->id)) {
                        $thrift_members[$i]->member_full_name = 'Prosperis';
                    } else {
                        $thrift_members[$i]->member_full_name = $member_details->first_name . ' ' . $member_details->last_name;
                    }

                    $employer_details = null;

                    if ($member_details->user_employer_id > 0) {
                        $employer_details = $this->Thrift_model->getMember($member_details->user_employer_id);
                        $thrift_members[$i]->employer_company = $employer_details->company;

                    } else if ($this->ion_auth->is_admin($member_details->id)) {

                        $site_name = $this->custom_settings_library->getASettingsValue('general_settings', 'site_name');

                        if ($site_name == false || $site_name == null || $site_name == '') {
                            $site_name = 'Prosperis';
                        }
                        $thrift_members[$i]->employer_company = $site_name;
                    }


                    if ($thrift_members[$i]->thrift_group_member_join_date == 0) {
                        $thrift_members[$i]->thrift_group_member_join_datestring = $this->lang->line('unavailable_text');
                    } else {
                        $thrift_members[$i]->thrift_group_member_join_datestring =
                            $this->custom_datetime_library->convert_and_return_TimestampToDateAndTime($thrift_members[$i]->thrift_group_member_join_date);
                    }

                }


                $i++;
            }
        }


        if ($thrift_group_payments) {

            $i = 0;
            foreach ($thrift_group_payments as $a_group_payment) {

                $thrift_group_payments[$i]->thrift_group_payee_or_payer = $this->lang->line('payer_text');
                $thrift_group_payments[$i]->thrift_group_payee_or_payer_another_var = 'payer';
                $thrift_group_payments[$i]->thrift_group_payment_recieve_number = '';
                $thrift_group_payments[$i]->thrift_group_is_payment_recieved = 0;

                //be careful !!!
                $thrift_group_payments[$i]->thrift_group_member_id = $a_group_payment->thrift_group_payer_member_id;
                $thrift_group_payments[$i]->thrift_group_member_number = $a_group_payment->thrift_group_payer_member_number;

                $thrift_group_payments[$i]->member_full_name = $this->lang->line('unavailable_text');

                $member_details = $this->Thrift_model->getMember($a_group_payment->thrift_group_payer_member_id);

                if ($member_details) {

                    $thrift_group_payments[$i]->mem_id_num = $member_details->mem_id_num;

                    if ($this->ion_auth->is_admin($member_details->id)) {
                        $thrift_group_payments[$i]->member_full_name = 'Prosperis';
                    } else {
                        $thrift_group_payments[$i]->member_full_name = $member_details->first_name . ' ' . $member_details->last_name;
                    }


                    $employer_details = null;


                    if ($thrift_group_payments[$i]->thrift_group_payment_date == 0) {
                        $thrift_group_payments[$i]->thrift_group_payment_datestring = $this->lang->line('unavailable_text');
                    } else {

                        $thrift_group_payments[$i]->thrift_group_payment_datestring =
                            $this->custom_datetime_library->convert_and_return_TimestampToDateAndTime($thrift_group_payments[$i]->thrift_group_payment_date);
                    }


                    $thrift_group_payments[$i]->thrift_group_payment_amount_text =
                        $this->getCurrencySign() .
                        number_format($thrift_group_payments[$i]->thrift_group_payment_amount, 2, '.', ',');

                }


                $i++;

            }

        }

        if ($thrift_group_payment_recieves) {

            $i = 0;
            foreach ($thrift_group_payment_recieves as $a_group_payment_recieves) {

                $thrift_group_payment_recieves[$i]->thrift_group_payee_or_payer = $this->lang->line('payee_text');
                $thrift_group_payment_recieves[$i]->thrift_group_payee_or_payer_another_var = 'payee';
                $thrift_group_payment_recieves[$i]->thrift_group_payment_number = '';
                $thrift_group_payment_recieves[$i]->thrift_group_is_payment_paid = 0;

                $thrift_group_payment_recieves[$i]->member_full_name = $this->lang->line('unavailable_text');

                $member_details = $this->Thrift_model->getMember($a_group_payment_recieves->thrift_group_member_id);

                if ($member_details) {

                    $thrift_group_payment_recieves[$i]->mem_id_num = $member_details->mem_id_num;

                    if ($this->ion_auth->is_admin($member_details->id)) {
                        $thrift_group_payment_recieves[$i]->member_full_name = 'Prosperis';
                    } else {
                        $thrift_group_payment_recieves[$i]->member_full_name = $member_details->first_name . ' ' . $member_details->last_name;
                    }

                    $employer_details = null;


                    if ($thrift_group_payment_recieves[$i]->thrift_group_payment_date == 0) {
                        $thrift_group_payment_recieves[$i]->thrift_group_payment_datestring = $this->lang->line('unavailable_text');
                    } else {

                        $thrift_group_payment_recieves[$i]->thrift_group_payment_datestring =
                            $this->custom_datetime_library->convert_and_return_TimestampToDateAndTime($thrift_group_payment_recieves[$i]->thrift_group_payment_date);
                    }

                    $thrift_group_payment_recieves[$i]->thrift_group_payment_amount = $thrift_group_payment_recieves[$i]->thrift_group_payment_recieve_amount;
                    $thrift_group_payment_recieves[$i]->thrift_group_payment_amount_text =
                        $this->getCurrencySign() .
                        number_format($thrift_group_payment_recieves[$i]->thrift_group_payment_amount, 2, '.', ',');

                }


                $i++;

            }


            if ($thrift_group_term_duration != null && $thrift_group_term_duration > 0) {

                for ($cycle = 1; $cycle <= $thrift_group_term_duration; $cycle++) {

                    if ($thrift_group_payments) {

                        foreach ($thrift_group_payments as $a_group_payment) {

                            if ($a_group_payment->thrift_group_payment_cycle_number == $cycle) {
                                $combined_payment_tables[] = $a_group_payment;
                            }

                        }
                    }

                    if ($thrift_group_payment_recieves) {

                        foreach ($thrift_group_payment_recieves as $a_group_payment_recieve) {

                            if ($a_group_payment_recieve->thrift_group_payment_cycle_number == $cycle) {
                                $combined_payment_tables[] = $a_group_payment_recieve;
                            }

                        }
                    }


                }

            }

        }

        /*echo '<pre>';
        print_r($combined_payment_tables);die();*/

        $data['thrift'] = $thrift;
        $data['thrift_group_product'] = $thrift_group_product;
        $data['thrift_members'] = $thrift_members;
        $data['thrift_group_payments'] = $thrift_group_payments;
        $data['thrift_group_payment_recieves'] = $thrift_group_payment_recieves;


        $data['combined_payment_tables'] = $combined_payment_tables;

        //print_r( $data['combined_payment_tables']);die();

        $this->load->view("common_module/header");
        //$this->load->view("common_module/common_left");
        $this->load->view("thrift_module/thrift_view_page", $data);
        $this->load->view("common_module/footer");
    }

    private function chkPermissionToViewThrift($thrift_group_id)
    {
        if ($this->ion_auth->is_admin() || $this->ion_auth->in_group('trustee')) {
            return true;
        } else if ($this->ion_auth->in_group('employer')) {
            //code to be written
            return $this->Thrift_model->checkThriftViewPermission($thrift_group_id, $which_list = 'my_employees', $this->session->userdata('user_id'));
        } else if ($this->ion_auth->in_group('employee')) {
            //code to be written
            return $this->Thrift_model->checkThriftViewPermission($thrift_group_id, $which_list = 'my', $this->session->userdata('user_id'));
        } else {
            return false;
        }

    }

    /*--------------------------------------------------------------------------------------------------------*/

    public function addCustomProduct()
    {
        if ($this->session->userdata('user_id') == null) {
            redirect('/');
        }

        $this->lang->load('custom_product_form');

        if (!$this->ion_auth->in_group('employee')) {
            redirect('users/auth/need_permission');
        }

        if ($this->is_payment_method_on()) {

            if ($this->chkChosenPaymentMethod() == false) {

                $this->choosePaymentMethod('thrift_module/add_custom_product');
            }
        }


        $vdata = array();

        if ($this->input->post()) {
            $this->createCustomProduct();
        }

        $custom_thrift_start_delay = $this->custom_settings_library->getASettingsValue('prosperisgold_settings', 'custom_thrift_start_delay');
        $custom_thrift_max_start_time_from_delay = $this->custom_settings_library->getASettingsValue('prosperisgold_settings', 'custom_thrift_max_start_time_from_delay');

        $ctsd = false;
        if (!is_numeric($custom_thrift_start_delay) ||
            $custom_thrift_start_delay == 0 ||
            $custom_thrift_start_delay == false ||
            $custom_thrift_start_delay == null) {
            $vdata['custom_thrift_start_delay'] = "+3d";
        } else {
            $ctsd = true;
            $vdata['custom_thrift_start_delay'] = "+" . $custom_thrift_start_delay . "d";
        }

        if (!is_numeric($custom_thrift_max_start_time_from_delay) ||
            $custom_thrift_max_start_time_from_delay == 0 ||
            $custom_thrift_max_start_time_from_delay == false ||
            $custom_thrift_max_start_time_from_delay == null) {

            if ($ctsd) {
                $end_delay = $custom_thrift_start_delay + 15;
                $vdata['custom_thrift_end_delay'] = "+" . $end_delay . "d";
            } else {
                $vdata['custom_thrift_end_delay'] = "+15d";
            }

        } else {

            if ($ctsd) {
                $end_delay = $custom_thrift_start_delay + $custom_thrift_max_start_time_from_delay;
                $vdata['custom_thrift_end_delay'] = "+" . $end_delay . "d";
            } else {
                $vdata['custom_thrift_end_delay'] = "+15d";
            }
        }

        if ($this->inter_org_thrift_allowed()) {
            $vdata['choose_from_colleagues'] = false;
        } else {
            $vdata['choose_from_colleagues'] = true;
        }

        $vdata['custom_product_invitation'] = false;
        $vdata['custom_product_invited_members'] = false;
        $vdata['custom_product_accepted_members'] = false;

        $vdata['cp_inv_mems'] = false;
        $vdata['cp_acc_mems'] = false;
        $vdata['cp_dec_mems'] = false;

        $vdata['which_form'] = 'add';
        $vdata['form_action'] = 'thrift_module/add_custom_product';
        $vdata['currency_sign'] = $this->getCurrencySign();

        $vdata['cpi_id'] = false;
        $vdata['invitor_name'] = '';

        $this->load->view("common_module/header");
        //$this->load->view("common_module/common_left");
        $this->load->view("thrift_module/custom_product_form_page", $vdata);
        $this->load->view("common_module/footer");

    }


    public function createCustomProduct()
    {
        if ($this->session->userdata('user_id') == null) {
            redirect('/');
        }

        $this->session->set_flashdata('flash_product_price', trim($this->input->post('product_price')));
        $this->session->set_flashdata('flash_start_date', trim($this->input->post('start_date')));

        $invited_members_ids = $this->input->post('select_colleagues');

        if (!$invited_members_ids) {
            $this->session->set_flashdata('unsuccessful', 'unsuccessful');
            $this->session->set_flashdata('atleast_need_one_colleague', $this->lang->line('atleast_need_one_colleague_text'));
            redirect('thrift_module/add_custom_product');
        } else {
            $this->session->set_flashdata('flash_selected_ids', $invited_members_ids);
        }

        $this->form_validation->set_rules('product_price', 'Product Price', 'numeric|required|greater_than_equal_to[0]');
        $this->form_validation->set_rules('start_date', 'Start Date', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('unsuccessful', 'unsuccessful');
            $this->session->set_flashdata('validation_errors', validation_errors());
            redirect('thrift_module/add_custom_product');
        }

        if (!$this->chk_custom_product_create_cond(trim($this->input->post('product_price')))) {
            $this->session->set_flashdata('unsuccessful', 'unsuccessful');

            redirect('thrift_module/add_custom_product');
        }

        $thrift_group_id = $this->createCustomThrift();


        /*creating log starts*/
        $this->custom_log_library->createALog
        (
            $this->session->userdata('user_id'),                                    //1.    $created_by
            '',                                                                     //2.    $created_for
            'thrift_group',                                                         //3.    $type
            $thrift_group_id,                                                       //4.    $type_id
            'created',                                                              //5.    $activity
            'employee',                                                             //6.    $activity_by
            '',                                                                     //7.    $activity_for
            '',                                                                     //8.    $sub_type
            '',                                                                     //9.    $sub_type_id
            '',                                                                     //10.   $super_type
            '',                                                                     //11.   $super_type_id
            '',                                                                     //12.   $other_information
            ''                                                                      //13.   $change_list
        );
        /*creating log ends*/

        $this->sendCustomOrIndividualProductInitiationEmail($thrift_group_id, $this->session->userdata('user_id'));

        $p_data['cpi_thrift_group_id'] = $thrift_group_id;
        $p_data['cpi_number'] = $this->generateCpiNumber();
        $p_data['cpi_created_by'] = $this->session->userdata('user_id');
        $p_data['cpi_product_price'] = $this->input->post('product_price');
        $p_data['cpi_created_at'] = $this->custom_datetime_library->getCurrentTimestamp();
        $p_data['cpi_start_date'] = 0;


        if ($this->input->post('start_date')) {
            $p_data['cpi_start_date'] =
                $this
                    ->custom_datetime_library
                    ->convert_and_return_DateAndTime_To_Timestamp_byGivenDateTimeFormat($this->input->post('start_date'), 'Y-m-d');
        }


        $cpi_id = $this->Thrift_model->insertCustomProductInvitation($p_data);


        if ($invited_members_ids && $cpi_id) {

            $this->session->set_flashdata('successful', 'successful');
            $this->session->set_flashdata('custom_product_create_success', 'custom_product_create_success');

            $this->session->set_flashdata('flash_product_price', '');
            $this->session->set_flashdata('flash_start_date', '');

            $this->session->set_flashdata('flash_cpi_id', $cpi_id);

            $p_inv_data = array();
            foreach ($invited_members_ids as $im) {

                $p_inv_data['cpi_id'] = $cpi_id;
                $p_inv_data['cpi_inv_from'] = $this->session->userdata('user_id');
                $p_inv_data['cpi_inv_to'] = $im;

                $this->Thrift_model->insertCustomProductInvitedMembers($p_inv_data);

                $this->notifyCustomProductInvitationMessage($thrift_group_id, $cpi_id, $im);

            }

        }


        //assuming everything wet well...
        $this->session->set_flashdata('thrift_success', 'thrift_success');
        $this->session->set_flashdata('thrift_create_success_text', 'thrift_create_success_text');

        if ($thrift_group_id != 0) {
            $this->session->set_flashdata('flash_thrift_group_id', $thrift_group_id);
        }

        redirect('thrift_module/show_thrifts/my');

        /* do not redirect to here */
        //redirect('thrift_module/add_custom_product');

    }

    private function sendCustomOrIndividualProductInitiationEmail($thrift_group_id, $im)
    {
        $user = $this->Thrift_model->getEmployee($im);

        if ($user) {

            $username = $user->first_name . ' ' . $user->last_name;
            if ($this->ion_auth->in_group('employer', $im)) {
                $username = $user->company;
            }

            $mail_data['to'] = $user->email;

            $template = $this->Thrift_model->getEmailTempltateByType('thrift_initiation');

            if ($template) {
                $subject = $template->email_template_subject;

                $template_message = $template->email_template;

                /*-------*/
                $base_url = $this->getBaseUrl($im);

                $actual_link = $base_url . 'thrift_module/view_thrift/' . $thrift_group_id;

                $find = array("{{username}}", "{{actual_link}}");
                $replace = array($username, $actual_link);
                $message = str_replace($find, $replace, $template_message);

                /*--------*/
                $mail_data['subject'] = $subject;
                $mail_data['message'] = $message;

                $this->sendEmail($mail_data);
            }

        }
    }

    public function getMessageNumber()
    {
        $num = 'MSG';
        $num .= $this->alphaNum(10, false, false);

        $exists = $this->Thrift_model->doesMessageNumberExist($num);

        if ($exists) {

            $this->getMessageNumber();

        } else {

            return $num;

        }

    }

    private function notifyCustomProductInvitationMessage($thrift_group_id, $cpi_id, $im)
    {
        $this->lang->load('custom_product_invitation');

        $tooltip = $this->lang->line('tooltip_text');
        $url = base_url() . 'thrift_module/custom_product_thrift/view/' . $cpi_id;

        $message_text =
            '<p>' . $this->lang->line('custom_product_invitation_description_text') . '</p>'
            . '<p>' . $this->lang->line('click_link_text') . '</p>'
            . '<p>'
            . '<a ' . ' title="' . $tooltip . '" ' . ' href="' . $url . '" ' . '>'
            . $this->lang->line('view_thrift_text')
            . '</a>'
            . '</p>';

        $this->sendCustomProductInvitationMessage($message_text, $thrift_group_id, $cpi_id, $im);
        $this->sendCustomProductInvitationEmail($message_text, $thrift_group_id, $cpi_id, $im);
    }

    private function sendCustomProductInvitationMessage($message_text, $thrift_group_id, $cpi_id, $im)
    {
        $to_whom = 'to_colleague';
        $message_reciever_id = $im;

        $p_data['message_sender_id'] = $this->session->userdata('user_id');
        $p_data['message'] = $message_text;
        $p_data['message_number'] = $this->getMessageNumber();
        $p_data['message_type'] = 'custom_product_invitation';
        $p_data['message_to_whom'] = $to_whom;
        $p_data['message_created_at'] = $this->custom_datetime_library->getCurrentTimestamp();
        $p_data['message_deleted_by_sender'] = 0;
        $p_data['message_archived_by_sender'] = 0;

        $inserted_message_id = $this->Thrift_model->insertMessage($p_data);

        $pr_data['message_id'] = $inserted_message_id;
        $pr_data['message_reciever_id'] = $message_reciever_id;
        $pr_data['reciever_read'] = 0;
        $pr_data['reciever_read_at'] = 0;
        $pr_data['message_deleted_by_reciever'] = 0;
        $pr_data['message_archived_by_reciever'] = 0;

        $this->Thrift_model->insertMessageReciever($pr_data);
    }

    private function sendCustomProductInvitationEmail($message_text, $thrift_group_id, $cpi_id, $im)
    {
        $user = $this->Thrift_model->getEmployee($im);

        if ($user) {

            $username = $user->first_name . ' ' . $user->last_name;
            if ($this->ion_auth->in_group('employer', $im)) {
                $username = $user->company;
            }

            $mail_data['to'] = $user->email;

            $template = $this->Thrift_model->getEmailTempltateByType('thrift_invitation');

            if ($template) {
                $subject = $template->email_template_subject;

                $template_message = $template->email_template;

                /*-------*/
                $base_url = $this->getBaseUrl($im);

                $actual_link = $base_url . 'thrift_module/custom_product_thrift/view/' . $cpi_id;

                $find = array("{{username}}", "{{actual_link}}");
                $replace = array($username, $actual_link);
                $message = str_replace($find, $replace, $template_message);

                /*--------*/
                $mail_data['subject'] = $subject;
                $mail_data['message'] = $message;

                $this->sendEmail($mail_data);
            }

        }
    }

    private function sendEmail($mail_data)
    {

        $site_name = $this->custom_settings_library->getASettingsValue('general_settings', 'site_name');
        $site_email = $this->custom_settings_library->getASettingsValue('general_settings', 'site_email');

        if (!$site_name) {
            $site_name = 'Prosperis';
        }

        if (!$site_email) {
            $site_email = 'prosperis@info.com';
        }

        try {

            //$mail_data['to'] = 'mahmud@sahajjo.com';
            $this->email->from($site_email, $site_name);
            $this->email->to($mail_data['to']);


            $this->email->subject($mail_data['subject']);
            $this->email->message($mail_data['message']);
            $this->email->set_mailtype("html");

            /*echo '<hr>'.'<br>';
            echo $mail_data['subject'].'<br>';
            echo $mail_data['message'],'<br>';
            echo '<hr>'.'<br>';*/

            @$this->email->send();

            /*$headers = 'From: '.$site_email.'\r\n';
            mail(mail_data['to'],$mail_data['subject'],$mail_data['message'],$headers);*/

        } catch (Exception $e) {
            // echo $e->getMessage();
        }

    }

    private function createCustomThrift()
    {
        $ins_data['thrift_group_number'] = $this->generateThriftGroupNumber();
        $ins_data['thrift_group_product_id'] = 0;

        $ins_data['thrift_group_is_custom_product'] = 1;
        $ins_data['thrift_group_custom_product_created_by'] = $this->session->userdata('user_id');

        $ins_data['thrift_group_product_price'] = $this->input->post('product_price');
        $ins_data['thrift_group_term_duration'] = 0; // will count later
        $ins_data['thrift_group_member_count'] = 0;
        $ins_data['thrift_group_member_limit'] = 0; // will count later
        $ins_data['thrift_group_activation_status'] = 1;
        $ins_data['thrift_group_open'] = 1;
        $ins_data['thrift_group_creation_date'] = $this->custom_datetime_library->getCurrentTimestamp();

        //when auto bot assign starts, this will be ignored for the custom product
        $ins_data['thrift_group_threshold_date'] = 0;

        //uncomment this
        $ins_data['thrift_group_start_date'] =
            $this
                ->custom_datetime_library
                ->convert_and_return_DateAndTime_To_Timestamp_byGivenDateTimeFormat($this->input->post('start_date'), 'Y-m-d');

        //testcheck
        //uncomment prev line and remove this. It is only for testing
        //$ins_data['thrift_group_start_date'] = $this->custom_datetime_library->getCurrentTimestamp();


        $ins_data['thrift_group_end_date'] = 0;

        $thrift_group_id = $this->Thrift_model->insertThriftGroup($ins_data);

        if ($this->is_payment_method_on()) {
            $this->createNewPaystackPlan($ins_data['thrift_group_product_price'], $thrift_group_id, $product_name = false);
        }


        $this->joinCustomThriftGroup($thrift_group_id, $admin_id = 1); //add admin to thrift as first member
        $this->joinCustomThriftGroup($thrift_group_id, $this->session->userdata('user_id')); //add creator to thrift as second member

        return $thrift_group_id;
    }

    private function joinCustomThriftGroup($thrift_group_id, $member_id)
    {
        $thrift_group_member_count = 0;

        $thrift_group = $this->Thrift_model->getThriftGroup($thrift_group_id);

        if ($thrift_group) {

            if (is_numeric($thrift_group->thrift_group_member_count)) {
                $thrift_group_member_count = $thrift_group->thrift_group_member_count;
            }
        }


        if ($thrift_group->thrift_group_open == 1) {

            /*insert in pg_thrift_group_members */
            $ins_data['thrift_group_id'] = $thrift_group_id;
            $ins_data['thrift_group_member_id'] = $member_id;
            $ins_data['thrift_group_member_number'] = $thrift_group_member_count + 1;
            $ins_data['thrift_group_member_join_date'] = $this->custom_datetime_library->getCurrentTimestamp();


            $upd_data['thrift_group_member_count'] = $thrift_group_member_count + 1;

            $inserted = false;

            $inserted = $this->Thrift_model->insertEmployeeInThriftGroup($ins_data); //return bool

            /*creating log starts*/

            $activity_by = '';

            if ($this->ion_auth->is_admin($member_id)) {
                $activity_by = 'admin';
            } else if ($this->ion_auth->in_group('employee', $member_id)) {
                $activity_by = 'employee';
            }

            $this->custom_log_library->createALog
            (
                $member_id,                                                             //1.    $created_by
                '',                                                                     //2.    $created_for
                'thrift_group',                                                         //3.    $type
                $thrift_group_id,                                                       //4.    $type_id
                'joined',                                                               //5.    $activity
                $activity_by,                                                           //6.    $activity_by
                '',                                                                     //7.    $activity_for
                '',                                                                     //8.    $sub_type
                '',                                                                     //9.    $sub_type_id
                '',                                                                     //10.   $super_type
                '',                                                                     //11.   $super_type_id
                '',                                                                     //12.   $other_information
                ''                                                                      //13.   $change_list
            );
            /*creating log ends*/


            if ($inserted) {
                $this->Thrift_model->updateThriftGroup($upd_data, $thrift_group_id);
            }


        }

        return $thrift_group_id;

    }

    public function autoStartCustomThriftGroup()
    {

        $thrift_group = false;
        $curr_ts = $this->custom_datetime_library->getCurrentTimestamp();
        $thrift_groups = $this->Thrift_model->getTimeCrossedCustomThriftGroups($curr_ts);

        if ($thrift_groups) {
            //load only a single one at time
            $thrift_group = $thrift_groups[0];
        }

        if ($thrift_group) {

            $inv_accepted_members = $this->Thrift_model->getCustomProductAcceptedMembers($thrift_group->thrift_group_id);

            if ($inv_accepted_members) {

                //do not auto worry about hte order as already fetched in a order tho accepted first
                foreach ($inv_accepted_members as $iam) {
                    $joined = $this->joinCustomThriftGroup($thrift_group->thrift_group_id, $iam->cpi_inv_to);

                    if ($joined) {
                        //send inbox message notifying the user that thrift started
                        //send email notifying the user that thrift started
                    }

                }

                //update the thrift group and set up payments
                if ($this->proceedCustomThriftGroup($thrift_group->thrift_group_id, count($inv_accepted_members))) {
                    $this->removeInvitation($thrift_group->thrift_group_id);
                }

            } else {
                //send inbox message notifying the user that thrift started
                //send email notifying the user that thrift started

                /*creating log starts*/
                $this->custom_log_library->createALog
                (
                    '',                                                                     //1.    $created_by
                    '',                                                                     //2.    $created_for
                    'thrift_group',                                                         //3.    $type
                    $thrift_group->thrift_group_id,                                         //4.    $type_id
                    'thrift_deleted',                                                       //5.    $activity
                    '',                                                                     //6.    $activity_by
                    '',                                                                     //7.    $activity_for
                    '',                                                                     //8.    $sub_type
                    '',                                                                     //9.    $sub_type_id
                    '',                                                                     //10.   $super_type
                    '',                                                                     //11.   $super_type_id
                    '',                                                                     //12.   $other_information
                    ''                                                                      //13.   $change_list
                );
                /*creating log ends*/

                $this->notifyThriftGroupRemoved($thrift_group->thrift_group_id, $reason = 'no_member_joined');

                $this->removeThriftGroupAndInvitation($thrift_group->thrift_group_id);
            }

        }

    }

    private function notifyThriftGroupRemoved($thrift_group_id, $reason)
    {
        $invited_members = $this->Thrift_model->getCustomProductInvitaionWithMembers($thrift_group_id);

        $thrift_group = $this->Thrift_model->getThriftGroup($thrift_group_id);

        $thrift_group ? $thrift_group->thrift_group_number : 'unavailable';

        if ($thrift_group) {
            $thrift_group_number = $thrift_group->thrift_group_number;
        } else {
            $thrift_group_number = 'unavailable';
        }

        $this->lang->load('thrift_group_removed');
        $message_text =
            '<p>' . $this->lang->line('thrift_group_removed_text') . '</p>'
            . '<p>' . $this->lang->line('thrift_group_id_text') . ':' . $thrift_group_number . '</p>'
            . '<p>' . $this->lang->line('reason_text') . ':' . $this->lang->line($reason . '_text') . '</p>';

        if ($invited_members) {
            foreach ($invited_members as $i_mem) {
                $this->notifyThriftGroupRemovedViaMessage($message_text, $thrift_group_id, $reason, $i_mem->cpi_inv_to);
                $this->notifyThriftGroupRemovedViaEmail($thrift_group_id, $this->lang->line($reason . '_text'), $i_mem->cpi_inv_to);
            }
        }

    }

    private function notifyThriftGroupRemovedViaMessage($message_text, $thrift_group_id, $reason, $im_id)
    {
        $to_whom = 'to_thrifters';

        $message_reciever_id = $im_id;

        if ($this->session->userdata('user_id')) {
            $message_sender_id = $this->session->userdata('user_id');
        } else {
            $message_sender_id = 1;
        }

        $p_data['message_sender_id'] = $message_sender_id;
        $p_data['message'] = $message_text;
        $p_data['message_number'] = $this->getMessageNumber();
        $p_data['message_type'] = 'thrift_group_removed';
        $p_data['message_to_whom'] = $to_whom;
        $p_data['message_created_at'] = $this->custom_datetime_library->getCurrentTimestamp();
        $p_data['message_deleted_by_sender'] = 0;
        $p_data['message_archived_by_sender'] = 0;

        $inserted_message_id = $this->Thrift_model->insertMessage($p_data);


        $pr_data['message_id'] = $inserted_message_id;
        $pr_data['message_reciever_id'] = $message_reciever_id;
        $pr_data['reciever_read'] = 0;
        $pr_data['reciever_read_at'] = 0;
        $pr_data['message_deleted_by_reciever'] = 0;
        $pr_data['message_archived_by_reciever'] = 0;

        $this->Thrift_model->insertMessageReciever($pr_data);
    }

    private function notifyThriftGroupRemovedViaEmail($thrift_group_id, $reason, $im_id)
    {
        $user = $this->Thrift_model->getEmployee($im_id);
        $tg = $this->Thrift_model->getThriftGroup($thrift_group_id);
        if ($user && $tg) {

            $thrift_group_number = $tg->thrift_group_number;
            $username = $user->first_name . ' ' . $user->last_name;

            $mail_data['to'] = $user->email;

            $template = $this->Thrift_model->getEmailTempltateByType('thrift_cancelled');

            if ($template) {
                $subject = $template->email_template_subject;

                $template_message = $template->email_template;


                $find = array("{{username}}", "{{reason}}", "{{thrift_group_number}}");
                $replace = array($username, $reason, $thrift_group_number);
                $message = str_replace($find, $replace, $template_message);

                /*--------*/

                $mail_data['subject'] = $subject;
                $mail_data['message'] = $message;

                $this->sendEmail($mail_data);

            }
        }
    }

    private function proceedCustomThriftGroup($thrift_group_id, $inv_accepted_members_count)
    {
        $upd_data['thrift_group_member_count'] = 2 + $inv_accepted_members_count; //admin and the creator is already in the thrift group
        $upd_data['thrift_group_member_limit'] = $upd_data['thrift_group_member_count'];
        $upd_data['thrift_group_term_duration'] = $upd_data['thrift_group_member_count'];
        $upd_data['thrift_group_open'] = 0;

        $this->Thrift_model->updateThriftGroup($upd_data, $thrift_group_id);

        $thrift_group = $this->Thrift_model->getThriftGroup($thrift_group_id);

        /*creating log starts*/
        $this->custom_log_library->createALog
        (
            '',                                                                     //1.    $created_by
            '',                                                                     //2.    $created_for
            'thrift_group',                                                         //3.    $type
            $thrift_group_id,                                                       //4.    $type_id
            'thrift_started',                                                       //5.    $activity
            '',                                                                     //6.    $activity_by
            '',                                                                     //7.    $activity_for
            '',                                                                     //8.    $sub_type
            '',                                                                     //9.    $sub_type_id
            '',                                                                     //10.   $super_type
            '',                                                                     //11.   $super_type_id
            '',                                                                     //12.   $other_information
            ''                                                                      //13.   $change_list
        );
        /*creating log ends*/

        $this->notifyThriftStarted($thrift_group_id);

        $this->setUpThriftGroupPayments($thrift_group_id, $thrift_group->thrift_group_start_date);

        if ($this->is_payment_method_on()) {
            $this->setUpPaystackSubscriptionForEmployees($thrift_group_id);
        }

        return true;
    }


    private function notifyThriftStarted($thrift_group_id)
    {
        //get all employee and email them that thrift started;

        $member_ids = $this->Thrift_model->getThriftMembersIds($thrift_group_id);

        if ($member_ids) {
            foreach ($member_ids as $member_id) {
                $this->notifyMemberThatThriftStarted($thrift_group_id, $member_id['thrift_group_member_id']);
            }
        }
    }

    private function notifyThriftEnded($thrift_group_id)
    {
        //get all employee and email them that thrift ended;

        $member_ids = $this->Thrift_model->getThriftMembersIds($thrift_group_id);

        if ($member_ids) {
            foreach ($member_ids as $member_id) {

                $this->notifyMemberThatThriftEnded($thrift_group_id, $member_id['thrift_group_member_id']);
            }
        }
    }

    private function notifyMemberThatThriftStarted($thrift_group_id, $member_id)
    {
        $user = $this->Thrift_model->getEmployee($member_id);

        //do not need to send email to admin and bot admins
        if ($user && !$this->ion_auth->in_group('admin', $member_id)) {

            $username = $user->first_name . ' ' . $user->last_name;
            if ($this->ion_auth->in_group('employer', $member_id)) {
                $username = $user->company;
            }

            $mail_data['to'] = $user->email;

            $template = $this->Thrift_model->getEmailTempltateByType('thrift_started');

            if ($template) {
                $subject = $template->email_template_subject;

                $template_message = $template->email_template;

                /*-------*/
                $base_url = $this->getBaseUrl($member_id);

                $actual_link = $base_url . 'thrift_module/view_thrift/' . $thrift_group_id;

                $find = array("{{username}}", "{{actual_link}}");
                $replace = array($username, $actual_link);
                $message = str_replace($find, $replace, $template_message);

                /*--------*/
                $mail_data['subject'] = $subject;
                $mail_data['message'] = $message;

                $this->sendEmail($mail_data);
            }

        }
    }

    private function notifyMemberThatThriftEnded($thrift_group_id, $member_id)
    {
        $user = $this->Thrift_model->getEmployee($member_id);

        //do not need to send email to admin and bot admins
        if ($user && !$this->ion_auth->in_group('admin', $member_id)) {

            $username = $user->first_name . ' ' . $user->last_name;
            if ($this->ion_auth->in_group('employer', $member_id)) {
                $username = $user->company;
            }

            $mail_data['to'] = $user->email;

            $template = $this->Thrift_model->getEmailTempltateByType('thrift_ended');

            if ($template) {
                $subject = $template->email_template_subject;

                $template_message = $template->email_template;

                /*-------*/
                $base_url = $this->getBaseUrl($member_id);

                $actual_link = $base_url . 'thrift_module/view_thrift/' . $thrift_group_id;

                $find = array("{{username}}", "{{actual_link}}");
                $replace = array($username, $actual_link);
                $message = str_replace($find, $replace, $template_message);

                /*--------*/
                $mail_data['subject'] = $subject;
                $mail_data['message'] = $message;

                $this->sendEmail($mail_data);
            }

        }
    }

    /*the functions below are actually removing rows from table, be careful before passing id*/
    /* be careful <starts>*/

    public function deleteCustomProduct()
    {
        $cpi_id = $this->uri->segment(3);

        $invitation = $this->Thrift_model->getInvitaionByCpiId($cpi_id);

        if ($invitation) {

            $thrift_group_id = $invitation->cpi_thrift_group_id;

            //get the accepted members and notify them via message and email
            $this->notifyThriftGroupRemoved($thrift_group_id, $reason = 'creator_deleted');

            /*creating log starts*/
            $this->custom_log_library->createALog
            (
                $this->session->userdata('user_id'),                                    //1.    $created_by
                '',                                                                     //2.    $created_for
                'thrift_group',                                                         //3.    $type
                $thrift_group_id,                                                       //4.    $type_id
                'deleted',                                                              //5.    $activity
                'employee',                                                             //6.    $activity_by
                '',                                                                     //7.    $activity_for
                '',                                                                     //8.    $sub_type
                '',                                                                     //9.    $sub_type_id
                '',                                                                     //10.   $super_type
                '',                                                                     //11.   $super_type_id
                '',                                                                     //12.   $other_information
                ''                                                                      //13.   $change_list
            );
            /*creating log ends*/

            $this->removeThriftGroupAndInvitation($thrift_group_id);

            $this->session->set_flashdata('success', 'success');
            $this->session->set_flashdata('thrift_delete_success_text', 'thrift_delete_success_text');
        }

        redirect('thrift_module/custom_product_list/created');
    }

    private function removeThriftGroupAndInvitation($thrift_group_id)
    {
        $invitaion = $this->Thrift_model->getInvitaionByThriftGroupId($thrift_group_id);

        if ($invitaion) {
            $cpi_id = $invitaion->cpi_id;
            $this->Thrift_model->deleteInvitation($cpi_id);
            $this->Thrift_model->deleteInvitedMembers($cpi_id);
        }

        $this->Thrift_model->deleteThriftGroup($thrift_group_id);
        $this->Thrift_model->deleteThriftGroupMembers($thrift_group_id);

    }

    private function removeInvitation($thrift_group_id)
    {
        $invitaion = $this->Thrift_model->getInvitaionByThriftGroupId($thrift_group_id);

        if ($invitaion) {
            $cpi_id = $invitaion->cpi_id;
            $this->Thrift_model->deleteInvitation($cpi_id);
            $this->Thrift_model->deleteInvitedMembers($cpi_id);
        }
    }

    /* be careful <ends>*/

    public function EditOrViewCustomProduct()
    {
        if ($this->session->userdata('user_id') == null) {
            redirect('/');
        }

        $which_form = $this->uri->segment(3);
        $cpi_id = $this->uri->segment(4);


        $this->lang->load('custom_product_form');

        if (!$this->ion_auth->in_group('employee')) {
            redirect('users/auth/need_permission');
        }

        /*currently not allowing to edit the invitation form */
        if ($which_form == 'edit') {
            redirect('users/auth/does_not_exist');
        }

        /*not updating currently*/
        if ($this->input->post()) {
            $this->updateCustomProduct();
        }

        $custom_product_invitation = false;
        $custom_product_invited_members = false;
        $custom_product_accepted_members = false;

        $cp_inv_mems = array();
        $cp_acc_mems = array();
        $cp_dec_mems = array();


        $custom_product_invitation = $this->Thrift_model->getCustomProductInvitation($cpi_id);

        if (!$custom_product_invitation) {
            redirect('users/auth/does_not_exist');
        }

        if ($custom_product_invitation) {

            $invitor = $this->Thrift_model->getEmployee($custom_product_invitation->cpi_created_by);

            if ($invitor) {
                $invitor_name = $invitor->first_name . ' ' . $invitor->last_name . ' ' . '(' . $invitor->email . ')';
            } else {
                $invitor_name = '';
            }

            $custom_product_invited_members = $this->Thrift_model->getCustomProductInvitationMembers($cpi_id, $only_accepted_members = false, $only_declined_members = false);
            $custom_product_accepted_members = $this->Thrift_model->getCustomProductInvitationMembers($cpi_id, $only_accepted_members = true, $only_declined_members = false);
            $custom_product_declined_members = $this->Thrift_model->getCustomProductInvitationMembers($cpi_id, $only_accepted_members = false, $only_declined_members = true);


            $custom_product_invitation->cpi_start_datestring =
                $this->custom_datetime_library->convert_and_return_TimestampToDateTimeGivenFormat($custom_product_invitation->cpi_start_date, 'Y-m-d');

            if ($custom_product_invited_members) {
                $i = 0;
                foreach ($custom_product_invited_members as $a_cpim) {

                    $cp_inv_mems[] = $a_cpim->cpi_inv_to;

                    $mem = $this->Thrift_model->getEmployee($a_cpim->cpi_inv_to);

                    $custom_product_invited_members[$i]->name = $mem->first_name . ' ' . $mem->last_name . ' ' . '(' . $mem->email . ')';


                    if ($a_cpim->cpi_inv_accepted == 1) {
                        $custom_product_invited_members[$i]->status = $this->lang->line('accepted_text');
                    } else if ($a_cpim->cpi_inv_accepted == -1) {
                        $custom_product_invited_members[$i]->status = $this->lang->line('declined_text');
                    } else {
                        $custom_product_invited_members[$i]->status = $this->lang->line('pending_text');

                    }

                    $i++;
                }
            }

            if ($custom_product_accepted_members) {

                $i = 0;
                foreach ($custom_product_accepted_members as $a_cpim) {

                    $cp_acc_mems[] = $a_cpim->cpi_inv_to;

                    $mem = $this->Thrift_model->getEmployee($a_cpim->cpi_inv_to);

                    $custom_product_accepted_members[$i]->name = $mem->first_name . ' ' . $mem->last_name . ' ' . '(' . $mem->email . ')';

                    $i++;
                }


            }

            if ($custom_product_declined_members) {

                $i = 0;
                foreach ($custom_product_declined_members as $a_cpim) {

                    $cp_dec_mems[] = $a_cpim->cpi_inv_to;

                    $i++;
                }

            }

        }

        if ($this->inter_org_thrift_allowed()) {
            $vdata['choose_from_colleagues'] = false;
        } else {
            $vdata['choose_from_colleagues'] = true;
        }

        $vdata['custom_product_invitation'] = $custom_product_invitation;
        $vdata['custom_product_invited_members'] = $custom_product_invited_members;
        $vdata['custom_product_accepted_members'] = $custom_product_accepted_members;

        $vdata['cp_inv_mems'] = $cp_inv_mems;
        $vdata['cp_acc_mems'] = $cp_acc_mems;
        $vdata['cp_dec_mems'] = $cp_dec_mems;

        $vdata['which_form'] = $which_form;
        $vdata['cpi_id'] = $cpi_id;

        $vdata['invitor_name'] = $invitor_name;


        $vdata['form_action'] = '';
        if ($which_form == 'edit') {
            $vdata['form_action'] = 'thrift_module/custom_product_thrift/edit/' . $cpi_id;
        }

        $vdata['currency_sign'] = $this->getCurrencySign();


        $this->load->view("common_module/header");
        //$this->load->view("common_module/common_left");
        $this->load->view("thrift_module/custom_product_form_page", $vdata);
        $this->load->view("common_module/footer");
    }


    public function getColleaguesBySelect2()
    {
        $my_id = $this->session->userdata('user_id');
        $my_details = $this->Thrift_model->getEmployee($my_id);
        $employer_id = false;

        $total_count = 0;
        $more_pages = false;
        $last_query = 'No query attempted';

        if ($my_details->user_employer_id != null || $my_details->user_employer_id > 0) {
            $employer_id = $my_details->user_employer_id;
        }

        if (isset($_REQUEST['choose_from_colleagues'])) {
            $choose_from_colleagues = $_REQUEST['choose_from_colleagues'];
        } else {
            $choose_from_colleagues = 'yes';
        }

        if ($choose_from_colleagues == 'no') {
            $employer_id = false;
        }

        if (isset($_REQUEST['keyword'])) {
            $keyword = $_REQUEST['keyword'];
        } else {
            $keyword = '';
        }

        if (isset($_REQUEST['page'])) {
            $page = $_REQUEST['page'];
        } else {
            $page = 1;
        }


        $limit = 10;

        $total_count = $this->Thrift_model->countTotalColleaguesBySelect2($my_id, $employer_id, $keyword);
        $offset = ($page - 1) * $limit;

        $end_count = $offset + $limit;
        $more_pages = $total_count > $end_count;

        $colleagues = $this->Thrift_model->getTotalColleaguesBySelect2($my_id, $employer_id, $keyword, $limit, $offset);

        $last_query = $this->db->last_query();

        $json_data = array();
        $items = array();


        if ($colleagues) {

            foreach ($colleagues as $a_colleage) {
                $p = array();
                $p['id'] = $a_colleage->id;
                $p['text'] =
                    $a_colleage->first_name . ' ' . $a_colleage->last_name . '(' . $a_colleage->email . ')';

                $items = $p;
                $json_data['items'][] = $items;
            }
        } else {
            $p = array();
            $p['id'] = '';
            $p['text'] = 'No Colleague Found';

            $items = $p;
            $json_data['items'][] = $items;
        }


        //echo '<pre>';print_r($json_data);die();


        $json_data['total_count'] = $total_count;
        $json_data['more_pages'] = $more_pages;
        $json_data['last_query'] = $last_query;

        echo json_encode($json_data);

    }


    public function getCustomProductCreationList()
    {
        if ($this->session->userdata('user_id') == null) {
            redirect('/');
        }

        $this->lang->load('custom_product_creation_list');

        $which_list = $this->uri->segment(3);


        if ($which_list == 'created' || $which_list == 'recieved') {

            if (!$this->ion_auth->in_group('employee')) {
                redirect('users/auth/does_not_exist');
            }
        }

        if ($which_list == 'all_created' || $which_list == 'all_recieved') {

            if (!$this->ion_auth->is_admin()) {
                redirect('users/auth/need_permission');
            }
        }

        $data = array();

        $data['which_list'] = $which_list;
        $data['which_list'] = $which_list;

        $this->load->view("common_module/header");
        //$this->load->view("common_module/common_left");
        $this->load->view("thrift_module/custom_product_creation_list", $data);
        $this->load->view("common_module/footer");
    }


    public function getCustomProductCreationListByAjax()
    {
        $this->lang->load('custom_product_creation_list');
        $cus_prods = array();

        $sender_id = $this->session->userdata('user_id');

        $requestData = $_REQUEST;
        //print_r($requestData);

        $columns[0] = 'cpi_created_at';
        $columns[1] = 'cpi_created_by';
        $columns[2] = 'thrift_group_number';
        $columns[3] = 'sent_to';
        $columns[4] = 'accepted_by';
        $columns[5] = 'pending';
        $columns[6] = 'cpi_product_price';
        $columns[7] = 'actions';

        $common_filter_value = false;
        $order_column = false;

        $specific_filters = array();
        $specific_filters = false;

        if (!empty($requestData['columns'][0]['search']['value'])) {
            $specific_filters['cpi_created_at'] = $requestData['columns'][0]['search']['value'];
        }

        if (!empty($requestData['columns'][1]['search']['value'])) {
            $specific_filters['cpi_created_by'] = $requestData['columns'][1]['search']['value'];
        }

        if (!empty($requestData['columns'][2]['search']['value'])) {
            $specific_filters['thrift_group_number'] = $requestData['columns'][2]['search']['value'];
        }

        if (!empty($requestData['columns'][6]['search']['value'])) {
            $specific_filters['cpi_product_price'] = $requestData['columns'][2]['search']['value'];
        }


        if (!empty($requestData['search']['value'])) {
            $common_filter_value = $requestData['search']['value'];
        }

        if ($specific_filters == true || !empty($specific_filters)) {
            $common_filter_value = false;       //either search with specific filters or with common filter
        }

        $order['column'] = $columns[$requestData['order'][0]['column']];
        $order['by'] = $requestData['order'][0]['dir'];


        $limit['start'] = $requestData['start'];
        $limit['length'] = $requestData['length'];

        $totalData = $this->Thrift_model->countSentProductCreationList(false, false, $sender_id);

        if ($common_filter_value == true || $specific_filters == true) {

            $totalFiltered = $this->Thrift_model->countSentProductCreationList($common_filter_value, $specific_filters, $sender_id);
        } else {
            $totalFiltered = $totalData; // when there is no search parameter then total number rows = total number filtered rows.
        }


        $cus_prods = $this->Thrift_model->getSentProductCreationList($common_filter_value, $specific_filters, $order, $limit, $sender_id);

        if ($cus_prods == false || empty($cus_prods) || $cus_prods == null) {
            $cus_prods = false;
        }

        $last_query = $this->db->last_query();


        $this->load->library('custom_datetime_library');


        if ($cus_prods) {
            $i = 0;
            foreach ($cus_prods as $a_cus_prod) {

                $cus_prods[$i]->sender = $this->lang->line('unavailable_text');


                $sender = $this->Thrift_model->getEmployee($a_cus_prod->cpi_created_by);

                if ($sender) {
                    $cus_prods[$i]->sender = $sender->first_name . ' ' . $sender->last_name;
                }

                $cus_prods[$i]->sent_to = $this->Thrift_model->countCpiSentTo($a_cus_prod->cpi_id);
                $cus_prods[$i]->accepted_by = $this->Thrift_model->countCpiAcceptedBy($a_cus_prod->cpi_id);
                $cus_prods[$i]->pending = $this->Thrift_model->countCpiPending($a_cus_prod->cpi_id);


                /*price starts*/
                $cus_prods[$i]->price = new stdClass();
                $cus_prods[$i]->price->display = $this->getCurrencySign() .
                    number_format($a_cus_prod->cpi_product_price, 2, '.', ',');
                $cus_prods[$i]->price->dec = $a_cus_prod->cpi_product_price;
                /*price ends*/

                /*date time starts*/
                $cus_prods[$i]->cr_on = new stdClass();
                $cus_prods[$i]->cr_on->timestamp = $a_cus_prod->cpi_created_at;


                if ($a_cus_prod->cpi_created_at == 0) {
                    $cus_prods[$i]->cr_on->display = $this->lang->line('creation_time_unknown_text');
                } else {
                    $cus_prods[$i]->cr_on->display =
                        $this->custom_datetime_library
                            ->convert_and_return_TimestampToDateAndTime($a_cus_prod->cpi_created_at);
                }

                /*date time ends*/


                /*action starts*/


                $view_tooltip = $this->lang->line('tooltip_view_text');
                $view_url = base_url() . 'thrift_module/custom_product_thrift/view/' . $a_cus_prod->cpi_id;
                $view_anchor =
                    '<a ' . ' title="' . $view_tooltip . '" ' . ' href="' . $view_url . '" ' . ' style="color:#2b2b2b" ' . '>'
                    . '<i class="fa fa-eye fa-lg" aria-hidden="true"></i>'
                    . '</a>';


                $delete_tooltip = $this->lang->line('tooltip_delete_text');
                $delete_url = base_url() . 'thrift_module/delete_custom_product/' . $a_cus_prod->cpi_id;
                $delete_anchor = '<a ' . ' title="' . $delete_tooltip . '" ' . ' href="' . $delete_url . '" ' . ' class="confirmation" ' . ' style="color:#2b2b2b" ' . '>'
                    . '<i class="fa fa-trash-o fa-lg" aria-hidden="true">'
                    . '</a>';

                $cus_prods[$i]->action = $view_anchor . '&nbsp;' . $delete_anchor;
                /*action ends*/


                $i++;

            }
        }


        $json_data['draw'] = intval($requestData['draw']);
        /* $totalData: for every request/draw by clientside ,
         they send a number as a parameter, when they recieve a response/data they first check the draw number,
         so we are sending same number in draw.*/
        $json_data['recordsTotal'] = intval($totalData); // total number of records after searching, if there is no searching then totalFiltered = totalData
        $json_data['recordsFiltered'] = intval($totalFiltered);

        //$cus_prods = $this->removeKeys($cus_prods); // converting to numeric indices.
        $json_data['data'] = $cus_prods;

        // checking requests in console.log() for testing starts;
        /*$json_data['last_query'] = $last_query;
        $json_data['common_filter_value'] = $common_filter_value;
        $json_data['specific_filters'] = $specific_filters;
        $json_data['order_column'] = $order['column'];
        $json_data['order_by'] = $order['by'];
        $json_data['limit_length'] = $limit['length'];
        $json_data['limit_start'] = $limit['start'];*/
        // checking requests in console.log() for testing ends;

        echo(json_encode($json_data));
    }


    public function getCustomProductRecievedList()
    {
        if ($this->session->userdata('user_id') == null) {
            redirect('/');
        }

        $this->lang->load('custom_product_recieved_list');

        $which_list = $this->uri->segment(3);


        if ($which_list == 'created' || $which_list == 'recieved') {

            if (!$this->ion_auth->in_group('employee')) {
                redirect('users/auth/does_not_exist');
            }
        }

        if ($which_list == 'all_created' || $which_list == 'all_recieved') {

            if (!$this->ion_auth->is_admin()) {
                redirect('users/auth/need_permission');
            }
        }

        $data = array();

        $data['which_list'] = $which_list;


        $this->load->view("common_module/header");
        //$this->load->view("common_module/common_left");
        $this->load->view("thrift_module/custom_product_recieved_list", $data);
        $this->load->view("common_module/footer");
    }


    public function getCustomProductRecievedListByAjax()
    {
        $this->lang->load('custom_product_recieved_list');
        $cus_prods = array();

        $reciever_id = $this->session->userdata('user_id');

        $requestData = $_REQUEST;
        //print_r($requestData);

        $columns[0] = 'cpi_created_at';
        $columns[1] = 'cpi_inv_from';
        $columns[2] = 'cpi_inv_to';
        $columns[3] = 'cpi_inv_accepted';
        $columns[4] = 'cpi_product_price';
        $columns[5] = 'actions';

        $common_filter_value = false;
        $order_column = false;

        $specific_filters = array();
        $specific_filters = false;

        if (!empty($requestData['columns'][0]['search']['value'])) {
            $specific_filters['cpi_created_at'] = $requestData['columns'][0]['search']['value'];
        }

        if (!empty($requestData['columns'][1]['search']['value'])) {
            $specific_filters['cpi_inv_from'] = $requestData['columns'][1]['search']['value'];
        }

        if (!empty($requestData['columns'][2]['search']['value'])) {
            $specific_filters['cpi_inv_to'] = $requestData['columns'][2]['search']['value'];
        }

        if (!empty($requestData['columns'][3]['search']['value'])) {
            $specific_filters['cpi_inv_accepted'] = $requestData['columns'][3]['search']['value'];
        }

        if (!empty($requestData['columns'][4]['search']['value'])) {
            $specific_filters['cpi_inv_product_price'] = $requestData['columns'][4]['search']['value'];
        }


        if (!empty($requestData['search']['value'])) {
            $common_filter_value = $requestData['search']['value'];
        }

        if ($specific_filters == true || !empty($specific_filters)) {
            $common_filter_value = false;       //either search with specific filters or with common filter
        }

        $order['column'] = $columns[$requestData['order'][0]['column']];
        $order['by'] = $requestData['order'][0]['dir'];


        $limit['start'] = $requestData['start'];
        $limit['length'] = $requestData['length'];

        $totalData = $this->Thrift_model->countRecievedProductCreationList(false, false, $reciever_id);

        if ($common_filter_value == true || $specific_filters == true) {

            $totalFiltered = $this->Thrift_model->countRecievedProductCreationList($common_filter_value, $specific_filters, $reciever_id);
        } else {
            $totalFiltered = $totalData; // when there is no search parameter then total number rows = total number filtered rows.
        }


        $cus_prods = $this->Thrift_model->getRecievedProductCreationList($common_filter_value, $specific_filters, $order, $limit, $reciever_id);

        if ($cus_prods == false || empty($cus_prods) || $cus_prods == null) {
            $cus_prods = false;
        }

        $last_query = $this->db->last_query();


        $this->load->library('custom_datetime_library');


        if ($cus_prods) {
            $i = 0;
            foreach ($cus_prods as $a_cus_prod) {

                $cus_prods[$i]->sender = $this->lang->line('unavailable_text');
                $cus_prods[$i]->reciever = $this->lang->line('unavailable_text');


                $sender = $this->Thrift_model->getEmployee($a_cus_prod->cpi_inv_from);
                $reciever = $this->Thrift_model->getEmployee($a_cus_prod->cpi_inv_to);

                if ($sender) {
                    $cus_prods[$i]->sender = $sender->first_name . ' ' . $sender->last_name;
                }

                if ($reciever) {
                    $cus_prods[$i]->reciever = $reciever->first_name . ' ' . $reciever->last_name;
                }

                /*price starts*/
                $cus_prods[$i]->price = new stdClass();
                $cus_prods[$i]->price->display = $this->getCurrencySign() .
                    number_format($a_cus_prod->cpi_product_price, 2, '.', ',');
                $cus_prods[$i]->price->dec = $a_cus_prod->cpi_product_price;
                /*price ends*/


                /*date time starts*/
                $cus_prods[$i]->cr_on = new stdClass();
                $cus_prods[$i]->cr_on->timestamp = $a_cus_prod->cpi_created_at;


                if ($a_cus_prod->cpi_created_at == 0) {
                    $cus_prods[$i]->cr_on->display = $this->lang->line('creation_time_unknown_text');
                } else {
                    $cus_prods[$i]->cr_on->display =
                        $this->custom_datetime_library
                            ->convert_and_return_TimestampToDateAndTime($a_cus_prod->cpi_created_at);
                }

                /*date time ends*/


                /*acception starts*/
                $cus_prods[$i]->acc = new stdClass();
                $cus_prods[$i]->acc->int = $a_cus_prod->cpi_inv_accepted;

                if ($a_cus_prod->cpi_inv_accepted == 0) {

                    $status_span = '<span class = "label label-primary">' . $this->lang->line('status_pending_text') . '</span>';

                    $status_accept_tooltip = $this->lang->line('tooltip_accept_text');
                    $status_accept_url = base_url() . 'thrift_module/accept_custom_product/' . $a_cus_prod->cpi_id;
                    $status_accept_anchor_span = '<span class="label label-success"><i class="fa fa-check" aria-hidden="true"></i></span>';

                    $status_decline_tooltip = $this->lang->line('tooltip_decline_text');
                    $status_decline_url = base_url() . 'thrift_module/decline_custom_product/' . $a_cus_prod->cpi_id;
                    $status_decline_anchor_span = '<span class="label label-danger"><i class="fa fa-times" aria-hidden="true"></i></span>';

                    $status_accept_anchor =
                        '<a ' . ' title="' . $status_accept_tooltip . '"' . ' href="' . $status_accept_url . '">' . $status_accept_anchor_span . '</a>';

                    $status_decline_anchor =
                        '<a ' . ' title="' . $status_decline_tooltip . '"' . ' href="' . $status_decline_url . '">' . $status_decline_anchor_span . '</a>';

                    $status_anchors = $status_accept_anchor . '&nbsp' . $status_decline_anchor;


                } else if ($a_cus_prod->cpi_inv_accepted == 1) {

                    $status_span = '<span class = "label label-success">' . $this->lang->line('status_accept_text') . '</span>';

                    $status_decline_tooltip = $this->lang->line('tooltip_decline_text');
                    $status_decline_url = base_url() . 'thrift_module/decline_custom_product/' . $a_cus_prod->cpi_id;
                    $status_decline_anchor_span = '<span class="label label-danger"><i class="fa fa-times" aria-hidden="true"></i></span>';


                    $status_decline_anchor =
                        '<a ' . ' title="' . $status_decline_tooltip . '"' . ' href="' . $status_decline_url . '">' . $status_decline_anchor_span . '</a>';

                    $status_anchors = $status_decline_anchor;

                } else {

                    $status_span = '<span class = "label label-danger">' . $this->lang->line('status_decline_text') . '</span>';

                    $status_accept_tooltip = $this->lang->line('tooltip_accept_text');
                    $status_accept_url = base_url() . 'thrift_module/accept_custom_product/' . $a_cus_prod->cpi_id;
                    $status_accept_anchor_span = '<span class="label label-success"><i class="fa fa-check" aria-hidden="true"></i></span>';

                    $status_accept_anchor =
                        '<a ' . ' title="' . $status_accept_tooltip . '"' . ' href="' . $status_accept_url . '">' . $status_accept_anchor_span . '</a>';


                    $status_anchors = $status_accept_anchor;
                }

                if ($this->ion_auth->in_group('employee')) {
                    $cus_prods[$i]->acc->html = $status_span . '&nbsp; &nbsp;' . $status_anchors;
                } else {
                    $cus_prods[$i]->acc->html = $status_span;
                }
                /*acception ends*/


                /*action starts*/


                $view_tooltip = $this->lang->line('tooltip_view_text');
                $view_url = base_url() . 'thrift_module/custom_product_thrift/view/' . $a_cus_prod->cpi_id;
                $view_anchor =
                    '<a ' . ' title="' . $view_tooltip . '" ' . ' href="' . $view_url . '" ' . ' style="color:#2b2b2b" ' . '>'
                    . '<i class="fa fa-eye fa-lg" aria-hidden="true"></i>'
                    . '</a>';


                $cus_prods[$i]->action = $view_anchor;
                /*action ends*/


                $i++;

            }
        }


        $json_data['draw'] = intval($requestData['draw']);
        /* $totalData: for every request/draw by clientside ,
         they send a number as a parameter, when they recieve a response/data they first check the draw number,
         so we are sending same number in draw.*/
        $json_data['recordsTotal'] = intval($totalData); // total number of records after searching, if there is no searching then totalFiltered = totalData
        $json_data['recordsFiltered'] = intval($totalFiltered);

        //$cus_prods = $this->removeKeys($cus_prods); // converting to numeric indices.
        $json_data['data'] = $cus_prods;

        // checking requests in console.log() for testing starts;
        /*$json_data['last_query'] = $last_query;
        $json_data['common_filter_value'] = $common_filter_value;
        $json_data['specific_filters'] = $specific_filters;
        $json_data['order_column'] = $order['column'];
        $json_data['order_by'] = $order['by'];
        $json_data['limit_length'] = $limit['length'];
        $json_data['limit_start'] = $limit['start'];*/
        // checking requests in console.log() for testing ends;

        echo(json_encode($json_data));
    }


    public function AcceptCustomProduct()
    {
        if ($this->session->userdata('user_id') == null) {
            redirect('/');
        }


        $this->lang->load('custom_product_recieved_list');

        $cpi_id = $this->uri->segment(3);
        $reciever_id = $this->session->userdata('user_id');

        if (!$this->chk_acc_cond($cpi_id)) {
            $this->session->set_flashdata('thrift_error', 'thrift_error');
            $this->session->set_flashdata('thrift_error_exceed_limit_text', 'thrift_error_exceed_limit_text');

            redirect('thrift_module/show_thrifts/my');
        }

        if ($this->is_payment_method_on()) {
            if ($this->chkChosenPaymentMethod() == false) {

                $this->choosePaymentMethod('thrift_module/accept_custom_product/' . $cpi_id);
            }
        }


        $curr_ts = $this->custom_datetime_library->getCurrentTimestamp();
        $accepted = $this->Thrift_model->AcceptCustomProduct($cpi_id, $reciever_id, $curr_ts);

        if ($accepted) {

            $this->sendCustomProductAcceptionEmail($cpi_id, $this->session->userdata('user_id'));

            $this->session->set_flashdata('success', 'success');
            $this->session->set_flashdata('accept_success', 'accept_success');
            $this->session->set_flashdata('flash_cpi_id', $cpi_id);

            redirect('thrift_module/custom_product_list/recieved');
        }

    }

    private function sendCustomProductAcceptionEmail($cpi_id, $im)
    {
        $user = $this->Thrift_model->getEmployee($im);

        if ($user) {

            $username = $user->first_name . ' ' . $user->last_name;
            if ($this->ion_auth->in_group('employer', $im)) {
                $username = $user->company;
            }

            $mail_data['to'] = $user->email;

            $template = $this->Thrift_model->getEmailTempltateByType('thrift_invitaion_accepted');

            if ($template) {
                $subject = $template->email_template_subject;

                $template_message = $template->email_template;

                /*-------*/
                $base_url = $this->getBaseUrl($im);

                $actual_link = $base_url . 'thrift_module/custom_product_thrift/view/' . $cpi_id;

                $find = array("{{username}}", "{{actual_link}}");
                $replace = array($username, $actual_link);
                $message = str_replace($find, $replace, $template_message);

                /*--------*/
                $mail_data['subject'] = $subject;
                $mail_data['message'] = $message;

                $this->sendEmail($mail_data);
            }

        }
    }

    private function chk_custom_product_create_cond($price)
    {
        $employee_id = $this->session->userdata('user_id');

        $employee = $this->Thrift_model->getEmployee($employee_id);

        $employer_id = $employee->user_employer_id;

        $employer = null;
        if ($employer_id != 0 && $employer_id != null) {

            $employer = $this->Thrift_model->getEmployer($employer_id);
        }

        if (!$employer) {
            $this->session->set_flashdata('unsuccessful', 'unsuccessful');
            $this->session->set_flashdata('thrift_error_no_employer_text', 'thrift_error_no_employer_text');
            redirect('thrift_module/add_custom_product');
        }

        $product = new stdClass();

        $product->product_price = $price;

        return $this->checkThriftCondition($employee, $employer, $product);
    }

    private function chk_acc_cond($cpi_id)
    {
        $employee_id = $this->session->userdata('user_id');

        $employee = $this->Thrift_model->getEmployee($employee_id);

        $employer_id = $employee->user_employer_id;

        $employer = null;
        if ($employer_id != 0 && $employer_id != null) {

            $employer = $this->Thrift_model->getEmployer($employer_id);
        }

        if (!$employer) {
            $this->session->set_flashdata('thrift_error', 'thrift_error');
            $this->session->set_flashdata('thrift_error_no_employer_text', 'thrift_error_no_employer_text');
            redirect('thrift_module/show_thrifts/my');
        }

        $product = new stdClass();

        $invitation = $this->Thrift_model->getCustomProductInvitation($cpi_id);

        $product->product_price = $invitation->cpi_product_price;


        return $this->checkThriftCondition($employee, $employer, $product);
    }

    public function DeclineCustomProduct()
    {
        if ($this->session->userdata('user_id') == null) {
            redirect('/');
        }

        $this->lang->load('custom_product_recieved_list');

        $cpi_id = $this->uri->segment(3);
        $reciever_id = $this->session->userdata('user_id');

        $declined = $this->Thrift_model->DeclineCustomProduct($cpi_id, $reciever_id);

        if ($declined) {

            $this->sendCustomProductDeclinationEmail($cpi_id, $this->session->userdata('user_id'));

            $this->session->set_flashdata('success', 'success');
            $this->session->set_flashdata('decline_success', 'decline_success');
            $this->session->set_flashdata('flash_cpi_id', $cpi_id);

            redirect('thrift_module/custom_product_list/recieved');
        }

    }

    private function sendCustomProductDeclinationEmail($cpi_id, $im)
    {
        $user = $this->Thrift_model->getEmployee($im);

        if ($user) {

            $username = $user->first_name . ' ' . $user->last_name;
            if ($this->ion_auth->in_group('employer', $im)) {
                $username = $user->company;
            }

            $mail_data['to'] = $user->email;

            $template = $this->Thrift_model->getEmailTempltateByType('thrift_invitation_declined');

            if ($template) {
                $subject = $template->email_template_subject;

                $template_message = $template->email_template;

                /*-------*/
                $base_url = $this->getBaseUrl($im);

                $actual_link = $base_url . 'thrift_module/custom_product_thrift/view/' . $cpi_id;

                $find = array("{{username}}", "{{actual_link}}");
                $replace = array($username, $actual_link);
                $message = str_replace($find, $replace, $template_message);

                /*--------*/
                $mail_data['subject'] = $subject;
                $mail_data['message'] = $message;

                $this->sendEmail($mail_data);
            }

        }
    }

    private function getBaseUrl($id)
    {
        $base_url = '';
        if ($this->ion_auth->in_group('admin', $id)) {
            $base_url = $this->config->item('office_base_url');
        } else if ($this->ion_auth->in_group('employer', $id) || $this->ion_auth->in_group('organization_contact', $id)) {
            $base_url = $this->config->item('partner_base_url');
        } else if ($this->ion_auth->in_group('employee', $id)) {
            $base_url = $this->Utility_model->makeThriftUrlWithEmployerSubdomain($this->config->item('thrift_base_url'), $id);
        } else if ($this->ion_auth->in_group('trustee', $id)) {
            $base_url = $this->config->item('trustee_base_url');
        }

        return $base_url;
    }

    /*----------------------------------------------------------------------------------------------------------------*/
    public function addIndividualProduct()
    {
        if ($this->session->userdata('user_id') == null) {
            redirect('/');
        }

        $this->lang->load('individual_product_form');

        if (!$this->ion_auth->in_group('employee')) {
            redirect('users/auth/need_permission');
        }

        if ($this->is_payment_method_on()) {

            if ($this->chkChosenPaymentMethod() == false) {

                $this->choosePaymentMethod('thrift_module/add_individual_product');
            }
        }


        $vdata = array();

        if ($this->input->post()) {
            $this->createIndividualProduct();
        }

        $individual_thrift_start_delay = $this->custom_settings_library->getASettingsValue('prosperisgold_settings', 'individual_thrift_start_delay');
        $individual_thrift_max_start_time_from_delay = $this->custom_settings_library->getASettingsValue('prosperisgold_settings', 'individual_thrift_max_start_time_from_delay');

        $itsd = false;
        if (!is_numeric($individual_thrift_start_delay) ||
            $individual_thrift_start_delay == 0 ||
            $individual_thrift_start_delay == false ||
            $individual_thrift_start_delay == null) {
            $vdata['individual_thrift_start_delay'] = "+3d";
        } else {
            $itsd = true;
            $vdata['individual_thrift_start_delay'] = "+" . $individual_thrift_start_delay . "d";
        }

        if (!is_numeric($individual_thrift_max_start_time_from_delay) ||
            $individual_thrift_max_start_time_from_delay == 0 ||
            $individual_thrift_max_start_time_from_delay == false ||
            $individual_thrift_max_start_time_from_delay == null) {

            if ($itsd) {
                $end_delay = $individual_thrift_start_delay + 15;
                $vdata['individual_thrift_end_delay'] = "+" . $end_delay . "d";
            } else {
                $vdata['individual_thrift_end_delay'] = "+15d";
            }

        } else {

            if ($itsd) {
                $end_delay = $individual_thrift_start_delay + $individual_thrift_max_start_time_from_delay;
                $vdata['individual_thrift_end_delay'] = "+" . $end_delay . "d";
            } else {
                $vdata['individual_thrift_end_delay'] = "+15d";
            }
        }

        $vdata['which_form'] = 'add';
        $vdata['form_action'] = 'thrift_module/add_individual_product';
        $vdata['currency_sign'] = $this->getCurrencySign();

        $this->load->view("common_module/header");
        //$this->load->view("common_module/common_left");
        $this->load->view("thrift_module/individual_product_form_page", $vdata);
        $this->load->view("common_module/footer");

    }

    public function createIndividualProduct()
    {
        if ($this->session->userdata('user_id') == null) {
            redirect('/');
        }

        $this->session->set_flashdata('flash_product_price', trim($this->input->post('product_price')));
        $this->session->set_flashdata('flash_start_date', trim($this->input->post('start_date')));
        $this->session->set_flashdata('flash_number_of_payments', trim($this->input->post('number_of_payments')));

        $this->form_validation->set_rules('product_price', 'Product Price', 'numeric|required|greater_than_equal_to[0]');
        $this->form_validation->set_rules('start_date', 'Start Date', 'required');

        $this->form_validation->set_rules('number_of_payments', 'Number of Payments', 'numeric|required|greater_than_equal_to[3]|less_than_equal_to[12]');


        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('unsuccessful', 'unsuccessful');
            $this->session->set_flashdata('validation_errors', validation_errors());
            redirect('thrift_module/add_individual_product');
        }

        $product = new stdClass();
        $product->product_price = trim($this->input->post('product_price'));
        $employee = $this->Thrift_model->getEmployee($this->session->userdata('user_id'));

        if (!$this->checkThriftCondition($employee,$employer = false,$product)) {
            $this->session->set_flashdata('unsuccessful', 'unsuccessful');

            redirect('thrift_module/add_individual_product');
        }


        /*echo '<pre>';
        print_r($_POST);
        echo '</pre>';
        die();*/

        $thrift_group_id = $this->createIndividualThrift();


        /*creating log starts*/
        $this->custom_log_library->createALog
        (
            $this->session->userdata('user_id'),                                    //1.    $created_by
            '',                                                                     //2.    $created_for
            'thrift_group',                                                         //3.    $type
            $thrift_group_id,                                                       //4.    $type_id
            'created',                                                              //5.    $activity
            'employee',                                                             //6.    $activity_by
            '',                                                                     //7.    $activity_for
            '',                                                                     //8.    $sub_type
            '',                                                                     //9.    $sub_type_id
            '',                                                                     //10.   $super_type
            '',                                                                     //11.   $super_type_id
            '',                                                                     //12.   $other_information
            ''                                                                      //13.   $change_list
        );
        /*creating log ends*/

        $this->sendCustomOrIndividualProductInitiationEmail($thrift_group_id, $this->session->userdata('user_id'));

        //assuming everything wet well...
        $this->session->set_flashdata('thrift_success', 'thrift_success');
        $this->session->set_flashdata('thrift_create_success_text', 'thrift_create_success_text');

        if ($thrift_group_id != 0) {
            $this->session->set_flashdata('flash_thrift_group_id', $thrift_group_id);
        }

        redirect('thrift_module/show_thrifts/my');

        /* do not redirect to here */
        //redirect('thrift_module/add_custom_product');

    }

    private function createIndividualThrift()
    {
        $ins_data['thrift_group_number'] = $this->generateThriftGroupNumber();
        $ins_data['thrift_group_product_id'] = -1;

        $ins_data['thrift_group_is_individual_product'] = 1;
        $ins_data['thrift_group_individual_product_created_by'] = $this->session->userdata('user_id');

        $ins_data['thrift_group_product_price'] = trim($this->input->post('product_price'));
        $ins_data['thrift_group_term_duration'] = trim($this->input->post('number_of_payments'));
        $ins_data['thrift_group_member_count'] = 0;
        $ins_data['thrift_group_member_limit'] = trim($this->input->post('number_of_payments'));
        $ins_data['thrift_group_activation_status'] = 1;
        $ins_data['thrift_group_open'] = 1;
        $ins_data['thrift_group_creation_date'] = $this->custom_datetime_library->getCurrentTimestamp();

        //when auto bot assign starts, this will be ignored for the custom product
        $ins_data['thrift_group_threshold_date'] = 0;

        //uncomment this
        $ins_data['thrift_group_start_date'] =
            $this
                ->custom_datetime_library
                ->convert_and_return_DateAndTime_To_Timestamp_byGivenDateTimeFormat($this->input->post('start_date'), 'Y-m-d');

        //testcheck
        //uncomment prev line and remove this. It is only for testing
        //$ins_data['thrift_group_start_date'] = $this->custom_datetime_library->getCurrentTimestamp();


        $ins_data['thrift_group_end_date'] = 0;

        $thrift_group_id = $this->Thrift_model->insertThriftGroup($ins_data);

        if ($this->is_payment_method_on()) {
            $this->createNewPaystackPlan($ins_data['thrift_group_product_price'], $thrift_group_id, $product_name = false);
        }


        $this->joinThriftGroup($thrift_group_id, $admin_id = 1); //add admin to thrift as first member

        $limit = (int)trim($this->input->post('number_of_payments')) -2 ;
        $bot_admins = $this->Thrift_model->getActiveBotAdminsIds($limit);

        if ($bot_admins) {

            foreach ($bot_admins as $a_bot_admin) {

                /*for checking the current status*/
                $tg_current = $this->Thrift_model->getThriftGroup($thrift_group_id);

                /*check if the bot is alreardy assigned*/
                $existence = $this->Thrift_model->checkIfMemberAlreadyInThriftGroup($tg_current->thrift_group_id, $a_bot_admin->user_id);


                $emp_pos_cnt = $tg_current->thrift_group_member_limit - $tg_current->thrift_group_member_count;

                if (!$existence && $emp_pos_cnt > 0) {
                    $this->joinThriftGroup($tg_current->thrift_group_id, $a_bot_admin->user_id);
                }


            }

        }

        $this->joinThriftGroup($thrift_group_id, $this->session->userdata('user_id')); //add creator to thrift as last member

        return $thrift_group_id;
    }


    /*----------------------------------------------------------------------------------------------------------------*/

    private function demo_success_event()
    {
        $event_string =
            "
            {
   \"event\":\"charge.success\",
   \"data\":{
      \"id\":12587961,
      \"domain\":\"test\",
      \"status\":\"success\",
      \"reference\":\"4r1xtjg4y3ztwod\",
      \"amount\":10100,
      \"message\":null,
      \"gateway_response\":\"Successful\",
      \"paid_at\":\"2018-01-31T04:00:03.000Z\",
      \"created_at\":\"2018-01-31T04:00:03.000Z\",
      \"channel\":\"card\",
      \"currency\":\"NGN\",
      \"ip_address\":null,
      \"metadata\":\"\",
      \"log\":null,
      \"fees\":152,
      \"fees_split\":null,
      \"customer\":{
         \"id\":1487346,
         \"first_name\":\"Sanath\",
         \"last_name\":\"Jayasuriya\",
         \"email\":\"sanath@jayasuria.com\",
         \"customer_code\":\"CUS_5z5i6sw36jx2wlu\",
         \"phone\":\"\",
         \"metadata\":null,
         \"risk_action\":\"default\"
      },
      \"authorization\":{
         \"authorization_code\":\"AUTH_1nah93q52j\",
         \"bin\":\"408408\",
         \"last4\":\"4081\",
         \"exp_month\":\"01\",
         \"exp_year\":\"2020\",
         \"channel\":\"card\",
         \"card_type\":\"visa DEBIT\",
         \"bank\":\"Test Bank\",
         \"country_code\":\"NG\",
         \"brand\":\"visa\",
         \"reusable\":true,
         \"signature\":\"SIG_JUaFHwGW4yWnZMthgA1e\"
      },
      \"plan\":{
         \"id\":8353,
         \"name\":\"Master Plan\",
         \"plan_code\":\"PLN_yfdirkd7zj5u085\",
         \"description\":\"Master Test Plan\",
         \"amount\":10100,
         \"interval\":\"hourly\",
         \"send_invoices\":true,
         \"send_sms\":true,
         \"currency\":\"NGN\"
      },
      \"subaccount\":{

      },
      \"paidAt\":\"2018-01-31T04:00:03.000Z\"
   }
}
            ";

        return trim($event_string);
    }

    public function getPaystackEventsByWebhook()
    {
        // Retrieve the request's body
        $body = @file_get_contents("php://input");
        $signature = (isset($_SERVER['HTTP_X_PAYSTACK_SIGNATURE']) ? $_SERVER['HTTP_X_PAYSTACK_SIGNATURE'] : '');

        /* It is a good idea to log all events received. Add code *
         * here to log the signature and body to db or file       */


        if (!$signature) {
            // only a post with paystack signature header gets our attention
            exit();
        }

        $paystack_sk = $this->custom_payment_library->getPayStackSecretKey();
        //echo $paystack_sk;


        if ($paystack_sk) {
            // confirm the event's signature
            if ($signature !== hash_hmac('sha512', $body, $paystack_sk)) {
                // silently forget this ever happened
                exit();
            }
        } else {
            exit();
        }


        http_response_code(200);
        // parse event (which is json string) as object
        // Give value to your customer but don't give any output
        // Remember that this is a call from Paystack's servers and
        // Your customer is not seeing the response here at all

        //testcheck, remove the next line below
        //$body = $this->demo_success_event();

        $event_data = json_decode($body);

        $txt = "-----empty-----";
        if ($body) {
            $txt = $body;
        }


        //send mail to test
        date_default_timezone_set("Asia/Dhaka");
        $dt = date("Y-m-d h:i:s");
        $dt_sep = "\n\n\n" . $dt . "\n--------------------" . "\n";

        $subject = "hook alert " . $dt;
        $email_body = "<pre>" . $txt . "</pre>";

        $to = "mahmud@sahajjo.com";
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
        $headers .= 'From: <thrift@paystackwebhook.com>' . "\r\n";
        @mail($to, $subject, $email_body, $headers);

        /*actual process <starts>*/
        if ($event_data && !empty($event_data)) {

            if ($event_data->event == "charge.success") {
                $this->processPaystackPayment($event_data, $body);
            }

        }
        /*actual process <ends>*/
        exit();
    }

    private function processPaystackPayment($event_data, $body)
    {
        $user = false;
        $thrift_group = false;

        if ($event_data->data->customer->customer_code != null && $event_data->data->customer->customer_code != '') {
            $user = $this->Thrift_model->getUserByPayStackCustomerCode($event_data->data->customer->customer_code);
        }
        if ($event_data->data->plan->plan_code != null && $event_data->data->plan->plan_code != '') {
            $thrift_group = $this->Thrift_model->getThriftGroupByPlanCode($event_data->data->plan->plan_code);
        }

        $thrift_group_member = false;
        if ($user && $thrift_group) {
            $thrift_group_member = $this->Thrift_model->getThriftGroupMember($user->user_id, $thrift_group->thrift_group_id);
        }

        if ($thrift_group_member) {

            /*echo '<pre>';
            print_r($thrift_group_member);
            echo '</pre><br><hr><br>';*/


            if ($thrift_group_member->paystack_subscription_code != null && $thrift_group_member->paystack_subscription_code != '') {
                $last_payment = $this->Thrift_model->getLastPayStackPaymentByGroupAndMember($thrift_group_member->thrift_group_id, $thrift_group_member->thrift_group_member_id);


                $ins_hook_data['thrift_group_id'] = $thrift_group_member->thrift_group_id;
                $ins_hook_data['thrift_group_payer_member_id'] = $thrift_group_member->thrift_group_member_id;
                $ins_hook_data['paystack_payment_reference'] = $event_data->data->reference;
                $ins_hook_data['paystack_payment_json'] = $body;


                if ($last_payment) {

                    $ins_hook_data['paystack_payment_cycle_number'] = $last_payment->paystack_payment_cycle_number + 1;

                } else {

                    $ins_hook_data['paystack_payment_cycle_number'] = 1;
                }

                $related_payment = $this->Thrift_model->getPaystackRelatedPayment($thrift_group_member->thrift_group_id, $thrift_group_member->thrift_group_member_id, $ins_hook_data['paystack_payment_cycle_number']);
                /* echo '<pre>';
                 print_r($related_payment);
                 echo '</pre><br><hr><br>';*/

                //die();

                $payment_hook_serial = $this->Thrift_model->insertPaystackPaymentHook($ins_hook_data);

                if ($related_payment) {
                    $payment_update_data['paystack_payment_cleared'] = 1;
                    $payment_update_data['paystack_payment_reference'] = $event_data->data->reference;
                    $this->Thrift_model->autoUpdateThriftGroupPayment($payment_update_data, $related_payment->thrift_group_payment_id);

                    $payment_hook_update_data['thrift_group_payment_id'] = $related_payment->thrift_group_payment_id;
                    $this->Thrift_model->updatePaystackPaymentHook($payment_hook_update_data, $payment_hook_serial);
                }

            }
        }


    }

    private function createNewPaystackPlan($amount, $thrift_group_id, $product_name = false)
    {
        $thrift_group = $this->Thrift_model->getThriftGroup($thrift_group_id);

        $plan_data = array();
        if ($product_name) {
            $plan_data['name'] = $product_name . ' ' . $thrift_group->thrift_group_number;
        } else {
            $plan_data['name'] = 'Private Thrift' . ' ' . $thrift_group->thrift_group_number;
        }

        $plan_data['amount'] = ((int)$amount) * 100;

        $plan_data['interval'] = 'monthly';

        //testcheck
        //remove the line below, only for test
        //$plan_data['interval'] = 'hourly';

        $ret_data = $this->custom_payment_library->createPaystackPlan($plan_data);

        if ($ret_data['got_data'] && !$ret_data['error_response_object'] && !$ret_data['error_sk']) {
            $created_plan = $ret_data['got_data'];

            if ($created_plan) {
                if ($created_plan->status == 1 && !empty($created_plan->data) && $created_plan->data != null) {
                    $upd_data['paystack_plan_code'] = $created_plan->data->plan_code;
                    $upd_data['paystack_plan_id'] = $created_plan->data->id;
                    $upd_data['paystack_integration'] = $created_plan->data->integration;

                    $this->Thrift_model->updateThriftGroup($upd_data, $thrift_group_id);

                }
            }
        } else {

            /*creating log starts*/
            $this->custom_log_library->createALog
            (
                '',                                                                     //1.    $created_by
                '',                                                                     //2.    $created_for
                'thrift_group',                                                         //3.    $type
                $thrift_group_id,                                                       //4.    $type_id
                'thrift_deleted',                                                       //5.    $activity
                '',                                                                     //6.    $activity_by
                '',                                                                     //7.    $activity_for
                '',                                                                     //8.    $sub_type
                '',                                                                     //9.    $sub_type_id
                '',                                                                     //10.   $super_type
                '',                                                                     //11.   $super_type_id
                '',                                                                     //12.   $other_information
                ''                                                                      //13.   $change_list
            );
            /*creating log ends*/

            $this->removeThriftGroupAndInvitation($thrift_group_id);

            //comment the redirect to see error
            redirect('users/auth/payment_method_error_page?payment_method=Paystack&error_type=Plan%20creation%20error');

            if ($ret_data['error_response_object']) {
                print_r($ret_data['error_response_object']);
            }
            if ($ret_data['error_message']) {
                echo $ret_data['error_message'];
            }
            if ($ret_data['error_sk']) {
                echo $ret_data['error_message'];
            }

        }


    }

    private function setUpPaystackSubscriptionForEmployees($thrift_group_id)
    {
        $thrift_members = $this->Thrift_model->getThriftMembers($thrift_group_id);
        $thrift_group = $this->Thrift_model->getThriftGroup($thrift_group_id);

        if ($thrift_members && $thrift_group) {

            foreach ($thrift_members as $tm) {

                //only need to create subscription for employee accounts, prosperis admin and bots are unnecessary
                if ($this->ion_auth->in_group('employee', $tm->thrift_group_member_id)) {

                    $user = $this->Thrift_model->getEmployee($tm->thrift_group_member_id);

                    if ($user) {

                        if (
                            $user->user_chosen_payment_method == 'paystack'
                            && $user->paystack_customer_code != null && $user->paystack_customer_code != ''
                            && $user->paystack_authorization_code != null && $user->paystack_authorization_code != ''
                            && $thrift_group->paystack_plan_code != null && $thrift_group->paystack_plan_code != ''
                            && $thrift_group->thrift_group_start_date > 0
                        ) {

                            $first_debit_timestamp = strtotime('+30 days', $thrift_group->thrift_group_start_date);

                            //testcheck remove this only for test
                            //$first_debit_timestamp = strtotime('+ 1 hour', $thrift_group->thrift_group_start_date);

                            $first_debit_iso_datetime = $this->custom_datetime_library->convert_and_return_TimestampToDateTimeGivenFormat($first_debit_timestamp, 'Y-m-d\TH:i:sO');

                            $subscription_data = array();
                            $subscription_data['customer'] = $user->paystack_customer_code;
                            $subscription_data['plan'] = $thrift_group->paystack_plan_code;
                            $subscription_data['authorization'] = $user->paystack_authorization_code;
                            $subscription_data['start_date'] = $first_debit_iso_datetime;

                            $this->createPaystackSubscription($subscription_data, $thrift_group_id, $tm->thrift_group_member_id);
                        }


                    }


                }

            }

        }

    }

    private function createPaystackSubscription($subscription_data, $thrift_group_id, $member_id)
    {
        $ret_data = $this->custom_payment_library->createPaystackSubscription($subscription_data);

        if ($ret_data['got_data'] && !$ret_data['error_response_object'] && !$ret_data['error_sk']) {
            $created_subscription = $ret_data['got_data'];

            if ($created_subscription) {
                if ($created_subscription->status == 1 && !empty($created_subscription->data) && $created_subscription->data != null) {
                    $upd_data['paystack_subscription_code'] = $created_subscription->data->subscription_code;
                    $upd_data['paystack_email_token'] = $created_subscription->data->email_token;

                    $this->Thrift_model->updateThriftGroupMember($upd_data, $thrift_group_id, $member_id);
                }
            }
        } else {

            $upd_data['paystack_subscription_creation_error'] = 1;

            if ($ret_data['error_response_object']) {
                $upd_data['paystack_subscription_creation_error_object'] = json_encode($ret_data['error_response_object']);
            }
            if ($ret_data['error_message']) {
                $upd_data['paystack_subscription_creation_error_message'] = $ret_data['error_message'];
            }
            if ($ret_data['error_sk']) {
                $upd_data['paystack_subscription_creation_error_message'] = $ret_data['error_message'];
            }

            $this->Thrift_model->updateThriftGroupMember($upd_data, $thrift_group_id, $member_id);
        }


    }

    private function disableGroupMembersPaystackSubscription($thrift_group_id)
    {
        $thrift_members = $this->Thrift_model->getThriftMembers($thrift_group_id);
        $thrift_group = $this->Thrift_model->getThriftGroup($thrift_group_id);

        if ($thrift_members && $thrift_group) {

            foreach ($thrift_members as $tm) {

                //only need to create subscription for employee accounts, prosperis admin and bots are unnecessary
                if ($this->ion_auth->in_group('employee', $tm->thrift_group_member_id)) {

                    $user = $this->Thrift_model->getEmployee($tm->thrift_group_member_id);

                    if ($user) {

                        if (
                            $tm->paystack_subscription_code != null && $tm->paystack_subscription_code != ''
                            &&
                            $tm->paystack_email_token != null && $tm->paystack_email_token != ''
                        ) {

                            $subscription_data['code'] = $tm->paystack_subscription_code;
                            $subscription_data['token'] = $tm->paystack_email_token;

                            $this->disablePaystackSubscription($subscription_data, $thrift_group_id, $tm->thrift_group_member_id);
                        }


                    }


                }

            }

        }
    }

    private function disablePaystackSubscription($subscription_data, $thrift_group_id, $member_id)
    {
        /*echo '<pre>';
        print_r($subscription_data);
        echo '<pre>';
        echo '<br>';
        echo '<hr>';
        echo '<br>';*/

        $ret_data = $this->custom_payment_library->disablePaystackSubscription($subscription_data);

        if ($ret_data['got_data'] && !$ret_data['error_response_object'] && !$ret_data['error_sk']) {
            $disabled_subscription = $ret_data['got_data'];

            if ($disabled_subscription) {
                if ($disabled_subscription->status == 1) {
                    $upd_data['paystack_subscription_disabled'] = 1;

                    $this->Thrift_model->updateThriftGroupMember($upd_data, $thrift_group_id, $member_id);
                }
            }
        } else {

            $upd_data['paystack_subscription_disable_error'] = 1;

            if ($ret_data['error_response_object']) {
                $upd_data['paystack_subscription_disable_error_object'] = json_encode($ret_data['error_response_object']);
            }
            if ($ret_data['error_message']) {
                $upd_data['paystack_subscription_disable_error_message'] = $ret_data['error_message'];
            }
            if ($ret_data['error_sk']) {
                $upd_data['paystack_subscription_disable_error_message'] = $ret_data['error_message'];
            }

        }


    }

    public function fetchPaystackSubscription()
    {
        $subscription_data = array();
        $subscription_data['id'] = 36190; //or subscription_code

        $ret_data = $this->custom_payment_library->fetchPaystackSubscription($subscription_data);

        if ($ret_data['got_data'] && !$ret_data['error_response_object'] && !$ret_data['error_sk']) {
            $fetched_subscription = $ret_data['got_data'];

            if ($fetched_subscription) {
                echo '<pre>';
                print_r($fetched_subscription);
                echo '<pre>';

            }
        } else {

            //this will run inside a loop , so do nothing

            //comment the redirect to see error
            //redirect('users/auth/payment_method_error_page?payment_method=Paystack&error_type=Subscription%20creation%20error');

            echo '<pre>';
            if ($ret_data['error_response_object']) {
                print_r($ret_data['error_response_object']);
            }
            if ($ret_data['error_message']) {
                echo $ret_data['error_message'];
            }
            if ($ret_data['error_sk']) {
                echo $ret_data['error_message'];
            }
            echo '<pre>';
            echo '<br>';


        }


    }

    public function getListPaystackSubscription()
    {
        $subscription_data = array();
        $subscription_data['per_page'] = 200;

        $ret_data = $this->custom_payment_library->getListPaystackSubscription($subscription_data);

        if ($ret_data['got_data'] && !$ret_data['error_response_object'] && !$ret_data['error_sk']) {
            $listed_subscription = $ret_data['got_data'];

            if ($listed_subscription) {
                echo '<pre>';
                print_r($listed_subscription);
                echo '<pre>';

            }
        } else {

            //this will run inside a loop , so do nothing

            //comment the redirect to see error
            //redirect('users/auth/payment_method_error_page?payment_method=Paystack&error_type=Subscription%20creation%20error');

            echo '<pre>';
            if ($ret_data['error_response_object']) {
                print_r($ret_data['error_response_object']);
            }
            if ($ret_data['error_message']) {
                echo $ret_data['error_message'];
            }
            if ($ret_data['error_sk']) {
                echo $ret_data['error_message'];
            }
            echo '<pre>';
            echo '<br>';


        }


    }


    private function createPaystackTransferRecipient($user)
    {
        $fname = $user->first_name ? $user->first_name : 'No-first-name';
        $lname = $user->last_name ? $user->last_name : 'No-last-name';

        $paystack_bank_code = false;
        $account_number = false;


        if ($user->user_bank != null && $user->user_bank > 0) {
            $bank = $this->Thrift_model->getBank($user->user_bank);
            if ($bank) {
                if (!($bank->paystack_bank_code == null || $bank->paystack_bank_code == '')) {
                    $paystack_bank_code = $bank->paystack_bank_code;
                } else {
                    redirect('users/auth/payment_method_error_page?payment_method=Paystack&error_type=No%20paystack%20bank%20code%20found');
                }

            } else {
                redirect('users/auth/payment_method_error_page?payment_method=Paystack&error_type=No%20bank%20found');
            }
        } else {
            redirect('users/auth/payment_method_error_page?payment_method=Paystack&error_type=User%20chose%20no%20bank');
        }

        if (!($user->user_bank_account_no == null || $user->user_bank_account_no == '')) {
            $account_number = $user->user_bank_account_no;
        } else {
            redirect('users/auth/payment_method_error_page?payment_method=Paystack&error_type=No%20bank%20account%20number%20found');
        }


        if ($paystack_bank_code && $account_number) {

            $recipient_data['type'] = "nuban";
            $recipient_data['name'] = $fname . ' ' . $lname;
            $recipient_data['account_number'] = $account_number;
            $recipient_data['bank_code'] = $paystack_bank_code;
            $ret_data = $this->custom_payment_library->createPaystackTransferRecipient($recipient_data);

            /*echo '<pre>';
            print_r($ret_data);
            echo '</pre>';

            die();*/
            if ($ret_data['got_data'] && !$ret_data['error_response_object'] && !$ret_data['error_sk']) {
                $created_recipient = $ret_data['got_data'];

                if ($created_recipient) {
                    if ($created_recipient->status == 1 && !empty($created_recipient->data) && $created_recipient->data != null) {

                        $upd_data['paystack_recipient_code'] = $created_recipient->data->recipient_code;
                        $this->Thrift_model->updateEmployeesPaymentInfo($upd_data, $user->user_id);
                    }
                }

            } else {

                //comment the redirect to see error
                redirect('users/auth/payment_method_error_page?payment_method=Paystack&error_type=Transfer%20recipient%20creation%20error');

                if ($ret_data['error_response_object']) {
                    print_r($ret_data['error_response_object']);
                }
                if ($ret_data['error_message']) {
                    echo $ret_data['error_message'];
                }
                if ($ret_data['error_sk']) {
                    echo $ret_data['error_message'];
                }

            }
        }


    }

    private function initiatePaystackTransfer($transfer_data, $payment_recieve_id)
    {
        $ret_data = $this->custom_payment_library->initiatePaystackTransfer($transfer_data);

        if ($ret_data['got_data'] && !$ret_data['error_response_object'] && !$ret_data['error_sk']) {
            $initiated_transfer = $ret_data['got_data'];

            if ($initiated_transfer) {
                if ($initiated_transfer->status == 1 && !empty($initiated_transfer->data) && $initiated_transfer->data != null) {

                    $upd_data['paystack_payment_transferred'] = 1;
                    $upd_data['paystack_payment_transfer_code'] = $initiated_transfer->data->transfer_code;
                    $this->Thrift_model->autoUpdateThriftGroupPaymentRecieve($upd_data, $payment_recieve_id);

                }
            }
        } else {

            $upd_data['paystack_payment_transferred'] = -1;

            if ($ret_data['error_response_object']) {
                $upd_data['paystack_payment_transfer_error_object'] = json_encode($ret_data['error_response_object']);
            }
            if ($ret_data['error_message']) {
                $upd_data['paystack_payment_transfer_error_message'] = $ret_data['error_message'];
            }
            if ($ret_data['error_sk']) {
                $upd_data['paystack_payment_transfer_error_message'] = $ret_data['error_message'];
            }

            $this->Thrift_model->autoUpdateThriftGroupPaymentRecieve($upd_data, $payment_recieve_id);

        }


    }


}