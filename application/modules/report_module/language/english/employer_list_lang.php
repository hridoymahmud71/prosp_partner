<?php

/*page texts*/
$lang['page_title_text'] = 'All Employers';

$lang['table_title_text'] = 'Organization List';
$lang['no_user_found_text'] = 'No Organization Is Found!';
$lang['no_matching_user_found_text'] = 'No matching User Is Found!';

$lang['breadcrumb_home_text'] = 'Home';
$lang['breadcrumb_section_text'] = 'All Organization List';


/*Column names of the table*/

$lang['option_all_text'] = 'All';
$lang['option_active_text'] = 'Active';
$lang['option_inactive_text'] = 'Inactive';

$lang['option_all_text'] = 'All';
$lang['option_admin_text'] = 'Admin';
$lang['option_staff_text'] = 'Staff';
$lang['option_client_text'] = 'Client';

$lang['column_firstname_text'] = 'First Name';
$lang['column_lastname_text'] = 'Last Name';
$lang['column_company_text'] = 'Organization';
$lang['column_email_text'] = 'Email';
$lang['column_group_text'] = 'Role';
$lang['column_status_text'] = 'Status';
$lang['column_created_on_text'] = 'Created ON';
$lang['column_last_login_text'] = 'Last Login';
$lang['column_actions_text'] = 'Actions';

$lang['status_active_text'] = 'Active';
$lang['status_inactive_text'] = 'Inactive';

/*swal texts*/
$lang['swal_title_text'] = 'Are You Sure To Delete This User?';
$lang['swal_confirm_button_text'] = 'yes delete this User';
$lang['swal_cancel_button_text'] = 'No, keep this user';


$lang['activate_success_text'] = 'User Activated';
$lang['dectivate_success_text'] = 'User Deactivated';

$lang['see_user_text'] = 'See User';
$lang['edit_user_text'] = 'Edit User';

$lang['creation_time_unknown_text'] = 'Unknown';
$lang['never_logged_in_text'] = 'Never Logged In';


/*tooltip text*/
$lang['tooltip_activate_text'] = 'Make User Active';
$lang['tooltip_deactivate_text'] = 'Make User Deactive';

$lang['tooltip_payment_report_text'] = 'Payment Report';
$lang['tooltip_payment_recieve_report_text'] = 'Payment Receive Report';
$lang['tooltip_edit_text'] = 'Edit User ';
$lang['tooltip_delete_text'] = 'Delete User ';
$lang['tooltip_message_text'] = 'Mail/Message User';


/*loading*/
$lang['loading_text'] = 'Loading Organizations . . .';




