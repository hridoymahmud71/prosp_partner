<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Author: Mahmudur Rahman
 * Web Dev: RS Soft
 *
 * */


class Custom_email_notification_library
{
    public $CI;

    public function __construct()
    {
        $this->CI = &get_instance();

        $this->CI->load->library('email');
        $this->CI->load->library('custom_image_library');
        $this->CI->load->library('custom_email_library');
        $this->CI->load->library('settings_module/custom_settings_library');
    }

    public function getMainImageDirectory()
    {
        $image_path = $this->CI->custom_image_library->getMainImageDirectory();
        return $image_path;
    }

    public function getSiteLogo()
    {
        $a_settings_code = 'general_settings';
        $a_settings_key = 'site_logo';
        $site_logo = $this->CI->custom_settings_library->getASettingsValue($a_settings_code, $a_settings_key);

        if ($site_logo && $site_logo != '') {

            return $site_logo;
        } else {

            return false;
        }
    }

    public function getSiteShortName()
    {
        $a_settings_code = 'general_settings';
        $a_settings_key = 'site_short_name';
        $site_short_name = $this->CI->custom_settings_library->getASettingsValue($a_settings_code, $a_settings_key);

        if ($site_short_name && $site_short_name != '') {

            return $site_short_name;
        } else {
            return false;
        }
    }

    public function getSiteName()
    {
        $a_settings_code = 'general_settings';
        $a_settings_key = 'site_name';
        $site_name = $this->CI->custom_settings_library->getASettingsValue($a_settings_code, $a_settings_key);

        if ($site_name && $site_name != '') {

            return $site_name;
        } else {
            return false;
        }
    }


    public function sendNotificationEmail($data)
    {

        $this->CI->lang->load('email_notification_module/email_notification');

        $image_path = $this->getMainImageDirectory();

        $data['image_path'] = $image_path;
        $data['site_logo'] = $this->getSiteLogo();
        $data['site_short_name'] = $this->getSiteShortName();

        $html = $this->CI->load->view('email_notification_module/email_notification_html_page', $data, TRUE);

        $config = $this->CI->custom_email_library->getEmailConfig('html');

        //replacing <p> tags with new line
        $log_title_message = preg_replace('/<p(?:\s+[^>]*)?>/i', " | ", $data['log_title_message']);

        $mail_subject = strip_tags($log_title_message);

        if ($config) {

            if ($config['protocol'] == 'smtp') {
                $mail_from = $config['smtp_user'];
            } else {
                $mail_from = 'no-reply@rspm.com';
            }

            $site_name = $this->getSiteName();
            if ($site_name) {
                $mail_from_name = $site_name;
            } else {
                $mail_from_name = 'RSPM';
            }

            $mail_to = $data['log_created_for_email'];

            $this->CI->email->initialize($config);
            $this->CI->email->to($mail_to);
            $this->CI->email->from($mail_from, $mail_from_name);
            $this->CI->email->subject($mail_subject);
            $this->CI->email->message($html);


            if ($this->CI->email->send() == false) {
                echo $this->CI->email->print_debugger('headers');
                exit;
            }

        }

    }


}