<?php

$lang['box_title_projectroom_sections'] = 'Project Room Sections';


$lang['item_project_overview_text'] = 'Project Overview';
$lang['item_edit_project_text'] = 'Edit Project';

$lang['item_all_tasks_text'] = 'All Tasks';
$lang['item_my_tasks_text'] = 'My Tasks';
$lang['item_add_tasks_text'] = 'Add Task';

$lang['item_project_files_text'] = 'Project Files';
$lang['item_upload_files_in_a_project_text'] = ' Upload File(s) in Project';

$lang['item_support_or_ticket_text'] = 'Support/Ticket';
$lang['item_add_ticket_text'] = 'Add Ticket';

$lang['item_invoices_text'] = 'Invoices';
$lang['item_add_invoice_text'] = 'Add Invoice';






?>