<?php

$lang['register_thrift_account_text'] = 'Create Your Thrifting Account';

$lang['submit_btn_text'] = 'Submit';

$lang['label_employer_text'] = 'Organization';
$lang['placeholder_employer_text'] = 'Select an organization';

$lang['employer_not_found_text'] = 'Organization not found';

$lang['label_user_optional_text'] = 'optional';


$lang['label_first_name_text'] = 'First Name';
$lang['label_last_name_text'] = 'Last Name';
$lang['label_email_text'] = 'Email';
$lang['label_password_text'] = 'Password';
$lang['label_passconf_text'] = 'Confirm Password';
$lang['label_user_ofc_id_text'] = 'Employee ID Number';
$lang['label_user_salary_text'] = 'Monthly Salary';
$lang['label_phone_text'] = 'Phone';

$lang['placeholder_first_name_text'] = 'Enter First Name';
$lang['placeholder_last_name_text'] = 'Enter Last Name';
$lang['placeholder_email_text'] = 'Enter Email';
$lang['placeholder_password_text'] = 'Enter Password';
$lang['placeholder_passconf_text'] = 'Confirm Password';
$lang['placeholder_user_ofc_id_text'] = 'Enter Employee ID Number';
$lang['placeholder_user_salary_text'] = 'Enter Salary';
$lang['placeholder_phone_text'] = 'Enter Phone Number';

$lang['label_user_gender_text'] = 'Gender';
$lang['option_select_gender_text'] = 'Select Your Gender';
$lang['option_user_gender_male_text'] = 'Male';
$lang['option_user_gender_female_text'] = 'Female';

$lang['label_user_dob_text'] = 'Date of Birth';
$lang['label_user_hire_date_text'] = 'Hire Date';

$lang['placeholder_user_dob_text'] = 'Enter Date of Birth';
$lang['placeholder_user_hire_date_text'] = 'Enter Hire Date';

$lang['label_user_bvn_text'] = 'BVN number';
$lang['placeholder_user_bvn_text'] = 'Enter BVN number';


$lang['bank_select_text'] = 'Select Bank';
$lang['label_user_bank_name_text'] = 'Bank Name';

$lang['label_user_bank_account_no_text'] = 'Bank account number';
$lang['placeholder_user_bank_account_no_text'] = 'Enter account number';

$lang['label_is_individual_text'] = 'Thrifting Membership Status';
$lang['option_select_status_text'] = 'Select Status';
$lang['option_not_individual_text'] = 'I am a member of a registered organization';
$lang['option_individual_text'] = 'I am not affiliated with any organization';


$lang['field_mandatory_text'] = 'Field Mandatory';

$lang['label_acception_checkbox_text'] = 'Read and accept our terms and conditions';

/*-------------------------------------------------------*/

$lang['label_user_street_1_text'] = 'Street 1';
$lang['label_user_street_2_text'] = 'Street 2';
$lang['label_user_country_text'] = 'Country';
$lang['label_user_state_text'] = 'State';
$lang['label_user_city_text'] = 'City';

$lang['placeholder_user_street_1_text'] = 'Enter Street 1';
$lang['placeholder_user_street_2_text'] = 'Enter Street 2';
$lang['placeholder_user_country_text'] = 'Select a Country';
$lang['placeholder_user_state_text'] = 'Select a State';
$lang['placeholder_user_city_text'] = 'Select a City';

$lang['country_not_found_text'] = 'Country Not Found';
$lang['state_not_found_text'] = 'State Not Found';
$lang['city_not_found_text'] = 'City Not Found';


//modal related
$lang['modal_title_terms_and_conditions_text'] = 'Terms and Conditions';
$lang['accept_btn_text'] = 'Accept';
$lang['decline_btn_text'] = 'Decline';
$lang['close_btn_text'] = 'Close';
$lang['acceptance_error_text'] = 'You need to accept the terms and conditions';



$lang['login_text'] = 'Already have an account? Log in';

$lang['thrifter_verification_email_text'] = 'Did not get verification email? send again';

$lang['registration_success_text'] = 'An email has been sent to you for verification';

$lang['strong_password_text']='Enter strong passwords – at least one upper case letter, one number and one symbol and at least 8 characters';



//validation
$lang['email_already_exist_text'] = 'Your email address is already registered on the system.';
$lang['only_login_text'] = 'login';
$lang['need_to_login_text'] = 'Need to %s ?';
