<?php

/*common_left menu items*/

$lang['cl_menu_main_navigation_text'] = 'Main Navigation';
$lang['cl_menu_dashboard_text'] = 'Dashboard';


$lang['cl_menu_message_text'] = 'Message';
$lang['cl_menu_message_sub_menu_send_message_text'] = 'Write Message';
$lang['cl_menu_message_sub_menu_outbox_text'] = 'Outbox';
$lang['cl_menu_message_sub_menu_inbox_text'] = 'Inbox';


$lang['cl_menu_settings_text'] = 'Settings';
$lang['cl_menu_settings_sub_menu_general_settings_text'] = 'General Settings';
$lang['cl_menu_settings_sub_menu_contact_settings_text'] = 'Contact Settings';
$lang['cl_menu_settings_sub_menu_image_settings_text'] = 'Image Settings';
$lang['cl_menu_settings_sub_menu_file_settings_text'] = 'File Settings';
$lang['cl_menu_settings_sub_menu_currency_settings_text'] = 'Currency Settings';
$lang['cl_menu_settings_sub_menu_datetime_settings_text'] = 'Date & Time Settings';
$lang['cl_menu_settings_sub_menu_email_settings_text'] = 'Email Settings';
$lang['cl_menu_settings_sub_menu_system_settings_text'] = 'System Settings';
$lang['cl_menu_settings_sub_menu_payment_settings_text'] = 'Payment Settings';
$lang['cl_menu_settings_sub_menu_individual_settings_text'] = 'Individual Settings';

$lang['cl_menu_settings_sub_menu_email_templates_text'] = 'Email Templates';

$lang['cl_menu_settings_sub_menu_all_bank_text'] = 'All Banks';
$lang['cl_menu_settings_sub_menu_add_bank_text'] = 'Add Bank';
$lang['cl_menu_settings_sub_menu_all_state_text'] = 'State Settings';


$lang['cl_menu_users_text'] = 'Users';
$lang['cl_menu_users_sub_menu_users_text'] = 'Users';
$lang['cl_menu_users_sub_menu_add_user_text'] = 'Add a user';
$lang['cl_menu_users_sub_menu_groups_text'] = 'Groups';

$lang['cl_menu_project_text'] = 'Project';
$lang['cl_menu_project_sub_menu_all_project_text'] = 'All Projects';
$lang['cl_menu_project_sub_menu_my_project_text'] = 'My Projects';
$lang['cl_menu_project_sub_menu_add_project_text'] = 'Add Project';

$lang['cl_menu_task_text'] = 'Task';
$lang['cl_menu_task_sub_menu_all_tasks_from_all_projects_text'] = 'All Tasks';
$lang['cl_menu_task_sub_menu_all_my_task_text'] = 'My Tasks';

$lang['cl_menu_file_manager_text'] = 'File Manager';
$lang['cl_menu_file_manager_sub_menu_all_files_text'] = 'All File';
$lang['cl_menu_file_manager_sub_menu_my_files_text'] = 'My File';


$lang['cl_menu_ticket_or_support_text'] = 'Ticket/Support';
$lang['cl_menu_ticket_or_support_sub_menu_all_tickets_text'] = 'All Tickets';
$lang['cl_menu_ticket_or_support_sub_menu_my_tickets_text'] = 'My Tickets';
$lang['cl_menu_ticket_or_support_sub_menu_create_ticket_text'] = 'Create Ticket';

$lang['cl_menu_invoice_text'] = 'Invoice';
$lang['cl_menu_invoice_sub_menu_all_invoices_text'] = 'All Invoices';
$lang['cl_menu_invoice_sub_menu_my_invoices_text'] = 'My Invoices';
$lang['cl_menu_invoice_sub_menu_create_invoice_text'] = 'Create Invoice';

$lang['cl_menu_contact_text'] = 'Contact';
$lang['cl_menu_contact_sub_menu_contact_page_text'] = 'Contact Page';

$lang['cl_menu_employer_dashboard_text'] = 'Organization Management';
$lang['cl_menu_employer_member_dashboard_text'] = 'Member Management';
$lang['cl_menu_add_employee_text'] = 'Add Thrifters';
$lang['cl_menu_all_unapproved_employee_text'] = 'All Unapproved Thrifters';
$lang['cl_menu_all_employee_text'] = 'All Thrifters';
$lang['cl_menu_my_employee_text'] = 'My Members';
$lang['cl_menu_all_employer_view_text'] = 'View All Organizations';

$lang['cl_menu_all_contact_person_text'] = 'All Contact Persons';
$lang['cl_menu_my_contact_person_text'] = 'My Contact Persons';
$lang['cl_menu_my_unapproved_employee_member_text'] = 'Unapproved Members';
$lang['cl_menu_my_employee_member_text'] = 'Members';
$lang['cl_menu_add_employee_member_text'] = 'Add Member';

$lang['cl_menu_add_employer_text'] = 'Add Organizations';

$lang['cl_menu_product_text'] = 'Product';
$lang['cl_menu_all_product_text'] = 'All Product';
$lang['cl_menu_add_product_text'] = 'Add Product';

$lang['cl_menu_thrift_text'] = 'Thrift';
$lang['cl_menu_all_thrift_text'] = 'All Thrifts';
$lang['cl_menu_my_thrift_text'] = 'My Thrifts';
$lang['cl_menu_my_employees_thrift_text'] = 'My employee\'s Thrifts';
$lang['cl_menu_custom_product_create_list_text'] = 'My Thrift Invitations';
$lang['cl_menu_custom_product_recieve_list_text'] = 'Received Invitations';

$lang['cl_menu_custom_product_add_text'] = 'New Private Thrift';
$lang['cl_menu_individual_product_add_text'] = 'New Individual Thrift';


$lang['cl_menu_loan_text'] = 'Loans';
$lang['cl_menu_loan_product_create_list_text'] = 'My Loan Invitations';
$lang['cl_menu_loan_product_recieve_list_text'] = 'Received Loan Invitations';
$lang['cl_menu_loan_product_add_text'] = 'New Loan Request';

$lang['cl_menu_all_payment_issue_text'] = 'All Payments';
$lang['cl_menu_all_payment_recieve_issue_text'] = 'All Disbursements';
$lang['cl_menu_my_payment_issue_text'] = 'My Payments';
$lang['cl_menu_my_payment_recieve_issue_text'] = 'My Disbursements';

$lang['cl_menu_report_text'] = 'Report';
$lang['cl_menu_report_and_statement_text'] = 'Statements and Reports';
$lang['cl_menu_all_employer_text'] = 'All Organizations';
$lang['cl_menu_all_employee_text'] = ' View All Thrifers';
$lang['cl_menu_all_payment_report_text'] = 'All Payments Report';
$lang['cl_menu_payment_recieve_report_text'] = 'All Statements';

$lang['cl_menu_my_payment_report_text'] = 'My Payments';
$lang['cl_menu_my_payment_recieve_report_text'] = 'My Statements';

$lang['cl_menu_payment_report_by_year_text'] = 'Payments (Years)';
$lang['cl_menu_payment_report_by_month_text'] = 'Payments  (Months)';

$lang['cl_menu_payment_recieve_report_by_year_text'] = 'Payment Statements  (Years)';
$lang['cl_menu_payment_recieve_report_by_month_text'] = 'Payment Statements  (Months)';

$lang['cl_menu_log_text'] = 'Log';
$lang['cl_menu_view_log_text'] = 'View Log';


$lang['cl_menu_content'] = 'Contents';
$lang['cl_menu_content_faq'] = 'FAQ';
$lang['cl_menu_content_news'] = 'News';
$lang['cl_menu_content_pages'] = 'Pages';


?>