<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code


$PROSPERIS_MAIL_TOP = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Carma</title>
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700,900" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet" type="text/css">
    <style media="all" type="text/css">
html
    {
        width:100%
    }
    body
    {
        width:100%;
        height:100%;
        margin:0;
        padding:0;
        background-color:#ececec;
        -webkit-font-smoothing:antialiased;
        -webkit-text-size-adjust:100%;
        -ms-text-size-adjust:100%;
        text-size-adjust:100%
    }
    a[x-apple-data-detectors]
    {
        color:inherit!important
    }
    p
    {
        margin:0
    }
    .img-full img
    {
        width:100%!important;
        height:auto!important
    }
    </style>
    <style type="text/css">
@media only screen and (max-width: 768px) {
        .main
        {
            min-width:0!important
        }       
        .img img
        {
            width:100%!important;
            height:auto!important
        }
    }
    @media only screen and (max-width: 640px) {
        .container
        {
            width:300px!important;
            min-width:0!important
        }       
        .col
        {
            display:block!important;
            float:left!important;
            width:100%!important
        }       
        .full
        {
            width:100%!important
        }       
        .img-bg
        {
            background-size:cover
        }       
        .title
        {
            background-position:left top!important
        }       
        .title td
        {
            padding-left:15px!important;
            padding-right:0!important
        }       
        .title-center
        {
            background-position:center bottom!important
        }       
        .title-center td
        {
            padding-left:0!important;
            padding-right:0!important
        }       
        .nav .text
        {
            width:31%
        }       
        .h1
        {
            font-size:36px!important
        }       
        .h2
        {
            font-size:30px!important
        }       
        .w-auto
        {
            width:auto!important
        }       
        .h-auto
        {
            height:auto!important
        }       
        .space45
        {
            height:45px!important;
            line-height:45px!important
        }       
        .height-0
        {
            display:none!important
        }       
        .btn td,.btn1 td
        {
            padding:0!important
        }       
        .btn td a
        {
            display:block;
            padding:13px 25px
        }       
        .btn1 td a
        {
            display:block;
            padding:12px 24px
        }       
        .footer .text
        {
            text-align:center!important
        }
    }
    @media only screen and (max-width: 380px) {
        .container
        {
            width:280px!important
        }
    }
    @media only screen and (max-width: 350px) {
        .container
        {
            width:240px!important
        }
    }
    </style>
  <!--[if gte mso 9]>   <style type="text/css">     body, table, tr, td, h1, h2, h3, h4, h5, h6, ul, li, ol, dl, dd, dt {       font-family: Helvetica, Arial, sans-serif !important;     }     .h1, .h2 { font-family: Cambria, Georgia, serif !important; }     .h2 { line-height: 94% !important;  }     .link  { line-height: 100% !important; }   </style>   <![endif]-->
  <!--[if gte mso 9]><xml>   <o:OfficeDocumentSettings>     <o:AllowPNG/>     <o:PixelsPerInch>96</o:PixelsPerInch>   </o:OfficeDocumentSettings> </xml><![endif]-->
  </head>
  <body style="-webkit-text-size-adjust: none; margin: 0; padding: 0; background-color: #ececec; color: #666666; font-family: \'Lato\', Arial, sans-serif;">
    ';

$PROSPERIS_MAIL_BOTTOM = '</body></html>';

define('PROSPERIS_MAIL_TOP', $PROSPERIS_MAIL_TOP);
define('PROSPERIS_MAIL_BOTTOM', $PROSPERIS_MAIL_BOTTOM);


